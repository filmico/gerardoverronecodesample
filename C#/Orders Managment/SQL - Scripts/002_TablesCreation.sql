USE [DB_A11FC5_desasistemabk]
GO
/****** Object:  Table [dbo].[ExpedienteEstados]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpedienteEstados](
	[ExpedienteEstadoID] [int] NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
	[Fecha] [datetime2](7) NULL,
 CONSTRAINT [PK_ExpedienteEstados] PRIMARY KEY CLUSTERED 
(
	[ExpedienteEstadoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Evaluadores]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Evaluadores](
	[EvaluadorID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
 CONSTRAINT [PK_Evaluadores] PRIMARY KEY CLUSTERED 
(
	[EvaluadorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadosLiquidacion]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadosLiquidacion](
	[EstadoLiquidacionID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](200) NULL,
 CONSTRAINT [PK_EstadosLiquidacion] PRIMARY KEY CLUSTERED 
(
	[EstadoLiquidacionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstadoSincronizacionesHistorico]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoSincronizacionesHistorico](
	[ExpedienteEstadoID] [int] NULL,
	[UltimaSincronizacionComienzo] [datetime2](7) NULL,
	[UltimaSincronizacionFinalizacion] [datetime2](7) NULL,
	[EstadoSincronizacion] [int] NULL,
	[ExpedientesObtenidos] [int] NULL,
	[ExpedientesPosibles] [int] NULL,
	[ExpedientesActualizados] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoSincronizaciones]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoSincronizaciones](
	[ExpedienteEstadoID] [int] NULL,
	[UltimaSincronizacionComienzo] [datetime2](7) NULL,
	[UltimaSincronizacionFinalizacion] [datetime2](7) NULL,
	[EstadoSincronizacion] [int] NULL,
	[ExpedientesObtenidos] [int] NULL,
	[ExpedientesPosibles] [int] NULL,
	[ExpedientesActualizados] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Son los que vienen a través de la consulta web. Antes de chequear si van a ser insertados en la BD.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstadoSincronizaciones', @level2type=N'COLUMN',@level2name=N'ExpedientesObtenidos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Son los que, de los obtenidos, los que pertenecen a la fecha igual o posterior a la de la última sincronización.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstadoSincronizaciones', @level2type=N'COLUMN',@level2name=N'ExpedientesPosibles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Son los que cumplen con la fecha para enviar a BD y en la BD registran cambios, por lo que se insertan. También pasan a históricos.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstadoSincronizaciones', @level2type=N'COLUMN',@level2name=N'ExpedientesActualizados'
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[EmpresaCuitID] [bigint] NOT NULL,
	[NroEmpresa] [nvarchar](5) NULL,
	[RazonSocial] [nvarchar](200) NULL,
 CONSTRAINT [PK_Empresas] PRIMARY KEY CLUSTERED 
(
	[EmpresaCuitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConfigSincronizaciones]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfigSincronizaciones](
	[DiaSemanaComienzo] [int] NULL,
	[DiaSemanaFinalizacion] [int] NULL,
	[HoraComienzo] [time](7) NULL,
	[HoraFinalizacion] [time](7) NULL,
	[Estado] [int] NULL,
	[EstadoExpedienteID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CargaDatosLog]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CargaDatosLog](
	[ejecucionAutomatica] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuariosTipos]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuariosTipos](
	[UsuarioTipoID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
 CONSTRAINT [PK_UsuariosTipos] PRIMARY KEY CLUSTERED 
(
	[UsuarioTipoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuariosEstados]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuariosEstados](
	[UsuarioEstadoID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](100) NULL,
 CONSTRAINT [PK_UsuariosEstados] PRIMARY KEY CLUSTERED 
(
	[UsuarioEstadoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExpedientesHistorico]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpedientesHistorico](
	[ExpedienteNumeroID] [bigint] NULL,
	[NroSolicitud] [int] NULL,
	[EmpresaCuitID] [bigint] NULL,
	[BeneficioSolicitado] [money] NULL,
	[BeneficioAprobado] [money] NULL,
	[FechaEstado] [date] NULL,
	[ExpedienteEstadoId] [int] NULL,
	[EvaluadorID] [int] NULL,
	[EvaluadorTecnicoID] [int] NULL,
	[EstadoLiquidacionID] [int] NULL,
	[Fecha] [datetime2](7) NULL,
	[FechaCambioLiquidacion] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expedientes]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expedientes](
	[ExpedienteNumeroID] [bigint] NOT NULL,
	[NroSolicitud] [int] NULL,
	[EmpresaCuitID] [bigint] NULL,
	[BeneficioSolicitado] [money] NULL,
	[BeneficioAprobado] [money] NULL,
	[FechaEstadoActual] [date] NULL,
	[ExpedienteEstadoId] [int] NULL,
	[EvaluadorID] [int] NULL,
	[EvaluadorTecnicoID] [int] NULL,
	[EstadoLiquidacionID] [int] NULL,
	[Fecha] [datetime2](7) NULL,
	[FechaCambioLiquidacion] [datetime2](7) NULL,
 CONSTRAINT [PK_Expedientes] PRIMARY KEY CLUSTERED 
(
	[ExpedienteNumeroID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Password] [varbinary](20) NULL,
	[Salt] [char](25) NULL,
	[UsuarioEstadoID] [int] NULL,
	[UsuarioTipoID] [int] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[UsuarioID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Suscripciones]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suscripciones](
	[SuscripcionID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NULL,
	[EmpresaCuitID] [bigint] NULL,
	[ExpedienteEstadoID] [int] NULL,
	[FechaEnvio] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExpedientesUsuarios]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpedientesUsuarios](
	[ExpedienteUsuarioID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NULL,
	[ExpedienteNumeroID] [bigint] NULL,
	[Porcentaje] [decimal](5, 2) NULL,
	[FechaUltimaModificacion] [datetime2](7) NULL,
 CONSTRAINT [PK_ExpedientesUsuarios] PRIMARY KEY CLUSTERED 
(
	[ExpedienteUsuarioID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Asignaciones]    Script Date: 02/14/2017 12:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asignaciones](
	[AsignacionID] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaCuitID] [bigint] NULL,
	[UsuarioID] [int] NULL,
	[Porcentaje] [decimal](5, 2) NULL,
 CONSTRAINT [PK_Asignaciones] PRIMARY KEY CLUSTERED 
(
	[AsignacionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__EstadoSin__Ultim__00750D23]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[EstadoSincronizaciones] ADD  DEFAULT (NULL) FOR [UltimaSincronizacionComienzo]
GO
/****** Object:  Default [DF__EstadoSin__Ultim__0169315C]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[EstadoSincronizaciones] ADD  DEFAULT (NULL) FOR [UltimaSincronizacionFinalizacion]
GO
/****** Object:  Default [DF__EstadoSin__Ultim__7D98A078]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[EstadoSincronizacionesHistorico] ADD  DEFAULT (NULL) FOR [UltimaSincronizacionComienzo]
GO
/****** Object:  Default [DF__EstadoSin__Ultim__7E8CC4B1]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[EstadoSincronizacionesHistorico] ADD  DEFAULT (NULL) FOR [UltimaSincronizacionFinalizacion]
GO
/****** Object:  ForeignKey [FK_Asignaciones_Empresas]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Asignaciones]  WITH CHECK ADD  CONSTRAINT [FK_Asignaciones_Empresas] FOREIGN KEY([EmpresaCuitID])
REFERENCES [dbo].[Empresas] ([EmpresaCuitID])
GO
ALTER TABLE [dbo].[Asignaciones] CHECK CONSTRAINT [FK_Asignaciones_Empresas]
GO
/****** Object:  ForeignKey [FK_Asignaciones_Usuarios]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Asignaciones]  WITH CHECK ADD  CONSTRAINT [FK_Asignaciones_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Asignaciones] CHECK CONSTRAINT [FK_Asignaciones_Usuarios]
GO
/****** Object:  ForeignKey [FK_Expedientes_Empresas]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Expedientes]  WITH CHECK ADD  CONSTRAINT [FK_Expedientes_Empresas] FOREIGN KEY([EmpresaCuitID])
REFERENCES [dbo].[Empresas] ([EmpresaCuitID])
GO
ALTER TABLE [dbo].[Expedientes] CHECK CONSTRAINT [FK_Expedientes_Empresas]
GO
/****** Object:  ForeignKey [FK_Expedientes_EstadosLiquidacion]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Expedientes]  WITH CHECK ADD  CONSTRAINT [FK_Expedientes_EstadosLiquidacion] FOREIGN KEY([EstadoLiquidacionID])
REFERENCES [dbo].[EstadosLiquidacion] ([EstadoLiquidacionID])
GO
ALTER TABLE [dbo].[Expedientes] CHECK CONSTRAINT [FK_Expedientes_EstadosLiquidacion]
GO
/****** Object:  ForeignKey [FK_Expedientes_Evaluadores]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Expedientes]  WITH CHECK ADD  CONSTRAINT [FK_Expedientes_Evaluadores] FOREIGN KEY([EvaluadorID])
REFERENCES [dbo].[Evaluadores] ([EvaluadorID])
GO
ALTER TABLE [dbo].[Expedientes] CHECK CONSTRAINT [FK_Expedientes_Evaluadores]
GO
/****** Object:  ForeignKey [FK_Expedientes_ExpedienteEstados]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Expedientes]  WITH CHECK ADD  CONSTRAINT [FK_Expedientes_ExpedienteEstados] FOREIGN KEY([ExpedienteEstadoId])
REFERENCES [dbo].[ExpedienteEstados] ([ExpedienteEstadoID])
GO
ALTER TABLE [dbo].[Expedientes] CHECK CONSTRAINT [FK_Expedientes_ExpedienteEstados]
GO
/****** Object:  ForeignKey [FK_ExpedientesUsuarios_Expedientes]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[ExpedientesUsuarios]  WITH CHECK ADD  CONSTRAINT [FK_ExpedientesUsuarios_Expedientes] FOREIGN KEY([ExpedienteNumeroID])
REFERENCES [dbo].[Expedientes] ([ExpedienteNumeroID])
GO
ALTER TABLE [dbo].[ExpedientesUsuarios] CHECK CONSTRAINT [FK_ExpedientesUsuarios_Expedientes]
GO
/****** Object:  ForeignKey [FK_ExpedientesUsuarios_Usuarios]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[ExpedientesUsuarios]  WITH CHECK ADD  CONSTRAINT [FK_ExpedientesUsuarios_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[ExpedientesUsuarios] CHECK CONSTRAINT [FK_ExpedientesUsuarios_Usuarios]
GO
/****** Object:  ForeignKey [FK_Suscripciones_Empresas]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Suscripciones]  WITH CHECK ADD  CONSTRAINT [FK_Suscripciones_Empresas] FOREIGN KEY([EmpresaCuitID])
REFERENCES [dbo].[Empresas] ([EmpresaCuitID])
GO
ALTER TABLE [dbo].[Suscripciones] CHECK CONSTRAINT [FK_Suscripciones_Empresas]
GO
/****** Object:  ForeignKey [FK_Suscripciones_Usuarios]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Suscripciones]  WITH CHECK ADD  CONSTRAINT [FK_Suscripciones_Usuarios] FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuarios] ([UsuarioID])
GO
ALTER TABLE [dbo].[Suscripciones] CHECK CONSTRAINT [FK_Suscripciones_Usuarios]
GO
/****** Object:  ForeignKey [FK_Usuarios_UsuariosEstados]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_UsuariosEstados] FOREIGN KEY([UsuarioEstadoID])
REFERENCES [dbo].[UsuariosEstados] ([UsuarioEstadoID])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_UsuariosEstados]
GO
/****** Object:  ForeignKey [FK_Usuarios_UsuariosTipos]    Script Date: 02/14/2017 12:27:08 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_UsuariosTipos] FOREIGN KEY([UsuarioTipoID])
REFERENCES [dbo].[UsuariosTipos] ([UsuarioTipoID])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_UsuariosTipos]
GO
