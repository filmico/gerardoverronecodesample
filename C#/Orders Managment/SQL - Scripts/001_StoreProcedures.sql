USE [DB_A11FC5_sistemabk]

GO
/****** Object:  StoredProcedure [dbo].[EvaluadoresInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EvaluadoresInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@Nombre		NVARCHAR(100)
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

	IF (NOT EXISTS (SELECT ev.Nombre FROM dbo.Evaluadores ev WHERE ev.Nombre = @Nombre))
		BEGIN
			-- Realiza el alta
			BEGIN TRY						
				BEGIN TRAN	
									
					INSERT dbo.Evaluadores
							( Nombre )
					VALUES  ( UPPER(@Nombre)  -- Nombre - nvarchar(100)
							  )				
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END	
									
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');
							SELECT * FROM dbo.Evaluadores
							WHERE dbo.Evaluadores.Nombre = @Nombre;
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK				
			END TRY	
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	
					
		END	
			
	ELSE
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El Evaluador ya existe registrado!';			
			SELECT (1) AS statusDB;
			RETURN;		
		END
		
END

/*
EXEC dbo.EvaluadoresInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'Esposito, Maria Laura' -- nvarchar(100)
    
EXEC dbo.EvaluadoresInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'Ugarte, Daniel Alfredo' -- nvarchar(100)    
    
select * from dbo.Evaluadores;    
    
*/
GO
/****** Object:  StoredProcedure [dbo].[ConfigSincronizacionesInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ConfigSincronizacionesInsProc]
	(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@DiaSemanaComienzo INT = NULL,
	@DiaSemanaFinalizacion INT = NULL,
	@HoraComienzoDT DATETIME = NULL,
	@HoraFinalizacionDT DATETIME = NULL,
	@Estado INT = NULL,
	@EstadoExpedienteID INT = NULL,	
	@SolicitanteUsuarioID INT = NULL -- ID del Solicitante	
	)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

	-- Convierte los DateTime de Entrada a Time
	DECLARE @HoraComienzo TIME = CAST(@HoraComienzoDT AS time);
	DECLARE @HoraFinalizacion TIME = CAST(@HoraFinalizacionDT AS time);

	---- Verifica que el tipo de usuario solicitante tenga permiso (nivel 3) para insertar.
	--IF ((SELECT us.UsuarioTipoID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID) <> 3)
	--	BEGIN
	--		IF (@ModoDebug = 1)
	--			PRINT 'El UsuarioTipoID del usuario que solicita el alta no tiene permisos para realizar el alta!';			
	--		SELECT (3) AS statusDB;
	--		RETURN;		
	--	END		
	
	-- Verifica que no exista en la base de datos la configuracion que se esta queriendo insertar	
	IF ((SELECT COUNT(conf.EstadoExpedienteID)	
		FROM dbo.ConfigSincronizaciones conf 
		WHERE conf.DiaSemanaComienzo = @DiaSemanaComienzo
		  AND conf.DiaSemanaFinalizacion = @DiaSemanaFinalizacion
		  AND conf.HoraComienzo = @HoraComienzo
		  AND conf.HoraFinalizacion = @HoraFinalizacion
		  AND conf.EstadoExpedienteID = @EstadoExpedienteID
		) > 0)
	
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'La configuracion que se intenta insertar ya existe!';			
			SELECT (3) AS statusDB;
			RETURN;		
		END		
	
	ELSE
		BEGIN
		
			BEGIN TRY
				INSERT dbo.ConfigSincronizaciones
						( DiaSemanaComienzo ,
						  DiaSemanaFinalizacion ,
						  HoraComienzo ,
						  HoraFinalizacion ,
						  Estado ,
						  EstadoExpedienteID
						)
				VALUES  ( @DiaSemanaComienzo , -- DiaSemanaComienzo - int
						  @DiaSemanaFinalizacion , -- DiaSemanaFinalizacion - int
						  @HoraComienzo , -- HoraComienzo - time
						  @HoraFinalizacion , -- HoraFinalizacion - time
						  1 , -- Estado - int
						  @EstadoExpedienteID  -- EstadoExpedienteID - int
						)
						
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END				
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
					SELECT * FROM dbo.ConfigSincronizaciones
						WHERE dbo.ConfigSincronizaciones.EstadoExpedienteID = @EstadoExpedienteID;
					IF @@TRANCOUNT > 0
						ROLLBACK;
				END	
					
			SELECT (0) AS statusDB; -- TODO OK				
												
			END TRY		
								
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					BEGIN	
						ROLLBACK;						
					END	
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;						
						
		END
		

END;
/*

-- Listar todas las configuraciones
-----------------------------------
EXEC dbo.ConfigSincronizacionesSelProc
    @Modo = 0, -- int
    @ModoDebug = 0 -- int


-- Listar todas las configuraciones en estado 1
------------------------------------------------
EXEC dbo.ConfigSincronizacionesSelProc
    @Modo = 1, -- int
    @ModoDebug = 0 -- int
    
EXEC dbo.ConfigSincronizacionesInsProc
    @Modo = 0, -- int
	@ModoDebug = 1, -- int
	@DiaSemanaComienzo = 1, -- int
	@DiaSemanaFinalizacion = 5, -- int
	@HoraComienzoDT = "1980-01-01 08:00", -- time(7)
	@HoraFinalizacionDT = "1980-01-01 18:00", -- time(7)
	@Estado = 1, -- int
	@EstadoExpedienteID = 4, -- int
	@SolicitanteUsuarioID = 0 -- int 
	
select * from ConfigSincronizaciones;

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesHistoricoInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesHistoricoInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@ExpedienteNumeroID		BIGINT,
	@NroSolicitud			INT,
	@EmpresaCuitID			BIGINT,
	@BeneficioSolicitado	MONEY,
	@BeneficioAprobado		MONEY,
	@FechaEstadoActual		DATE,
	@ExpedienteEstadoId		INT,
	@EvaluadorID			INT,
	@EvaluadorTecnicoID		INT,
	@EstadoLiquidacionID    INT,
    @Fecha					DATETIME2,
	@FechaCambioLiquidacion DATETIME2
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	-- Realiza el alta
	BEGIN TRY						
		BEGIN TRAN	

			INSERT INTO dbo.ExpedientesHistorico
			        ( ExpedienteNumeroID ,
			          NroSolicitud ,
			          EmpresaCuitID ,
			          BeneficioSolicitado ,
			          BeneficioAprobado ,
			          FechaEstado ,
			          ExpedienteEstadoId ,
			          EvaluadorID ,
			          EvaluadorTecnicoID,
			          EstadoLiquidacionID,
			          Fecha,
			          FechaCambioLiquidacion
			        )
			VALUES  ( @ExpedienteNumeroID , -- ExpedienteNumeroID - bigint
			          @NroSolicitud , -- NroSolicitud - int
			          @EmpresaCuitID , -- EmpresaCuitID - bigint
			          @BeneficioSolicitado , -- BeneficioSolicitado - money
			          @BeneficioAprobado , -- BeneficioAprobado - money
			          @FechaEstadoActual , -- FechaEstado - date
			          @ExpedienteEstadoId , -- ExpedienteEstadoId - int
			          @EvaluadorID , -- EvaluadorID - int
			          @EvaluadorTecnicoID , -- EvaluadorTecnicoID - int
			          @EstadoLiquidacionID,
			          @Fecha,
			          @FechaCambioLiquidacion
			        )
												
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END	
									
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
					SELECT * FROM dbo.ExpedientesHistorico eh
					WHERE (eh.ExpedienteNumeroID = @ExpedienteNumeroID) AND (eh.ExpedienteEstadoId = @ExpedienteEstadoId);
					IF @@TRANCOUNT > 0
						ROLLBACK;
				END								
			SELECT (0) AS statusDB; -- TODO OK				
	END TRY	
			
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;	
					

END

/*
delete from ExpedientesHistorico 

EXEC dbo.ExpedientesHistoricoInsProc 
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @ExpedienteNumeroID = '02435682011', -- bigint
    @NroSolicitud = '201119589', -- int
    @EmpresaCuitID = 30708019840, -- bigint
    @BeneficioSolicitado = '100', -- money
    @BeneficioAprobado = '100', -- money
    @FechaEstadoActual = '2014/10/01 00:00:00', -- date
    @ExpedienteEstadoId = 0, -- int
    @EvaluadorID = 0, -- int
    @EvaluadorTecnicoID = 0, -- int
    @EstadoLiquidacionID = 0,
    @Fecha = '2014/10/20 00:00:00',
	@FechaCambioLiquidacion = '2014/10/20 00:00:00'

SELECT * FROM dbo.ExpedientesHistorico;
*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesEstadosSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ExpedientesEstadosSelProc]
	(
	@modo				INT = 0, -- 0 trae todos | 1 trae campos específicos
	@ModoDebug INT = 0 -- 0 False / 1 True
	)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

			BEGIN TRY	
				SELECT 
					*
				FROM dbo.ExpedienteEstados exest
				ORDER BY exest.ExpedienteEstadoID
			END TRY

			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	    

END;
/*
EXEC dbo.ExpedientesEstadosSelProc



*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedienteEstadosInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedienteEstadosInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@ExpedienteEstadoID		INT,
	@Descripcion			NVARCHAR(100)
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	IF (NOT EXISTS (SELECT ee.ExpedienteEstadoID FROM dbo.ExpedienteEstados ee WHERE ee.Descripcion = @Descripcion))
		BEGIN
			-- Realiza el alta
			BEGIN TRY						
				BEGIN TRAN	
				
					INSERT dbo.ExpedienteEstados
					        ( ExpedienteEstadoID, Descripcion )
					VALUES  ( @ExpedienteEstadoID, -- ExpedienteEstadoID - int
					          UPPER(@Descripcion)  -- Descripcion - nvarchar(100)
					          )

					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END	
									
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');	
							
							SELECT * 
							FROM dbo.ExpedienteEstados ee 
							WHERE ee.Descripcion = @Descripcion;
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK				
			END TRY	
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	
					
		END
		
	ELSE
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El estado de Expediente ya existe registrado!';			
			SELECT (1) AS statusDB;
			RETURN;		
		END
END

/*
EXEC dbo.ExpedienteEstadosInsProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @ExpedienteEstadoID = 4, -- int
    @Descripcion = N'FORMAL' -- nvarchar(100)
    
select * from dbo.ExpedienteEstados;
*/
GO
/****** Object:  StoredProcedure [dbo].[EmpresasSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EmpresasSelProc]
	(
	@modo				INT = 0, -- 0 trae todos | 1 trae campos específicos
	@EmpresaCuitID		BIGINT = NULL,
	@NroEmpresa			NVARCHAR(5) = NULL,
	@RazonSocial		NVARCHAR(200) = NULL,
	@ModoDebug INT = 0 -- 0 False / 1 True
	)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

	IF (@EmpresaCuitID IS NULL) 
	AND (@NroEmpresa = '' OR @NroEmpresa IS NULL) 
	AND (@NroEmpresa = '' OR @NroEmpresa IS NULL)
	AND (@RazonSocial = '' OR @RazonSocial IS NULL) -- Lista todas las empresas
		BEGIN		
			BEGIN TRY	
				SELECT * FROM dbo.Empresas
				ORDER BY RazonSocial
			END TRY

			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	    
		END

	BEGIN TRY	
		IF @EmpresaCuitID IS NOT NULL -- Lista por Cuit
			BEGIN
            	SELECT * FROM dbo.Empresas
				WHERE EmpresaCuitID = @EmpresaCuitID
			END
	END TRY

	BEGIN CATCH
		SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;	    

	BEGIN TRY	
		IF @NroEmpresa <> '' AND @NroEmpresa IS NOT NULL -- Lista por nro Empresa
			BEGIN
				SELECT * FROM dbo.Empresas
				WHERE NroEmpresa = @NroEmpresa
			END
	END TRY

	BEGIN CATCH
		SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;	    

	BEGIN TRY	
		IF @RazonSocial <> '' AND @RazonSocial IS NOT NULL -- Lista por razon social
			BEGIN
				SELECT * FROM dbo.Empresas
				WHERE RazonSocial LIKE '%'+ @RazonSocial + '%'
			END
	END TRY

	BEGIN CATCH
		SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;	    

END;
/*

EXEC dbo.EmpresasSelProc 

EXEC dbo.EmpresasSelProc @modo = 1, -- int
	@EmpresaCuitID = 30506133730, -- bigint
	@ModoDebug = 0

EXEC dbo.EmpresasSelProc @modo = 2, -- int
	@NroEmpresa = 'R0006', -- nvarchar(5)
	@ModoDebug = 0

EXEC dbo.EmpresasSelProc @modo = 3, -- int
		@RazonSocial = 'SOLA', -- nvarchar(5)
		@ModoDebug = 0
*/
GO
/****** Object:  StoredProcedure [dbo].[EmpresasInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmpresasInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@EmpresaCuitID		BIGINT,
	@NroEmpresa			NVARCHAR(5),
	@RazonSocial		NVARCHAR(200)
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN
	IF (NOT EXISTS (SELECT EmpresaCuitID FROM dbo.Empresas emp WHERE emp.EmpresaCuitID = @EmpresaCuitID))
		BEGIN
			-- Realiza el alta
			BEGIN TRY						
				BEGIN TRAN	

					INSERT INTO dbo.Empresas
							( EmpresaCuitID ,
							  NroEmpresa ,
							  RazonSocial
							)
					VALUES  ( @EmpresaCuitID , -- EmpresaCuitID - bigint
							  @NroEmpresa , -- NroEmpresa - nvarchar(5)
							  UPPER(@RazonSocial)  -- RazonSocial - nvarchar(200)
							)
							
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END	
									
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
							SELECT * FROM dbo.Empresas
							WHERE dbo.Empresas.EmpresaCuitID = @EmpresaCuitID;
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK				
			END TRY	
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	
					
		END
		
	ELSE
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'La Empresa ya existe registrada!';			
			SELECT (1) AS statusDB;
			RETURN;		
		END
END

/*
EXEC dbo.EmpresasInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @EmpresaCuitID = 30708019840, -- bigint
    @NroEmpresa = N'R0013', -- nvarchar(5)
    @RazonSocial = N'INDUSTRIAS GENERAL LOPEZ S.A.' -- nvarchar(200)
    
EXEC dbo.EmpresasInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @EmpresaCuitID = 30569766253, -- bigint
    @NroEmpresa = N'R0023', -- nvarchar(5)
    @RazonSocial = N'INGENIERIA GASTRONOMICA SA' -- nvarchar(200)
    
*/
GO
/****** Object:  StoredProcedure [dbo].[CargaDatosLogSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CargaDatosLogSelProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0 -- 0 False / 1 True
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	-- Lista CargaDatosLog
			
	SELECT *
	FROM CargaDatosLog cdl
	ORDER BY cdl.ejecucionAutomatica
	
			
		
END

/*
	EXEC CargaDatosLogSelProc;
*/
GO
/****** Object:  StoredProcedure [dbo].[CargaDatosLogInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CargaDatosLogInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@fechaEjecucionCargaDatos		DATETIME2
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	-- Realiza el alta
	BEGIN TRY						
		BEGIN TRAN	
			
			INSERT INTO	dbo.CargaDatosLog 
			        ( ejecucionAutomatica )
			VALUES  ( @fechaEjecucionCargaDatos  -- ejecucionAutomatica - datetime2(7)
			        )
					
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END	
							
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
					SELECT cdl.ejecucionAutomatica
					FROM dbo.CargaDatosLog AS cdl
					ORDER BY cdl.ejecucionAutomatica
					DESC

					IF @@TRANCOUNT > 0
						ROLLBACK;
				END								
			SELECT (0) AS statusDB; -- TODO OK				
	END TRY	
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;						
		
END

/*
EXEC dbo.CargaDatosLogInsProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @fechaEjecucionCargaDatos = '2017-01-12 20:02:58' -- datetime2(7)
*/
GO
/****** Object:  StoredProcedure [dbo].[EstadoSincronizacionesSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EstadoSincronizacionesSelProc]
	(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@ExpedienteEstadoID INT = NULL
	)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

	IF @ExpedienteEstadoID IS null -- Obtener listado de las sincronizaciones
		BEGIN		
			BEGIN TRY	
				SELECT 
				expest.Descripcion,
				es.*
				FROM dbo.EstadoSincronizaciones es
				JOIN dbo.ExpedienteEstados expest ON expest.ExpedienteEstadoID = es.ExpedienteEstadoID;
			END TRY

			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	    
		END

	IF @ExpedienteEstadoID IS NOT NULL  -- Obtener listado de la sincronizacion de un estado
		BEGIN		
			BEGIN TRY	
				SELECT 
				expest.Descripcion,
				es.*
				FROM dbo.EstadoSincronizaciones es
				JOIN dbo.ExpedienteEstados expest ON expest.ExpedienteEstadoID = es.ExpedienteEstadoID
				WHERE es.ExpedienteEstadoID = @ExpedienteEstadoID;
			END TRY

			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	    
		END
END;
/*

-- Listar todas las configuraciones
-----------------------------------
EXEC dbo.EstadoSincronizacionesSelProc
    @Modo = 0, -- int
    @ModoDebug = 0 -- int


-- Listar todas las configuraciones en estado 1
------------------------------------------------
EXEC dbo.EstadoSincronizacionesSelProc
    @Modo = 1, -- int
    @ModoDebug = 0, -- int
	@ExpedienteEstadoID = 4
*/
GO
/****** Object:  StoredProcedure [dbo].[EstadoSincronizacionesInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EstadoSincronizacionesInsProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @ExpedienteEstadoID INT ,
      @UltimaSincronizacionComienzo DATETIME2(7) ,
      @UltimaSincronizacionFin DATETIME2(7) ,
      @ExpedientesObtenidos INT ,
      @ExpedientesPosibles INT ,
      @ExpedientesActualizados INT
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN
	

	
        BEGIN TRY						
            BEGIN TRAN	
					-- Realiza el alta
            INSERT  INTO dbo.EstadoSincronizacionesHistorico
                    SELECT  *
                    FROM    dbo.EstadoSincronizaciones
                    WHERE   ExpedienteEstadoID = @ExpedienteEstadoID

					-- Realiza Update
            UPDATE  dbo.EstadoSincronizaciones
            SET     UltimaSincronizacionComienzo = @UltimaSincronizacionComienzo ,
                    UltimaSincronizacionFinalizacion = @UltimaSincronizacionFin ,
                    EstadoSincronizacion = 1 ,
					ExpedientesObtenidos = @ExpedientesObtenidos ,
					ExpedientesPosibles = @ExpedientesPosibles ,
					ExpedientesActualizados = @ExpedientesActualizados

            WHERE   ExpedienteEstadoID = @ExpedienteEstadoID

            IF ( @ModoDebug = 0 )
                BEGIN
                    COMMIT;
                END	
									
            ELSE
                BEGIN
                    PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                    SELECT  *
                    FROM    dbo.EstadoSincronizaciones
                    WHERE   ExpedienteEstadoID = @ExpedienteEstadoID;
                    IF @@TRANCOUNT > 0
                        ROLLBACK;
                END								
            SELECT  ( 0 ) AS statusDB; -- TODO OK				
        END TRY	
			
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;
            SELECT  ( 99 ) AS statusDB; -- ERROR					
            SELECT  ERROR_NUMBER() AS ErrorNumber ,
                    ERROR_SEVERITY() AS ErrorSeverity ,
                    ERROR_STATE() AS ErrorState ,
                    ERROR_PROCEDURE() AS ErrorProcedure ,
                    ERROR_LINE() AS ErrorLine ,
                    ERROR_MESSAGE() AS ErrorMessage;
        END CATCH;	
					
    END
		
/*
EXEC dbo.EstadoSincronizacionesInsProc @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @ExpedienteEstadoID = 4, -- int
    @UltimaSincronizacionComienzo = '2016-12-13 17:08:50', -- datetime2
    @UltimaSincronizacionFin = '2016-12-13 17:08:50', -- datetime2
    @ExpedientesObtenidos = 100, -- int
    @ExpedientesPosibles = 20, -- int
    @ExpedientesActualizados = 1 -- int
    
*/
GO
/****** Object:  StoredProcedure [dbo].[EstadosLiquidacionSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROCEDURE [dbo].[EstadosLiquidacionSelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @EstadoLiquidacionID INT = 0 ,
      @SolicitanteUsuarioID	INT -- ID del Solicitante
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

	DECLARE @SolicitanteUsuarioTipoID INT = NULL;

BEGIN
	-- Obtiene el tipo de usuario que desea realizar el cambio de estado de liquidación.
	SELECT @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @SolicitanteUsuarioID;



	-- Solo los gestores y administradores puede visualizar el estado de liquidacion de un expediente.
	IF (@SolicitanteUsuarioTipoID IS NULL) OR (@SolicitanteUsuarioTipoID < 2)
		BEGIN
			IF (@ModoDebug = 1)
				BEGIN
					PRINT 'Solo los socios y administradores pueden visualizar estados de liquidacion.!';			
				END				
			SELECT (2) AS statusDB;
			RETURN;			
		END	

		BEGIN TRY		
			IF @EstadoLiquidacionID = 0
				BEGIN
					SELECT  *
					FROM    dbo.EstadosLiquidacion
				END
			ELSE		-- Obtiene las asignaciones de todos los usuarios que sean igual o menor a mi tipo de usuario
				BEGIN	
					SELECT  *
					FROM    dbo.EstadosLiquidacion
					WHERE   EstadoLiquidacionID = @EstadoLiquidacionID	
				END		
		END TRY
		
		BEGIN CATCH
			SELECT (99) AS statusDB; -- ERROR					
			SELECT 
				ERROR_NUMBER() AS ErrorNumber
				,ERROR_SEVERITY() AS ErrorSeverity
				,ERROR_STATE() AS ErrorState
				,ERROR_PROCEDURE() AS ErrorProcedure
				,ERROR_LINE() AS ErrorLine
				,ERROR_MESSAGE() AS ErrorMessage;
		END CATCH;	
			
END;
		
/*
EXEC dbo.EstadosLiquidacionSelProc @SolicitanteUsuarioID = 2

EXEC dbo.EstadosLiquidacionSelProc @SolicitanteUsuarioID = 1, @EstadoLiquidacionID = 0
*/
GO
/****** Object:  StoredProcedure [dbo].[ConfigSincronizacionesSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ConfigSincronizacionesSelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @Estado INT = 2,
      @SolicitanteUsuarioID INT
	)
AS
    SET XACT_ABORT, NOCOUNT ON;   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
    
    DECLARE @SolicitanteUsuarioTipoID INT;
    
    BEGIN
    
	-- Existe el usuario que solicita el listado?
	IF (@SolicitanteUsuarioID IS NULL) OR (@SolicitanteUsuarioID = '') OR
		(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El Id del usuario que solicita el listado, no existe!';			
			SELECT (3) AS statusDB;
			RETURN;		
		END		

	-- Obtiene el Tipo de Usuario que realiza la consulta.
	SET @SolicitanteUsuarioTipoID = (SELECT us.UsuarioTipoID
									FROM dbo.Usuarios us
									WHERE us.UsuarioID = @SolicitanteUsuarioID);
									

	-- Sólo los usuarios Administradores generan listados de usuarios
	IF (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'Listado prohibido para el nivel de usuario del solicitante!';			
			SELECT (2) AS statusDB;
			RETURN;
		END				
    

		-- Listar por Estado 0 o 1
        IF ( @Estado = 0 ) OR ( @Estado = 1 )
            BEGIN
                BEGIN TRY	
                    SELECT  *
                    FROM    dbo.ConfigSincronizaciones cs
                    WHERE   cs.Estado = @Estado;
                    RETURN;
                END TRY

                BEGIN CATCH
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;	
            END;
	
	
		-- Listar todo
        BEGIN
            BEGIN TRY	
                SELECT  *
                FROM    dbo.ConfigSincronizaciones cs;
                RETURN;
            END TRY

            BEGIN CATCH
                SELECT  ( 99 ) AS statusDB; -- ERROR					
                SELECT  ERROR_NUMBER() AS ErrorNumber ,
                        ERROR_SEVERITY() AS ErrorSeverity ,
                        ERROR_STATE() AS ErrorState ,
                        ERROR_PROCEDURE() AS ErrorProcedure ,
                        ERROR_LINE() AS ErrorLine ,
                        ERROR_MESSAGE() AS ErrorMessage;
            END CATCH;	
        END;	
		

    END;
/*

-- Listar todas las configuraciones en estado 0
------------------------------------------------
EXEC dbo.ConfigSincronizacionesSelProc
    @Modo = 1, -- int
    @ModoDebug = 0, -- int
    @SolicitanteUsuarioID = 1, -- int
    @Estado = 0
    
-- Listar todas las configuraciones en estado 1
------------------------------------------------
EXEC dbo.ConfigSincronizacionesSelProc
    @Modo = 1, -- int
    @ModoDebug = 0, -- int   
    @SolicitanteUsuarioID = 1, -- int 
    @Estado = 1
    
-- Listar todas las configuraciones
-----------------------------------
EXEC dbo.ConfigSincronizacionesSelProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @SolicitanteUsuarioID = 1, -- int
    @Estado = 2
        
*/
GO
/****** Object:  StoredProcedure [dbo].[EvaluadoresSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EvaluadoresSelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @Nombre NVARCHAR(100) = NULL ,
      @SolicitanteUsuarioID INT      
    )
AS
    SET XACT_ABORT, NOCOUNT ON;   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
    
    DECLARE @SolicitanteUsuarioTipoID INT;
    
    BEGIN

            
		-- Existe el usuario que solicita el listado?
        IF ( @SolicitanteUsuarioID IS NULL )
            OR ( @SolicitanteUsuarioID = '' )
            OR ( NOT EXISTS ( SELECT    us.UsuarioID
                              FROM      dbo.Usuarios us
                              WHERE     us.UsuarioID = @SolicitanteUsuarioID )
               )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'El Id del usuario que solicita el listado, no existe!';			
                SELECT  ( 3 ) AS statusDB;
                RETURN;		
            END;		
			
		-- Obtiene el Tipo de Usuario que realiza la consulta.
        SET @SolicitanteUsuarioTipoID = ( SELECT    us.UsuarioTipoID
                                          FROM      dbo.Usuarios us
                                          WHERE     us.UsuarioID = @SolicitanteUsuarioID
                                        );
											

		-- Sólo los usuarios Administradores generan listados de evaluadores
        IF ( @SolicitanteUsuarioTipoID < 3 )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'Listado prohibido para el nivel de usuario del solicitante!';			
                SELECT  ( 2 ) AS statusDB;
                RETURN;
            END;		            
            
            
		-- Lista todos los evaluadores 
        IF ( @Nombre IS NULL )
            OR ( @Nombre = '' )
            BEGIN
                SELECT  ev.EvaluadorID ,
                        ev.Nombre
                FROM    dbo.Evaluadores ev
                ORDER BY ev.Nombre;
            END;
        ELSE
			-- Lista el evaluador solicitado
            BEGIN
                SELECT  ev.EvaluadorID ,
                        ev.Nombre
                FROM    dbo.Evaluadores ev
                WHERE   ev.Nombre = @Nombre
                ORDER BY ev.Nombre;                
            END;
            
    END;

/*
   
EXEC dbo.EvaluadoresSelProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'', -- nvarchar(100)
    @SolicitanteUsuarioID = 1 -- int
        
select * from dbo.Evaluadores;    
    
*/
GO
/****** Object:  StoredProcedure [dbo].[UsuariosUpdProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UsuariosUpdProc]
	(
	@Modo INT = 1,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@Nombre NVARCHAR(50) = NULL,
	@Email NVARCHAR(50) = NULL,
	@Password NVARCHAR(20) = NULL,
	@NuevoUsuarioEstadoID INT = NULL, -- Nuevo estado de usuario que quiero asignar (1 => Inactivo / 2 => Activo)
	@NuevoUsuarioTipoID INT = NULL,  -- Nuevo tipo de usuario que quiero asignar (1 => Gestor / 2 => Socio / 3 => Administrador)
	@UsuarioID INT, -- ID del usuario que quiero modificar
	@SolicitanteUsuarioID INT -- ID del Solicitante
	)
AS

SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

-- Variables para Generar el Password Encriptado
DECLARE @Salt NVARCHAR(25) = NULL;
DECLARE @SaltMasPassword NVARCHAR(125) = NULL;
DECLARE @Seed INT  = NULL;
DECLARE @CiclosHashing tinyint;
DECLARE @PasswordEncriptado VARBINARY(20) = NULL;

DECLARE @FechaHora DATETIME;
DECLARE @SolicitanteUsuarioTipoID INT = NULL;
DECLARE @UsuarioTipoID INT;

BEGIN

	-- Existe el usuario que solicita la modificación?
	IF (@SolicitanteUsuarioID IS NULL) OR (@SolicitanteUsuarioID = '') OR
		(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El Id del usuario que solicita la modificación, no existe!';			
			SELECT (3) AS statusDB;
			RETURN;		
		END	

	-- Existe el usuario que se quiere modificar?
	IF (@UsuarioID IS NULL) OR (@UsuarioID = '') OR
		(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @UsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El Id del usuario que quiere modificar, no existe!';			
			SELECT (2) AS statusDB;
			RETURN;		
		END		

	-- Obtiene el Tipo de Usuario que realiza la consulta.
	SET @SolicitanteUsuarioTipoID = (SELECT us.UsuarioTipoID
									 FROM dbo.Usuarios us
									 WHERE us.UsuarioID = @SolicitanteUsuarioID);
									
	-- Obtiene el Tipo de Usuario al que se le quiere modificar el password.
	SET @UsuarioTipoID = (SELECT us.UsuarioTipoID
						  FROM dbo.Usuarios us
						  WHERE us.UsuarioID = @UsuarioID);	
						  								
	
	-- Si se recibe Password, Encriptarlo!
	IF (@Password <> '') AND (@Password IS NOT NULL)
		BEGIN										
			IF (@ModoDebug = 1) PRINT 'Tiene Password';
			-- Obtiene Fecha y Hora
			SET @FechaHora = GETDATE();
			SET @Seed = (DATEPART(hh, @FechaHora) * 10000000) + (DATEPART(n, @FechaHora) * 100000)
			+ (DATEPART(s, @FechaHora) * 1000) + DATEPART(ms, @FechaHora);
			SET @CiclosHashing = 1;

			-- Genera un salt random  
			SET @Salt = CHAR(ROUND((RAND(@Seed) * 94.0) + 32, 3));

			-- Hashea el salt random 25 veces mas 
			WHILE (@CiclosHashing < 25)
				BEGIN
					SET @Salt = @Salt + CHAR(ROUND((RAND() * 94.0) + 32, 3));
					SET @CiclosHashing = @CiclosHashing + 1;
				END

			-- Concatena el Salt + el password
			SET @SaltMasPassword = @Salt + @Password;		

			-- Setea el el Password Encriptado
			SET @PasswordEncriptado = HASHBYTES('SHA1', @SaltMasPassword);
			
			-- Print Encriptacion
			IF (@ModoDebug = 1)
				BEGIN
					PRINT (
					  CONVERT(VARCHAR(1000), @PasswordEncriptado, 2)  + ', ' +
					  CONVERT(VARCHAR(1000), @Salt, 2)  + ', ' +
					  CAST(@NuevoUsuarioEstadoID AS nvarchar(6)) + ', ' +
					  CAST(@NuevoUsuarioTipoID AS nvarchar(6))
							);
				END
		END	
	ELSE
		BEGIN
			-- Capturar el estado actual del password y del salt para actualizarlo por el mismo que tenia el usuario
			SELECT @PasswordEncriptado = us.Password, 
			       @Salt = us.Salt
			FROM dbo.Usuarios us
			WHERE us.UsuarioID = @UsuarioID
		END		

	-- Captura el nombre actual
	IF (@Nombre = '') OR (@Nombre IS NULL)
	BEGIN
		-- Capturar el email actual
		SELECT @Nombre = us.Nombre
		FROM dbo.Usuarios us
		WHERE us.UsuarioID = @UsuarioID
	END

	-- Si se recibe un email vacío lo convierte en null en la base de datos
	IF (@Email = '')
	BEGIN
		SET @Email = NULL;
	END
	
	-- Captura el usuarioEstadoID actual
	IF (@NuevoUsuarioEstadoID = '') OR (@NuevoUsuarioEstadoID IS NULL)
	BEGIN
		-- Capturar el usuarioEstadoID actual
		SELECT @NuevoUsuarioEstadoID = us.UsuarioEstadoID
		FROM dbo.Usuarios us
		WHERE us.UsuarioID = @UsuarioID
	END	
	
	-- Captura el UsuarioTipoID	actual
	IF (@NuevoUsuarioTipoID = '') OR (@NuevoUsuarioTipoID IS NULL)
	BEGIN
		-- Capturar el usuarioEstadoID actual
		SELECT @NuevoUsuarioTipoID = us.UsuarioTipoID
		FROM dbo.Usuarios us
		WHERE us.UsuarioID = @UsuarioID
	END		
	

	-- Los Usuarios de Nivel Socio o Gestor, sólo pueden modificar su propia contraseña.
	-- Los Usuarios de nivel Administrador pueden modificar cualquier registro.
	
	-- Actualización Contraseña Propia (Todos los usuarios)
	IF (@SolicitanteUsuarioID = @UsuarioID) AND (@PasswordEncriptado IS NOT NULL)
		BEGIN
			BEGIN TRY						
				BEGIN TRAN				

					UPDATE dbo.Usuarios 
					SET [Password] = @PasswordEncriptado,
					    Salt = @Salt
					WHERE UsuarioID = @UsuarioID;
							
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END				
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
							SELECT * FROM dbo.Usuarios
								WHERE dbo.Usuarios.UsuarioID = @UsuarioID;
							IF @@TRANCOUNT > 0
								BEGIN
									ROLLBACK;
									SELECT * FROM dbo.Usuarios
										WHERE dbo.Usuarios.UsuarioID = @UsuarioID;
								END
						END	
							
					SELECT (0) AS statusDB; -- TODO OK
				
			END TRY
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;		
		END
		
	-- Actualizacion todos los datos de cualquier usuario (Solo Administradores)
	IF (@SolicitanteUsuarioTipoID = 3)
		BEGIN
			-- Evitar que un administrador se pueda degradar su propio nivel a uno mas bajo!
			IF (@SolicitanteUsuarioID = @UsuarioID) AND (@SolicitanteUsuarioTipoID > @NuevoUsuarioTipoID)
				BEGIN
					SET @NuevoUsuarioTipoID = @SolicitanteUsuarioTipoID;
				END

			IF (@ModoDebug = 1)
				BEGIN
					PRINT ('Un usuario con nivel Administrador no se puede degradar su propio nivel');
				END			

			BEGIN TRY						
				BEGIN TRAN	

					UPDATE dbo.Usuarios 
					SET Nombre = @Nombre,
						Email = @Email,
					    Password = @PasswordEncriptado,
					    Salt = @Salt,
						UsuarioTipoID = @NuevoUsuarioTipoID,
						UsuarioEstadoID = @NuevoUsuarioEstadoID					    
					WHERE UsuarioID = @UsuarioID;				
					
					-- si el usuario se desactiva, se borra password y salt
					IF @NuevoUsuarioEstadoID = 1
					BEGIN
						UPDATE dbo.Usuarios
						SET Password = NULL, Salt = NULL
						WHERE UsuarioID = @UsuarioID;
					END
							
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END				
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
							SELECT * FROM dbo.Usuarios
								WHERE dbo.Usuarios.UsuarioID = @UsuarioID;
							IF @@TRANCOUNT > 0
								BEGIN
									ROLLBACK;
									SELECT * FROM dbo.Usuarios
										WHERE dbo.Usuarios.UsuarioID = @UsuarioID;
								END
						END							
					SELECT (0) AS statusDB; -- TODO OK				
			END TRY
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;			
		END
		
END;
/*

-- Actualizar Password propio (Gestor / Socio)
----------------------------------------------
EXEC dbo.UsuariosUpdProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'', -- nvarchar(50)
    @Email = N'pepe@pepe.com', -- nvarchar(50)
    @Password = N'password', -- nvarchar(20)
    @NuevoUsuarioEstadoID = 2, -- int
    @NuevoUsuarioTipoID = 1, -- int
    @UsuarioID = 1, -- int
    @SolicitanteUsuarioID = 1 -- int
    

select * from usuarios;

-- Actualizar Todos los registros (Administrador)
-------------------------------------------------
EXEC dbo.UsuariosUpdProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'', -- nvarchar(50)
    @Email = N'pepe2@pepe.com', -- nvarchar(50)
    @Password = N'sarasa', -- nvarchar(20)
    @NuevoUsuarioEstadoID = 2, -- int
    @NuevoUsuarioTipoID = 2, -- int
    @UsuarioID = 3, -- int
    @SolicitanteUsuarioID = 1 -- int
    

select * from usuarios;


-- Actualizar Datos del administrador degradando su 
-- tipo de cuenta a un nivel inferior (Administrador)
-----------------------------------------------------
EXEC dbo.UsuariosUpdProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @Nombre = N'', -- nvarchar(50)
    @Email = N'', -- nvarchar(50)
    @Password = N'', -- nvarchar(20)
    @NuevoUsuarioEstadoID = 2, -- int
    @NuevoUsuarioTipoID = 2, -- int
    @UsuarioID = 1, -- int
    @SolicitanteUsuarioID = 1 -- int


-- Actualizacion desde administrador los Datos de un usuario
-- sin pisarle el password que ya tiene.
------------------------------------------------------------
EXEC dbo.UsuariosUpdProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @Nombre = N'', -- nvarchar(50)
    @Email = N'', -- nvarchar(50)
    @Password = N'', -- nvarchar(20)
    @NuevoUsuarioEstadoID = 2, -- int
    @NuevoUsuarioTipoID = 2, -- int
    @UsuarioID = 3, -- int
    @SolicitanteUsuarioID = 1 -- int


select * from usuarios;
    
*/
GO
/****** Object:  StoredProcedure [dbo].[UsuariosDelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UsuariosDelProc]
	(
	@Modo INT = 1,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@UsuarioID INT,
	@SolicitanteUsuarioID INT -- ID del Solicitante
	)
AS

SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

DECLARE @SolicitanteUsuarioTipoID INT;

BEGIN

	-- Existe el usuario que solicita la Baja?
	IF (@SolicitanteUsuarioID IS NULL) OR (@SolicitanteUsuarioID = '') OR
		(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El Id del usuario que solicita la baja, no existe!';			
			SELECT (3) AS statusDB;
			RETURN;		
		END	

	-- Obtiene el Tipo de Usuario que realiza la peticion de baja.
	SET @SolicitanteUsuarioTipoID = (SELECT us.UsuarioTipoID
									FROM dbo.Usuarios us
									WHERE us.UsuarioID = @SolicitanteUsuarioID);
									
	-- Sólo los usuarios de nivel 3 (admin) pueden dar de baja usuarios.
	IF (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'Baja prohibida para el nivel de usuario del solicitante!';			
			SELECT (2) AS statusDB;
			RETURN;
		END		
		
	-- Existe el id del usuario que queremos dar de baja?
	IF (NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @UsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'El usuario que se desea dar de baja no existe!';								
			SELECT (1) AS statusDB;
			RETURN				
		END
		
	-- Realiza la baja.
	BEGIN TRY						
		BEGIN TRAN	
			DELETE FROM dbo.Usuarios
			WHERE UsuarioID = @UsuarioID
		
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END				
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
					SELECT * 
					FROM dbo.Usuarios
					IF @@TRANCOUNT > 0
						ROLLBACK;
					SELECT * 
					FROM dbo.Usuarios						
				END	
					
			SELECT (0) AS statusDB; -- TODO OK
		
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;					
	
END;
/*
-- *********************
-- Limpar Tabla Usuarios
-- *********************

DELETE FROM dbo.Usuarios WHERE UsuarioID >= 0;

DBCC CHECKIDENT (Usuarios, RESEED, 0)


INSERT dbo.Usuarios
        ( Nombre ,
          Email ,
          Password ,
          Salt ,
          UsuarioEstadoID ,
          UsuarioTipoID
        )
VALUES  ( N'admin' , -- Nombre - nvarchar(50)
          NULL , -- Email - nvarchar(50)
          NULL , -- Password - varbinary(20)
          NULL , -- Salt - char(25)
          2 , -- UsuarioEstadoID - int
          3  -- UsuarioTipoID - int
        )

-- Baja Gestor (desde Admin)
---------------------------
EXEC dbo.UsuariosDelProc
    @Modo = 1, -- int
    @ModoDebug = 0, -- int
    @UsuarioID = 2, -- int
    @SolicitanteUsuarioID = 1 -- int

-- Baja Socio (desde Admin)
---------------------------
EXEC dbo.UsuariosDelProc
    @Modo = 1, -- int
    @ModoDebug = 0, -- int
    @UsuarioID = 3, -- int
    @SolicitanteUsuarioID = 1 -- int
*/
GO
/****** Object:  StoredProcedure [dbo].[UsuariosAutenticarProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Crea el procedure para verificar cuentas
CREATE PROC [dbo].[UsuariosAutenticarProc]
	@ModoDebug INT = 0 , -- 0 False / 1 True
	@Nombre NVARCHAR(50) ,
	@Password NVARCHAR(100)
AS
	BEGIN
		SET NOCOUNT ON;
        
        -- Existe el usuario que solicita login?
		IF (@Nombre IS NULL) OR
			(@Nombre = '') OR
			(@Password IS NULL) OR
			(@Password = '') OR
			(
			 NOT EXISTS ( SELECT
							us.UsuarioID
						  FROM
							dbo.Usuarios us
						  WHERE
							us.Nombre = @Nombre ))
			BEGIN
				IF (@ModoDebug = 1)
					PRINT 'El nombre del usuario que solicita login no existe o no se suministro password!';			
				SELECT
					-1 AS UsuarioID ,
					'' AS Nombre ,
					'' AS Email ,
					0 AS UsuarioEstadoID ,
					'' AS UsuarioEstadoDescripcion ,
					0 AS UsuarioTipoID ,
					'' AS UsuarioTipoDescripcion ,
					0 AS PoseeLogin ,
					'' AS PoseeLoginDescripcion ,
					0 AS Autenticado					
				RETURN;		
			END;        

		DECLARE	@Salt CHAR(25);
		DECLARE	@SaltMasPassword NVARCHAR(125);
		DECLARE	@PasswordHash VARBINARY(20);  

		-- Selecciona el Salt y el Password actual del usuario recibido
		SELECT
			@Salt = Salt ,
			@PasswordHash = [Password]
		FROM
			dbo.Usuarios us
		WHERE
			us.Nombre = @Nombre;

		SET @SaltMasPassword = @Salt + @Password;

		IF (HASHBYTES('SHA1', @SaltMasPassword) = @PasswordHash)
			BEGIN
				
				-- Password OK				
				IF (@ModoDebug = 0)
					SELECT
						us.UsuarioID ,
						us.Nombre ,
						us.Email ,
						us.UsuarioEstadoID ,
						'' AS UsuarioEstadoDescripcion ,
						us.UsuarioTipoID ,
						ut.Descripcion AS UsuarioTipoDescripcion ,
						1 AS PoseeLogin ,
						'' AS PoseeLoginDescripcion ,
						1 AS Autenticado
					FROM
						dbo.Usuarios us
						JOIN dbo.UsuariosTipos AS ut
						ON ut.UsuarioTipoID = us.UsuarioTipoID 
					WHERE
						us.Nombre = @Nombre;
				ELSE
					PRINT ('Password OK!');
			END;
	
		ELSE
			BEGIN
				-- Password Inválido
				IF (@ModoDebug = 0)
					SELECT
						us.UsuarioID ,
						us.Nombre ,
						us.Email ,
						us.UsuarioEstadoID ,
						'' AS UsuarioEstadoDescripcion ,
						us.UsuarioTipoID ,
						'' AS UsuarioTipoDescripcion ,
						CASE WHEN (us.Password IS NULL) THEN 0
							 WHEN (us.Password IS NOT NULL) THEN 1
						END AS PoseeLogin ,
						'' AS PoseeLoginDescripcion ,
						0 AS Autenticado
					FROM
						dbo.Usuarios us
					WHERE
						us.Nombre = @Nombre;												
				ELSE
					PRINT ('Password Inválido!');
			END;
		
	END;
/*

EXEC dbo.UsuariosAutenticarProc
    @ModoDebug = 0, -- int
	@Nombre = N'admin', -- nvarchar(50)
	@Password = N'test' -- nvarchar(100)
	
EXEC dbo.UsuariosAutenticarProc
    @ModoDebug = 0, -- int
	@Nombre = N'Juan', -- nvarchar(50)
	@Password = N'' -- nvarchar(100)	
	
*/
GO
/****** Object:  StoredProcedure [dbo].[UsuariosSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UsuariosSelProc]
	(
	@Modo INT = 1,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@SolicitanteUsuarioID INT -- ID del Solicitante
	)
AS

SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

DECLARE @SolicitanteUsuarioTipoID INT;

BEGIN

	-- Existe el usuario que solicita el listado?
	IF (@SolicitanteUsuarioID IS NULL) OR (@SolicitanteUsuarioID = '') OR
		(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID))
		BEGIN
			IF (@ModoDebug = 1)
				BEGIN
					PRINT 'El Id del usuario que solicita el listado, no existe!';			
				END					
			SELECT (3) AS statusDB;
			RETURN;		
		END		

	-- Obtiene el Tipo de Usuario que realiza la consulta.
	SET @SolicitanteUsuarioTipoID = (SELECT us.UsuarioTipoID
									FROM dbo.Usuarios us
									WHERE us.UsuarioID = @SolicitanteUsuarioID);
									

	-- Sólo los usuarios Administradores generan listados de usuarios
	IF (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
			BEGIN
				PRINT 'Listado prohibido para el nivel de usuario del solicitante!';			
			END
			SELECT (2) AS statusDB;
			RETURN;
		END		
		
	
	-- Obtener listado completo de todos los usuarios (por ser administrador)
	IF (@SolicitanteUsuarioTipoID = 3)
		BEGIN
			BEGIN TRY	
				SELECT us.UsuarioID, us.Nombre, us.Email, 
				       us.UsuarioEstadoID, usest.Descripcion AS 'UsuarioEstadoDescripcion', 
					   us.UsuarioTipoID, ustipo.Descripcion AS 'UsuarioTipoDescripcion',
				       CASE 
							WHEN (us.Password IS NULL) THEN 0
							WHEN (us.Password IS NOT NULL) THEN 1
					   END AS PoseeLogin,
					   CASE 
							WHEN (us.Password IS NULL) THEN 'no'
							WHEN (us.Password IS NOT NULL) THEN 'si'
					   END AS PoseeLoginDescripcion,
					   0 AS Autenticado
				FROM dbo.Usuarios us
				JOIN dbo.UsuariosEstados usest ON usest.UsuarioEstadoID = us.UsuarioEstadoID
				JOIN dbo.UsuariosTipos ustipo ON ustipo.UsuarioTipoID = us.UsuarioTipoID
				WHERE us.UsuarioTipoID <= @SolicitanteUsuarioTipoID;
			END TRY

			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;		
		END	
		

END;
/*
-- Listar todos los usuario de igual o menor nivel
---------------------------------------------------
EXEC dbo.UsuariosSelProc
    @Modo = 1, -- int
    @ModoDebug = 1, -- int
    @SolicitanteUsuarioID = 1 -- int
    
EXEC dbo.UsuariosSelProc     @SolicitanteUsuarioID = 1 -- int


select * from usuarios    

*/
GO
/****** Object:  StoredProcedure [dbo].[UsuariosInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UsuariosInsProc]
	(
	@Modo INT = 1,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@Nombre NVARCHAR(50),
	@Email NVARCHAR(50) = NULL,
	@Password NVARCHAR(20) = NULL,
	@UsuarioEstadoID INT = 2, -- 1 => Inactivo / 2 => Activo
	@UsuarioTipoID INT,  -- 1 => Gestor / 2 => Socio / 3 => Administrador
	@SolicitanteUsuarioID INT -- ID del Solicitante
	)
AS

SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

-- Variables para Generar el Password Encriptado
DECLARE @Salt NVARCHAR(25) = NULL;
DECLARE @SaltMasPassword NVARCHAR(125) = NULL;
DECLARE @Seed INT  = NULL;
DECLARE @CiclosHashing tinyint;
DECLARE @PasswordEncriptado VARBINARY(20) = NULL;

DECLARE @FechaHora DATETIME;
DECLARE @SolicitanteUsuarioTipoID INT = NULL;

BEGIN

	-- Si la Tabla de usuarios tiene usuarios, verifica que el usuario que solicita el alta
	-- tenga permiso para hacerlo (nivel 3), que el usuario solicitante exista en la tabla y
	-- Que el usuario solicitante no venga en blanco.
	IF (SELECT COUNT(us.UsuarioID) FROM dbo.Usuarios us) > 0
		BEGIN			
			-- Existe el usuario que solicita el alta?
			IF (@SolicitanteUsuarioID IS NULL) OR (@SolicitanteUsuarioID = '') OR
				(NOT EXISTS (SELECT us.UsuarioID FROM dbo.Usuarios us WHERE us.UsuarioID = @SolicitanteUsuarioID))
				BEGIN
					IF (@ModoDebug = 1)
						PRINT 'El Id del usuario que solicita el alta, no existe!';			
					SELECT (3) AS statusDB;
					RETURN;		
				END			

			-- Obtiene el Tipo de Usuario que realiza la peticion de alta.
			SET @SolicitanteUsuarioTipoID = (SELECT us.UsuarioTipoID
											FROM dbo.Usuarios us
											WHERE us.UsuarioID = @SolicitanteUsuarioID);
											
			-- Las altas de Usuarios las realizan los usuarios con nivel Administrador	
			IF (@SolicitanteUsuarioTipoID < 3)
				BEGIN
					IF (@ModoDebug = 1)
						PRINT 'Alta prohibida para el nivel de usuario del solicitante!';			
					SELECT (2) AS statusDB;
					RETURN;
				END			    
			    
			-- Existe el usuario recibido para dar de alta o no se ingresó nombre de usuario?
			IF (@Nombre = '') OR (EXISTS (SELECT us.Nombre FROM dbo.Usuarios us WHERE us.Nombre = @Nombre))
				BEGIN
					IF (@ModoDebug = 1)
						PRINT 'El usuario ya existe o no ingreso nombre de usuario!';								
					SELECT (1) AS statusDB;
					RETURN				
				END
				
		END
		

		
	-- Si se recibe Password, Encriptarlo!
	IF (@Password <> '') AND (@Password IS NOT NULL)
		BEGIN										
			IF (@ModoDebug = 1) PRINT 'Tiene Password';
			-- Obtiene Fecha y Hora
			SET @FechaHora = GETDATE();
			SET @Seed = (DATEPART(hh, @FechaHora) * 10000000) + (DATEPART(n, @FechaHora) * 100000)
			+ (DATEPART(s, @FechaHora) * 1000) + DATEPART(ms, @FechaHora);
			SET @CiclosHashing = 1;

			-- Genera un salt random  
			SET @Salt = CHAR(ROUND((RAND(@Seed) * 94.0) + 32, 3));

			-- Hashea el salt random 25 veces mas 
			WHILE (@CiclosHashing < 25)
				BEGIN
					SET @Salt = @Salt + CHAR(ROUND((RAND() * 94.0) + 32, 3));
					SET @CiclosHashing = @CiclosHashing + 1;
				END

			-- Concatena el Salt + el password
			SET @SaltMasPassword = @Salt + @Password;		

			-- Setea el el Password Encriptado
			SET @PasswordEncriptado = HASHBYTES('SHA1', @SaltMasPassword);
			
			-- Print Encriptacion
			IF (@ModoDebug = 1)
				BEGIN
					PRINT (@Nombre + ', ' + 
					       @Email + ', ' + 
					  CONVERT(VARCHAR(1000), @PasswordEncriptado, 2)  + ', ' +
					  CONVERT(VARCHAR(1000), @Salt, 2)  + ', ' +
					  CAST(@UsuarioEstadoID AS nvarchar(6)) + ', ' +
					  CAST(@UsuarioTipoID AS nvarchar(6))
							);
				END
		END	
	ELSE
		BEGIN
			SET @PasswordEncriptado = NULL;
		END			
		
	-- Prepara Email
	IF (@Email = '')
	BEGIN
		SET @Email = NULL;
	END
	

	-- Realiza el alta
	BEGIN TRY						
		BEGIN TRAN	
			INSERT dbo.Usuarios	
					( Nombre ,
					  Email ,
					  [Password] ,
					  Salt ,
					  UsuarioEstadoID ,
					  UsuarioTipoID
					)
			VALUES  ( @Nombre , -- Nombre - nvarchar(50)
					  @Email , -- Email - nvarchar(50)
					  @PasswordEncriptado , -- Password - varbinary(20)
					  @Salt , -- Salt - char(25)
					  @UsuarioEstadoID , -- UsuarioEstadoID - int
					  @UsuarioTipoID  -- UsuarioTipoID - int
					)			
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END				
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
					SELECT * FROM dbo.Usuarios
						WHERE dbo.Usuarios.Nombre = @Nombre;
					IF @@TRANCOUNT > 0
						ROLLBACK;
				END	
					
			SELECT (0) AS statusDB; -- TODO OK
		
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;					
	
END;
/*
-- *********************
-- Limpar Tabla Usuarios
-- *********************

DELETE FROM dbo.Usuarios WHERE UsuarioID >= 0;
DBCC CHECKIDENT (Usuarios, RESEED, 0)

EXEC dbo.UsuariosInsProc
	@ModoDebug = 0, -- int
    @Nombre = N'admin', -- nvarchar(50)
    @Email = N'admin@admin.com', -- nvarchar(50)
    @Password = N'test', -- nvarchar(20)
    @UsuarioEstadoID = 2, -- int
    @UsuarioTipoID = 3, -- int
    @SolicitanteUsuarioID = 1 -- int
        
SELECT TOP 1000 [UsuarioID]
      ,[Nombre]
      ,[Email]
      ,[Password]
      ,[Salt]
      ,[UsuarioEstadoID]
      ,[UsuarioTipoID]
  FROM [DB_A11FC5_sistemabk].[dbo].[Usuarios]



-- Alta Socio (desde admin)
---------------------------
EXEC dbo.UsuariosInsProc
	@ModoDebug = 0, -- int
    @Nombre = N'Socio', -- nvarchar(50)
    @Email = N'', -- nvarchar(50)
    @Password = N'test', -- nvarchar(20)
    @UsuarioEstadoID = 2, -- int
    @UsuarioTipoID = 2, -- int
    @SolicitanteUsuarioID = 1 -- int


-- Alta Gestor (desde admin)
---------------------------
EXEC dbo.UsuariosInsProc
	@ModoDebug = 0, -- int
    @Nombre = N'Gestor', -- nvarchar(50)
    @Email = N'', -- nvarchar(50)
    @Password = N'', -- nvarchar(20)
    @UsuarioEstadoID = 2, -- int
    @UsuarioTipoID = 1, -- int
    @SolicitanteUsuarioID = 1 -- int  


-- Alta Gestor con password (desde admin)
-----------------------------------------
EXEC dbo.UsuariosInsProc
	@ModoDebug = 0, -- int
    @Nombre = N'Gestor2', -- nvarchar(50)
    @Email = N'', -- nvarchar(50)
    @Password = N'test', -- nvarchar(20)
    @UsuarioEstadoID = 2, -- int
    @UsuarioTipoID = 1, -- int
    @SolicitanteUsuarioID = 1 -- int 


-- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesUpdProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesUpdProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@ExpedienteNumeroID		BIGINT,
	@EstadoLiquidacionID	INT,
	@SolicitanteUsuarioID	INT -- ID del Solicitante
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN
	
	DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el tipo de usuario que desea realizar el cambio de estado de liquidacióo.
	SELECT @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @SolicitanteUsuarioID;

	-- Solo los administradores puede cambiar el estado de liquidacion de un expediente.
	IF (@SolicitanteUsuarioTipoID IS NOT NULL) AND (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'Solo los usuarios administradores pueden cambiar el estado de liquidacion de un expediente.!';			
			SELECT (2) AS statusDB;
			RETURN;			
		END		


-- Capturar estado actual, mandarlo a historico y recien ahi actualizar estdo actual



	-- Realiza la actualizacion del estado de liquidacion
	BEGIN TRY								
		BEGIN TRAN		
		
			-- Pasar el expediente a expedientesHistoricos para actualizar 
			-- el estado de liquidacion del expediente en la tabla expedientes
			--INSERT INTO dbo.ExpedientesHistorico
			--		(
			--		 ExpedienteNumeroID ,
			--		 NroSolicitud ,
			--		 EmpresaCuitID ,
			--		 BeneficioSolicitado ,
			--		 BeneficioAprobado ,
			--		 FechaEstado ,
			--		 ExpedienteEstadoId ,
			--		 EvaluadorID ,
			--		 EvaluadorTecnicoID ,
			--		 EstadoLiquidacionID ,
			--		 Fecha ,
			--		 FechaCambioLiquidacion)
			--SELECT e.ExpedienteNumeroID ,
			--	   e.NroSolicitud ,
			--	   e.EmpresaCuitID ,
			--	   e.BeneficioSolicitado ,
			--	   e.BeneficioAprobado ,
			--	   e.FechaEstadoActual ,
			--	   e.ExpedienteEstadoId ,
			--	   e.EvaluadorID ,
			--	   e.EvaluadorTecnicoID ,
			--	   e.EstadoLiquidacionID ,
			--	   e.Fecha ,
			--	   e.FechaCambioLiquidacion FROM dbo.Expedientes AS e
			--	   WHERE e.ExpedienteNumeroID = @ExpedienteNumeroID;
			
		
			-- Actualizar el estado de liquidacion del expediente		
			UPDATE dbo.Expedientes 
			SET EstadoLiquidacionID = @EstadoLiquidacionID,
				FechaCambioLiquidacion = CONVERT(datetime2, cast(SWITCHOFFSET(SYSDATETIMEOFFSET(), '-03:00') as datetimeoffset(7))) -- HoraArgentina en datetime2
			WHERE dbo.Expedientes.ExpedienteNumeroID = @ExpedienteNumeroID;
					
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END				
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');					
					SELECT * FROM dbo.Expedientes AS ex
						WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID;
					IF @@TRANCOUNT > 0
						BEGIN
							ROLLBACK;
							SELECT * FROM dbo.Expedientes AS ex
								WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID;
						END
				END	
					
			SELECT (0) AS statusDB; -- TODO OK
		
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;		
				
END														

/*
EXEC dbo.ExpedientesUpdProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@ExpedienteNumeroID = 2435682011, -- bigint
	@EstadoLiquidacionID = 4, -- int
	@SolicitanteUsuarioID = 1 -- int
	
select * from expedientes;	
select * from ExpedientesHistorico;
	
*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ExpedientesSelProc]
    (
      @modo INT = 0 , -- 0 trae todos | 1 trae campos específicos
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @EmpresaCuitID BIGINT = NULL ,
      @NroEmpresa NVARCHAR(5) = NULL ,
      @RazonSocial NVARCHAR(200) = NULL ,
      @ExpedienteNumeroID BIGINT = NULL ,
      @ExpedienteEstadoID INT = NULL ,
      @sinAsignacion INT = 1 , -- Estado de Liquidacion
      @pendiente INT = 2 ,		-- Estado de Liquidacion
      @aLiquidar INT = 3 ,		-- Estado de Liquidacion
      @liquidado INT = 4 ,		-- Estado de Liquidacion
      @finalizado INT = 5 ,		-- Estado de Liquidacion		
      @SolicitanteUsuarioID INT = NULL-- ID del Solicitante
	)
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
    BEGIN

	-- Existe el usuario que solicita la consulta?
        IF ( @SolicitanteUsuarioID IS NULL )
            OR ( @SolicitanteUsuarioID = '' )
            OR ( NOT EXISTS ( SELECT    us.UsuarioID
                              FROM      dbo.Usuarios us
                              WHERE     us.UsuarioID = @SolicitanteUsuarioID )
               )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'El Id del usuario que solicita el listado, no existe!';			
                SELECT  ( 3 ) AS statusDB;
                RETURN;		
            END			
		

        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el Tipo de Usuario que realiza la peticion de alta.
        SET @SolicitanteUsuarioTipoID = ( SELECT    us.UsuarioTipoID
                                          FROM      dbo.Usuarios us
                                          WHERE     us.UsuarioID = @SolicitanteUsuarioID
                                        );	
	
	/*
	
	ESTRUCTURA
	----------

	(
		SELECT
			-----
		FROM
			-----
			JOIN ..
			INNER JOIN ....
		GROUP BY
			----
	)
	UNION
	(
		SELECT
			-----
		FROM
			-----
			JOIN ..
			INNER JOIN ....
		GROUP BY
			----
	)
	ORDER BY ... (Sin Alias)

	*/	
	

	---- Obtener listado de todos los expedientes (para Administradores)
	---- ===============================================================	
	--IF ((@EmpresaCuitID IS null) 
	--AND (@NroEmpresa IS NULL OR @NroEmpresa = '')
	--AND (@RazonSocial IS NULL OR @RazonSocial = '')  
	--AND (@ExpedienteNumeroID IS null)
	--AND (@ExpedienteEstadoID IS null)
	--AND (@SolicitanteUsuarioTipoID = 3)) 
	--	BEGIN
	--		BEGIN TRY
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																							
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (el.EstadoLiquidacionID = @sinAsignacion)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID					
	--			)  
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																							
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (el.EstadoLiquidacionID = @pendiente)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID				
	--			)
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																							
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (el.EstadoLiquidacionID = @aLiquidar)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID				
	--			)				
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																							
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (el.EstadoLiquidacionID = @liquidado)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID				
	--			)	
	--			UNION
	--							(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																							
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (el.EstadoLiquidacionID = @finalizado)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID					
	--			)  
				
	--			ORDER BY RazonSocial, EstadoLiquidacionID, ExpedienteEstadoID, ExpedienteNumeroID

	--			RETURN;
	--		END TRY
							
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;					
	--	END	
	
	
	---- Obtener listado de expedientes (Para Socios)
	---- ============================================	
	--IF ((@EmpresaCuitID IS null) 
	--AND (@NroEmpresa IS NULL OR @NroEmpresa = '')
	--AND (@RazonSocial IS NULL OR @RazonSocial = '')  
	--AND (@ExpedienteNumeroID IS null)
	--AND (@ExpedienteEstadoID IS null)
	--AND (@SolicitanteUsuarioTipoID = 2)) 
	--	BEGIN
	--		BEGIN TRY			
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (ut.UsuarioTipoID < 3) AND (el.EstadoLiquidacionID = @sinAsignacion)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			)
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (ut.UsuarioTipoID < 3) AND (el.EstadoLiquidacionID = @pendiente)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			)				
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (ut.UsuarioTipoID < 3) AND (el.EstadoLiquidacionID = @aLiquidar)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			)
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (ut.UsuarioTipoID < 3) AND (el.EstadoLiquidacionID = @liquidado)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			)	
	--			UNION
	--			(
	--				SELECT
	--					--usr.UsuarioID,										
	--					--usr.UsuarioTipoID,
	--					--ut.Descripcion,
	--					--usr.Nombre AS NombreUsuario,
	--					MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--					MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--					MAX(emp.RazonSocial) AS 'RazonSocial',
	--					MAX(ex.ExpedienteNumeroID) AS 'ExpedienteNumeroID',
	--					MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--					MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--					MAX(exest.Descripcion) AS 'EstadoActual',
	--					MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--					MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--					MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--					MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--					MAX(eva.Nombre) AS 'Evaluador',
	--					MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--					MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--					MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--					MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--				FROM dbo.Expedientes ex
	--				JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--				JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--				JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--				JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--				JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--				JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--				JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
	--				WHERE (ut.UsuarioTipoID < 3) AND (el.EstadoLiquidacionID = @finalizado)
	--				GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			)									
	--			ORDER BY RazonSocial, EstadoLiquidacionID, ExpedienteEstadoID, ExpedienteNumeroID
	--			RETURN;
				
	--		END TRY
							
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;					
	--	END		
	
	---- Obtener listado de expedientes (Solo los del Gestor que solicita)
	---- =================================================================
	--IF ((@EmpresaCuitID IS null) 
	--AND (@NroEmpresa IS NULL OR @NroEmpresa = '')
	--AND (@RazonSocial IS NULL OR @RazonSocial = '')  
	--AND (@ExpedienteNumeroID IS null)
	--AND (@ExpedienteEstadoID IS null)
	--AND (@SolicitanteUsuarioTipoID = 1)) 
	--	BEGIN
	--		BEGIN TRY
	--			SELECT
	--				--usr.UsuarioID,										
	--				--usr.UsuarioTipoID,
	--				--ut.Descripcion,
	--				--usr.Nombre AS NombreUsuario,
	--				MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID',
	--			    MAX(emp.NroEmpresa) AS 'NroEmpresa',					
	--				MAX(emp.RazonSocial) AS 'RazonSocial',
	--				ex.ExpedienteNumeroID AS 'ExpedienteNumeroID',
	--				MAX(ex.NroSolicitud) AS 'NroSolicitud',
	--				MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID',
	--				MAX(exest.Descripcion) AS 'EstadoActual',
	--				MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual',
	--				MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado',
	--				MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado',
	--				MAX(eva.EvaluadorID) AS 'EvaluadorID',
	--				MAX(eva.Nombre) AS 'Evaluador',
	--				MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID',
	--				MAX(evatec.Nombre) AS 'EvaluadorTecnico',
	--				MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID',
	--				MAX(el.Descripcion) AS 'EstadoLiquidacion'																								
	--			FROM dbo.Expedientes ex
	--			JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--			JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--			JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--			JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--			--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
	--			JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
	--			JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
	--			JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
	--			JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID				
	--			WHERE usr.UsuarioID = @SolicitanteUsuarioID
	--			GROUP BY emp.RazonSocial, ex.ExpedienteNumeroID	
	--			ORDER BY emp.RazonSocial, EstadoLiquidacionID, ExpedienteEstadoID, ex.ExpedienteNumeroID
	--			RETURN;
	--		END TRY
							
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;					
	--	END
	
	-- Obtener listado de los expedientes por cuit que sean de un Gestor (TipoUsuario = 1)
	-- ====================================================================================	
        IF ( @EmpresaCuitID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID = 1 )
            BEGIN
                BEGIN TRY
                    SELECT
					--usr.UsuarioID,										
					--usr.UsuarioTipoID,
					--ut.Descripcion,
					--usr.Nombre AS NombreUsuario,
                            MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                            MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                            MAX(emp.RazonSocial) AS 'RazonSocial' ,
                            ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                            MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                            MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                            MAX(exest.Descripcion) AS 'EstadoActual' ,
                            MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                            MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                            MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                            MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                            MAX(eva.Nombre) AS 'Evaluador' ,
                            MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                            MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                            MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                            MAX(el.Descripcion) AS 'EstadoLiquidacion'
                    FROM    dbo.Expedientes ex
                            JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                            JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                            JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                            JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
				--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                            JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                            JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                            JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                            JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                    WHERE   ( emp.EmpresaCuitID = @EmpresaCuitID )
                            AND ( asign.UsuarioID = @SolicitanteUsuarioID )
                    GROUP BY ex.ExpedienteNumeroID
                    ORDER BY EstadoLiquidacionID ,
                            ExpedienteEstadoID ,
                            ex.ExpedienteNumeroID
                    RETURN;
                END TRY
							
                BEGIN CATCH
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;					
            END	
		
	-- Obtener listado de los expedientes por cuit que sean de un Socio (TipoUsuario => 2)
	-- ====================================================================================	
        IF ( @EmpresaCuitID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID = 2 )
            BEGIN
                BEGIN TRY
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @sinAsignacion )
                                AND ex.ExpedienteNumeroID IN (
                                SELECT  eu.ExpedienteNumeroID
                                FROM    dbo.ExpedientesUsuarios eu
                                        JOIN dbo.Expedientes ex ON ex.ExpedienteNumeroID = eu.ExpedienteNumeroID
                                WHERE   UsuarioID = @SolicitanteUsuarioID
                                        AND ex.EmpresaCuitID = @EmpresaCuitID )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @pendiente )
                                AND ex.ExpedienteNumeroID IN (
                                SELECT  eu.ExpedienteNumeroID
                                FROM    dbo.ExpedientesUsuarios eu
                                        JOIN dbo.Expedientes ex ON ex.ExpedienteNumeroID = eu.ExpedienteNumeroID
                                WHERE   UsuarioID = @SolicitanteUsuarioID
                                        AND ex.EmpresaCuitID = @EmpresaCuitID )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @aLiquidar )
                                AND ex.ExpedienteNumeroID IN (
                                SELECT  eu.ExpedienteNumeroID
                                FROM    dbo.ExpedientesUsuarios eu
                                        JOIN dbo.Expedientes ex ON ex.ExpedienteNumeroID = eu.ExpedienteNumeroID
                                WHERE   UsuarioID = @SolicitanteUsuarioID
                                        AND ex.EmpresaCuitID = @EmpresaCuitID )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @liquidado )
                                AND ex.ExpedienteNumeroID IN (
                                SELECT  eu.ExpedienteNumeroID
                                FROM    dbo.ExpedientesUsuarios eu
                                        JOIN dbo.Expedientes ex ON ex.ExpedienteNumeroID = eu.ExpedienteNumeroID
                                WHERE   UsuarioID = @SolicitanteUsuarioID
                                        AND ex.EmpresaCuitID = @EmpresaCuitID )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @finalizado )
                                AND ex.ExpedienteNumeroID IN (
                                SELECT  eu.ExpedienteNumeroID
                                FROM    dbo.ExpedientesUsuarios eu
                                        JOIN dbo.Expedientes ex ON ex.ExpedienteNumeroID = eu.ExpedienteNumeroID
                                WHERE   UsuarioID = @SolicitanteUsuarioID
                                        AND ex.EmpresaCuitID = @EmpresaCuitID )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    ORDER BY EstadoLiquidacionID ,
                            ExpedienteEstadoID ,
                            ExpedienteNumeroID
				
                    RETURN;
				
                END TRY
							
                BEGIN CATCH
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;					
            END	

	-- Obtener listado de los expedientes por cuit que sean de un Administrador (TipoUsuario => 3)
	-- ====================================================================================	
        IF ( @EmpresaCuitID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID = 3 )
            BEGIN
                BEGIN TRY
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @sinAsignacion )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @pendiente )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @aLiquidar )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @liquidado )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    UNION
                    ( SELECT
						--usr.UsuarioID,										
						--usr.UsuarioTipoID,
						--ut.Descripcion,
						--usr.Nombre AS NombreUsuario,
                                MAX(emp.EmpresaCuitID) AS 'EmpresaCuitID' ,
                                MAX(emp.NroEmpresa) AS 'NroEmpresa' ,
                                MAX(emp.RazonSocial) AS 'RazonSocial' ,
                                ex.ExpedienteNumeroID AS 'ExpedienteNumeroID' ,
                                MAX(ex.NroSolicitud) AS 'NroSolicitud' ,
                                MAX(exest.ExpedienteEstadoID) AS 'ExpedienteEstadoID' ,
                                MAX(exest.Descripcion) AS 'EstadoActual' ,
                                MAX(ex.FechaEstadoActual) AS 'FechaEstadoActual' ,
                                MAX(ex.BeneficioSolicitado) AS 'BeneficioSolicitado' ,
                                MAX(ex.BeneficioAprobado) AS 'BeneficioAprobado' ,
                                MAX(eva.EvaluadorID) AS 'EvaluadorID' ,
                                MAX(eva.Nombre) AS 'Evaluador' ,
                                MAX(evatec.EvaluadorID) AS 'EvaluadorTecnicoID' ,
                                MAX(evatec.Nombre) AS 'EvaluadorTecnico' ,
                                MAX(ex.EstadoLiquidacionID) AS 'EstadoLiquidacionID' ,
                                MAX(el.Descripcion) AS 'EstadoLiquidacion'
                      FROM      dbo.Expedientes ex
                                JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
                                JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
                                JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
                                JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
					--JOIN dbo.Asignaciones AS asign ON asign.EmpresaCuitID = emp.EmpresaCuitID
                                JOIN dbo.ExpedientesUsuarios AS asign ON asign.ExpedienteNumeroID = ex.ExpedienteNumeroID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                                JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = ex.EstadoLiquidacionID
                      WHERE     ( emp.EmpresaCuitID = @EmpresaCuitID )
                                AND ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( el.EstadoLiquidacionID = @finalizado )
                      GROUP BY  ex.ExpedienteNumeroID
                    )
                    ORDER BY EstadoLiquidacionID ,
                            ExpedienteEstadoID ,
                            ExpedienteNumeroID
				
                    RETURN;
				
                END TRY
							
                BEGIN CATCH
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;					
            END	


	-- TODO   (LO QUE SIGUE HAY QUE ADAPTARLO A LAS NECESIDADES DE BUSQUEDA PARTICULAR QUE PUDIERA APARECER)
	-- =====================================================================================================
		
	
	
	---- Obtener listado de todos los expedientes por nro de empresa
	---- ===========================================================
	--IF @NroEmpresa <> '' AND @NroEmpresa IS NOT NULL 
	--	BEGIN
	--		BEGIN TRY
	--			SELECT
	--				emp.NroEmpresa,
	--				ex.ExpedienteNumeroID,
	--				emp.EmpresaCuitID,
	--				emp.RazonSocial,
	--				ex.NroSolicitud,
	--				ex.BeneficioSolicitado,
	--				ex.BeneficioAprobado,
	--				exest.ExpedienteEstadoID,
	--				exest.Descripcion AS 'EstadoActual',
	--				ex.FechaEstadoActual AS 'FechaEstadoActual',
	--				eva.EvaluadorID EvaluadorID,
	--				eva.Nombre Evaluador,
	--				evatec.EvaluadorID AS EvaluadorTecnicoID,
	--				evatec.Nombre AS 'EvaluadorTecnico'
	--			FROM dbo.Expedientes ex
	--			JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--			JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--			JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--			JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--			WHERE emp.NroEmpresa = @NroEmpresa
	--			ORDER BY emp.NroEmpresa
	--			RETURN;
	--		END TRY					
			
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;				
	--	END

	---- Obtener listado de expedientes por Razon Social
	---- ===============================================
	--IF @RazonSocial <> '' AND @RazonSocial IS NOT NULL 
	--	BEGIN
	--		BEGIN TRY
	--			SELECT
	--				emp.NroEmpresa,
	--				ex.ExpedienteNumeroID,
	--				emp.EmpresaCuitID,
	--				emp.RazonSocial,
	--				ex.NroSolicitud,
	--				ex.BeneficioSolicitado,
	--				ex.BeneficioAprobado,
	--				exest.ExpedienteEstadoID,
	--				exest.Descripcion AS 'EstadoActual',
	--				ex.FechaEstadoActual AS 'FechaEstadoActual',
	--				eva.EvaluadorID EvaluadorID,
	--				eva.Nombre Evaluador,
	--				evatec.EvaluadorID AS EvaluadorTecnicoID,
	--				evatec.Nombre AS 'EvaluadorTecnico'
	--			FROM dbo.Expedientes ex
	--			JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--			JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--			JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--			JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--			WHERE emp.RazonSocial LIKE '%' + @RazonSocial + '%'
	--			ORDER BY emp.NroEmpresa
	--			RETURN;
	--		END TRY
	
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;			
	--	END
	
	---- Obtener listado de expedientes por Nro de Expediente
	---- ====================================================
	--IF @ExpedienteNumeroID IS NOT NULL 
	--	BEGIN
	--		BEGIN TRY				
	--			SELECT
	--				emp.NroEmpresa,
	--				ex.ExpedienteNumeroID,
	--				emp.EmpresaCuitID,
	--				emp.RazonSocial,
	--				ex.NroSolicitud,
	--				ex.BeneficioSolicitado,
	--				ex.BeneficioAprobado,
	--				exest.ExpedienteEstadoID,
	--				exest.Descripcion AS 'EstadoActual',
	--				ex.FechaEstadoActual AS 'FechaEstadoActual',
	--				eva.EvaluadorID EvaluadorID,
	--				eva.Nombre Evaluador,
	--				evatec.EvaluadorID AS EvaluadorTecnicoID,
	--				evatec.Nombre AS 'EvaluadorTecnico'
	--			FROM dbo.Expedientes ex
	--			JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--			JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--			JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--			JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--			WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID
	--			ORDER BY emp.NroEmpresa
	--			RETURN;
	--		END TRY
			
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;		
	--	END
    
	---- Obtener listado de Expedientes por estadoId
	---- ===========================================
	--IF @ExpedienteEstadoID IS NOT NULL 
	--	BEGIN
	--		BEGIN TRY
	--			SELECT
	--				emp.NroEmpresa,
	--				ex.ExpedienteNumeroID,
	--				emp.EmpresaCuitID,
	--				emp.RazonSocial,
	--				ex.NroSolicitud,
	--				ex.BeneficioSolicitado,
	--				ex.BeneficioAprobado,
	--				exest.ExpedienteEstadoID,
	--				exest.Descripcion AS 'EstadoActual',
	--				ex.FechaEstadoActual AS 'FechaEstadoActual',
	--				eva.EvaluadorID EvaluadorID,
	--				eva.Nombre Evaluador,
	--				evatec.EvaluadorID AS EvaluadorTecnicoID,
	--				evatec.Nombre AS 'EvaluadorTecnico'
	--			FROM dbo.Expedientes ex
	--			JOIN dbo.ExpedienteEstados exest ON exest.ExpedienteEstadoID = ex.ExpedienteEstadoId
	--			JOIN dbo.Empresas emp ON emp.EmpresaCuitID = ex.EmpresaCuitID
	--			JOIN dbo.Evaluadores eva ON eva.EvaluadorID = ex.EvaluadorID
	--			JOIN dbo.Evaluadores evatec ON evatec.EvaluadorID = ex.EvaluadorTecnicoID
	--			WHERE ex.ExpedienteEstadoId = @ExpedienteEstadoID
	--			ORDER BY emp.NroEmpresa
	--			RETURN;			
	--		END TRY
			
	--		BEGIN CATCH
	--			SELECT (99) AS statusDB; -- ERROR					
	--			SELECT 
	--				ERROR_NUMBER() AS ErrorNumber
	--				,ERROR_SEVERITY() AS ErrorSeverity
	--				,ERROR_STATE() AS ErrorState
	--				,ERROR_PROCEDURE() AS ErrorProcedure
	--				,ERROR_LINE() AS ErrorLine
	--				,ERROR_MESSAGE() AS ErrorMessage;
	--		END CATCH;	 
	--	END	
	
	
	-- Este tramo debe ser el ultimo del procedure y solo se ejecutara si no entro en ningun if
	-- ya que todos tienen un return que sale del proc sin llegar a esta parte
        IF ( @ModoDebug = 1 )
            PRINT 'No se ingresaron parametros!';			
        SELECT  ( 2 ) AS statusDB;
        RETURN;		
	
   
    END;


/*
EXEC dbo.ExpedientesSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 3
EXEC dbo.ExpedientesSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 2, @EmpresaCuitID = 30708070722, @liquidado= 0, @finalizado = 0
EXEC dbo.ExpedientesSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 1, @sinAsignacion = 0, @pendiente = 2, @aLiquidar = 0, @liquidado = 0


EXEC dbo.ExpedientesSelProc @EmpresaCuitID = 30708070722 , @SolicitanteUsuarioID = 3

EXEC dbo.ExpedientesSelProc @NroEmpresa = N'R0010'

EXEC dbo.ExpedientesSelProc @RazonSocial = N'ruben'

EXEC dbo.ExpedientesSelProc @ExpedienteEstadoID = 102

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@ExpedienteNumeroID		BIGINT,
	@NroSolicitud			INT,
	@EmpresaCuitID			BIGINT,
	@BeneficioSolicitado	MONEY,
	@BeneficioAprobado		MONEY,
	@FechaEstadoActual		DATE =  NULL,
	@ExpedienteEstado		NVARCHAR(100),
	@Evaluador				NVARCHAR(100),
	@EvaluadorTecnico		NVARCHAR(100)
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
BEGIN

	DECLARE	@EvaluadorID INT = NULL;
	DECLARE @EvaluadorTecnicoID INT = NULL;
	DECLARE @ExpedienteEstadoID INT = NULL;
	
	-- para los expedientes que entran nuevos de una empresa que ya tiene asignaciones
	DECLARE	@CantAsignaciones INT;
	DECLARE @EstadoLiquidacion INT;
	SET @EstadoLiquidacion = 1;
		
	-- Variables Utilizadas para capturar en el update el estado actual 
	-- de cada fila del expediente que se quiere actualizar y comparar
	-- Si cambió algo.
	DECLARE @ExpedienteNumeroID_Actual BIGINT;
	DECLARE @NroSolicitud_Actual INT;
	DECLARE @EmpresaCuitID_Actual BIGINT;
	DECLARE @BeneficioSolicitado_Actual MONEY;
	DECLARE @BeneficioAprobado_Actual MONEY;
	DECLARE @FechaEstadoActual_Actual DATE;
	DECLARE @ExpedienteEstadoId_Actual INT;
	DECLARE @EvaluadorID_Actual INT;
	DECLARE @EvaluadorTecnicoID_Actual INT;
	DECLARE @EstadoLiquidacionID_Actual INT;
	DECLARE @Fecha_Actual DATETIME2;
	DECLARE @FechaCambioLiquidacion_Actual DATETIME2;
	SET @FechaCambioLiquidacion_Actual = NULL;	 -- Por default pone en null la FechaCambioLiquidacion
	
	DECLARE @ActualizarDatos INT = 0;

	-- Si ya existen asignaciones para esta empresa, pasa el estado de Liquidacion del expediente a Pendiente y
	-- guarda la fecha en que se cambio el estado de liquidacion.
	SET @CantAsignaciones = (SELECT COUNT(*) FROM dbo.Asignaciones WHERE EmpresaCuitID = @EmpresaCuitID);
	IF @CantAsignaciones > 0
	BEGIN
		SET @EstadoLiquidacion = 2;
		SET @FechaCambioLiquidacion_Actual = convert(datetime2, cast(SWITCHOFFSET(SYSDATETIMEOFFSET(), '-03:00') as datetimeoffset(7))) -- HoraArgentina en datetime2
	END    

	-- Obtiene El ID del Evaluador por su nombre
	SELECT @EvaluadorID = Evaluadores.EvaluadorID 
	FROM dbo.Evaluadores 
	WHERE Evaluadores.Nombre = @Evaluador;
	
	-- Obtiene El ID del Evaluador Ténico por su nombre
	SELECT @evaluadorTecnicoID = dbo.Evaluadores.EvaluadorID 
	FROM dbo.Evaluadores 
	WHERE dbo.Evaluadores.Nombre = @EvaluadorTecnico;

	-- Obtiene El ID del Estado de Expediente por el nombre del estado.
	SELECT @ExpedienteEstadoID = ExpedienteEstados.ExpedienteEstadoID
	FROM dbo.ExpedienteEstados
	WHERE ExpedienteEstados.Descripcion = @ExpedienteEstado;

	-- Si el expediente no existe y obtuvimos los IDs de los evaluadores, el ID del Estado del expediente, y la empresa existe, lo insertamos.
	IF (
	   (NOT EXISTS (SELECT ex.ExpedienteNumeroID FROM dbo.Expedientes ex WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID)) AND
	   (@EvaluadorID IS NOT NULL) AND (@EvaluadorTecnicoID IS NOT NULL) AND (@EvaluadorTecnicoID IS NOT NULL) AND
	   (EXISTS (SELECT emp.EmpresaCuitID FROM dbo.Empresas emp WHERE emp.EmpresaCuitID = @EmpresaCuitID))
	   )
		BEGIN
			-- Realiza el alta
			BEGIN TRY						
				BEGIN TRAN	

					INSERT INTO dbo.Expedientes
							( ExpedienteNumeroID ,
							  NroSolicitud ,
							  EmpresaCuitID ,
							  BeneficioSolicitado ,
							  BeneficioAprobado ,
							  FechaEstadoActual ,
							  ExpedienteEstadoId ,
							  EvaluadorID ,
							  EvaluadorTecnicoID ,
							  EstadoLiquidacionID,
							  Fecha,
							  FechaCambioLiquidacion
							)
						VALUES
							( @ExpedienteNumeroID , -- ExpedienteNumeroID - bigint
							  @NroSolicitud , -- NroSolicitud - int
							  @EmpresaCuitID , -- EmpresaCuitID - bigint
							  @BeneficioSolicitado , -- BeneficioSolicitado - money
							  @BeneficioAprobado , -- BeneficioAprobado - money
							  @FechaEstadoActual , -- FechaEstadoActual - date
							  @ExpedienteEstadoID , -- ExpedienteEstadoId - int
							  @EvaluadorID , -- EvaluadorID - int
							  @EvaluadorTecnicoID ,  -- EvaluadorTecnicoID - int
							  @EstadoLiquidacion,
							  convert(datetime2, cast(SWITCHOFFSET(SYSDATETIMEOFFSET(), '-03:00') as datetimeoffset(7))), -- HoraArgentina en datetime2
							  @FechaCambioLiquidacion_Actual
							)
							
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END	
									
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
							SELECT * FROM dbo.Empresas
							WHERE dbo.Empresas.EmpresaCuitID = @EmpresaCuitID;
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK		
					RETURN;			
			END TRY	
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;	
					
		END

	ELSE	-- El expediente ya existe y debemos actualizar su estado, copiando el estado actual al historico.
		BEGIN  
						
			-- Obtiene el estado actual del expediente
			SELECT
				@ExpedienteNumeroID_Actual = ex.ExpedienteNumeroID,
				@NroSolicitud_Actual = ex.NroSolicitud,
				@EmpresaCuitID_Actual = ex.EmpresaCuitID,
				@BeneficioSolicitado_Actual = ex.BeneficioSolicitado,
				@BeneficioAprobado_Actual = ex.BeneficioAprobado,
				@FechaEstadoActual_Actual = ex.FechaEstadoActual,
				@ExpedienteEstadoId_Actual = ex.ExpedienteEstadoId,
				@EvaluadorID_Actual = ex.EvaluadorID,
				@EvaluadorTecnicoID_Actual = ex.EvaluadorTecnicoID,				
				@EstadoLiquidacionID_Actual = EstadoLiquidacionID,
				@Fecha_Actual = ex.Fecha,
				@FechaCambioLiquidacion_Actual = ex.FechaCambioLiquidacion		
			FROM dbo.Expedientes ex
			WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID;


			-- Cambio algo??????
			IF  (@BeneficioSolicitado_Actual <> @BeneficioSolicitado) OR
				(@BeneficioAprobado_Actual <> @BeneficioAprobado) OR
				(@FechaEstadoActual_Actual <> @FechaEstadoActual) OR
				(@ExpedienteEstadoId_Actual <> @ExpedienteEstadoID) OR
				(@EvaluadorID_Actual <> @EvaluadorID) OR
				(@EvaluadorTecnicoID_Actual <> @evaluadorTecnicoID)								
				BEGIN	
					SET @ActualizarDatos = 1;
				END				
		END

	IF (@ActualizarDatos = 0)	-- No cambio nada. No hace nada!
		BEGIN			
			IF (@ModoDebug = 1)
				BEGIN
					PRINT ('No es necesario actualizar el registro ya que los datos son coincidentes!');
				END	
			SELECT (1) AS statusDB; -- Registro existente y sin cambios
		END			
	
	ELSE			-- Si existe algun cambio, actualizamos los datos.
		BEGIN
			BEGIN TRY								
				-- Pasar el expediente actual al historico para actualizar su estado	
				BEGIN TRAN	

					IF (@ModoDebug = 1)
						BEGIN
							PRINT ('Se Produjo un cambio en el expediente. Se procede a enviar el registro actual al historico y actualizar el registro actual.');
						END
						
					---- Pasar los datos del expediente al historial
					--INSERT dbo.ExpedientesHistorico
					--		( ExpedienteNumeroID ,
					--		  NroSolicitud ,
					--		  EmpresaCuitID ,
					--		  BeneficioSolicitado ,
					--		  BeneficioAprobado ,
					--		  FechaEstado ,
					--		  ExpedienteEstadoId ,
					--		  EvaluadorID ,
					--		  EvaluadorTecnicoID,
					--		  EstadoLiquidacionID,
					--		  Fecha,
					--		  FechaCambioLiquidacion
					--		)
					--SELECT ex.ExpedienteNumeroID,
					--		ex.NroSolicitud,
					--		ex.EmpresaCuitID,
					--		ex.BeneficioSolicitado,
					--		ex.BeneficioAprobado,
					--		ex.FechaEstadoActual,
					--		ex.ExpedienteEstadoId,
					--		ex.EvaluadorID,
					--		ex.EvaluadorTecnicoID,
					--		ex.EstadoLiquidacionID,
					--		ex.Fecha,
					--		ex.FechaCambioLiquidacion
					--FROM	dbo.Expedientes ex
					--WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID
					
					-- Actualizar el estado del expediente
					UPDATE dbo.Expedientes
						SET ExpedienteNumeroID = @ExpedienteNumeroID,
							NroSolicitud = @NroSolicitud,
							EmpresaCuitID = @EmpresaCuitID,
							BeneficioSolicitado = @BeneficioSolicitado,
							BeneficioAprobado = @BeneficioAprobado,
							FechaEstadoActual = @FechaEstadoActual,
							ExpedienteEstadoId = @ExpedienteEstadoID,
							EvaluadorID = @EvaluadorID,
							EvaluadorTecnicoID = @EvaluadorTecnicoID,
							Fecha = CONVERT(datetime2, cast(SWITCHOFFSET(SYSDATETIMEOFFSET(), '-03:00') as datetimeoffset(7))), -- HoraArgentina en datetime2							
							FechaCambioLiquidacion = @FechaCambioLiquidacion_Actual							
					WHERE ExpedienteNumeroID = @ExpedienteNumeroID																
					
					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END											
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
							SELECT * FROM dbo.ExpedientesHistorico ex WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID
							SELECT * FROM dbo.Expedientes ex WHERE ex.ExpedienteNumeroID = @ExpedienteNumeroID
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK																			

			END TRY	

			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;
														
		END

END
/*

delete from expedientes;
delete from ExpedientesHistorico;

EXEC dbo.EmpresasInsProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@EmpresaCuitID = 30569766253, -- bigint
	@NroEmpresa = N'1234', -- nvarchar(5)
	@RazonSocial = N'test' -- nvarchar(200)
	
EXEC dbo.AsignacionesInsProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@EmpresaCuitID = 30569766253, -- bigint
	@UsuarioID = 1, -- int
	@Porcentaje = 10, -- decimal(5, 2)
	@SolicitanteUsuarioID = 1 -- int	
	
EXEC dbo.EvaluadoresInsProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@Nombre = N'Esposito, Maria Laura' -- nvarchar(100)
	
EXEC dbo.EvaluadoresInsProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@Nombre = N'Ugarte, Daniel Alfredo' -- nvarchar(100)		

EXEC dbo.ExpedientesInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @ExpedienteNumeroID = '2435682011', -- bigint
    @NroSolicitud = '201119589', -- int
    @EmpresaCuitID = 30569766253, -- bigint
    @BeneficioSolicitado = '100', -- money
    @BeneficioAprobado = '100', -- money
    @FechaEstadoActual = '2014/10/20 00:00:00', -- date
    @ExpedienteEstado = 'TECNICA', -- int
    @Evaluador = 'Esposito, Maria Laura', -- int
    @EvaluadorTecnico = 'Ugarte, Daniel Alfredo' -- int

EXEC dbo.ExpedientesInsProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @ExpedienteNumeroID = '2435682011', -- bigint
    @NroSolicitud = '201119589', -- int
    @EmpresaCuitID = 30569766253, -- bigint
    @BeneficioSolicitado = '100', -- money
    @BeneficioAprobado = '100', -- money
    @FechaEstadoActual = '2014/10/21 00:00:00', -- date
    @ExpedienteEstado = 'BONO IMPRESO', -- int
    @Evaluador = 'Esposito, Maria Laura', -- int
    @EvaluadorTecnico = 'Ugarte, Daniel Alfredo' -- int

SELECT * from expedientes;
SELECT * FROM dbo.ExpedientesHistorico;


*/
GO
/****** Object:  StoredProcedure [dbo].[ReporteAsignacionesEmpresaSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ReporteAsignacionesEmpresaSelProc]
    (
      @modo INT = 0 , -- 0 trae todos | 1 trae campos específicos
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @EmpresaCuitID BIGINT = NULL ,
      @NroEmpresa NVARCHAR(5) = NULL ,
      @RazonSocial NVARCHAR(200) = NULL ,
      @sinAsignacion INT = 1 , -- Estado de Liquidacion
      @pendiente INT = 2 ,		-- Estado de Liquidacion
      @aLiquidar INT = 3 ,		-- Estado de Liquidacion
      @liquidado INT = 4 ,		-- Estado de Liquidacion
      @finalizado INT = 5 ,		-- Estado de Liquidacion	
      @gestor INT = 1,
      @socio INT = 2,
      @administrador INT = 3,     	
      @SolicitanteUsuarioID INT = NULL-- ID del Solicitante
	)
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados
    BEGIN


	-- Existe el usuario que solicita la consulta?
        IF ( @SolicitanteUsuarioID IS NULL )
            OR ( @SolicitanteUsuarioID = '' )
            OR ( NOT EXISTS ( SELECT    us.UsuarioID
                              FROM      dbo.Usuarios us
                              WHERE     us.UsuarioID = @SolicitanteUsuarioID )
               )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'El Id del usuario que solicita el listado, no existe!';			
                SELECT  ( 3 ) AS statusDB;
                RETURN;		
            END			
		

        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el Tipo de Usuario que realiza la peticion del Reporte.
    SET @SolicitanteUsuarioTipoID = ( SELECT    us.UsuarioTipoID
                                      FROM      dbo.Usuarios us
                                      WHERE     us.UsuarioID = @SolicitanteUsuarioID
                                    );	
                                        
    -- Si el tipo de usuario que solicita el reporte no es del tipo Administrador, sale del procedure sin hacer nada
	IF (@SolicitanteUsuarioTipoID <> 3)
		BEGIN
            IF ( @ModoDebug = 1 )
                PRINT 'Sólo los administardores pueden emitir reportes!';			
            SELECT  ( 4 ) AS statusDB;
            RETURN;			
		END                                        
	

	-- Si no se recibe el cuit, listar las empresas con asignaciones para armar el drop down para que
	-- el usuario seleccione la empresa o empresas. 
	IF (@EmpresaCuitID IS NULL)
		BEGIN
			SELECT DISTINCT
				emp.EmpresaCuitID,
				emp.NroEmpresa,
				emp.RazonSocial
			FROM dbo.ExpedientesUsuarios AS eu	
			JOIN dbo.Expedientes AS exped ON exped.ExpedienteNumeroID = eu.ExpedienteNumeroID
			JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = exped.EmpresaCuitID		
			ORDER BY emp.RazonSocial		
			RETURN;	
		END	
	

	-- Reporte de una Empresa y sus Expedientes ordenado por tipo de usuario segun filtro
	-- ==================================================================================
	IF ((@EmpresaCuitID IS NOT NULL) 
	AND (@SolicitanteUsuarioTipoID = 3)) 
		BEGIN
			BEGIN TRY	
						
					SELECT 
                           -- emp.EmpresaCuitID ,
                           -- emp.NroEmpresa,
                           -- emp.RazonSocial,
                           expe.ExpedienteNumeroID,
                           expe.NroSolicitud,
                           expe.ExpedienteEstadoId,
                           ee.Descripcion,					
                           expe.FechaEstadoActual ,
                           expe.BeneficioSolicitado ,
                           expe.BeneficioAprobado ,
                           expeus.Porcentaje ,                           
                           expe.EstadoLiquidacionID,
                           el.Descripcion,
                           usr.UsuarioID ,
                           usr.UsuarioTipoID,
                           usr.Nombre                           
					FROM dbo.ExpedientesUsuarios expeus
					JOIN dbo.Expedientes expe ON expe.ExpedienteNumeroID = expeus.ExpedienteNumeroID
					JOIN dbo.Usuarios AS usr ON usr.UsuarioID = expeus.UsuarioID	
					JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
					JOIN dbo.ExpedienteEstados AS ee ON ee.ExpedienteEstadoID = expe.ExpedienteEstadoId	
					JOIN dbo.EstadosLiquidacion AS el ON el.EstadoLiquidacionID = expe.EstadoLiquidacionID			
					WHERE (expe.EstadoLiquidacionID IN (@sinAsignacion, @pendiente, @aLiquidar, @liquidado, @finalizado))
					      AND (usr.UsuarioTipoID IN (@gestor, @socio, @administrador))
						  AND (expe.EmpresaCuitID = @EmpresaCuitID)
					ORDER BY usr.UsuarioTipoID, usr.Nombre, ee.ExpedienteEstadoID, expe.ExpedienteNumeroID
				RETURN;
			END TRY
							
			BEGIN CATCH
				SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;
			RETURN;						
		END	


	
	
	-- Este tramo debe ser el ultimo del procedure y solo se ejecutara si no entro en ningun if
	-- ya que todos tienen un return que sale del proc sin llegar a esta parte
        IF ( @ModoDebug = 1 )
            PRINT 'No se ingresaron parametros!';			
        SELECT  ( 2 ) AS statusDB;
        RETURN;	
   
    END;

/*

Nivel 1 (Lista Empresas con Asignaciones)
-----------------------------------------
EXEC dbo.ReporteAsignacionesEmpresaSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 1 


Nivel 2
-------
EXEC dbo.ReporteAsignacionesEmpresaSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 1, @EmpresaCuitID = 30550248685


Nivel 2 (Filtrando Estados de Liquidacion)
-------------------------------------------
EXEC dbo.ReporteAsignacionesEmpresaSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 1, @EmpresaCuitID = 30550248685, @sinAsignacion = 1, @pendiente = 2, @aLiquidar = 3, @liquidado = 4, @finalizado = 5


Nivel 2 (Filtrando Tipo de Usuario)
-----------------------------------
EXEC dbo.ReporteAsignacionesEmpresaSelProc @ModoDebug = 1, @SolicitanteUsuarioID = 1, @EmpresaCuitID = 30550248685, @gestor = 0, @socio = 0, @administrador = 3

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesUsuariosUpdProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesUsuariosUpdProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @ExpedienteNro BIGINT ,
      @UsuarioID INT ,  -- ID del usuario al que queremos actualizarle el porcentaje
      @Porcentaje DECIMAL(5, 2) ,
      @SolicitanteUsuarioID INT -- ID del Solicitante
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN

        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
        DECLARE @UsuarioTipoID INT = NULL;

	-- Obtiene el tipo de usuario que desea realizar la operacion de alta.
        SELECT  @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @SolicitanteUsuarioID;
	
	-- Obtiene el tipo de usuario al que hay que cambiarle el porcentaje.
        SELECT  @UsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @UsuarioID;	

	-- Solo los administradores pueden Actualizar asignaciones
        IF ( @SolicitanteUsuarioTipoID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID < 3 )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'Solo los usuarios administradores pueden actualizar asignaciones!';			
                SELECT  ( 2 ) AS statusDB;
                RETURN;			
            END	

	-- Existe la Asignacion que queremos actualizar?
        IF ( NOT EXISTS ( SELECT    asign.ExpedienteUsuarioID
                          FROM      dbo.ExpedientesUsuarios AS asign
                          WHERE     ( asign.ExpedienteNumeroID = @ExpedienteNro )
                                    AND ( asign.UsuarioID = @UsuarioID ) )
           )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'El expediente o el usuario no existen en la BD!!';			
                SELECT  ( 3 ) AS statusDB;
                RETURN;			
            END			
	
	
	-- Realiza el Update
        BEGIN TRY						
            BEGIN TRAN
						
            UPDATE  dbo.ExpedientesUsuarios
            SET     Porcentaje = @Porcentaje,
					FechaUltimaModificacion = GETDATE()
            WHERE   (ExpedienteNumeroID = @ExpedienteNro )
                    AND ( UsuarioID = @UsuarioID )						
					
            IF ( @ModoDebug = 0 )
                BEGIN
                    COMMIT;
                END	
							
            ELSE
                BEGIN
                    PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                    SELECT  asign.ExpedienteUsuarioID ,
                            asign.ExpedienteNumeroID ,
                            asign.UsuarioID ,
                            asign.Porcentaje ,
                            usr.UsuarioID ,
                            usr.Nombre ,
                            usr.UsuarioTipoID
                    FROM    dbo.ExpedientesUsuarios AS asign
                            JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                    WHERE   ( asign.ExpedienteNumeroID = @ExpedienteNro )
                            AND ( usr.UsuarioID = @UsuarioID )
                    IF @@TRANCOUNT > 0
                        ROLLBACK;
                END								
            SELECT  ( 0 ) AS statusDB; -- TODO OK				
        END TRY	
	
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;
            SELECT  ( 99 ) AS statusDB; -- ERROR					
            SELECT  ERROR_NUMBER() AS ErrorNumber ,
                    ERROR_SEVERITY() AS ErrorSeverity ,
                    ERROR_STATE() AS ErrorState ,
                    ERROR_PROCEDURE() AS ErrorProcedure ,
                    ERROR_LINE() AS ErrorLine ,
                    ERROR_MESSAGE() AS ErrorMessage;
        END CATCH;	
	
    END

/*

EXEC dbo.ExpedientesUsuariosUpdProc
    @ExpedienteNro = 3292016, -- bigint
    @UsuarioID = 2, -- int
    @Porcentaje = 1.44, -- decimal
    @SolicitanteUsuarioID = 1 -- int   
    

EXEC dbo.ExpedientesUsuariosUpdProc
    @ExpedienteNro = 3292016, -- bigint
    @UsuarioID = 2, -- int
    @Porcentaje = 1.27, -- decimal
    @SolicitanteUsuarioID = 1 -- int   

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesUsuariosSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesUsuariosSelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @UsuarioSolicitanteID INT ,
      @ExpedienteNumeroID BIGINT = NULL
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN
        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
        DECLARE @semaforoSocioAsignado INT = 0;
	
	-- Obtiene el tipo de usuario que desea realizar la consulta
        SELECT  @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @UsuarioSolicitanteID;
	
	
	-- Si no le envío parámetros, mostrar 1 vez el nombre de cada empresa asignada 
	-- Las demás columnas ponerlas en cero.
	
        IF ( @ExpedienteNumeroID IS NULL )
            BEGIN
			-- Si el usuario es tipo 1 (Gestor)... lista sólo sus propias asignaciones)
                IF @SolicitanteUsuarioTipoID = 1
                    BEGIN
                        SELECT DISTINCT
                                emp.EmpresaCuitID ,
                                emp.NroEmpresa ,
                                emp.RazonSocial
                        FROM    dbo.ExpedientesUsuarios AS asign
                                JOIN dbo.Expedientes AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
                                JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                        WHERE   usr.UsuarioID = @UsuarioSolicitanteID
                        ORDER BY 3		
                    END
                ELSE
                    IF @SolicitanteUsuarioTipoID = 2	-- Es Socio
                        BEGIN				
                            SELECT DISTINCT
                                    emp.EmpresaCuitID ,
                                    emp.NroEmpresa ,
                                    emp.RazonSocial
                            FROM    dbo.ExpedientesUsuarios AS asign
                                    JOIN dbo.Expedientes AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
                                    JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                    JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                    JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                            WHERE   usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID
                                    AND emp.EmpresaCuitID IN (
                                    SELECT  expe.EmpresaCuitID
                                    FROM    dbo.ExpedientesUsuarios expeus
                                            JOIN dbo.Expedientes expe ON expe.ExpedienteNumeroID = expeus.ExpedienteNumeroID
                                    WHERE   UsuarioID = @UsuarioSolicitanteID )
                            ORDER BY 3
                        END		
                    ELSE -- Es Administrador
                        BEGIN				
                            SELECT DISTINCT
                                    emp.EmpresaCuitID ,
                                    emp.NroEmpresa ,
                                    emp.RazonSocial
                            FROM    dbo.ExpedientesUsuarios AS asign
                                    JOIN dbo.Expedientes AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
                                    JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                    JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                    JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
                            WHERE   usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID
                            ORDER BY 3
                        END	
            END
		
        ELSE	-- Si le paso parámetros (CUIT) debe traer el select filtrado por ese cuit.
            BEGIN
			-- Si el usuario es tipo 1 (Gestor)... lista sólo sus propias asignaciones)
                IF @SolicitanteUsuarioTipoID = 1
                    BEGIN
                        SELECT  expe.ExpedienteNumeroID ,
                                usr.UsuarioTipoID ,
                                usrtipo.Descripcion ,
                                usr.UsuarioID ,
                                usr.Nombre ,
                                asign.Porcentaje
                        FROM    dbo.ExpedientesUsuarios AS asign
                                JOIN dbo.Expedientes AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
								--JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS usrtipo ON usrtipo.UsuarioTipoID = usr.UsuarioTipoID
                        WHERE   ( usr.UsuarioID = @UsuarioSolicitanteID )
                                AND ( expe.ExpedienteNumeroID = @ExpedienteNumeroID )
                        ORDER BY 1	
                    END
            
                IF @SolicitanteUsuarioTipoID = 2	-- es Socio
                    BEGIN	
                        SET @semaforoSocioAsignado = ( SELECT COUNT(*)
                                                       FROM   dbo.ExpedientesUsuarios
                                                       WHERE  UsuarioID = @UsuarioSolicitanteID
                                                              AND ExpedienteNumeroID = @ExpedienteNumeroID
                                                     )
					
						IF @semaforoSocioAsignado > 0
						BEGIN
                                                SELECT  expe.ExpedienteNumeroID ,
                                                        usr.UsuarioTipoID ,
                                                        usrtipo.Descripcion ,
                                                        usr.UsuarioID ,
                                                        usr.Nombre ,
                                                        asign.Porcentaje
                                                FROM    dbo.ExpedientesUsuarios
                                                        AS asign
                                                        JOIN dbo.Expedientes
                                                        AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
								--JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                                        JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                                        JOIN dbo.UsuariosTipos
                                                        AS usrtipo ON usrtipo.UsuarioTipoID = usr.UsuarioTipoID
                                                WHERE   ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                                        AND ( expe.ExpedienteNumeroID = @ExpedienteNumeroID )
                                                        AND asign.UsuarioID IN (
                                                        SELECT
                                                              UsuarioID
                                                        FROM  dbo.ExpedientesUsuarios
                                                        WHERE ExpedienteNumeroID = @ExpedienteNumeroID )
                                                ORDER BY 1
						END
								

                    END	

                IF @SolicitanteUsuarioTipoID = 3 -- es Administrador
                    BEGIN				
                        SELECT  expe.ExpedienteNumeroID ,
                                usr.UsuarioTipoID ,
                                usrtipo.Descripcion ,
                                usr.UsuarioID ,
                                usr.Nombre ,
                                asign.Porcentaje
                        FROM    dbo.ExpedientesUsuarios AS asign
                                JOIN dbo.Expedientes AS expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
								--JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = expe.EmpresaCuitID
                                JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
                                JOIN dbo.UsuariosTipos AS usrtipo ON usrtipo.UsuarioTipoID = usr.UsuarioTipoID
                        WHERE   ( usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID )
                                AND ( expe.ExpedienteNumeroID = @ExpedienteNumeroID )
                        ORDER BY 1
                    END	

            END
    END

/*

	EXEC dbo.ExpedientesUsuariosSelProc
	@UsuarioSolicitanteID = 2, -- int
	@ExpedienteNumeroID = 2597392009 -- bigint
	
	  
	select * from expedientesUsuarios       
    order by 3
*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesUsuariosInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesUsuariosInsProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @EmpresaCuitID BIGINT = 0 ,
      @ExpedienteNroID BIGINT = 0 ,
      @UsuarioID INT ,
      @Porcentaje DECIMAL(5, 2) = 0 ,
      @SolicitanteUsuarioID INT -- ID del Solicitante
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN

        DECLARE @UsuarioTipoID INT;
        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el tipo de usuario que desea realizar la operacion de alta.
        SELECT  @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @SolicitanteUsuarioID;

	-- Solo los administradores puede dar de alta asignaciones
        IF ( @SolicitanteUsuarioTipoID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID < 3 )
            BEGIN
                IF ( @ModoDebug = 1 )
                    PRINT 'Solo los usuarios administradores pueden asignar empresas a usuarios!';			
                SELECT  ( 2 ) AS statusDB;
                RETURN;			
            END	
	


		-- Alta por Empresa
        IF ( @EmpresaCuitID > 0 )
            BEGIN

	-- Existe en la base de datos la empresa y el usuario que queremos asignar?
                IF ( ( NOT EXISTS ( SELECT  emp.EmpresaCuitID
                                    FROM    dbo.Empresas emp
                                    WHERE   emp.EmpresaCuitID = @EmpresaCuitID )
                     )
                     OR ( NOT EXISTS ( SELECT   usr.UsuarioID
                                       FROM     dbo.Usuarios usr
                                       WHERE    usr.UsuarioID = @UsuarioID )
                        )
                   )
                    BEGIN
                        IF ( @ModoDebug = 1 )
                            PRINT 'La empresa o el usuario no existen en la BD!!';			
                        SELECT  ( 3 ) AS statusDB;
                        RETURN;			
                    END
		
	-- Si no existe la asignacion, Realiza el alta
                IF ( (NOT EXISTS ( SELECT   expe.EmpresaCuitID
                                   FROM     dbo.ExpedientesUsuarios expeus
                                            JOIN dbo.Expedientes expe ON expe.ExpedienteNumeroID = expeus.ExpedienteNumeroID
                                   WHERE    expe.EmpresaCuitID = @EmpresaCuitID
                                            AND expeus.UsuarioID = @UsuarioID ))
                   )
                    BEGIN
                        BEGIN TRY						
                            BEGIN TRAN	
                    
                            INSERT  INTO dbo.ExpedientesUsuarios
                                    ( UsuarioID ,
                                      ExpedienteNumeroID ,
                                      Porcentaje ,
                                      FechaUltimaModificacion
                                    )
                                    ( SELECT    @UsuarioID ,
                                                ExpedienteNumeroID ,
                                                @Porcentaje ,
                                                GETDATE()
                                      FROM      dbo.Expedientes
                                      WHERE     EmpresaCuitID = @EmpresaCuitID
                                    )
				
					-- cambia todos los expedientes que estén sin asignación a estado de liquidación Pendiente para ese EmpresaCuitID
                            UPDATE  dbo.Expedientes
                            SET     EstadoLiquidacionID = 2, FechaCambioLiquidacion = GETDATE()
                            WHERE   EstadoLiquidacionID = 1
                                    AND EmpresaCuitID = @EmpresaCuitID		
					
					-- cambia todos los expedientes que estén en estado archivado a estado de liquidación Finalizado para ese EmpresaCuitID
                            UPDATE  dbo.Expedientes
                            SET     EstadoLiquidacionID = 5
                            WHERE   ExpedienteEstadoId = 18
                                    AND EmpresaCuitID = @EmpresaCuitID

                            IF ( @ModoDebug = 0 )
                                BEGIN
                                    COMMIT;
                                END	
									
                            ELSE
                                BEGIN
                                    PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                                    SELECT  *
                                    FROM    dbo.ExpedientesUsuarios asign
                                            JOIN dbo.Expedientes expe ON expe.ExpedienteNumeroID = asign.ExpedienteNumeroID
                                    WHERE   expe.EmpresaCuitID = @EmpresaCuitID
                                            AND asign.UsuarioID = @UsuarioID;

                                    IF @@TRANCOUNT > 0
                                        ROLLBACK;
                                END								
                            SELECT  ( 0 ) AS statusDB; -- TODO OK				
                        END TRY	
			
                        BEGIN CATCH
                            IF @@TRANCOUNT > 0
                                ROLLBACK TRANSACTION;
                            SELECT  ( 99 ) AS statusDB; -- ERROR					
                            SELECT  ERROR_NUMBER() AS ErrorNumber ,
                                    ERROR_SEVERITY() AS ErrorSeverity ,
                                    ERROR_STATE() AS ErrorState ,
                                    ERROR_PROCEDURE() AS ErrorProcedure ,
                                    ERROR_LINE() AS ErrorLine ,
                                    ERROR_MESSAGE() AS ErrorMessage;
                        END CATCH;		
                    END	
	
                ELSE	-- El expediente ya existe!
                    BEGIN
                        IF ( @ModoDebug = 1 )
                            BEGIN
                                PRINT 'El Expediente ya Existe!';
                            END			
                        SELECT  ( 0 ) AS statusDB; -- TODO OK	
                    END	
            END

        IF ( @ExpedienteNroID > 0 ) 		-- Alta por Expediente
            BEGIN
					-- Existe en la base de datos el Expediente y el usuario que queremos asignar?
                IF ( ( NOT EXISTS ( SELECT  expe.ExpedienteNumeroID
                                    FROM    dbo.Expedientes expe
                                    WHERE   expe.ExpedienteNumeroID = @ExpedienteNroID )
                     )
                     OR ( NOT EXISTS ( SELECT   usr.UsuarioID
                                       FROM     dbo.Usuarios usr
                                       WHERE    usr.UsuarioID = @UsuarioID )
                        )
                   )
                    BEGIN
                        IF ( @ModoDebug = 1 )
                            PRINT 'La empresa o el usuario no existen en la BD!!';			
                        SELECT  ( 3 ) AS statusDB;
                        RETURN;			
                    END

			-- Si no existe la asignacion, Realiza el alta
                IF ( (NOT EXISTS ( SELECT   expeus.ExpedienteUsuarioID
                                   FROM     dbo.ExpedientesUsuarios expeus
                                   WHERE    expeus.ExpedienteNumeroID = @ExpedienteNroID
                                            AND expeus.UsuarioID = @UsuarioID ))
                   )
                    BEGIN
                        BEGIN TRY						
                            BEGIN TRAN	
                    
                            INSERT  INTO dbo.ExpedientesUsuarios
                                    ( UsuarioID ,
                                      ExpedienteNumeroID ,
                                      Porcentaje ,
                                      FechaUltimaModificacion
                                    )
                            VALUES  ( @UsuarioID , -- UsuarioID - int
                                      @ExpedienteNroID , -- ExpedienteNumeroID - bigint
                                      @Porcentaje , -- Porcentaje - decimal
                                      GETDATE()  -- FechaUltimaModificacion - datetime2
                                    )
				
                            IF ( @ModoDebug = 0 )
                                BEGIN
                                    COMMIT;
                                END	
									
                            ELSE
                                BEGIN
                                    PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                                    SELECT  *
                                    FROM    dbo.ExpedientesUsuarios expeus
                                    WHERE   expeus.ExpedienteNumeroID = @ExpedienteNroID
                                            AND expeus.UsuarioID = @UsuarioID;

                                    IF @@TRANCOUNT > 0
                                        ROLLBACK;
                                END								
                            SELECT  ( 0 ) AS statusDB; -- TODO OK				
                        END TRY	
			
                        BEGIN CATCH
                            IF @@TRANCOUNT > 0
                                ROLLBACK TRANSACTION;
                            SELECT  ( 99 ) AS statusDB; -- ERROR					
                            SELECT  ERROR_NUMBER() AS ErrorNumber ,
                                    ERROR_SEVERITY() AS ErrorSeverity ,
                                    ERROR_STATE() AS ErrorState ,
                                    ERROR_PROCEDURE() AS ErrorProcedure ,
                                    ERROR_LINE() AS ErrorLine ,
                                    ERROR_MESSAGE() AS ErrorMessage;
                        END CATCH;		
                    END	
	
                ELSE	-- El expediente ya existe!
                    BEGIN
                        IF ( @ModoDebug = 1 )
                            BEGIN
                                PRINT 'El Expediente ya Existe!';
                            END			
                        SELECT  ( 1 ) AS statusDB; -- YA EXISTE
                    END	

            END
            
    END


/*

	EXEC dbo.ExpedientesUsuariosInsProc
	    @Modo = 0, -- int
	    @ModoDebug = 0, -- int
	    @EmpresaCuitID = 30708070722, -- bigint
	    @UsuarioID = 1, -- int
	    @SolicitanteUsuarioID = 1 -- int

	EXEC dbo.ExpedientesUsuariosInsProc
	    @Modo = 0, -- int
	    @ModoDebug = 0, -- int
	    @EmpresaCuitID = 30708070722, -- bigint
	    @UsuarioID = 2, -- int
	    @SolicitanteUsuarioID = 1 -- int

*/
GO
/****** Object:  StoredProcedure [dbo].[ExpedientesUsuariosDelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExpedientesUsuariosDelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @ExpedienteNro BIGINT ,
      @UsuarioID INT ,
      @SolicitanteUsuarioID INT -- ID del Solicitante
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN

        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
        DECLARE @CantUsuarios INT;
        DECLARE @ExpedientesAvanzados INT;

	-- Obtiene el tipo de usuario que desea realizar la operacion de baja.
        SELECT  @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @SolicitanteUsuarioID;

	-- Solo los administradores puede dar de baja asignaciones
        IF ( @SolicitanteUsuarioTipoID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID < 3 )
            BEGIN
                IF ( @ModoDebug = 1 )
                    BEGIN
                        PRINT 'Solo los usuarios administradores pueden realizar bajas!';				
                    END			
                SELECT  ( 2 ) AS statusDB;
                RETURN;			
            END	

	-- Existe la Asignacion que queremos borrar?
        IF ( NOT EXISTS ( SELECT    asign.ExpedienteUsuarioID
                          FROM      dbo.ExpedientesUsuarios AS asign
                          WHERE     ( asign.ExpedienteNumeroID = @ExpedienteNro )
                                    AND ( asign.UsuarioID = @UsuarioID ) )
           )
            BEGIN
                IF ( @ModoDebug = 1 )
                    BEGIN				
                        PRINT 'El Expediente o el usuario no existen en la BD!!';			
                    END				
                SELECT  ( 3 ) AS statusDB;
                RETURN;			
            END			
	
        SET @ExpedientesAvanzados = ( SELECT    COUNT(*)
                                      FROM      dbo.Expedientes
                                      WHERE     ExpedienteNumeroID = @ExpedienteNro
                                                AND EstadoLiquidacionID > 2
                                    )
		-- si no tiene un estado de liquidación superior a 2 (Pendiente)
        IF ( @ExpedientesAvanzados = 0 )
            BEGIN

			-- Realiza la baja
                BEGIN TRY						
                    BEGIN TRAN
			
                    DELETE  FROM dbo.ExpedientesUsuarios
                    WHERE   ( ( ExpedienteNumeroID = @ExpedienteNro )
                              AND ( UsuarioID = @UsuarioID )
                            )	
						
                    IF ( @ModoDebug = 0 )
                        BEGIN
                            COMMIT;
                        END	
                    ELSE
                        BEGIN
                            PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                            SELECT  *
                            FROM    dbo.ExpedientesUsuarios asign
                            WHERE   asign.ExpedienteNumeroID = @ExpedienteNro
                                    AND asign.UsuarioID = @UsuarioID;
                            IF @@TRANCOUNT > 0
                                ROLLBACK;
                        END		
			
			-- Chequea que queden usuarios asignados a la empresa
                    SET @CantUsuarios = ( SELECT    COUNT(*)
                                          FROM      dbo.ExpedientesUsuarios
                                          WHERE     ExpedienteNumeroID = @ExpedienteNro
                                        );

			-- Si No quedan usuarios asignados a empresas, busca los expedientes de la empresa y los setea como "sin asignación"
                    IF ( @CantUsuarios = 0 )
                        BEGIN
                            UPDATE  dbo.Expedientes
                            SET     EstadoLiquidacionID = 1
                            WHERE   ExpedienteNumeroID = @ExpedienteNro;
                        END
										
                    SELECT  ( 0 ) AS statusDB; -- TODO OK				
                END TRY	
	
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        ROLLBACK TRANSACTION;
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;	
            END
        ELSE
            BEGIN
                SELECT  ( 1 ) AS statusDB; -- EXISTÍAN EXPEDIENTES CON ESTADO DE LIQUIDACIÓN SUPERIOR A 2 (PENDIENTES) - NO SE BORRÓ.
            END
    END

/*
EXEC dbo.ExpedientesUsuariosDelProc
    @ExpedienteNro = 3292016, -- bigint
    @UsuarioID = 2, -- int
    @SolicitanteUsuarioID = 1 -- int
    

select * from dbo.ExpedientesUsuarios;             
*/
GO
/****** Object:  StoredProcedure [dbo].[AsignacionesUpdProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AsignacionesUpdProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@EmpresaCuitID		BIGINT,
	@UsuarioID			INT,  -- ID del usuario al que queremos actualizarle el porcentaje
	@Porcentaje			DECIMAL(5,2),
	@SolicitanteUsuarioID INT -- ID del Solicitante
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	DECLARE @UsuarioTipoID INT = NULL;

	-- Obtiene el tipo de usuario que desea realizar la operacion de alta.
	SELECT @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @SolicitanteUsuarioID;
	
	-- Obtiene el tipo de usuario al que hay que cambiarle el porcentaje.
	SELECT @UsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @UsuarioID;	

	-- Solo los administradores puede dar de alta asignaciones
	IF (@SolicitanteUsuarioTipoID IS NOT NULL) AND (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'Solo los usuarios administradores pueden actualizar asignaciones!';			
			SELECT (2) AS statusDB;
			RETURN;			
		END	

	-- Existe la Asignacion que queremos actualizar?
	IF (NOT EXISTS (SELECT asign.AsignacionID FROM dbo.Asignaciones AS asign 
					WHERE (asign.EmpresaCuitID = @EmpresaCuitID) AND 
						  (asign.UsuarioID = @UsuarioID) ))
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'La empresa o el usuario no existen en la BD!!';			
			SELECT (3) AS statusDB;
			RETURN;			
		END			
	
	
	-- Realiza el Update
	BEGIN TRY						
		BEGIN TRAN
						
			UPDATE dbo.Asignaciones
				SET	Porcentaje = @Porcentaje	   
				FROM dbo.Asignaciones AS asign
				JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
				WHERE (asign.EmpresaCuitID = @EmpresaCuitID) AND
					(usr.UsuarioTipoID = @UsuarioTipoID)						
					
			IF (@ModoDebug = 0)
				BEGIN
					COMMIT;
				END	
							
			ELSE
				BEGIN
					PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
					SELECT asign.AsignacionID ,
						   asign.EmpresaCuitID ,
						   asign.UsuarioID ,
						   asign.Porcentaje,
						   usr.UsuarioID ,
						   usr.Nombre ,
						   usr.UsuarioTipoID
					FROM dbo.Asignaciones AS asign
					  JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
					  WHERE (asign.EmpresaCuitID = @EmpresaCuitID) AND
							(usr.UsuarioTipoID = @UsuarioTipoID)
					IF @@TRANCOUNT > 0
						ROLLBACK;
				END								
			SELECT (0) AS statusDB; -- TODO OK				
	END TRY	
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
			SELECT (99) AS statusDB; -- ERROR					
		SELECT 
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;	
	
END

/*
EXEC dbo.AsignacionesUpdProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @EmpresaCuitID = 20042706176, -- bigint
    @UsuarioID = 3, -- int
    @Porcentaje = 4, -- decimal
    @SolicitanteUsuarioID = 1 -- int
    
EXEC dbo.AsignacionesUpdProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @EmpresaCuitID = 20042706176, -- bigint
    @UsuarioID = 3, -- int
    @Porcentaje = 1, -- decimal
    @SolicitanteUsuarioID = 2 -- int
    
SELECT * FROM dbo.Asignaciones AS asign 
WHERE asign.EmpresaCuitID = 20042706176
ORDER BY asign.EmpresaCuitID;        
    
*/
GO
/****** Object:  StoredProcedure [dbo].[AsignacionesSelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	PROCEDURE [dbo].[AsignacionesSelProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@UsuarioID INT,
	@CUIT BIGINT = NULL
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el tipo de usuario que desea realizar la consulta
	SELECT @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @UsuarioID;
	PRINT @SolicitanteUsuarioTipoID;
	
	
	-- Si no le envío parámetros, mostrar 1 vez el nombre de cada empresa asignada 
	-- Las demás columnas ponerlas en cero.
	
	IF (@CUIT IS NULL)
		BEGIN
			-- Si el usuario es tipo 1 (Gestor)... lista sólo sus propias asignaciones)
			IF @SolicitanteUsuarioTipoID < 2
				BEGIN
					SELECT DISTINCT
						   asign.EmpresaCuitID ,
						   emp.NroEmpresa,
						   emp.RazonSocial,
						   0 AS UsuarioID,
						   0 AS Nombre,
						   0 AS UsuarioTipoID,
						   0 AS Porcentaje
					FROM dbo.Asignaciones AS asign
					JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = asign.EmpresaCuitID
					JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
					JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
					WHERE usr.UsuarioID = @UsuarioID
					ORDER BY emp.RazonSocial		
				END
			
			ELSE		-- Obtiene las asignaciones de todos los usuarios que sean igual o menor a mi tipo de usuario
				BEGIN				
					SELECT DISTINCT
					       asign.EmpresaCuitID ,
						   emp.NroEmpresa,
						   emp.RazonSocial,
						   0 AS UsuarioID,
						   0 AS Nombre,
						   0 AS UsuarioTipoID,
						   0 AS Porcentaje
					FROM dbo.Asignaciones AS asign
					JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = asign.EmpresaCuitID
					JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
					JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
					WHERE usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID
					ORDER BY emp.RazonSocial	
				END		
		
		END
		
	ELSE	-- Si le paso parámetros (CUIT) debe traer el select filtrado por ese cuit.
		BEGIN
			-- Si el usuario es tipo 1 (Gestor)... lista sólo sus propias asignaciones)
			IF @SolicitanteUsuarioTipoID < 2
				BEGIN
					SELECT asign.EmpresaCuitID ,
						   emp.NroEmpresa,
						   emp.RazonSocial,
						   usr.UsuarioID,
						   usr.Nombre,
						   usr.UsuarioTipoID,
						   asign.Porcentaje
					FROM dbo.Asignaciones AS asign
					JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = asign.EmpresaCuitID
					JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
					JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
					WHERE (usr.UsuarioID = @UsuarioID) AND (emp.EmpresaCuitID = @CUIT)
					ORDER BY emp.RazonSocial, ut.UsuarioTipoID		
				END
			
			ELSE		-- Obtiene las asignaciones de todos los usuarios que sean igual o menor a mi tipo de usuario
				BEGIN				
					SELECT asign.EmpresaCuitID ,
						   emp.NroEmpresa,
						   emp.RazonSocial,
						   usr.UsuarioID,
						   usr.Nombre,
						   usr.UsuarioTipoID,
						   asign.Porcentaje
					FROM dbo.Asignaciones AS asign
					JOIN dbo.Empresas AS emp ON emp.EmpresaCuitID = asign.EmpresaCuitID
					JOIN dbo.Usuarios AS usr ON usr.UsuarioID = asign.UsuarioID
					JOIN dbo.UsuariosTipos AS ut ON ut.UsuarioTipoID = usr.UsuarioTipoID
					WHERE (usr.UsuarioTipoID <= @SolicitanteUsuarioTipoID) AND (emp.EmpresaCuitID = @CUIT)
					ORDER BY emp.RazonSocial, ut.UsuarioTipoID			
				END		
		END
	
	

END

/*

EXEC dbo.AsignacionesSelProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @UsuarioID = 1 -- int
        
EXEC dbo.AsignacionesSelProc
    @Modo = 0, -- int
	@ModoDebug = 0, -- int
	@UsuarioID = 4, -- int
	@CUIT = 20042706176 -- int        
        
select * from Asignaciones       
    
*/
GO
/****** Object:  StoredProcedure [dbo].[AsignacionesInsProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AsignacionesInsProc]
(
	@Modo INT = 0,
	@ModoDebug INT = 0, -- 0 False / 1 True
	@EmpresaCuitID		BIGINT,
	@UsuarioID			INT,
	@Porcentaje			DECIMAL(5,2),
	@SolicitanteUsuarioID INT -- ID del Solicitante
)
AS
SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

BEGIN

	DECLARE @UsuarioTipoID INT;
	DECLARE @SolicitanteUsuarioTipoID INT = NULL;
	
	-- Obtiene el tipo de usuario que desea realizar la operacion de alta.
	SELECT @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
	FROM dbo.Usuarios usr
	WHERE usr.UsuarioID = @SolicitanteUsuarioID;

	-- Solo los administradores puede dar de alta asignaciones
	IF (@SolicitanteUsuarioTipoID IS NOT NULL) AND (@SolicitanteUsuarioTipoID < 3)
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'Solo los usuarios administradores pueden asignar empresas a usuarios!';			
			SELECT (2) AS statusDB;
			RETURN;			
		END	
	
	-- Existe en la base de datos la empresa y el usuario que queremos asignar?
	IF (
		(NOT EXISTS (SELECT emp.EmpresaCuitID FROM dbo.Empresas emp WHERE emp.EmpresaCuitID = @EmpresaCuitID)) OR
		(NOT EXISTS (SELECT usr.UsuarioID FROM dbo.Usuarios usr WHERE usr.UsuarioID = @UsuarioID))
		)		
		BEGIN
			IF (@ModoDebug = 1)
				PRINT 'La empresa o el usuario no existen en la BD!!';			
			SELECT (3) AS statusDB;
			RETURN;			
		END		
		
	-- Si no existe la asignacion, Realiza el alta
	
	IF (
		(NOT EXISTS (	SELECT asign.AsignacionID 
						FROM dbo.Asignaciones AS asign 
						WHERE (asign.EmpresaCuitID = @EmpresaCuitID) AND (asign.UsuarioID = @UsuarioID)	
					)))	
		BEGIN
			BEGIN TRY						
				BEGIN TRAN	
					INSERT dbo.Asignaciones
							( EmpresaCuitID ,
							  UsuarioID ,
							  Porcentaje
							)
					VALUES  ( @EmpresaCuitID , -- EmpresaCuitID - bigint
							  @UsuarioID , -- UsuarioID - int
							  @Porcentaje  -- Porcentaje - decimal
							)
					
					-- cambia todos los expedientes que estén sin asignación a estado de liquidación Pendiente para ese EmpresaCuitID
					UPDATE dbo.Expedientes
					SET EstadoLiquidacionID = 2
					WHERE EstadoLiquidacionID = 1 AND EmpresaCuitID = @EmpresaCuitID		
					
					-- cambia todos los expedientes que estén en estado archivado a estado de liquidación Finalizado para ese EmpresaCuitID
					UPDATE dbo.Expedientes
					SET EstadoLiquidacionID = 5
					WHERE ExpedienteEstadoId = 18 AND EmpresaCuitID = @EmpresaCuitID

					IF (@ModoDebug = 0)
						BEGIN
							COMMIT;
						END	
									
					ELSE
						BEGIN
							PRINT ('Revirtiendo la operacion por @ModoDebug = 1!');		
							SELECT * FROM dbo.Asignaciones asign
							WHERE asign.EmpresaCuitID = @EmpresaCuitID AND asign.UsuarioID = @UsuarioID;
							IF @@TRANCOUNT > 0
								ROLLBACK;
						END								
					SELECT (0) AS statusDB; -- TODO OK				
			END TRY	
			
			BEGIN CATCH
				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;
					SELECT (99) AS statusDB; -- ERROR					
				SELECT 
					ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;
			END CATCH;		
		END	
	
	ELSE	-- El expediente ya existe!
		BEGIN
			IF (@ModoDebug = 1)
				BEGIN
					PRINT 'El Expediente ya Existe!';
				END			
			SELECT (0) AS statusDB; -- TODO OK	
		END	

END

/*
EXEC dbo.AsignacionesInsProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @EmpresaCuitID = 30708070722, -- bigint
    @UsuarioID = 1, -- int
    @Porcentaje = 3, -- decimal
    @SolicitanteUsuarioID = 1 -- int 
    
EXEC dbo.AsignacionesInsProc
    @Modo = 0, -- int
    @ModoDebug = 0, -- int
    @EmpresaCuitID = 20050639623, -- bigint
    @UsuarioID = 2, -- int
    @Porcentaje = 3, -- decimal
    @SolicitanteUsuarioID = 1 -- int     
    
    
    
EXEC dbo.AsignacionesInsProc
    @Modo = 0, -- int
    @ModoDebug = 1, -- int
    @EmpresaCuitID = 30708070722, -- bigint
    @UsuarioID = 2, -- int
    @Porcentaje = 3, -- decimal
    @SolicitanteUsuarioID = 1 -- int  
    
select * from Asignaciones       
    
*/
GO
/****** Object:  StoredProcedure [dbo].[AsignacionesDelProc]    Script Date: 02/14/2017 12:27:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AsignacionesDelProc]
    (
      @Modo INT = 0 ,
      @ModoDebug INT = 0 , -- 0 False / 1 True
      @EmpresaCuitID BIGINT ,
      @UsuarioID INT ,
      @SolicitanteUsuarioID INT -- ID del Solicitante
    )
AS
    SET XACT_ABORT, NOCOUNT ON   -- Error de bajo nivel RollBack + Apaga display Cantidad Registros Procesados

    BEGIN

        DECLARE @SolicitanteUsuarioTipoID INT = NULL;
        DECLARE @CantUsuarios INT;
        DECLARE @ExpedientesAvanzados INT;

	-- Obtiene el tipo de usuario que desea realizar la operacion de baja.
        SELECT  @SolicitanteUsuarioTipoID = usr.UsuarioTipoID
        FROM    dbo.Usuarios usr
        WHERE   usr.UsuarioID = @SolicitanteUsuarioID;

	-- Solo los administradores puede dar de baja asignaciones
        IF ( @SolicitanteUsuarioTipoID IS NOT NULL )
            AND ( @SolicitanteUsuarioTipoID < 3 )
            BEGIN
                IF ( @ModoDebug = 1 )
                    BEGIN
                        PRINT 'Solo los usuarios administradores pueden realizar bajas!';				
                    END			
                SELECT  ( 2 ) AS statusDB;
                RETURN;			
            END	

	-- Existe la Asignacion que queremos borrar?
        IF ( NOT EXISTS ( SELECT    asign.AsignacionID
                          FROM      dbo.Asignaciones AS asign
                          WHERE     ( asign.EmpresaCuitID = @EmpresaCuitID )
                                    AND ( asign.UsuarioID = @UsuarioID ) )
           )
            BEGIN
                IF ( @ModoDebug = 1 )
                    BEGIN				
                        PRINT 'La empresa o el usuario no existen en la BD!!';			
                    END				
                SELECT  ( 3 ) AS statusDB;
                RETURN;			
            END			
	
        SET @ExpedientesAvanzados = ( SELECT    COUNT(*)
                                      FROM      dbo.Expedientes
                                      WHERE     EmpresaCuitID = @EmpresaCuitID
                                                AND EstadoLiquidacionID > 2
                                    )
		-- si no tiene expedientes en un estado de liquidación superior a 2 (Pendiente)
        IF ( @ExpedientesAvanzados = 0 )
            BEGIN

			-- Realiza la baja
                BEGIN TRY						
                    BEGIN TRAN
		
			
                    DELETE  FROM dbo.Asignaciones
                    WHERE   ( ( EmpresaCuitID = @EmpresaCuitID )
                              AND ( UsuarioID = @UsuarioID )
                            )	
						
                    IF ( @ModoDebug = 0 )
                        BEGIN
                            COMMIT;
                        END	
                    ELSE
                        BEGIN
                            PRINT ( 'Revirtiendo la operacion por @ModoDebug = 1!' );		
                            SELECT  *
                            FROM    dbo.Asignaciones asign
                            WHERE   asign.EmpresaCuitID = @EmpresaCuitID
                                    AND asign.UsuarioID = @UsuarioID;
                            IF @@TRANCOUNT > 0
                                ROLLBACK;
                        END		
			
			-- Chequea que queden usuarios asignados a la empresa
                    SET @CantUsuarios = ( SELECT    COUNT(*)
                                          FROM      dbo.Asignaciones
                                          WHERE     EmpresaCuitID = @EmpresaCuitID
                                        );

			-- Si No quedan usuarios asignados a empresas, busca los expedientes de la empresa y los setea como "sin asignación"
                    IF ( @CantUsuarios = 0 )
                        BEGIN
                            UPDATE  dbo.Expedientes
                            SET     EstadoLiquidacionID = 1
                            WHERE   EmpresaCuitID = @EmpresaCuitID;
                        END
										
                    SELECT  ( 0 ) AS statusDB; -- TODO OK				
                END TRY	
	
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        ROLLBACK TRANSACTION;
                    SELECT  ( 99 ) AS statusDB; -- ERROR					
                    SELECT  ERROR_NUMBER() AS ErrorNumber ,
                            ERROR_SEVERITY() AS ErrorSeverity ,
                            ERROR_STATE() AS ErrorState ,
                            ERROR_PROCEDURE() AS ErrorProcedure ,
                            ERROR_LINE() AS ErrorLine ,
                            ERROR_MESSAGE() AS ErrorMessage;
                END CATCH;	
            END
			ELSE
            BEGIN
				SELECT  ( 1 ) AS statusDB; -- EXISTÍAN EXPEDIENTES CON ESTADO DE LIQUIDACIÓN SUPERIOR A 2 (PENDIENTES) - NO SE BORRÓ.
			END
    END

/*
EXEC dbo.AsignacionesDelProc
	@Modo = 0, -- int
	@ModoDebug = 0, -- int
	@EmpresaCuitID = 20050639623, -- bigint
	@UsuarioID = 1, -- int
	@SolicitanteUsuarioID = 1 -- int
    
select * from asignaciones;         
    
*/
GO


/****** Object:  Trigger [dbo].[Expedientes_TrgUpd]    Script Date: 14/02/2017 05:36:32 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[Expedientes_TrgUpd] ON [dbo].[Expedientes]
    FOR UPDATE
AS
    INSERT  INTO dbo.ExpedientesHistorico
            ( ExpedienteNumeroID ,
              NroSolicitud ,
              EmpresaCuitID ,
              BeneficioSolicitado ,
              BeneficioAprobado ,
              FechaEstado ,
              ExpedienteEstadoId ,
              EvaluadorID ,
              EvaluadorTecnicoID ,
              EstadoLiquidacionID ,
              Fecha ,
              FechaCambioLiquidacion
            )
            SELECT  Deleted.ExpedienteNumeroID , -- ExpedienteNumeroID - bigint
                    Deleted.NroSolicitud , -- NroSolicitud - int
                    Deleted.EmpresaCuitID , -- EmpresaCuitID - bigint
                    Deleted.BeneficioSolicitado , -- BeneficioSolicitado - money
                    Deleted.BeneficioAprobado , -- BeneficioAprobado - money
                    Deleted.FechaEstadoActual , -- FechaEstado - date
                    Deleted.ExpedienteEstadoId , -- ExpedienteEstadoId - int
                    Deleted.EvaluadorID , -- EvaluadorID - int
                    Deleted.EvaluadorTecnicoID , -- EvaluadorTecnicoID - int
                    Deleted.EstadoLiquidacionID , -- EstadoLiquidacionID - int
                    Deleted.Fecha , -- Fecha - datetime2
                    Deleted.FechaCambioLiquidacion  -- FechaCambioLiquidacion - datetime2
            FROM    Deleted
GO            