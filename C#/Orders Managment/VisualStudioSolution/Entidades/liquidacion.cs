﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class liquidacion
    {
        // DE LA CLASE

        public int EstadoLiquidacionID { get; set; }
        public string Descripcion { get; set; }
        public int SolicitanteUsuarioID { get; set; }

        // CONSTRUCTORES
        public liquidacion() { }

        public liquidacion(Int32 estadoLiquidacionID,  Int32 solicitanteUsuarioID, string descripcion)
        {
            this.EstadoLiquidacionID = estadoLiquidacionID;
            this.SolicitanteUsuarioID = solicitanteUsuarioID;
            this.Descripcion = descripcion;
        }

    }
}
