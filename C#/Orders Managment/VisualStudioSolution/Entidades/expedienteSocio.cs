﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class expedienteSocio : expediente
    {
        public virtual decimal MontoB { get; set; }
        public int EstadoLiquidacionID { get; set; }
        public string EstadoLiquidacion { get; set; }

    }
}
