﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class empresa
    {
        // DE LA CLASE
        public Nullable<Int64> EmpresaCuitID { get; set; }
        public string NroEmpresa { get; set; }
        public string RazonSocial { get; set; }

        // CONSTRUCTORES
        public empresa() { }

        public empresa(Int64 empresaCuitID, string nroEmpresa, string razonSocial)
        {
            this.EmpresaCuitID = empresaCuitID;
            this.NroEmpresa = nroEmpresa;
            this.RazonSocial = razonSocial;
        }
    }
}

