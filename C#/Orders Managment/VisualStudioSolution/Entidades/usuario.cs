﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class usuario
    {
        public int UsuarioID { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int UsuarioEstadoID { get; set; }
        public string UsuarioEstadoDescripcion { get; set; }
        public int UsuarioTipoID { get; set; }
        public string UsuarioTipoDescripcion { get; set; }
        public int SolicitanteUsuarioID { get; set; }
        public int PoseeLogin { get; set; }
        public string PoseeLoginDescripcion { get; set; }
        public int Autenticado { get; set; }

    }
}
