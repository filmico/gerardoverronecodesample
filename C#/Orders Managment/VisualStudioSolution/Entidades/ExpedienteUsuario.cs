﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class ExpedienteUsuario
    {
        public Int64 ExpedienteNumeroID { get; set; }
        public empresa Empresa { get; set; }
        public usuario Usuario { get; set; }
        public Decimal Porcentaje { get; set; }
        public int SolicitanteUsuarioID { get; set; }
        public int AutoAsignados { get; set; }


        // CONSTRUCTORES
        public ExpedienteUsuario()
        {
            this.Empresa = new empresa();
            this.Usuario = new usuario();
        }
    }

}