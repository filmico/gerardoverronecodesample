﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Entidades
{
    public class expedientesEstado
    {
        // DE LA CLASE
        public int ExpedienteEstadoID { get; set; }
        public string Descripcion { get; set; }

        // CONSTRUCTORES
        public expedientesEstado()
        {
            
        }

        public expedientesEstado(int expedienteEstadoID, string descripcion)
        {
            this.ExpedienteEstadoID = expedienteEstadoID;
            this.Descripcion = descripcion;
        }

    }
}
