﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class configSincronizacion
    {
        public int DiaSemanaComienzo { get; set; }
        public int DiaSemanaFinalizacion { get; set; }
        public DateTime HoraComienzo { get; set; }
        public DateTime HoraFinalizacion { get; set; }
        public int Estado { get; set; }
        public int EstadoExpedienteID { get; set; }
        public int SolicitanteUsuarioID { get; set; }

        // Constructores
        public configSincronizacion() { }

        public configSincronizacion(int _diaSemanaComienzo, int _diaSemanaFinalizacion,
                                    DateTime _horaComienzo, DateTime _horaFinalizacion,
                                    int _estado, int _estadoExpedienteID)
        {

            this.DiaSemanaComienzo = _diaSemanaComienzo;
            this.DiaSemanaFinalizacion = _diaSemanaFinalizacion;
            this.HoraComienzo = _horaComienzo;
            this.HoraFinalizacion = _horaFinalizacion;
            this.Estado = _estado;
            this.EstadoExpedienteID = _estadoExpedienteID;
        }

    }
}
