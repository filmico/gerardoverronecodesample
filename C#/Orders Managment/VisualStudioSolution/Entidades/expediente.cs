﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Entidades
{
    public class expediente
    {
        // DE LA CLASE
        public Int64 ExpedienteNumeroID { get; set; }
        public int NroSolicitud { get; set; }
        public empresa Empresa { get; set; }
        public decimal BeneficioSolicitado { get; set; }
        public decimal BeneficioAprobado { get; set; }
        public Nullable<DateTime> FechaEstadoActual { get; set; }
        public expedientesEstado ExpedienteEstado { get; set; }
        public evaluador Evaluador { get; set; }
        public evaluador EvaluadorTecnico { get; set; }
        public decimal MontoA { get; set; }
        public int SolicitanteUsuarioID { get; set; }

        // Para el filtro de estado de Liquidacion
        public Nullable<int> sinAsignacion { get; set; }
        public Nullable<int> pendiente { get; set; }
        public Nullable<int> aLiquidar { get; set; }
        public Nullable<int> liquidado { get; set; }
        public Nullable<int> finalizado { get; set; }


        // CONSTRUCTORES
        public expediente()
        {

            // instanciamos objetos para cargar las propiedades
            empresa Emp = new empresa();
            evaluador Eval = new evaluador();
            evaluador EvalTec = new evaluador();
            expedientesEstado ExpEstado = new expedientesEstado();

            // Cargamos las propiedades con los objetos
            this.Empresa = Emp;
            this.Evaluador = Eval;
            this.EvaluadorTecnico = EvalTec;
            this.ExpedienteEstado = ExpEstado;
        }




    }
}
