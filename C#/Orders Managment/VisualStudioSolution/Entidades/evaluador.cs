﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Entidades
{
    public class evaluador
    {
        // DE LA CLASE
        public int EvaluadorID { get; set; }
        public string Nombre { get; set; }
        public int SolicitanteUsuarioID { get; set; }


        // CONSTRUCTORES
        public evaluador() { }

        public evaluador(int evaluadorID, string nombre)
        {
            this.EvaluadorID = evaluadorID;
            this.Nombre = nombre;
        }
    }
}