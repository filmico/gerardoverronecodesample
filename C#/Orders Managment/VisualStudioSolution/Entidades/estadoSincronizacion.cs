﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Entidades
{
    public class estadoSincronizacion
    {
        // DE LA CLASE
        public expedientesEstado ExpedienteEstado { get; set; }
        public DateTime UltimaSincronizacionComienzo { get; set; }
        public DateTime UltimaSincronizacionFinalizacion { get; set; }
        public int EstadoSincronizacion { get; set; }
        public Nullable<int> ExpedientesObtenidos { get; set; }
        public Nullable<int> ExpedientesPosibles { get; set; }
        public Nullable<int> ExpedientesActualizados { get; set; }

        // CONSTRUCTORES
        public estadoSincronizacion()
        {
            this.ExpedienteEstado = new expedientesEstado();
        }

        public estadoSincronizacion(int expedienteEstadoID, DateTime ultimaSincronizacionComienzo, DateTime ultimaSincronizacionFinalizacion)
        {
            this.ExpedienteEstado = new expedientesEstado();
            this.ExpedienteEstado.ExpedienteEstadoID = expedienteEstadoID;
            this.UltimaSincronizacionComienzo = ultimaSincronizacionComienzo;
            this.UltimaSincronizacionFinalizacion = ultimaSincronizacionFinalizacion;
        }
    }
}
