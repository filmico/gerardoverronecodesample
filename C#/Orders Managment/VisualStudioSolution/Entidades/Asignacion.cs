﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Asignacion
    {
        public int AsignacionID { get; set; }
        public Int64 EmpresaCuitID { get; set; }
        public string NroEmpresa { get; set; }

        public string RazonSocial { get; set; }
        public int UsuarioID { get; set; }
        public string UsuarioNombre { get; set; }
        public int UsuarioTipoID { get; set; }
        public Decimal Porcentaje { get; set; }
        public int SolicitanteUsuarioID { get; set; }

        // Constructores
        public Asignacion()
        {
           
        }

        public Asignacion(int _AsignacionID, Int64 _EmpresaCuitID, int _UsuarioID, Decimal _Porcentaje)
        {
           this.AsignacionID = _AsignacionID;
            this.EmpresaCuitID = _EmpresaCuitID;
            this.UsuarioID = _UsuarioID;
            this.Porcentaje = _Porcentaje;
        }


    }
}
