﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usuarios.aspx.cs" Inherits="Presentacion.usuarios" %>

<%@ Register Src="~/cabecera.ascx" TagPrefix="uc1" TagName="cabecera" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>bksys - usuarios</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <asp:PlaceHolder ID="PHjs" runat="server"></asp:PlaceHolder>

    <form id="form1" runat="server">
        <%--Mensajes de Error--%>
        <asp:Label ID="LBLAlert" runat="server" Text="" CssClass="hide"></asp:Label>

        <div>
            <%--CABECERA--%>
            <uc1:cabecera runat="server" ID="cabecera" />

            <div class="row">

                <div class="col m12 l12">

                    <ul class="collapsible collection with-header z-depth-1" data-collapsible="accordion">

                        <li class="collection-header">
                            <h4>Usuarios del Sistema</h4>

                        </li>
                        <li>

                            <table class="usuarios-table bordered striped">
                                <thead>
                                    <tr class="blue-text">
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="fixed-action-btn">
                <a id="nuevoUsuario" class="btn-floating btn-large waves-effect waves-light red boton-agregar"><i class="material-icons">add</i></a>
            </div>
            <!-- Modal Structure -->
            <div id="modal" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h5>Modal Header</h5>

                    <%--CAMPO NOMBRE--%>
                    <div class="input-field col m8 s12">
                        <i class="material-icons prefix active">account_circle</i>
                        <input name="TB_nombre" type="text" id="TB_nombre">
                        <label for="TB_nombre" class="active">ingrese Nombre de Usuario</label>
                    </div>

                    <%--CAMPO EMAIL--%>
                    <div class="input-field col m8 s12">
                        <i class="material-icons prefix active">email</i>
                        <input name="TB_email" type="text" id="TB_email">
                        <label for="TB_email" class="active">Ingrese un Correo Electrónico</label>
                    </div>

                    <%--CAMPO PASSWORD--%>
                    <div class="input-field col m8 s12">
                        <i class="material-icons prefix active">lock</i>
                        <input name="TB_password" type="password" id="TB_password">
                        <label for="TB_password" class="active">Ingrese una Contraseña</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">supervisor_account</i>
                        <select name="usuarioTipo" id="usuarioTipo">
                            <option value="0" disabled selected>Elija un Tipo de Usuario</option>
                            <option value="1">Gestor</option>
                            <option value="2">Socio</option>
                            <option value="3">Administrador</option>
                        </select>
                        <%-- <label>Tipo de Usuario</label>--%>
                    </div>
                    <!-- Switch -->
                    <div class="switch right">
                        <label>
                            Inactivo
                            <input name="usuarioEstado" id="usuarioEStado" type="checkbox">
                            <span class="lever"></span>
                            Activo
                        </label>
                    </div>
                    <input type="hidden" id="TB_usuarioID" name="TB_usuarioID" />
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="BTNAgregar" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNAgregar_Click">Guardar</asp:LinkButton>

                    <asp:LinkButton ID="BTNActualizar" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNActualizar_Click">Actualizar</asp:LinkButton>
                    <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>
                </div>
            </div>


             <%--MODAL BAJA--%>
            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Baja de Usuario</h4>
                    <p>Texto de advertencia</p>
                </div>
                <div class="modal-footer">

                    <asp:LinkButton ID="BTNBorrar" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNBorrar_Click">Eliminar</asp:LinkButton>

                    <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>



                </div>
            </div>
        </div>

                <%--Barra de Progreso--%>
        <div id="barraProgresoFooter" class="row">
            <div id="barraProgreso" class="col s12 m12 l12">
                <div class="indeterminate"></div>
            </div>
        </div>
    </form>
    <asp:PlaceHolder ID="PHjsFunciones" runat="server"></asp:PlaceHolder>
</body>
</html>
