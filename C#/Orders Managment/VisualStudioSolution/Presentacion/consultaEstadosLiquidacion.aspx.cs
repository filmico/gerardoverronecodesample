﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Configuration;

namespace Presentacion
{
    public partial class consultaEstadosLiquidacion : System.Web.UI.Page
    {

        /// <summary>
        /// Instancia un usuario
        /// </summary>
        usuario usr = new usuario();

        protected void Page_Load(object sender, EventArgs e)
        {

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];

            bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);

            // Chequea que el usuario sea Socio o Administrador para producir el listado
            if (this.usr.UsuarioTipoID > 1)
            {

                try
                {
                    // Toma el parametro opcional EstadoLiquidacion del query string.
                    // Si no recibimos nada listamos todos los estados posibles de una liquidacion.
                    string EstadoLiquidacionID = Request.QueryString["EstadoLiquidacionID"];


                    List<liquidacion> ListaObjetosRetorno = new List<liquidacion>();

                    liquidacion objLiquidacion = new liquidacion();

                    objLiquidacion.SolicitanteUsuarioID = this.usr.UsuarioID;

                    if (EstadoLiquidacionID != null)
                    {
                        objLiquidacion.EstadoLiquidacionID = Convert.ToInt32(EstadoLiquidacionID);
                    }


                    ListaObjetosRetorno = AdmEstadosLiquidacion.listarEstadosLiquidacion(objLiquidacion);

                    // Importar con Nuget Newtonsoft -> Newtonsoft.Json  
                    // http://www.newtonsoft.com/json    ->  using Newtonsoft.Json;

                    var json = JsonConvert.SerializeObject(ListaObjetosRetorno);
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);

                }
                catch (Exception ex)
                {

                    // Devuelve mensaje de error al usuario
                    if (modoDebug)
                    {
                        var json = JsonConvert.SerializeObject(ex.ToString());
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }
                    else
                    {
                        var json = JsonConvert.SerializeObject("No se puede realizar la operacion solicitada!");
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }

                    // Envia email a los desarrolladores
                    // ---------------------------------
                    try
                    {
                        string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                        string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                        string _subject = "bksys - Error de Aplicacion!";
                        // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                        _body += ex.ToString();
                        bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                    }
                    catch (Exception)
                    {

                        // Devuelve mensaje de error al usuario por problemas de conexion
                        if (modoDebug == true)
                        {
                            var json = JsonConvert.SerializeObject("Hubo un Problema al intentar enviar el email a los desarrolladores!");
                            Response.Clear();
                            Response.ContentType = "application/json; charset=utf-8";
                            Response.Write(json);
                        }

                    }

                }

            }
            else
            {
                Response.Redirect("dashboard.aspx", false);
            }
            
        }
    }
}