﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Presentacion.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>bksys - Login</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <div class="container" style="height: 100%;">
        <!-- Page Content goes here -->
        <div class="row">
            <div class="col s12 m12">

                <form id="form1" runat="server">

                    <%--Mensajes de Error--%>
                    <asp:Label ID="LBLAlert" runat="server" Text="" CssClass="hide"></asp:Label>

                    <div>
                        <br />
                        <br />
                        <br />
                        <div class="valign-wrapper" id="login-box">

                            <div class="valign">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <img class="activator" id="logoLogin" src="img/logobksys-01.svg">
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">Ingreso al Sistema<i class="material-icons right">more_vert</i></span>
                                        <p><a href="#">Ayuda</a></p>
                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">Ingrese sus Datos<i class="material-icons right">close</i></span><br />


                                        <%--TEXTBOX DE USUARIO--%>
                                        <div class="row">
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>
                                            <div class="input-field col m8 s12">
                                                <i class="material-icons prefix">account_circle</i>
                                                <asp:TextBox ID="TB_nombre" runat="server"></asp:TextBox>
                                                <%--<label for="icon_prefix" class="">ingrese su Usuario</label>--%>
                                                <label for="TB_nombre" class="">ingrese su Usuario</label>
                                            </div>
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>
                                        </div>

                                        <%--TEXTBOX DE PASSWORD--%>
                                        <div class="row">
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>
                                            <div class="input-field col m8 s12">
                                                <i class="material-icons prefix">lock</i>
                                                <asp:TextBox ID="TB_Pass" runat="server" TextMode="Password"></asp:TextBox>
                                                <label for="TB_Pass" class="">ingrese su Contraseña</label>
                                            </div>
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>
                                        </div>


                                        <%--BOTON--%>
                                        <div class="row">
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>
                                            <div class="col m8 s12">
                                                <asp:LinkButton ID="LB_iniciarSesion" CssClass="waves-effect waves-light btn" runat="server" OnClick="LB_iniciarSesion_Click"><i class="material-icons left">assignment_turned_in</i>Iniciar Sesion</asp:LinkButton>
                                            </div>
                                            <div class="col m2 hide-on-small-only">&nbsp;</div>

                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div>


                    </div>

                    <br />
                    <br />
                    <br />
                </form>
                <asp:PlaceHolder ID="PHjsFunciones" runat="server"></asp:PlaceHolder>
            </div>
        </div>

    </div>

</body>
</html>
