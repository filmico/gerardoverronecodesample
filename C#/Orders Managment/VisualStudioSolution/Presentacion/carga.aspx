﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="carga.aspx.cs" Inherits="Presentacion.carga" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>carga</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.min.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>


    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <div class="row">
        <form id="form1" runat="server" class="col s12">
            <div class="row">
                <form class="col s12">

                    <div class="row">
                        <div class="col s12 m5">
                            <div class="card-panel">
                                <span class="teal-text">
                                    
                        <div class="input-field">
                            <i class="material-icons prefix">settings_input_component</i>
                            <asp:TextBox ID="TBOperacion"  runat="server" CssClass="validate"></asp:TextBox>
                            
                            <%--<input id="icon_prefix" type="text" class="validate">--%>
                            <label for="icon_prefix">tipo de Consulta</label>
                        </div>
                                                   <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn waves-effect waves-light" name="action" OnClick="LinkButton1_Click">consultar
                            <i class="material-icons right">send</i>
                    </asp:LinkButton>     
                                    
                                   
                                </span>
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </form>
    </div>
</body>
</html>
