﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Configuration;
using System.Configuration;

namespace Presentacion
{
    public partial class cargaDatos : System.Web.UI.Page
    {

        bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            string modo = Request.QueryString["modo"];
            string estado = Request.QueryString["estado"];

            try
            {
                // AUTOMATICO
                if (string.IsNullOrEmpty(modo) || string.IsNullOrWhiteSpace(modo))
                {


                    // Loguea el Job Automatico
                    // -------------------------                
                    CargaDatosLog DatosLog = new CargaDatosLog();   // Instancia para cargar la propiedad de la hora actual
                    DatosLog.fechaEjecucionCargaDatos = Negocio.CargadorWeb.conversionHoraria();
                    AdmCargaDatosLog.agregarCargaDatosLog(DatosLog);    // Logueamos la fecha y hora en que se corrio esta pagina


                    // Pide la plantilla de estados de Expediente a la Base de Datos
                    List<expedientesEstado> ListaObjetosRetorno = new List<expedientesEstado>();
                    expedientesEstado Objeto = new expedientesEstado();
                    ListaObjetosRetorno = Negocio.AdmExpedientesEstado.listarExpedientesEstado(Objeto);

                    // Carga todos los expedientes excepto los de estado 18 que se cargan con el segundo job
                    foreach (var estadosExpediente in ListaObjetosRetorno)
                    {

                        if (estadosExpediente.ExpedienteEstadoID != 18)
                        {
                            Negocio.CargadorWeb.cargaWeb(estadosExpediente.ExpedienteEstadoID);
                        }
                    }

                    // Carga solo los de estado 18
                    // Negocio.CargadorWeb.cargaWeb(18);


                }
                else // MANUAL
                {
                    if (string.IsNullOrEmpty(estado) == false && string.IsNullOrWhiteSpace(estado) == false)
                    {
                        Negocio.CargadorWeb.cargaWeb(Convert.ToInt32(estado), 1);
                    }
                }


                List<estadoSincronizacion> ListaRetornoEstadoSincro = new List<estadoSincronizacion>();
                estadoSincronizacion ObjEstadoSincronizacion = new estadoSincronizacion();

                ObjEstadoSincronizacion.ExpedienteEstado.ExpedienteEstadoID = Convert.ToInt32(estado);

                ListaRetornoEstadoSincro = Negocio.AdmEstadoSincronizacion.listarEstadosSincronizacion(ObjEstadoSincronizacion);

                var json = JsonConvert.SerializeObject(ListaRetornoEstadoSincro);
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);

            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario
                if (modoDebug)
                {
                    var json = JsonConvert.SerializeObject(ex.ToString());
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }
                else
                {
                    var json = JsonConvert.SerializeObject("No se puede realizar la operacion solicitada!");
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }

                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        var json = JsonConvert.SerializeObject("Hubo un Problema al intentar enviar el email a los desarrolladores!");
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }

                }

            }

        }
    }
}