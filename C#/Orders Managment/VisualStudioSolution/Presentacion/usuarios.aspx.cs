﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Entidades;
using Negocio;
using System.Web.Configuration;

namespace Presentacion
{
    public partial class usuarios : System.Web.UI.Page
    {
        bool modoDebug = Convert.ToBoolean(WebConfigurationManager.AppSettings["modoDebug"]);
        usuario usr = new usuario();

        NameValueCollection coleccionCampos = new NameValueCollection();

        protected void Page_Load(object sender, EventArgs e)
        {

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];

            // Si el usuario no es del tipo Administrador lo redirecciona al dashboard
            if (this.usr.UsuarioTipoID == 3)
            {

                bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);
                PHjs.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/usuarios.js", "Scripts/usuarios.min.js")));
                PHjsFunciones.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/funciones.js", "Scripts/funciones.min.js")));
            }
            else
            {
                Response.Redirect("dashboard.aspx", false);
            }

        }

        protected void BTNAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario entidad = new usuario();
                int retorno;
                entidad = cargarObjeto();
                entidad.SolicitanteUsuarioID = this.usr.UsuarioID;
                retorno = Negocio.AdmUsuario.agregarUsuarioSinLogin(entidad);

                if (retorno == 0)
                {
                    LBLAlert.Text = "El usuario " 
                        + entidad.Nombre
                        + " se agregó correctamente";
                    LBLAlert.ToolTip = "green";
                }
                else
                {
                    LBLAlert.Text = "El Usuario " 
                        + entidad.Nombre
                        + " no se pudo agregar.";
                    LBLAlert.ToolTip = "amber";
                }

            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario por problemas de conexion
                if (modoDebug == true)
                {
                    LBLAlert.Text = "Hubo un Problema al actualizar el Usuario. " + ex.ToString();
                    LBLAlert.ToolTip = "pink";
                }
                else
                {
                    LBLAlert.Text = "Hubo un Problema.Inténtelo más tarde.";
                    LBLAlert.ToolTip = "pink";
                }


                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrolladores. " + ex.ToString();
                        LBLAlert.ToolTip = "pink";
                    }

                }

            }



        }

        protected void BTNActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario entidad = new usuario();
                int retorno;
                entidad = cargarObjeto();
                entidad.SolicitanteUsuarioID = this.usr.UsuarioID;
                retorno = Negocio.AdmUsuario.actualizarUsuario(entidad);

                if (retorno == 0)
                {
                    LBLAlert.Text = "El usuario " 
                        + entidad.Nombre
                        + " se actualizó correctamente";
                    LBLAlert.ToolTip = "green";
                } else
                {
                    LBLAlert.Text = "El Usuario " 
                        + entidad.Nombre
                        + " no se pudo actualizar.";
                    LBLAlert.ToolTip = "amber";
                }
            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario por problemas de conexion
                if (modoDebug == true)
                {
                    LBLAlert.Text = "Hubo un Problema al actualizar el Usuario. " + ex.ToString();
                    LBLAlert.ToolTip = "pink";
                }
                else
                {
                    LBLAlert.Text = "Hubo un Problema.Inténtelo más tarde.";
                    LBLAlert.ToolTip = "pink";
                }


                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrolladores. " + ex.ToString();
                        LBLAlert.ToolTip = "pink";
                    }

                }

            }


        }

        protected void BTNBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario entidad = new usuario();
                int retorno;
                entidad = cargarObjeto();
                entidad.SolicitanteUsuarioID = this.usr.UsuarioID;
                retorno = Negocio.AdmUsuario.borrarUsuario(entidad);

                if (retorno == 0)
                {
                    LBLAlert.Text = "El usuario " + entidad.Nombre + " se dio de baja correctamente";
                    LBLAlert.ToolTip = "green";
                }
                else
                {
                    LBLAlert.Text = "El Usuario " + entidad.Nombre + " no se pudo dar de baja.";
                    LBLAlert.ToolTip = "amber";
                }

            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario por problemas de conexion
                if (modoDebug == true)
                {
                    LBLAlert.Text = "Hubo un Problema al dar de baja el Usuario. " + ex.ToString();
                    LBLAlert.ToolTip = "pink";
                }
                else
                {
                    LBLAlert.Text = "Hubo un Problema al dar de baja el Usuario.";
                    LBLAlert.ToolTip = "pink";
                }


                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrolladores. " + ex.ToString();
                        LBLAlert.ToolTip = "pink";
                    }

                }

            }


        }

        internal usuario cargarObjeto()
        {
            coleccionCampos = Request.Form;
            string[] nombresCampos = coleccionCampos.AllKeys;
            usuario objUsuario = new usuario();
            objUsuario.UsuarioEstadoID = this.usr.UsuarioID;
            for (int i = 0; i < coleccionCampos.Count; i++)
            {
                if (nombresCampos[i] == "TB_nombre")
                {
                    objUsuario.Nombre = coleccionCampos[i];
                }

                if (nombresCampos[i] == "TB_email")
                {
                    objUsuario.Email = coleccionCampos[i];
                }

                if (nombresCampos[i] == "TB_password")
                {
                    objUsuario.Password = coleccionCampos[i];
                }
                if (nombresCampos[i] == "usuarioTipo")
                {
                    objUsuario.UsuarioTipoID = Convert.ToInt32(coleccionCampos[i]);
                }
                if (nombresCampos[i] == "usuarioEstado")
                {
                    objUsuario.UsuarioEstadoID = 2;
                }
                if (nombresCampos[i] == "TB_usuarioID")
                {
                    if (Convert.ToInt32(coleccionCampos[i]) > 0)
                    {
                        objUsuario.UsuarioID = Convert.ToInt32(coleccionCampos[i]);
                    }
                    
                }
            }
            return objUsuario;
        }
    }
}