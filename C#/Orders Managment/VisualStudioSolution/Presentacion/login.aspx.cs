﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Security;
using System.Web.Configuration;


namespace Presentacion
{
    public partial class Login : System.Web.UI.Page
    {

        /// <summary>
        /// Instancia un usuario
        /// </summary>
        usuario usr = new usuario();
        bool modoDebug = Convert.ToBoolean(WebConfigurationManager.AppSettings["modoDebug"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
            // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            // Apaga el history del navegador para que no guarde esta pagina.
            Response.Cache.SetAllowResponseInBrowserHistory(false);


            if (modoDebug == true)
            {
                TB_nombre.Text = "animatorstudio";
                TB_Pass.Text = "1234";
            }

            this.Page.Form.DefaultButton = LB_iniciarSesion.UniqueID; // permite submitir con enter y no sólo con click en el botón


            // Carga funciones.js o funciones.min.js dependiendo del estado de modoDebug
            PHjsFunciones.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/funciones.js", "Scripts/funciones.js")));

        }

        protected void LB_iniciarSesion_Click(object sender, EventArgs e)
        {

            // Guardar la variable de sesion del usuario
            if (!string.IsNullOrEmpty(Session["Usuario"] as string))
            {
                this.usr = (usuario)Session["Usuario"];
            }

            // Verifica que los validadores esten OK

            if ((Page.IsValid) && (!String.IsNullOrEmpty(TB_nombre.Text)) && (!String.IsNullOrEmpty(TB_Pass.Text)))
            {


                try
                {

                    // Instanciamos un usuario
                    usuario usrLogin = new usuario();

                    usrLogin.Nombre = TB_nombre.Text;
                    usrLogin.Password = TB_Pass.Text;

                    // Instancia una lista de la antidad que queremos usar
                    List<usuario> ListaObjetosRetorno = new List<usuario>();


                    // Obtenemos las credenciales del usuario en la base de datos
                    ListaObjetosRetorno = Negocio.AdmUsuario.autenticarUsuario(usrLogin);

                    // Autenticamos al usuario
                    if ((ListaObjetosRetorno[0].UsuarioID > 0) & (ListaObjetosRetorno[0].Autenticado == 1))
                    {

                        Session["Usuario"] = ListaObjetosRetorno[0];

                        // Autorizar al usuario (Carga la pagina que indica en web.config en defaultUrl)
                        FormsAuthentication.RedirectFromLoginPage(ListaObjetosRetorno[0].Nombre.ToString(), false);
                    }
                    else
                    {
                        // Blanquea los campos de ingreso
                        TB_nombre.Text = "";
                        TB_Pass.Text = "";

                        // Response.Write("<BR/><BR/>" + "Usuario / Contraseña Incorrecta!");

                        // Devuelve mensaje de error al usuario por problemas de conexion
                        LBLAlert.Text = "Usuario o Contraseña erronea! ";
                        LBLAlert.ToolTip = "red";

                    }

                }

                catch (Exception ex)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        LBLAlert.Text = "Hubo un Problema de Conexión a Base de Datos. " + ex.ToString();
                        LBLAlert.ToolTip = "pink";
                    }
                    else
                    {
                        LBLAlert.Text = "Hubo un Problema.Inténtelo más tarde.";
                        LBLAlert.ToolTip = "pink";
                    }


                    // Envia email a los desarrolladores
                    // ---------------------------------
                    try
                    {
                        string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                        string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                        string _subject = "bksys - Error de Aplicacion!";
                        // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                        _body += ex.ToString();
                        bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                    }
                    catch (Exception)
                    {

                        // Devuelve mensaje de error al usuario por problemas de conexion
                        if (modoDebug == true)
                        {
                            LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrolladores. " + ex.ToString();
                            LBLAlert.ToolTip = "pink";
                        }

                    }

                }


            }


        }


    }
}