﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Globalization;

namespace Presentacion
{
    public partial class carga : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int tipoConsulta = Convert.ToInt32(TBOperacion.Text);
            int retorno = Negocio.CargadorWeb.cargaWeb(tipoConsulta);
            Response.Write(retorno);

        }
    }
}