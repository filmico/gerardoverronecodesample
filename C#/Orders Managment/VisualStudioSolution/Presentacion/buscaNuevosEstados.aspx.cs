﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Configuration;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Presentacion
{
    public partial class buscaNuevosEstados : System.Web.UI.Page
    {
        bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                // Instancia una lista del tipo expedientesEstado para cargarla mas abajo con la data de la BD
                List<expedientesEstado> ListaObjetosRetorno = new List<expedientesEstado>();

                // Instancia un Objeto del tipo ObjexpedientesEstado para enviarlo como parametro al negocio
                expedientesEstado ObjExpedientesEstado = new expedientesEstado();

                // Pide la plantilla de estados de Expediente a la Base de Datos
                ListaObjetosRetorno = Negocio.AdmExpedientesEstado.listarExpedientesEstado(ObjExpedientesEstado);

                // Cicla del 1 al 300 buscando nuevos expedientes en el server externo y salteando los ID que ya tenemos
                for (int i = 1; i < 2; i++)
                {

                    int index = ListaObjetosRetorno.FindIndex(item => item.ExpedienteEstadoID == i);

                    // Rastrear los ID que no tenemos (>=0 Ya existen)
                    if (index < 0)
                    {
                        // Response.Write("El ID: " + i + " No Existe<br/>");

                        string url = "http://localhost:14552/emulaServerExterno.html?estado=" + i;

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        request.Timeout = 999999999;

                        request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

                        //request.Accept = "text/html";
                        request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.Default, true);

                        // StringBuilder html = new StringBuilder(sr.ReadToEnd());
                        StringBuilder html = new StringBuilder(sr.ReadToEnd());

                        // Limpiamos el html
                        html.Replace(@"<td  >", "<td >");

                        // Usamos una expresion regular para encontrar la ocurrencia de dos etiquetas y nos quedamos con el contenido
                        // interno de las etiquetas. 
                        MatchCollection m1 = Regex.Matches(html.ToString(), @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>", RegexOptions.Singleline);

                        // m1[18] Es la columna que tiene el Nombre del Estado (Ej. Formal, Legales, etc)
                        // Si m1.count no supera los 11 quiere decir que ese estado no hay que considerarlo por no tener datos.

                        if (m1.Count > 11)
                        {
                            string estadoNuevo = m1[18].ToString();

                            // Limpiamos los <td> </td> del nombre del Estado
                            estadoNuevo = estadoNuevo.Replace(@"<td >", "");
                            estadoNuevo = estadoNuevo.Replace(@"<td>", "");
                            estadoNuevo = estadoNuevo.Replace(@"</td>", "");

                            if (modoDebug)
                            {
                                Response.Write("Nuevo estado!!!! ID: " + i + " " + estadoNuevo + "<br/>");
                            }

                            // Envia email a los desarrolladores avisando del nuevo estado
                            // -----------------------------------------------------------
                            string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                            string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                            string _subject = "bksys - Error de Aplicacion!";
                            string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                            _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                            _body += ("Nuevo estado!!!! ID: " + i + " " + estadoNuevo + "<br/>");
                            bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);

                            // Guarda el Nuevo estado en la BD e inserta un alta de dia y horario de sincronizacion
                            // -------------------------------------------------------------------------------------

                            // Instancia un Objeto del tipo ObjexpedientesEstado para enviarlo como parametro al negocio
                            expedientesEstado ObjExpedientesEstado2 = new expedientesEstado();

                            // Carga el objeto
                            ObjExpedientesEstado2.ExpedienteEstadoID = i;
                            ObjExpedientesEstado2.Descripcion = estadoNuevo;

                            // Guarda el nuevo estado en la Base de Datos
                            int altaNuevoEstado = Negocio.AdmExpedientesEstado.agregarExpedienteEstado(ObjExpedientesEstado2);


                            // Instancia un Objeto del tipo ConfigSincronizaciones para cargarlo y enviarlo a la base de datos
                            configSincronizacion ObjConfigSincronizaciones = new configSincronizacion();

                            // Carga el objeto
                            ObjConfigSincronizaciones.DiaSemanaComienzo = 1;
                            ObjConfigSincronizaciones.DiaSemanaFinalizacion = 5;
                            ObjConfigSincronizaciones.HoraComienzo = Convert.ToDateTime("1980-01-01 08:00");
                            ObjConfigSincronizaciones.HoraFinalizacion = Convert.ToDateTime("1980-01-01 19:00");
                            ObjConfigSincronizaciones.Estado = 1;
                            ObjConfigSincronizaciones.EstadoExpedienteID = i;


                            // Inserta una nueva configuracion de Sincronizacion
                            int altaNuevaConfigSincronizaciones = Negocio.AdmConfigSincronizacion.agregarConfigSincronizacion(ObjConfigSincronizaciones);


                            // Inserta un estado de sincronizacion al 1980 para que sincronice en el próximo automatismo.
                            estadoSincronizacion ObjEstadoSincronizacion = new estadoSincronizacion();
                            ObjEstadoSincronizacion.ExpedienteEstado.ExpedienteEstadoID = i;
                            ObjEstadoSincronizacion.UltimaSincronizacionComienzo = Convert.ToDateTime("1980-01-01");
                            ObjEstadoSincronizacion.UltimaSincronizacionFinalizacion = Convert.ToDateTime("1980-01-01");
                            ObjEstadoSincronizacion.EstadoSincronizacion = 1;
                            ObjEstadoSincronizacion.ExpedientesObtenidos = 0;
                            ObjEstadoSincronizacion.ExpedientesPosibles = 0;
                            ObjEstadoSincronizacion.ExpedientesActualizados = 0;

                            int altaNuevoEstadoSincronizacion = Negocio.AdmEstadoSincronizacion.agregarEstadoSincronizacion(ObjEstadoSincronizacion);

                            Response.Write(altaNuevoEstadoSincronizacion);
                        }
                        else
                        {
                            if (modoDebug)
                            {
                                Response.Write("El ID: " + i + " No Tiene Estado" + "<br/>");
                            }

                        }

                    }

                }


            }
            catch (Exception ex)
            {

                if (modoDebug)
                {
                    Response.Write(ex.ToString());
                }

                
            }

        }
    }
}