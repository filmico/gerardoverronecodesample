﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using System.Dynamic;
using Newtonsoft.Json;
using System.Configuration;

namespace Presentacion
{
    public partial class consultaExpedientes : System.Web.UI.Page
    {

        /// <summary>
        /// Instancia un usuario
        /// </summary>
        usuario usr = new usuario();

        bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
            // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            // Apaga el history del navegador para que no guarde esta pagina.
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];

            List<expediente> ListaObjetosRetorno = new List<expediente>();
            expediente Objeto = new expediente();

            // Carga el ID del solicitante en el objeto expediente
            Objeto.SolicitanteUsuarioID = this.usr.UsuarioID;

            // Instancia las clases que vamos a utilizar
            empresa emp = new empresa();
            expediente exp = new expediente();
            expedientesEstado expEst = new expedientesEstado();


            try
            {

                /*
                    Ejemplos Query String
                    --------------------
                    http://localhost:14552/consultaexpedientes.aspx?
                    http://localhost:14552/consultaexpedientes.aspx?EmpresaCuitID=20060585289
                    http://localhost:14552/consultaexpedientes.aspx?sinAsignacion=1&pendiente=1&aLiquidar=1&liquidado=1
                */


                // Obtiene las variables del QueryString
                Int64 EmpresaCuitID = Convert.ToInt64(Request.QueryString["EmpresaCuitID"]);
                string NroEmpresa = Request.QueryString["NroEmpresa"];
                string RazonSocial = Request.QueryString["RazonSocial"];
                Int64 ExpedienteNumeroID = Convert.ToInt64(Request.QueryString["ExpedienteNumeroID"]);
                string ExpedienteEstadoDescripcion = Request.QueryString["EstadoDescripcion"];

                // Carga el objeto con el valor de busqueda que contiene el QueryString
                if (EmpresaCuitID != 0)
                {
                    Objeto.Empresa.EmpresaCuitID = EmpresaCuitID;
                }
                else if (NroEmpresa != null)
                {
                    Objeto.Empresa.NroEmpresa = NroEmpresa;
                }
                else if (RazonSocial != null)
                {
                    Objeto.Empresa.RazonSocial = RazonSocial;
                }
                else if (ExpedienteNumeroID != 0)
                {
                    Objeto.ExpedienteNumeroID = ExpedienteNumeroID;
                }
                else if (ExpedienteEstadoDescripcion != null)
                {
                    Objeto.ExpedienteEstado.Descripcion = ExpedienteEstadoDescripcion;
                }

                // Para el filtro de estado de Liquidacion
                if (this.usr.UsuarioTipoID > 1)
                {
                    // Filtro de Estado de Liquidacion

                    if (Request.QueryString["sinAsignacion"] == null)
                    {
                        Objeto.sinAsignacion = 1;
                    }
                    else
                    {
                        Objeto.sinAsignacion = 0;   // En cero significa que no trae este estado
                    }

                    if (Request.QueryString["pendiente"] == null)
                    {
                        Objeto.pendiente = 2;
                    }
                    else
                    {
                        Objeto.pendiente = 0;   // En cero significa que no trae este estado
                    }

                    if (Request.QueryString["aLiquidar"] == null)
                    {
                        Objeto.aLiquidar = 3;
                    }
                    else
                    {
                        Objeto.aLiquidar = 0;   // En cero significa que no trae este estado
                    }

                    if (Request.QueryString["liquidado"] == null)
                    {
                        Objeto.liquidado = 4;
                    }
                    else
                    {
                        Objeto.liquidado = 0;   // En cero significa que no trae este estado
                    }

                    if (Request.QueryString["finalizado"] == null)
                    {
                        Objeto.finalizado = 5;
                    }
                    else
                    {
                        Objeto.finalizado = 0;
                    }
                }

                // Lista expedientes de la base de datos.
                ListaObjetosRetorno = Negocio.AdmExpediente.listarExpedientes(Objeto);

                // Importar con Nuget Newtonsoft -> Newtonsoft.Json  
                // http://www.newtonsoft.com/json    ->  using Newtonsoft.Json;

                var json = JsonConvert.SerializeObject(ListaObjetosRetorno);
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);


            }
            catch (Exception ex)
            {
                // Devuelve mensaje de error al usuario
                if (modoDebug)
                {
                    var json = JsonConvert.SerializeObject(ex.ToString());
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }
                else
                {
                    var json = JsonConvert.SerializeObject("No se puede realizar la operacion solicitada!");
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }

                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        var json = JsonConvert.SerializeObject("Hubo un Problema al intentar enviar el email a los desarrolladores!");
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }

                }
            }


        }
    }
}