﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Configuration;

namespace Presentacion
{
    public partial class consultaExpedientesEstado : System.Web.UI.Page
    {

        bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
                // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
                // Apaga el history del navegador para que no guarde esta pagina.
                Response.Cache.SetAllowResponseInBrowserHistory(false);

                List<expedientesEstado> ListaObjetosRetorno = new List<expedientesEstado>();
                expedientesEstado Objeto = new expedientesEstado();

                ListaObjetosRetorno = Negocio.AdmExpedientesEstado.listarExpedientesEstado(Objeto);

                // Importar con Nuget Newtonsoft -> Newtonsoft.Json  
                // http://www.newtonsoft.com/json    ->  using Newtonsoft.Json;

                var json = JsonConvert.SerializeObject(ListaObjetosRetorno);
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);

            }
            catch (Exception ex)
            {
                // Devuelve mensaje de error al usuario
                if (modoDebug)
                {
                    var json = JsonConvert.SerializeObject(ex.ToString());
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }
                else
                {
                    var json = JsonConvert.SerializeObject("No se puede realizar la operacion solicitada!");
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }

                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        var json = JsonConvert.SerializeObject("Hubo un Problema al intentar enviar el email a los desarrolladores!");
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }

                }
            }


        }
    }
}