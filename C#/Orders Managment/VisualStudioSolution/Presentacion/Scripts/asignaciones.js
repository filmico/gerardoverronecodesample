﻿$(document).ready(function() {
	var grupoEmpresa = $('#grupoEmpresa');
	var grupoTipoUsuario = $('#grupoTipoUsuario');
	var grupoUsuario = $('#grupoUsuario');
	var grupoPorcentaje = $('#grupoPorcentaje');
	var selectEmpresas = $('#empresa');
	var selectTipoUsuario = $('#tipoUsuario');
	var selectUsuario = $('#usuario');
	var tbPorcentaje = $('#TB_porcentaje');
	var btnActualizar = $('#BTNActualizar');
	var btnAgregar = $('#BTNAgregar');

	/* Funcion que corre al salir del modal para apagar el boton (+) */
	function apagarAgregaAsignacion() {
		$('#agregaAsignacion').addClass('disabled');
	}

	function apagarCerrar() {
		$('#cierraModalAbajo').addClass('disabled');
	}

	/* Funcion que corre al salir del modal para volver a encender el boton (+) */
	var encenderBotones = function() {
		$('#agregaAsignacion').removeClass('disabled');
		$('#cierraModalAbajo').removeClass('disabled');
		$('.btn-floating.btn-large').removeClass('disabled');
	};

	function activaSelect() {
        // body...
        $('select').material_select();
    }

    function reiniciaSelectTipoUsuario() {
    	selectTipoUsuario.html('');
    	/* cargar tipo de usuario en el select */
    	selectTipoUsuario.append('<option value="0" disabled selected>Elija un Tipo de Usuario</option>');
    	selectTipoUsuario.append('<option value="1">Gestor</option>')
    	selectTipoUsuario.append('<option value="2">Socio</option>')
    	selectTipoUsuario.append('<option value="3">Administrador</option>')
    	activaSelect();
    }
    reiniciaSelectTipoUsuario();

    function cargaSelectUsuarioPorTipo(objeto, UsuarioTipoID, selectID, UsuarioID) {

    	var selectUsuario = $('#' + selectID);
    	selectUsuario.html('');
    	var UsuariosPorTipo = [];
    	for (i in objeto) {
    		if (objeto[i].UsuarioTipoID == UsuarioTipoID) {
    			UsuariosPorTipo.push(objeto[i]);
    		}
    	}
    	/* carga el select con el tipo de usuario que se le pasó */
    	for (i in UsuariosPorTipo) {
    		selectUsuario.append('<option value="' + UsuariosPorTipo[i].UsuarioID + '">' + UsuariosPorTipo[i].Nombre + '</option>')
    	}

    	if (UsuarioID != null) {
    		selectUsuario.val(parseInt(UsuarioID));
    	};

    	activaSelect();
    }

    var asignacionesCard = $('.asignaciones-card');
    asignacionesCard.append('<li class="collection-header"><h4>Asignaciones</h4></li>');

    // arma las asignaciones por empresa
    function armaAsignaciones(objeto) {
    	var str = '';
    	for (i in objeto) {

            var AutoAsignados = '';
            var Palabra = '';
            objeto[i].AutoAsignados > 1 ? Palabra = 'Autoasignados' : Palabra = 'Autoasignado'

            objeto[i].AutoAsignados > 0 ? 
            AutoAsignados = '<span class="new badge bold" data-badge-caption="' 
            + Palabra
            + '" style="text-transform: uppercase;">' 
            + objeto[i].AutoAsignados 
            + '</span>' 
            : AutoAsignados = '';

    		str = str + ('<li EmpresaCuitID="'
    			+ objeto[i].Empresa.EmpresaCuitID
    			+ '"><div class="collapsible-header"><div class="row"><div class="col s12 m12 l12"><i class="material-icons">more_vert</i><span class="light-blue-text">'
    			+ formatoCUIT(objeto[i].Empresa.EmpresaCuitID)
    			+ '</span> - <span>'
    			+ objeto[i].Empresa.NroEmpresa
    			+ '</span> - <span>'
    			+ objeto[i].Empresa.RazonSocial
                + AutoAsignados
    			+ '</span></div></div></div><div class="collapsible-body"></div></li>')
    	}
    	return str;
    }

    // arma la tabla de expedientes
    function tablaExpedientes(objeto) {
    	var tabla;
    	var cabecerasTabla = [];

        // CABECERA
        cabecerasTabla.push('Expediente', 'Solicitud', 'Estado', 'Fecha', 'Solicitado', 'Aprobado', 'Evaluador', 'Evaluador Técnico', 'info');

        tabla = '<table class="dashboard-table bordered striped">'
        var thead = '<thead><tr class="blue-text">'

        for (i in cabecerasTabla) {
        	if (cabecerasTabla[i] == 'Solicitud'
        		|| cabecerasTabla[i] == 'Solicitado'
        		|| cabecerasTabla[i] == 'Evaluador'
        		|| cabecerasTabla[i] == 'Evaluador Técnico'
        		) {
        		thead = thead + '<th class="hide-on-med-and-down">' + cabecerasTabla[i] + '</th>';
        } else {
        	thead = thead + '<th>' + cabecerasTabla[i] + '</th>';
        }
    }
    if (tipoUsuario == 3) {
    	thead = thead + '<th class="hide-on-med-and-down">Liq</th>'
    }

    thead = thead + '</tr></thead>';
    tabla = tabla + thead;


        // FILAS
        var filas = '<tbody>'
        for (i in objeto) {
            objeto[i].AutoAsignado != 0 ? clase = 'textoRojo' : clase = '';

        	var montos = '<td class="hide-on-med-and-down"> $ '

        	filas = filas + '<tr><td>'
        	+ formatoNroExpediente(objeto[i].ExpedienteNumeroID)
        	+ '</td><td class="hide-on-med-and-down">'
        	+ formatoNroSolicitud(objeto[i].NroSolicitud)
        	+ '</td><td class="bold ExpedienteEstadoID">'
        	+ objeto[i].ExpedienteEstado.Descripcion
        	+ '</td><td>'
        	+ formatoFechaCorta(fechaJs(objeto[i].FechaEstadoActual))
        	+ '</td><td class="hide-on-med-and-down"> $ '
        	+ local(objeto[i].BeneficioSolicitado)
        	+ '</td><td> $ '
        	+ local(objeto[i].BeneficioAprobado)
        	+ '</td><td class="hide-on-med-and-down">'
        	+ objeto[i].Evaluador.Nombre
        	+ '</td><td class="hide-on-med-and-down">'
        	+ objeto[i].EvaluadorTecnico.Nombre
        	+ '</td><td class="info punteroLink" nroExpediente="'
        	+ objeto[i].ExpedienteNumeroID
        	+ '" aprobado="'
        	+ objeto[i].BeneficioAprobado
        	+ '"><span><i class="material-icons ' 
            + clase
            + '">info_outline</i></span></td>'

        	if (tipoUsuario == 3) {
        		filas = filas
        		+ '<td class="liq hide-on-med-and-down"><span EstadoLiquidacionID="'
        		+ objeto[i].EstadoLiquidacionID
        		+ '" ExpedienteNumeroID="'
        		+ objeto[i].ExpedienteNumeroID
        		+ '" class="punteroLink cambiaEstadoLiq"><i class="material-icons">monetization_on</i></span></td>'
        	}
        	filas = filas + '</tr>';
        	filas = filas + '<tr id="'
        	+ objeto[i].ExpedienteNumeroID
        	+ '"></tr>'
        }

        filas = filas + '</tbody>';
        tabla = tabla + filas;
        tabla = tabla + '</table>';

        return tabla;
    }

    /* Armar tabla de Usuarios Asignados */
    function armaUsuariosAsignados(objeto) {
    	var tabla;
    	var cabecerasTabla = [];

    	tabla = '<table class="usuariosAsignados-table modal-table bordered striped">'
        // CABECERA
        cabecerasTabla.push('Tipo de Usuario', 'Usuario', 'Porcentaje', 'Acciones');
        var thead = '<thead>'

        for (i in cabecerasTabla) {
        	thead += '<th>' + cabecerasTabla[i] + '</th>'
        }
        thead += '</thead>'
        tabla += thead;
        //CUERPO
        var tbody = '<tbody>'

        for (i in objeto) {
        	tbody += '<tr>'
        	tbody += '<td UsuarioTipoID="'
        	+ objeto[i].Usuario.UsuarioTipoID
        	+ '">'
        	+ objeto[i].Usuario.UsuarioTipoDescripcion
        	+ '</td><td UsuarioID="'
        	+ objeto[i].Usuario.UsuarioID
        	+ '" class="bold">'
        	+ objeto[i].Usuario.Nombre
        	+ '</td><td porcentaje="'
        	+ objeto[i].Porcentaje
        	+ '">'
        	+ local(objeto[i].Porcentaje)
        	+ '%</td><td><i class="material-icons punteroLink edicion">mode_edit</i> '
        	+ '<i class="material-icons punteroLink baja red-text">delete</i></td></td></tr>'
        }

        tabla += tbody;

        tabla += '</table>'
        return tabla;
    }




    /* Empresas Asignadas */
    var EmpresasAsignadas = $.get('consultaexpedientesusuarios.aspx', function(data) {
    	return (data);
    });

    EmpresasAsignadas.then(function success(resEmpresasAsignadas) {
    	var objEmpresasAsignadas = (resEmpresasAsignadas);
    	asignacionesCard.append(armaAsignaciones(objEmpresasAsignadas));
    	var liEmpresa = $('li[Empresacuitid]');



        // guarda el elemento que debe quedar abierto luego del postback;
        var cuitSeleccionado = $('span#opened').html();
        cuitSeleccionado = cuitSeleccionado.trim();

        // Obtiene Nro de Expediente
        var expedienteAbierto = $('span#opened2').html();
        expedienteAbierto = expedienteAbierto.trim();


        if (cuitSeleccionado > 0 || expedienteAbierto > 0) {
        	var padreEmpresa = $('li[empresacuitid=' + cuitSeleccionado + ']');
        	padreEmpresa.addClass('active');
        	padreEmpresa.find('div.collapsible-body').attr('style', 'display: block;');
        	padreEmpresa.trigger('click');

        	$.get('consultaexpedientes.aspx?EmpresaCuitID=' + cuitSeleccionado, function(data) {
        		/*optional stuff to do after success */
        		var ObjExpedientesPorCuit = (data);
        		var Tabla = tablaExpedientes(ObjExpedientesPorCuit);
        		$('li.active').find('table').remove();
        		$('li.active').find('.collapsible-body').append(Tabla);


        		var modalAbajo = $('#modalabajo');
        		var modalAbajoContenido = modalAbajo.find('.modal-content');
        		/* Act on the event */

        		$('#modal1').attr('nroexpediente', expedienteAbierto);
        		$('#modal1').attr('cuitempresaid', cuitSeleccionado);


                // trae todos los usuarios asignados al expediente
                if (expedienteAbierto > 0) {
                	$.get('consultaexpedientesusuarios.aspx?expedientenumeroid=' + expedienteAbierto, function(data) {
                		/*optional stuff to do after success */
                		var objAsignaciones = (data);
                		var tabla = armaUsuariosAsignados(objAsignaciones);
                		modalAbajoContenido.html('')
                		modalAbajoContenido.append('<h5>Editar Asignaciones del Expediente #' + formatoNroExpediente(expedienteAbierto) + '</h5>');
                		modalAbajoContenido.append(tabla);
                		modalAbajoContenido.find('table').attr('nroexpediente', expedienteAbierto);
                		modalAbajo.openModal();

                        // Edición
                        $('#modalabajo td i.edicion').click(function(event) {
                        	enciendeBarraProgreso()
                        	apagarFloatingButton();
                        	apagarCerrar();
                        	apagarAgregaAsignacion();

                        	$('#modal').openModal({
                        		complete: encenderBotones
                        	});
                        	/* grupos */
                        	grupoTipoUsuario.hide();
                        	grupoUsuario.hide();
                        	grupoPorcentaje.hide();
                        	grupoEmpresa.hide();

                        	/* campos */
                        	selectEmpresas.html('');
                        	selectTipoUsuario.html('');

                        	/* botones */
                        	btnAgregar.hide();
                        	btnActualizar.show();

                        	var fila = $(this).closest('tr');
                        	var nroExpediente = fila.closest('table').attr('nroexpediente');
                        	var usuarioID = fila.find('td[usuarioid]').attr('usuarioid');
                        	var usuarioTipoID = fila.find('td[usuariotipoid]').attr('usuariotipoid');
                        	var usuarioTipoDescripcion = fila.find('td[usuariotipoid]').html();
                        	var usuarioNombre = fila.find('td[usuarioid]').html();
                        	var cuitEmpresaID = $('li[empresacuitid].active').attr('empresacuitid');
                        	var Porcentaje = fila.find('td[porcentaje]').attr('porcentaje');

                        	selectTipoUsuario.val(usuarioTipoID);

                        	selectEmpresas.append('<option value="' + cuitEmpresaID + '" selected>0</option>')
                        	selectEmpresas.val(cuitEmpresaID);


                        	$('#modal').find('#nroexpediente').val(nroExpediente);

                        	selectTipoUsuario.append('<option value="' + usuarioTipoID + '" selected>' + usuarioTipoDescripcion + '</option>')
                        	selectTipoUsuario.val(usuarioTipoID);

                        	selectUsuario.append('<option value="' + usuarioID + '" selected>' + usuarioNombre + '</option>')
                        	selectUsuario.val(usuarioID);
                        	tbPorcentaje.val(parseFloat(Porcentaje));
                        	grupoEmpresa.find('i').addClass('active');
                        	grupoTipoUsuario.find('i').addClass('active');
                        	grupoUsuario.find('i').addClass('active');

                        	grupoPorcentaje.find('i').addClass('active');
                        	grupoPorcentaje.find('label').addClass('active');

                        	$.get('consultausuarios.aspx', function(data) {
                        		var objUsuarios = (data);
                        		grupoTipoUsuario.show();
                        		grupoPorcentaje.show();
                        		grupoUsuario.show();
                        		grupoUsuario.show();
                        		activaSelect();


                        		apagaBarraProgreso();
                        	});
                        });

                        // Borrado
                        $('#modalabajo td i.baja').click(function(event) {
                        	/* Act on the event */
                        	selectEmpresas.html('');
                        	selectUsuario.html('');
                        	apagarFloatingButton();
                        	$('#modal1 div.modal-content p').html('');
                        	var padre = $(this).closest('tr');
                        	var nroexpediente = $('#modal1').attr('nroexpediente');

                        	var idUsuario = padre.find('td[usuarioid]').attr('usuarioid');
                        	var nombreUsuario = padre.find('td[usuarioid]').html();
                        	var tipoUsuario = padre.find('td[usuariotipoid]').attr('usuariotipoid');

                        	var cuitEmpresa = 0;

                        	selectEmpresas.append('<option value="0" selected>0</option>')
                        	selectEmpresas.val(cuitEmpresa);

                        	selectUsuario.append('<option value="' + idUsuario + '" selected>' + 'usuario' + '</option>')
                        	selectUsuario.val(idUsuario);

                        	selectTipoUsuario.val(tipoUsuario);
                        	tbPorcentaje.val(1);
                        	$('#nroexpediente').val(nroexpediente);

                        	activaSelect();

                        	$('#modal1 div.modal-content p').append('Ud está a punto de dar de baja la asignación de ' + nombreUsuario + '. Si desea confirmar, haga click en Eliminar.');
                        	$('#modal1').openModal({ complete: encenderBotones });
                        });


});
}



});
apagaBarraProgreso();
}

liEmpresa.click(function(event) {
	var cuit = $(this).attr('empresacuitid');
	enciendeBarraProgreso();
	$('#modal').closeModal();
	$('#modal1').closeModal();
	$('#modalabajo').closeModal();

	$.get('consultaexpedientes.aspx?EmpresaCuitID=' + cuit, function(data) {
		/*optional stuff to do after success */
		var ObjExpedientesPorCuit = (data);
		var Tabla = tablaExpedientes(ObjExpedientesPorCuit);
		$('li.active').find('table').remove();
		$('li.active').find('.collapsible-body').append(Tabla);


		$('.info').click(function(event) {
			var modalAbajo = $('#modalabajo');
			var modalAbajoContenido = modalAbajo.find('.modal-content');
			/* Act on the event */
                    // Obtiene Nro de Expediente
                    var nroexpediente = $(this).closest('td').attr('nroexpediente');
                    $('#modal1').attr('nroexpediente', nroexpediente);
                    $('#modal1').attr('cuitempresaid', cuit);
                    // trae todos los usuarios asignados al expediente
                    $.get('consultaexpedientesusuarios.aspx?expedientenumeroid=' + nroexpediente, function(data) {
                    	/*optional stuff to do after success */
                    	var objAsignaciones = (data);
                    	var tabla = armaUsuariosAsignados(objAsignaciones);
                    	modalAbajoContenido.html('')
                    	modalAbajoContenido.append('<h5 >Editar Asignaciones del Expediente #' + formatoNroExpediente(nroexpediente) + '</h5>');
                    	modalAbajoContenido.append(tabla);
                    	modalAbajoContenido.find('table').attr('nroexpediente', nroexpediente);
                    	modalAbajo.openModal();

                        // Edición
                        $('#modalabajo td i.edicion').click(function(event) {
                        	enciendeBarraProgreso()
                        	apagarFloatingButton();
                        	apagarCerrar();
                        	apagarAgregaAsignacion();

                        	$('#modal').openModal({
                        		complete: encenderBotones
                        	});
                        	/* grupos */
                        	grupoTipoUsuario.hide();
                        	grupoUsuario.hide();
                        	grupoPorcentaje.hide();
                        	grupoEmpresa.hide();

                        	/* campos */
                        	selectEmpresas.html('');
                        	selectTipoUsuario.html('');

                        	/* botones */
                        	btnAgregar.hide();
                        	btnActualizar.show();

                        	var fila = $(this).closest('tr');
                        	var nroExpediente = fila.closest('table').attr('nroexpediente');
                        	var usuarioID = fila.find('td[usuarioid]').attr('usuarioid');
                        	var usuarioTipoID = fila.find('td[usuariotipoid]').attr('usuariotipoid');
                        	var usuarioTipoDescripcion = fila.find('td[usuariotipoid]').html();
                        	var usuarioNombre = fila.find('td[usuarioid]').html();
                        	var cuitEmpresaID = $('li[empresacuitid].active').attr('empresacuitid');
                        	var Porcentaje = fila.find('td[porcentaje]').attr('porcentaje');

                        	selectTipoUsuario.val(usuarioTipoID);

                        	selectEmpresas.append('<option value="' + cuitEmpresaID + '" selected>0</option>')
                        	selectEmpresas.val(cuitEmpresaID);


                        	$('#modal').find('#nroexpediente').val(nroExpediente);

                        	selectTipoUsuario.append('<option value="' + usuarioTipoID + '" selected>' + usuarioTipoDescripcion + '</option>')
                        	selectTipoUsuario.val(usuarioTipoID);

                        	selectUsuario.append('<option value="' + usuarioID + '" selected>' + usuarioNombre + '</option>')
                        	selectUsuario.val(usuarioID);
                        	tbPorcentaje.val(parseFloat(Porcentaje));
                        	grupoEmpresa.find('i').addClass('active');
                        	grupoTipoUsuario.find('i').addClass('active');
                        	grupoUsuario.find('i').addClass('active');

                        	grupoPorcentaje.find('i').addClass('active');
                        	grupoPorcentaje.find('label').addClass('active');

                        	$.get('consultausuarios.aspx', function(data) {
                        		var objUsuarios = (data);
                        		grupoTipoUsuario.show();
                        		grupoPorcentaje.show();
                        		grupoUsuario.show();
                        		grupoUsuario.show();
                        		activaSelect();

                        		apagaBarraProgreso();
                        	});
                        });

                        // Borrado
                        $('#modalabajo td i.baja').click(function(event) {
                        	/* Act on the event */
                        	selectEmpresas.html('');
                        	selectUsuario.html('');
                        	apagarFloatingButton();
                        	$('#modal1 div.modal-content p').html('');
                        	var padre = $(this).closest('tr');
                        	var nroexpediente = $('#modal1').attr('nroexpediente');

                        	var idUsuario = padre.find('td[usuarioid]').attr('usuarioid');
                        	var nombreUsuario = padre.find('td[usuarioid]').html();
                        	var tipoUsuario = padre.find('td[usuariotipoid]').attr('usuariotipoid');

                        	var cuitEmpresa = 0;

                        	selectEmpresas.append('<option value="0" selected>0</option>')
                        	selectEmpresas.val(cuitEmpresa);

                        	selectUsuario.append('<option value="' + idUsuario + '" selected>' + 'usuario' + '</option>')
                        	selectUsuario.val(idUsuario);

                        	selectTipoUsuario.val(tipoUsuario);
                        	tbPorcentaje.val(1);
                        	$('#nroexpediente').val(nroexpediente);

                        	activaSelect();

                        	$('#modal1 div.modal-content p').append('Ud está a punto de dar de baja la asignación de ' + nombreUsuario + '. Si desea confirmar, haga click en Eliminar.');
                        	$('#modal1').openModal({ complete: encenderBotones });
                        });
});
});
});
apagaBarraProgreso();
});


}, function failure(resEmpresasAsignadas) {

})

$('#agregaAsignacion').click(function(event) {
	apagarAgregaAsignacion();
	apagarCerrar();

	var nroexpediente = $('#modal1').attr('nroexpediente');
	$('#modal').find('.modal-content h5').html('');
	$('#modal').find('.modal-content h5').html('Agregar Asignación al expediente: ' + formatoNroExpediente(nroexpediente));
	$('#modal').find('#nroexpediente').val(nroexpediente);

	grupoTipoUsuario.find('i').removeClass('active');
	grupoUsuario.find('i').removeClass('active');

	btnActualizar.hide();
	btnAgregar.show();

	selectEmpresas.html('');
	selectUsuario.html('');

	reiniciaSelectTipoUsuario();

	grupoUsuario.hide();
	grupoPorcentaje.hide();
	grupoEmpresa.hide();
	grupoEmpresa.show();


	var cuitEmpresaID = $('#modal1').attr('cuitempresaid')
	selectEmpresas.append('<option value="' + cuitEmpresaID + '" selected>' + cuitEmpresaID + '</option>')
	selectEmpresas.val(cuitEmpresaID);

	activaSelect();

	selectTipoUsuario.change(function(event) {
		grupoTipoUsuario.find('i').addClass('active');

		/* Act on the event */
		$.get('consultaUsuarios.aspx', function(data) {
			/*optional stuff to do after success */
			var objUsuarios = (data);
			var tipoUsuario = $('#tipoUsuario').val();
			cargaSelectUsuarioPorTipo(objUsuarios, tipoUsuario, 'usuario')
			grupoUsuario.slideDown();
			tbPorcentaje.val(1);
			grupoUsuario.find('i').addClass('active');
			grupoPorcentaje.find('i').addClass('active');
			grupoPorcentaje.find('label').addClass('active');
			grupoPorcentaje.slideDown();

		});
	});

        // Abre el modal y le adjunta una funcion de cierre para re-encender el boton (+)
        // $('#modal').openModal();
        $("#modal").openModal({
        	complete: encenderBotones
        });

    });

$('#nuevaAsignacion').click(function(event) {
	/* Act on the event */

        // Desactiva el Boton + mientras se asigna con el modal			
        apagarFloatingButton();

        enciendeBarraProgreso();
        grupoEmpresa.show();
        grupoTipoUsuario.hide();
        grupoUsuario.hide();
        grupoPorcentaje.hide();
        selectEmpresas.html('');
        selectUsuario.html('');
        btnAgregar.show();
        btnActualizar.hide();

        grupoEmpresa.find('i').removeClass('active');
        grupoTipoUsuario.find('i').removeClass('active');
        $('#modal').find('.modal-content h5').html('');
        $('#modal').find('.modal-content h5').html('Agregar Asignación');

        $.get('consultaEmpresas.aspx').then(function success(res) {
            // body...
            var objEmpresas = (res);

            $.get('consultaUsuarios.aspx', function(data) {
            	/*optional stuff to do after success */
            	var objUsuarios = (data);

            	selectEmpresas.append('<option value="0" disabled selected>Elija una Empresa</option>');
            	for (i in objEmpresas) {
            		selectEmpresas.append('<option value="' + objEmpresas[i].EmpresaCuitID + '">' + objEmpresas[i].RazonSocial + '</option>')
            	}

                // Abre el modal y le adjunta una funcion de cierre para re-encender el boton (+)
                // $('#modal').openModal();
                $("#modal").openModal({
                	complete: encenderFloatingButton
                });

                selectEmpresas.change(function(event) {
                	grupoUsuario.slideUp();
                	grupoPorcentaje.slideUp();
                	grupoTipoUsuario.find('i').removeClass('active');
                	reiniciaSelectTipoUsuario();

                	/* Act on the event */
                	if (selectEmpresas.val() != 0) {
                		grupoTipoUsuario.slideDown();
                		grupoEmpresa.find('i').addClass('active');
                	} else {
                		grupoTipoUsuario.slideUp();
                	}

                });

                selectTipoUsuario.change(function(event) {
                	/* Act on the event */
                	grupoTipoUsuario.find('i').addClass('active');
                	var tipoUsuario = $('#tipoUsuario').val();
                	cargaSelectUsuarioPorTipo(objUsuarios, tipoUsuario, 'usuario')
                	grupoUsuario.slideDown();
                	grupoPorcentaje.slideDown();
                	tbPorcentaje.val(1);
                	grupoUsuario.find('i').addClass('active');
                	grupoPorcentaje.find('i').addClass('active');
                	grupoPorcentaje.find('label').addClass('active');
                });

                selectUsuario.change(function(event) {
                	/* Act on the event */
                	tbPorcentaje.val(1);
                });
                $('#nroexpediente').val(0);

                activaSelect();
                grupoEmpresa.find('ul').addClass('selectEmpresasMaxHeight');

                /* Apaga la barra de progreso */
                apagaBarraProgreso();

            });

}, function failure(res) {
            // body...
        });


});


});