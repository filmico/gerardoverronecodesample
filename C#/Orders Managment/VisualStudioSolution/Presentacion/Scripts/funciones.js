﻿$(".button-collapse").sideNav();
$(".dropdown-button").dropdown();

var URLactual = window.location;
var mataSesion, avisaSesion;

function expulsaUsuario() {
    window.location.assign("login.aspx");
}

/* Función que reformatea el nro de expediente */
function formatoNroExpediente(nro) {
    
    var nroExpediente = '' + nro;
        
    if (nroExpediente.length > 11) {
        var año = nroExpediente.substring(0,4);
        var nro = nroExpediente.substring(4, nroExpediente.length)
        nroExpediente = año + '-' + nro
    } else {
        var ceros = '000000000000000';
        var extensionExpediente = nroExpediente.length;
        var diferenciaExpediente = 11 - extensionExpediente;
        nroExpediente = ceros.substring(0, diferenciaExpediente) + nroExpediente;
        nroExpediente = nroExpediente.substring(0, 7) + '/' + nroExpediente.substring(7);
    }
    return nroExpediente;
}

/* Función que reformatea el nro de expediente */
function formatoCUIT(nro) {
    var nroCuit = '' + nro;
    var prefijo = nroCuit.substring(0, 2);
    var cuerpo = nroCuit.substring(2, 10);
    var sufijo = nroCuit.substring(10)
    var cuitFormateado = prefijo + '-' + cuerpo + '-' + sufijo;
    return cuitFormateado;
}

/* Función que reformatea el nro de solicitud */
function formatoNroSolicitud(nro) {
    var ceros = '000000000000000';

    var nroSolicitud = '' + nro;
    var extensionSolicitud = nroSolicitud.length;
    var diferenciaSolicitud = 9 - extensionSolicitud;
    nroSolicitud = nroSolicitud.substring(0, 4)
        + '-'
        + ceros.substring(0, diferenciaSolicitud)
        + nroSolicitud.substring(4);

    return nroSolicitud;
}


/* Funcion que corre al salir del modal para apagar el boton (+) */
function apagarFloatingButton() {
    $('.btn-floating.btn-large').addClass('disabled');
}

/* Funcion que corre al salir del modal para volver a encender el boton (+) */
var encenderFloatingButton = function () {
    $('.btn-floating.btn-large').removeClass('disabled');
};



/* Enciende la barra de progreso */
function enciendeBarraProgreso() {
    $('#barraProgreso').addClass('progress');
}

/* Apaga la barra de progreso */
function apagaBarraProgreso() {
    $('#barraProgreso').removeClass('progress');
}

function stopTimer() {
        clearTimeout(mataSesion);
        clearTimeout(avisaSesion);
}


/* Si no es Login, empieza la cuenta regresiva. */
if (URLactual.pathname != '/login.aspx') {

    $(document).click(function () {
        stopTimer();

        console.log('click y stop');
        mataSesion = setTimeout(function () {
            Materialize.toast('<h6 style="display:block"> Atención: su sesión está a punto de caducar! </h6>', 4000, 'amber') // 4000 is the duration of the toast
            avisaSesion = setTimeout(function () {
                Materialize.toast('<h6 style="display:block"> Atención: su sesión acaba de caducar! </h6>', 4000, 'pink', function () {
                    expulsaUsuario()
                })
            }, 60000); // 60000 1 minutos
        }, 240000); // 240000 4 minutos
    });
}

function fechaJs(fechaJason) {
    var fecha = Date.parse(fechaJason);
    var date = new Date(fecha);
    return date;
}

function activaSelect() {
    $('select').material_select();
}

function formatoFecha(date) {
    var monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];

    fecha = Date.parse(date);

    var fechaServer = new Date(fecha);

    var day = fechaServer.getDate();
    var monthIndex = fechaServer.getMonth();
    var year = fechaServer.getFullYear();
    
    var hora = fechaServer.getHours();
    var hora = hora.toLocaleString();

    var minutos = fechaServer.getMinutes();

    if (minutos < 10) {
        minutos = '0' + minutos;
    }
    
    if (hora < 10) {
        hora = '0' + hora;
    }

    if (day < 10) {
        day = '0' + day;
    }
    var numeroMes = monthIndex;

    if (numeroMes < 10) {
        numeroMes = '0' + (numeroMes + 1);
    }
    return day + '/'
        //+ monthNames[monthIndex]  
        + numeroMes + '/' + year + ' - ' + hora + ':' + minutos + ' hs.';
}

function formatoFechaCorta(date) {
    var monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];

    fecha = Date.parse(date) + date.getTimezoneOffset() * 60000;
    var fechaUTC = new Date(fecha);


    var day = fechaUTC.getDate();
    var monthIndex = fechaUTC.getMonth();
    var year = fechaUTC.getFullYear();

    var numeroMes;

    if (day < 10) {
        day = '0' + day;
    };

    if ((monthIndex + 1) < 10) {
        numeroMes = '0' + (monthIndex + 1);
    } else {
        numeroMes = (monthIndex + 1);
    }

    return year + '-' + numeroMes + '-' + day;
}

function local(valor) {
    return valor.toLocaleString('de-DE', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
}

function remove(arr, item) {
    for (var i = arr.length; i--;) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}

function replace(arr, item, newItem) {
    var index = arr.indexOf(item);
    if (index !== -1) {
        arr[index] = newItem;
    }
    return arr;
}

function obtieneAtributo(selector, atributo) {
    var str = selector.attr(atributo);
    return str;
}

function obtienePropiedad(selector, atributo) {
    var str = selector.prop(atributo);
    return str;
}

$('#cambioPassword').click(function (event) {
    /* Act on the event */
    $('#modal-cambioPassword').openModal()
    var usupass = $('#usuarioPassword');
    var usupass2 = $('#usuarioPassword2');
    usupass.val('');
    usupass2.val('');

    usupass.parent().find('i').removeClass('active').parent().find('label').removeClass('active');
    usupass2.parent().find('i').removeClass('active').parent().find('label').removeClass('active');
    $('#alerta-password').hide();
});

$('#usuarioPassword2').keyup(function (event) {
    var alerta = $('#alerta-password');
    /* Act on the event */
    if ($(this).val() != $('#usuarioPassword').val()) {
        alerta.removeClass('light-green-text');
        alerta.addClass('red-text');
        alerta.html('');
        alerta.html('* su contraseña debe ser idéntica en ambos campos');
        alerta.show();

        $('#cabecera_BTNcambioPassword').attr('disabled', 'disabled');
    } else {
        alerta.removeClass('red-text');
        alerta.addClass('light-green-text');
        alerta.html('');
        alerta.html('* su contraseña ya es idéntica');
        alerta.show();

        $('#cabecera_BTNcambioPassword').removeAttr('disabled');
    };
});

if ($.trim($('#resultadoCambioPass').html()) == 'ok') {
    Materialize.toast('<h6 class="bold" style="display:block"> La contraseña se cambió correctamente!</h6><h6 class="" style="padding: 0px 0px 0px 10px;">  Utilícela la próxima vez que entre al sistema...</h6>', 4000, 'green') // 4000 is the duration of the toast
};

var mensaje = $.trim($('#LBLAlert').html());
var colorAlerta = obtieneAtributo($('#LBLAlert'), 'title');

if (mensaje != "") {
    Materialize.toast('<h6 class="">  ' + mensaje + '</h6>', 4000, colorAlerta) // 4000 is the duration of the toast
}