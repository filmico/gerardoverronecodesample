﻿$(document).ready(function() {
    enciendeBarraProgreso();

    function tiempoTranscurrido(fechaSincroFinalizacion, fechaSincroCominenzo) {
        var tiempo = (fechaSincroFinalizacion - fechaSincroCominenzo) / 1000;

        if (tiempo >= 0.5 && tiempo < 60) {
            if (tiempo < 9.5) {
                tiempo = '00:00:0' + Math.round(tiempo)
            } else {
                tiempo = '00:00:' + Math.round(tiempo)
            }

        } else if (tiempo >= 60) {
            var tiempoRedondeado = Math.trunc(Math.round(tiempo));
            var minutos = Math.trunc(tiempoRedondeado / 60)
            var segundos = Math.round(((tiempoRedondeado / 60) % 1) * 60)
            var horas = Math.trunc(minutos / 60)

            /* Tratamiento de las horas */
            if (horas < 10) {
                horas = '0' + horas;
            }
            /* Tratamiento de los minutos */
            if (minutos < 10) {
                minutos = '0' + minutos;
            }

            if (segundos < 10) {
                segundos = '0' + segundos;
            }

            tiempo = horas + ':' + minutos + ':' + segundos;
        } else if (tiempo < 0.5 && tiempo != 0) {
            tiempo = '00:00:0' + Math.round(tiempo);
        } else {
            tiempo = '--:--:--'
        };

        return tiempo;
    }

    $.get('consultaEstadoSincronizacion.aspx', function(data) {
        /*optional stuff to do after success */
        var objEstadoSincronizacion = (data);

        var PanelSincronizaciones = $('#PanelSincronizaciones').find('ul');
        PanelSincronizaciones.html('');
        PanelSincronizaciones.append('<li class="collection-header"><h4>Sincronizaciones</h4></li>')
        for (i in objEstadoSincronizacion) {

            objEstadoSincronizacion[i].UltimaSincronizacionFinalizacion = fechaJs(objEstadoSincronizacion[i].UltimaSincronizacionFinalizacion);
            objEstadoSincronizacion[i].UltimaSincronizacionComienzo = fechaJs(objEstadoSincronizacion[i].UltimaSincronizacionComienzo);

            var tiempo = tiempoTranscurrido(objEstadoSincronizacion[i].UltimaSincronizacionFinalizacion, objEstadoSincronizacion[i].UltimaSincronizacionComienzo)

            $('#PanelSincronizaciones').find('ul').append('<li class="collection-item '
                + 'estado'
                + objEstadoSincronizacion[i].ExpedienteEstado.ExpedienteEstadoID
                + '"><div class="row"><div class="col l4 m12 s12"><span class="bold">Estado</span> <span id="ExpedienteEstadoID" class="bold">'
                + objEstadoSincronizacion[i].ExpedienteEstado.ExpedienteEstadoID
                + '</span><span class="bold"> - '
                + objEstadoSincronizacion[i].ExpedienteEstado.Descripcion
                + '</span><br><span id="UltimaSincronizacionFinalizacion" class="bold orange-text darken-4">'
                + formatoFecha(objEstadoSincronizacion[i].UltimaSincronizacionFinalizacion)
                + '</span> <span class="bold">T:</span><span id="TiempoTranscurrido" class="bold cyan-text accent-4"> '
                //+ ((objEstadoSincronizacion[i].UltimaSincronizacionFinalizacion - objEstadoSincronizacion[i].UltimaSincronizacionComienzo) / 1000)
                + tiempo
                + '</span></div><div class="col l2 m12 s12"><span class="bold">Exp. Obtenidos:</span> <span id="ExpedientesObtenidos" class="bold light-blue-text">'
                + objEstadoSincronizacion[i].ExpedientesObtenidos
                + '</span></div><div class="col l2 m12 s12"> <span class="bold tooltipped" data-position="bottom" data-delay="50" data-tooltip="(Fecha >= Ultimo Sync)">Exp. Posibles:</span> <span id="ExpedientesPosibles" class="bold orange-text">'
                + objEstadoSincronizacion[i].ExpedientesPosibles
                + '</span></div><div class="col l3 m11 s11"> <span class="bold">Exp. Actualizados:</span> <span id="ExpedientesActualizados" class="bold green-text">'
                + objEstadoSincronizacion[i].ExpedientesActualizados
                + '</span></div><div class="col l1 m1 s1 boton-sync"><span class="secondary-content cargaDatos icono" target="'
                + objEstadoSincronizacion[i].ExpedienteEstado.ExpedienteEstadoID
                + '"><i class="material-icons">refresh</i></span></div>'
                + '<div class="progress hide"><div class="indeterminate"></div></div>'
                + '</div></li>'
            )
        }
        $('.tooltipped').tooltip({ delay: 50 })

        apagaBarraProgreso();

        $('.icono').click(function(event) {
            /* Act on the event */
            var linea = $(this).parent().parent();
            var spinner = $(this).parent().parent().find('.progress');
            spinner.removeClass('hide');

            var id = $(this).attr('target');
            var cargaDatos = $('#PanelSincronizaciones').find('.cargaDatos');
            cargaDatos.find('i').text('sync_disabled');
            cargaDatos.addClass('disabled');
            cargaDatos.prop('disabled', true);
            cargaDatos.addClass('grey-text');

            $.get('/cargadatos.aspx?modo=1&estado=' + id)
                .then(function success(res) {
                    var objCargaDatos = (res);

                    if (objCargaDatos[0] != 'S') {
                        objCargaDatos[0].UltimaSincronizacionFinalizacion = fechaJs(objCargaDatos[0].UltimaSincronizacionFinalizacion);
                        objCargaDatos[0].UltimaSincronizacionComienzo = fechaJs(objCargaDatos[0].UltimaSincronizacionComienzo);

                        var tiempo = tiempoTranscurrido(objCargaDatos[0].UltimaSincronizacionFinalizacion, objCargaDatos[0].UltimaSincronizacionComienzo);

                        linea.find('#UltimaSincronizacionFinalizacion').text(formatoFecha(objCargaDatos[0].UltimaSincronizacionFinalizacion));
                        linea.find('#TiempoTranscurrido').text(' ' + tiempo);
                        linea.find('#ExpedientesObtenidos').text(objCargaDatos[0].ExpedientesObtenidos);
                        linea.find('#ExpedientesPosibles').text(objCargaDatos[0].ExpedientesPosibles);
                        linea.find('#ExpedientesActualizados').text(objCargaDatos[0].ExpedientesActualizados);

                        Materialize.toast('<h6 style="display:block"> La Sincronización del estado fue satisfactoria!. Expedientes Obtenidos: ' + objCargaDatos[0].ExpedientesObtenidos + '</h6>', 4000) // 4000 is the duration of the toast
                        $('.toast').addClass('green');
                    } else {
                        Materialize.toast('<h6 class="" style="display:block"> Hubo un problema con la conexión a la fuente de datos!. Por favor inténtelo más tarde...</h6>', 4000) // 4000 is the duration of the toast
                        $('.toast').addClass('red');
                    }
                    cargaDatos.find('i').text('refresh');
                    cargaDatos.removeClass('grey-text');
                    cargaDatos.removeClass('disabled');
                    cargaDatos.prop('disabled', false);
                    spinner.addClass('hide');

                }, function failure(res) {

                    cargaDatos.find('i').text('refresh');
                    cargaDatos.removeClass('grey-text');
                    cargaDatos.removeClass('disabled');
                    cargaDatos.prop('disabled', false);
                    spinner.addClass('hide');

                }
                );
        });
    });
});