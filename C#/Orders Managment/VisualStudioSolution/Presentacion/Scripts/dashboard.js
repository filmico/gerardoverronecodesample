﻿$(document).ready(function () {

	var dashboardCard = $('ul.dashboard-card');
	var selectEstadoLiquidacion = $('#estadoLiquidacion');
	var tipoUsuario = $.trim($('#tu').html());

	$('#pendientes').prop('checked', true);
	$('#aLiquidar').prop('checked', false);
	$('#liquidados').prop('checked', false);
	$('#finalizados').prop('checked', false);

	// arma las asignaciones por empresa
	function armaAsignaciones(objeto) {
		var str = '';
		for (i in objeto) {
			str = str + ('<li EmpresaCuitID="'
				+ objeto[i].Empresa.EmpresaCuitID
				+ '"><div class="collapsible-header"><div class="row"><div class="col s12 m12 l12"><i class="material-icons">more_vert</i><span class="light-blue-text">'
				+ formatoCUIT(objeto[i].Empresa.EmpresaCuitID)
				+ '</span> - <span>'
				+ objeto[i].Empresa.NroEmpresa
				+ '</span> - <span>'
				+ objeto[i].Empresa.RazonSocial
				+ '</span></div></div></div><div class="collapsible-body"></div></li>')
		}
		return str;
	}

	// MODAL PARA CAMBIAR ESTADO DE LIQUIDACION
	function modalEstadoLiq() {
		// body...
		$('.cambiaEstadoLiq').click(function (event) {
			/* Act on the event */
			var expedienteNro = obtieneAtributo($(this), 'expedientenumeroid');
			var EstadoLiquidacionID = obtieneAtributo($(this), 'estadoliquidacionid');
			var empresacuitid = obtieneAtributo($(this).closest('li.active'), 'empresacuitid');

			$('#ExpedienteNumeroID').val(expedienteNro);
			$('#EmpresaCuitID').val(empresacuitid);
			selectEstadoLiquidacion.val(EstadoLiquidacionID);
			$('#modal').find('h5').html('');
			$('#modal').find('h5').html('Cambiar estado de Liquidación #' + expedienteNro);
			activaSelect();

			$('#modal').openModal();
		});
	}



	// TABLA DE DATOS DE EXPEDIENTES
	function tablaDatos(objeto, suma) {
		var tabla;
		var cabecerasTabla = [];
		cabecerasTabla.push('Tipo Usuario', 'Usuario', 'Porcentaje', 'Suma');

		tabla = '<table class="modal-table bordered striped">'
		var thead = '<thead><tr class="blue-text">'

		for (i in cabecerasTabla) {
			thead = thead
				+ '<th>'
				+ cabecerasTabla[i]
				+ '</th>'
		}

		tabla = tabla + thead + '</tr></thead>';

		var tbody = '<tbody>';

		//FILAS
		var filasGestores = '';
		var filasSocios = '';
		var filasAdministradores = '';

		// SUMAS 
		var sumaPorcentualGestores = 0;
		var sumaPorcentualSocios = 0;
		var sumaPorcentualAdministradores = 0;
		var sumaGestores = 0;
		var sumaSocios = 0;
		var sumaAdministradores = 0;

		// CUENTAVUELTAS
		var cuentaVueltasGestores = 0
		var cuentaVueltasSocios = 0
		var cuentaVueltasAdministradores = 0

		tabla += tbody;

		// BLOQUE GESTORES
		for (i in objeto) {
			objeto[i].Usuario.UsuarioTipoID;
			if (objeto[i].Usuario.UsuarioTipoID == 1) {
				cuentaVueltasGestores++;

				var fila = '<tr><td>'
					+ objeto[i].Usuario.UsuarioTipoDescripcion
					+ '</td><td>'
					+ objeto[i].Usuario.Nombre
					+ '</td><td>'
					+ local(objeto[i].Porcentaje)
					+ '%</td><td> $ '
					+ local(objeto[i].Porcentaje * suma / 100)
					+ '</td></tr>'
				filasGestores += fila;

				sumaPorcentualGestores += objeto[i].Porcentaje;
				sumaGestores += (objeto[i].Porcentaje * suma / 100)

			}
		}
		if (cuentaVueltasGestores > 0) {
			var filaSumatoriaGestores = '<tr class="filaSumaGestores bold"><td colspan="2">Subtotal Gestores</td><td>' + local(sumaPorcentualGestores) + '%</td><td>$ ' + local(sumaGestores) + '</td></tr>'
			filasGestores += filaSumatoriaGestores;
			tabla += filasGestores;
		}

		// BLOQUE SOCIOS
		for (i in objeto) {
			objeto[i].Usuario.UsuarioTipoID;
			if (objeto[i].Usuario.UsuarioTipoID == 2) {
				cuentaVueltasSocios++;

				var fila = '<tr><td>'
					+ objeto[i].Usuario.UsuarioTipoDescripcion
					+ '</td><td>'
					+ objeto[i].Usuario.Nombre
					+ '</td><td>'
					+ local(objeto[i].Porcentaje)
					+ '%</td><td> $ '
					+ local(objeto[i].Porcentaje * suma / 100)
					+ '</td></tr>'
				filasSocios += fila;

				sumaPorcentualSocios += objeto[i].Porcentaje;
				sumaSocios += (objeto[i].Porcentaje * suma / 100)

			}
		}
		if (cuentaVueltasSocios > 0) {
			var filaSumatoriaSocios = '<tr class="filaSocios bold"><td colspan="2">Subtotal Socios</td><td>' + local(sumaPorcentualSocios) + '%</td><td>$ ' + local(sumaSocios) + '</td></tr>'
			filasSocios += filaSumatoriaSocios;
			tabla += filasSocios;
		}

		// BLOQUE ADMINISTRADORES
		for (i in objeto) {
			objeto[i].Usuario.UsuarioTipoID;
			if (objeto[i].Usuario.UsuarioTipoID == 3) {
				cuentaVueltasAdministradores++;

				var fila = '<tr><td>'
					+ objeto[i].Usuario.UsuarioTipoDescripcion
					+ '</td><td>'
					+ objeto[i].Usuario.Nombre
					+ '</td><td>'
					+ local(objeto[i].Porcentaje)
					+ '%</td><td> $ '
					+ local(objeto[i].Porcentaje * suma / 100)
					+ '</td></tr>'
				filasAdministradores += fila;

				sumaPorcentualAdministradores += objeto[i].Porcentaje;
				sumaAdministradores += (objeto[i].Porcentaje * suma / 100)

			}
		}
		if (cuentaVueltasAdministradores > 0) {
			var filaSumatoriaAdministradores = '<tr class="filaAdministradores bold"><td colspan="2">Subtotal Administradores</td><td>' + local(sumaPorcentualAdministradores) + '%</td><td>$ ' + local(sumaAdministradores) + '</td></tr>'
			filasAdministradores += filaSumatoriaAdministradores;
			tabla += filasAdministradores;
		}

		var sumaTotalPorcentual = sumaPorcentualGestores + sumaPorcentualSocios + sumaPorcentualAdministradores;
		var sumatotal = sumaGestores + sumaSocios + sumaAdministradores;

		var filaSumatoriaTotal = '<tr class="filaGeneral bold"><td colspan="2">Total General</td><td>' + local(sumaTotalPorcentual) + '%</td><td>$ ' + local(sumatotal) + '</td></tr>'
		tabla += filaSumatoriaTotal;

		tabla += '</tbody></table>';
		return tabla;
	}




	// TABLA DE EXPEDIENTES
	function tablaExpedientes(objeto) {
		var tabla;
		var cabecerasTabla = [];

		var cabeceras = Object.keys(objeto[0]);


		// CABECERA
		cabecerasTabla.push('Expediente', 'Solicitud', 'Estado', 'Fecha', 'Solicitado', 'Aprobado', 'Evaluador', 'Evaluador Técnico', 'info');

		tabla = '<table class="dashboard-table bordered striped">'
		var thead = '<thead><tr class="blue-text">'

		for (i in cabecerasTabla) {
			if (cabecerasTabla[i] == 'Solicitud'
				|| cabecerasTabla[i] == 'Solicitado'
				|| cabecerasTabla[i] == 'Evaluador'
				|| cabecerasTabla[i] == 'Evaluador Técnico'
			) {
				thead = thead + '<th class="hide-on-med-and-down">' + cabecerasTabla[i] + '</th>';
			} else {
				thead = thead + '<th>' + cabecerasTabla[i] + '</th>';
			}
		}
		if (tipoUsuario == 3) {
			thead = thead + '<th class="hide-on-med-and-down">Liq</th>'
		}

		thead = thead + '</tr></thead>';
		tabla = tabla + thead;


		// FILAS
		var filas = '<tbody>'
		for (i in objeto) {

			var montos = '<td class="hide-on-med-and-down"> $ '

			filas = filas + '<tr class="estadoLiq'
				+ objeto[i].EstadoLiquidacionID
				+ '"><td>'
				+ formatoNroExpediente(objeto[i].ExpedienteNumeroID)
				+ '</td><td class="hide-on-med-and-down">'
				+ formatoNroSolicitud(objeto[i].NroSolicitud)
				+ '</td><td class="bold ExpedienteEstadoID">'
				+ objeto[i].ExpedienteEstado.Descripcion
				+ '</td><td>'
				+ formatoFechaCorta(fechaJs(objeto[i].FechaEstadoActual))
				+ '</td><td class="hide-on-med-and-down"> $ '
				+ local(objeto[i].BeneficioSolicitado)
				+ '</td><td> $ '
				+ local(objeto[i].BeneficioAprobado)
				+ '</td><td class="hide-on-med-and-down">'
				+ objeto[i].Evaluador.Nombre
				+ '</td><td class="hide-on-med-and-down">'
				+ objeto[i].EvaluadorTecnico.Nombre
				+ '</td><td class="info punteroLink" nroExpediente="'
				+ objeto[i].ExpedienteNumeroID
				+ '" aprobado="'
				+ objeto[i].BeneficioAprobado
				+ '"><span><i class="material-icons">info_outline</i></span></td>'

			if (tipoUsuario == 3) {
				filas = filas
					+ '<td class="liq hide-on-med-and-down"><span EstadoLiquidacionID="'
					+ objeto[i].EstadoLiquidacionID
					+ '" ExpedienteNumeroID="'
					+ objeto[i].ExpedienteNumeroID
					+ '" class="punteroLink cambiaEstadoLiq"><i class="material-icons">monetization_on</i></span></td>'
			}
			filas = filas + '</tr>';
			filas = filas + '<tr id="'
				+ objeto[i].ExpedienteNumeroID
				+ '"></tr>'
		}

		filas = filas + '</tbody>';
		tabla = tabla + filas;
		tabla = tabla + '</table>';

		return tabla;
	}

	$('.interruptor').change(function (event) {
		/* Act on the event */
		var boolPendientes = obtienePropiedad($('#pendientes'), 'checked');
		var boolALiquidar = obtienePropiedad($('#aLiquidar'), 'checked');
		var boolLiquidados = obtienePropiedad($('#liquidados'), 'checked');
		var boolFinalizados = obtienePropiedad($('#finalizados'), 'checked');

		if (boolPendientes == false) {
			pendientes = '&pendiente=1';
		} else {
			pendientes = '';
		}

		if (boolALiquidar == false) {
			aLiquidar = '&aLiquidar=2';
		} else {
			aLiquidar = '';
		}

		if (boolLiquidados == false) {
			liquidados = '&liquidado=3';
		} else {
			liquidados = '';
		}

		if (boolFinalizados == false) {
			finalizados = '&finalizado=4';
		} else {
			finalizados = '';
		}

		var empresacuitid = 0;

		empresacuitid = obtieneAtributo($('li.active'), 'empresacuitid');
		if (empresacuitid != null) {
			$.get('consultaexpedientes.aspx?EmpresaCuitID=' + empresacuitid + pendientes + aLiquidar + liquidados + finalizados).then(function success(data) {
				/*optional stuff to do after success */
				var objExpedientesAsignadosXEmpresa = (data);
				if (objExpedientesAsignadosXEmpresa.length > 0) {
					var Tabla = tablaExpedientes(objExpedientesAsignadosXEmpresa);
					$('li.active').find('table').remove();
					$('li.active').find('.collapsible-body').append(Tabla);
					modalEstadoLiq();
				} else {
					$('li.active').find('table').remove();
					modalEstadoLiq();
				}
			}, function failure(data) {
				// body...
			})
		};

	});


	/* Empresas Asignadas */
	var EmpresasAsignadas = $.get('consultaexpedientesusuarios.aspx', function (data) {
		return (data);
	});

	EmpresasAsignadas.then(function success(resEmpresasAsignadas) {
		dashboardCard.append(armaAsignaciones(resEmpresasAsignadas));
		var liEmpresa = $('li[Empresacuitid]');

		// guarda el elemento que debe quedar abierto luego del postback;
		var valores = $('span#opened').html();
		valores = valores.trim();
		var arregloValores = valores.split('-');
		var cuitSeleccionado = arregloValores[0];
		var estadoLiqSeleccionado = arregloValores[1];
		if (estadoLiqSeleccionado == 3) {
			$('#aLiquidar').prop('checked', true);
		}

		if (estadoLiqSeleccionado == 4) {
			$('#liquidados').prop('checked', true);
		}

		if (estadoLiqSeleccionado == 5) {
			$('#finalizados').prop('checked', true);
		}
		apagaBarraProgreso();
		var padreEmpresa = $('li[empresacuitid="' + cuitSeleccionado + '"]');

		// click en empresa
		liEmpresa.click(function (event) {
			var cuit = $(this).attr('empresacuitid');

			enciendeBarraProgreso();
			var boolPendientes = obtienePropiedad($('#pendientes'), 'checked');
			var boolALiquidar = obtienePropiedad($('#aLiquidar'), 'checked');
			var boolLiquidados = obtienePropiedad($('#liquidados'), 'checked');
			var boolFinalizados = obtienePropiedad($('#finalizados'), 'checked');

			var pendientes, aLiquidar, liquidados, finalizados;

			if (boolPendientes == false) {
				pendientes = '&pendiente=1';
			} else {
				pendientes = '';
			}

			if (boolALiquidar == false) {
				aLiquidar = '&aLiquidar=2';
			} else {
				aLiquidar = '';
			}

			if (boolLiquidados == false) {
				liquidados = '&liquidado=3';
			} else {
				liquidados = '';
			}

			if (boolFinalizados == false) {
				finalizados = '&finalizado=4';
			} else {
				finalizados = '';
			}

			$.get('consultaexpedientes.aspx?EmpresaCuitID=' + cuit + pendientes + aLiquidar + liquidados + finalizados, function (data) {
				/*optional stuff to do after success */
				var ObjExpedientesPorCuit = (data);

				if (ObjExpedientesPorCuit.length > 0) {
					var Tabla = tablaExpedientes(ObjExpedientesPorCuit);
					$('li.active').find('table').remove();
					$('li.active').find('.collapsible-body').append(Tabla);
					modalEstadoLiq();
				} else {
					$('li.active').find('table').remove();
					modalEstadoLiq();
				}


				$('td.info').click(function (event) {
					var nroExpediente = $(this).attr('nroexpediente');
					var montoAprobado = $(this).attr('aprobado');
					$.get('consultaexpedientesusuarios.aspx?expedientenumeroid=' + nroExpediente, function (data) {
						/*optional stuff to do after success */
						var objExpedientesUsuarios = (data);
						var modalAbajo = $('#modalabajo');
						modalAbajo.find('div.modal-content').html('');
						modalAbajo.find('div.modal-content').html('<h5>Asignaciones del Expediente #' + formatoNroExpediente(nroExpediente) + '</h5>');

						var tablaRetorno = tablaDatos(objExpedientesUsuarios, montoAprobado);

						modalAbajo.find('div.modal-content').append(tablaRetorno);

						modalAbajo.openModal();
					});
				});



			});


		});
				padreEmpresa.addClass('active');
				padreEmpresa.find('div.collapsible-body').attr('style', 'display: block;');
				padreEmpresa.find('div.collapsible-header').trigger('click');

	}, function failure(res) {

	})

	$.get('consultaestadosliquidacion.aspx', function (data) {
		/*optional stuff to do after success */
		var objEstadosLiquidacion = (data);

		selectEstadoLiquidacion.html('');
		/* cargar tipo de usuario en el select */
		for (i in objEstadosLiquidacion) {

			if (objEstadosLiquidacion[i].EstadoLiquidacionID > 1) {
				selectEstadoLiquidacion.append('<option value="'
					+ objEstadosLiquidacion[i].EstadoLiquidacionID
					+ '">'
					+ objEstadosLiquidacion[i].Descripcion
					+ '</option>'
				)
			}
		}
		activaSelect();

	}); /* cierre get consultaestadosliquidacion.aspx */



});