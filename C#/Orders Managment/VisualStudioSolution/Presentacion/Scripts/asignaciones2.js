$(document).ready(function() {
	var grupoEmpresa = $('#grupoEmpresa');
	var grupoTipoUsuario = $('#grupoTipoUsuario');
	var grupoUsuario = $('#grupoUsuario');
	var grupoPorcentaje = $('#grupoPorcentaje');
	var selectEmpresas = $('#empresa');
	var selectTipoUsuario = $('#tipoUsuario');
	var selectUsuario = $('#usuario');
	var tbPorcentaje = $('#TB_porcentaje');
	var btnActualizar = $('#BTNActualizar');
	var btnAgregar = $('#BTNAgregar');

	function activaSelect() {
		// body...
		$('select').material_select();
	}

	function reiniciaSelectTipoUsuario() {
		selectTipoUsuario.html('');
		/* cargar tipo de usuario en el select */
		selectTipoUsuario.append('<option value="0" disabled selected>Elija un Tipo de Usuario</option>');
		selectTipoUsuario.append('<option value="1">Gestor</option>')
		selectTipoUsuario.append('<option value="2">Socio</option>')
		selectTipoUsuario.append('<option value="3">Administrador</option>')
		activaSelect();
	}
	reiniciaSelectTipoUsuario();

	function cargaSelectUsuarioPorTipo(objeto, UsuarioTipoID, selectID, UsuarioID) {

		var selectUsuario = $('#' + selectID);
		selectUsuario.html('');
		var UsuariosPorTipo = [];
		for (i in objeto) {
			if (objeto[i].UsuarioTipoID == UsuarioTipoID) {
				UsuariosPorTipo.push(objeto[i]);
			}
		}
		/* carga el select con el tipo de usuario que se le pasó */
		for (i in UsuariosPorTipo) {
			selectUsuario.append('<option value="' + UsuariosPorTipo[i].UsuarioID + '">' + UsuariosPorTipo[i].Nombre + '</option>')
		}

		if (UsuarioID != null) {
			selectUsuario.val(parseInt(UsuarioID));
		};

		activaSelect();
	}

	var objEmpresa = function(cuitEmpresaID, nroEmpresa, razonSocial) {
		this.cuitEmpresaID = cuitEmpresaID;
		this.nroEmpresa = nroEmpresa;
		this.razonSocial = razonSocial;
	};

	var ulAsignaciones = $('.asignaciones-card');
	ulAsignaciones.append('<li class="collection-header"><h4>Asignaciones</h4></li>');

	$.get('consultaAsignaciones.aspx', function(data) {
		/*optional stuff to do after success */
		enciendeBarraProgreso();
		var objAsignaciones = (data);

		for (i in objAsignaciones) {
			ulAsignaciones.append('<li class="empresa"><div class="collapsible-header"><div class="row"><div class="col s12 m9 l9"><a><i class="material-icons">more_vert</i></a>' + '<span class="light-blue-text cuitEmpresaID" EmpresaCuitID="' + objAsignaciones[i].EmpresaCuitID + '">' + objAsignaciones[i].EmpresaCuitID + '</span> - <span class="nroEmpresa">' + objAsignaciones[i].NroEmpresa + '</span> - <span class="razonSocial">' + objAsignaciones[i].RazonSocial + '</span></div><div class="col s12 m3 l3">'
				//+ '<a href="#"><i class="material-icons left editar">edit</i></a>'
				//+ '<a href="#"><i class="material-icons left duplicar">content_copy</i></a>'
				//+ '<a href="#"><i class="material-icons left red-text borrar" target="'
				//+ objAsignaciones[i].EmpresaCuitID
				//+ '">delete</i></a>' 
				+ '</div></div></div>'
				)
			ulAsignaciones.find('li:last()').append('<div class="collapsible-body">' + '<div class="section gestor light-blue lighten-5">' + '<h6 class="bold gestoresPorcentaje">Gestores: <span class="light-blue-text"></span></h6>' + '</div>' + '<div class="divider gestor"></div>' + '<div class="section socio green lighten-5">' + '<h6 class="bold sociosPorcentaje">Socios: <span class="green-text"></span></h6></div>' + '<div class="divider socio"></div>' + '<div class="section administrador orange lighten-5">' + '<h6 class="bold administradoresPorcentaje">Administradores: <span class="orange-text"></span></h6></div>' + '<div class="divider administrador"></div>' + '</div>');
		}

		apagaBarraProgreso();
		$('li.empresa').click(function(event) {
			/* Act on the event */
			var liEmpresa = $(this);
			$('.section.agregarUsuario').remove();

			if (liEmpresa.hasClass('active') == false) {
				liEmpresa.removeClass('marco');
				$(this).addClass('marco');
			} else {
				$(this).removeClass('marco');
			};

			var EmpresaCuitID = $(this).find('span.cuitEmpresaID').html();
			$.get('consultaAsignaciones.aspx?cuit=' + EmpresaCuitID, function(data) {
				/*optional stuff to do after success */
				var objAsignacionesPorCuit = (data);

				var gestores = [];
				var socios = [];
				var administradores = [];

				for (i in objAsignacionesPorCuit) {
					/* gestores */
					if (objAsignacionesPorCuit[i].UsuarioTipoID == 1) {
						gestores.push(objAsignacionesPorCuit[i]);
						/* socios */
					} else if (objAsignacionesPorCuit[i].UsuarioTipoID == 2) {
						socios.push(objAsignacionesPorCuit[i]);
						/* administradores */
					} else if (objAsignacionesPorCuit[i].UsuarioTipoID == 3) {
						administradores.push(objAsignacionesPorCuit[i]);
					}
				}
				liEmpresa.find('.gestoresPorcentaje span').html('');
				liEmpresa.find('.sociosPorcentaje span').html('');
				liEmpresa.find('.administradoresPorcentaje span').html('');

				liEmpresa.find('div.item-gestor').remove();
				liEmpresa.find('div.item-socio').remove();
				liEmpresa.find('div.item-administrador').remove();


				if (gestores.length > 0) {
					for (j in gestores) {
						liEmpresa.find('div.section.gestor')
						.append('<div class="item-gestor"><div class="row" target="' + gestores[j].UsuarioID + '" tipoUsuario="' + gestores[j].UsuarioTipoID + '">' + '<div class="col s12 m2 l2 bold">' + '<span class="editaUsuario punteroLink"><i class="material-icons left light-blue-text">edit</i></span>' + '<span class="desasignaUsuario punteroLink"><i class="material-icons left red-text">delete</i></span></div>'
								//+ '<div class="col s12 m2 l2 bold">'
								//+ 'ID Usuario: <span class="bold light-blue-text">'
								//+ gestores[j].UsuarioID
								//+ '</span></div>'
								+ '<div class="col s12 m10 l10 bold">Nombre: <span class="bold light-blue-text usuarioNombre">' + gestores[j].UsuarioNombre + '</span></div>' + '</div></div>');
					}
					liEmpresa.find('.gestoresPorcentaje span').append(gestores[0].Porcentaje + '%');
				} else {
					liEmpresa.find('.section.gestor').hide();
					liEmpresa.find('.divider.gestor').hide();
				}

				if (socios.length > 0) {
					liEmpresa.find('.section.socio').show();
					liEmpresa.find('.divider.socio').show();

					for (j in socios) {
						liEmpresa.find('div.section.socio')
						.append('<div class="item-socio"><div class="row" target="' + socios[j].UsuarioID + '" tipoUsuario="' + socios[j].UsuarioTipoID + '">' + '<div class="col s12 m2 l2 bold">' + '<span class="editaUsuario punteroLink"><i class="material-icons left light-blue-text">edit</i></span>' + '<span class="desasignaUsuario punteroLink"><i class="material-icons left red-text">delete</i></span></div>'
								//+ '<div class="col s12 m2 l2 bold">'
								//+ 'ID Usuario: <span class="bold light-blue-text">'
								//+ socios[j].UsuarioID
								//+ '</span></div>'
								+ '<div class="col s12 m10 l10 bold">Nombre: <span class="bold light-blue-text usuarioNombre">' + socios[j].UsuarioNombre + '</span></div>' + '</div></div>');
					}
					liEmpresa.find('.sociosPorcentaje span').append(socios[0].Porcentaje + '%');
				} else {
					liEmpresa.find('.section.socio').hide();
					liEmpresa.find('.divider.socio').hide();
				}

				if (administradores.length > 0) {
					liEmpresa.find('.section.administrador').show();
					liEmpresa.find('.divider.administrador').show();

					for (j in administradores) {
						liEmpresa.find('div.section.administrador')
						.append('<div class="item-administrador"><div class="row" target="' + administradores[j].UsuarioID + '" tipoUsuario="' + administradores[j].UsuarioTipoID + '">' + '<div class="col s12 m2 l2 bold">' + '<span class="editaUsuario punteroLink"><i class="material-icons left light-blue-text">edit</i></span>' + '<span class="desasignaUsuario punteroLink"><i class="material-icons left red-text">delete</i></span></div>'
								//+ '<div class="col s12 m2 l2 bold">'
								//+ 'ID Usuario: <span class="bold light-blue-text">'
								//+ administradores[j].UsuarioID
								//+ '</span></div>'
								+ '<div class="col s12 m10 l10 bold">Nombre: <span class="bold light-blue-text usuarioNombre">' + administradores[j].UsuarioNombre + '</span></div>' + '</div></div>');
					}
					liEmpresa.find('.administradoresPorcentaje span').append(administradores[0].Porcentaje + '%');
				} else {
					liEmpresa.find('.section.administrador').hide();
					liEmpresa.find('.divider.administrador').hide();
				}


				$('.collapsible-body').find('.section.gestor .row').append('<div class="divider"></div>');
				$('.collapsible-body').find('.section.socio .row').append('<div class="divider"></div>')
				$('.collapsible-body').find('.section.administrador .row').append('<div class="divider"></div>')

				$('.collapsible-body').append('<div class="section agregarUsuario"><div class="row"><div class="col s12 m8 l8"></div><div class="col s12 m4 l4 bold"><span class="agregaUsuarioAsignacion"><i class="material-icons left green-text">add_circle</i>Agregar Usuario</span></div></div></div>');


				$('.editaUsuario').click(function(event) {
					enciendeBarraProgreso()
					apagarFloatingButton();
					/* grupos */
					grupoTipoUsuario.hide();
					grupoUsuario.hide();
					grupoPorcentaje.hide();

					/* campos */
					selectEmpresas.html('');
					selectTipoUsuario.html('');

					/* botones */
					btnAgregar.hide();
					btnActualizar.show();

					var usuarioID = $(this).closest('div.row').attr('target');

					var usuarioTipoID = $(this).closest('div.row').attr('tipoUsuario');
					var usuarioTipoDescripcion;
					var usuarioNombre = $(this).closest('div.row').find('.usuarioNombre').html();
					var cuitEmpresaID = $(this).closest('li').find('.cuitEmpresaID').html();
					var razonSocial = $(this).closest('li').find('.razonSocial').html();
					var Porcentaje

					if (usuarioTipoID == 1) {
						Porcentaje = $(this).closest('li').find('.gestoresPorcentaje span').html();
						usuarioTipoDescripcion = 'Gestor';
					} else if (usuarioTipoID == 2) {
						Porcentaje = $(this).closest('li').find('.sociosPorcentaje span').html();
						usuarioTipoDescripcion = 'Socio';
					} else if (usuarioTipoID == 3) {
						Porcentaje = $(this).closest('li').find('.administradoresPorcentaje span').html();
						usuarioTipoDescripcion = 'Administrador';
					};

					Porcentaje = Porcentaje.replace('%', '');

					selectTipoUsuario.val(usuarioTipoID);

					selectEmpresas.append('<option value="' + cuitEmpresaID + '" selected>' + razonSocial + '</option>')
					selectEmpresas.val(cuitEmpresaID);

					selectTipoUsuario.append('<option value="' + usuarioTipoID + '" selected>' + usuarioTipoDescripcion + '</option>')
					selectTipoUsuario.val(usuarioTipoID);

					selectUsuario.append('<option value="' + usuarioID + '" selected>' + usuarioNombre + '</option>')
					selectUsuario.val(usuarioID);

					tbPorcentaje.val(parseFloat(Porcentaje));

					grupoEmpresa.find('i').addClass('active');
					grupoTipoUsuario.find('i').addClass('active');
					grupoUsuario.find('i').addClass('active');

					grupoPorcentaje.find('i').addClass('active');
					grupoPorcentaje.find('label').addClass('active');

					$.get('consultausuarios.aspx', function(data) {
						var objUsuarios = (data);
						grupoTipoUsuario.show();
						grupoPorcentaje.show();
						grupoUsuario.show();
						grupoEmpresa.show();
						$('#modal').openModal({complete: encenderFloatingButton});
						grupoUsuario.slideDown();
						activaSelect();
						apagaBarraProgreso();
					});
				});

				$('.desasignaUsuario').click(function(event) {
					/* Act on the event */
					selectEmpresas.html('');
					selectUsuario.html('');
					apagarFloatingButton();
					$('#modal1 div.modal-content p').html('');


					var padre = $(this).closest("div[class^='item-']");

					var idUsuario = padre.find('div.row').attr('target');
					var nombreUsuario = padre.find('span.usuarioNombre').html();
					var tipoUsuario = padre.find('div.row').attr('tipousuario');
					var cuitEmpresa = padre.closest('li').find('div.collapsible-header span.cuitEmpresaID').attr('empresacuitid');
					var nombreEmpresa = padre.closest('li').find('div.collapsible-header span.razonSocial').html();

					selectEmpresas.append('<option value="' + cuitEmpresa + '" selected>' + nombreEmpresa + '</option>')
					selectEmpresas.val(cuitEmpresa);

					selectUsuario.append('<option value="' + idUsuario + '" selected>' + 'usuario' + '</option>')
					selectUsuario.val(idUsuario);

					selectTipoUsuario.val(tipoUsuario);
					tbPorcentaje.val(1);

					activaSelect();

					$('#modal1 div.modal-content p').append('Ud está a punto de dar de baja la asignación de ' + nombreUsuario + '. Si desea confirmar, haga click en Eliminar.');


					$('#modal1').openModal({complete: encenderFloatingButton});
				});

				$('.agregaUsuarioAsignacion').parent().click(function(event) {
					apagarFloatingButton();
					grupoEmpresa.find('i').addClass('active');

					grupoTipoUsuario.find('i').removeClass('active');
					grupoUsuario.find('i').removeClass('active');

					var padre = $(this).closest('li');

					btnActualizar.hide();
					btnAgregar.show();

					selectEmpresas.html('');
					selectUsuario.html('');

					reiniciaSelectTipoUsuario();

					grupoUsuario.hide();
					grupoPorcentaje.hide();

					var cuitEmpresaID = padre.find('span.cuitEmpresaID').html();
					var razonSocial = padre.find('span.razonSocial').html();

					selectEmpresas.append('<option value="' + cuitEmpresaID + '" selected>' + razonSocial + '</option>')
					selectEmpresas.val(cuitEmpresaID);

					activaSelect();

					selectTipoUsuario.change(function(event) {
						grupoTipoUsuario.find('i').addClass('active');

						/* Act on the event */
						$.get('consultaUsuarios.aspx', function(data) {
							/*optional stuff to do after success */
							var objUsuarios = (data);
							var tipoUsuario = $('#tipoUsuario').val();
							cargaSelectUsuarioPorTipo(objUsuarios, tipoUsuario, 'usuario')
							grupoUsuario.slideDown();
							tbPorcentaje.val(1);
							grupoUsuario.find('i').addClass('active');
							grupoPorcentaje.find('i').addClass('active');
							grupoPorcentaje.find('label').addClass('active');
							grupoPorcentaje.slideDown();

						});
					});

					grupoEmpresa.show();
					$('#modal').openModal({complete: encenderFloatingButton});
				});

			});
});


$('#nuevaAsignacion').click(function(event) {
	/* Act on the event */

			// Desactiva el Boton + mientras se asigna con el modal			
			apagarFloatingButton();

			enciendeBarraProgreso();

			grupoTipoUsuario.hide();
			grupoUsuario.hide();
			grupoPorcentaje.hide();
			selectEmpresas.html('');
			selectUsuario.html('');
			btnAgregar.show();
			btnActualizar.hide();

			grupoEmpresa.find('i').removeClass('active');
			grupoTipoUsuario.find('i').removeClass('active');


			$.get('consultaEmpresas.aspx').then(function success(res) {
				// body...
				var objEmpresas = (res);

				$.get('consultaUsuarios.aspx', function(data) {
					/*optional stuff to do after success */
					var objUsuarios = (data);

					selectEmpresas.append('<option value="0" disabled selected>Elija una Empresa</option>');
					for (i in objEmpresas) {
						selectEmpresas.append('<option value="' + objEmpresas[i].EmpresaCuitID + '">' + objEmpresas[i].RazonSocial + '</option>')
					}

					// Abre el modal y le adjunta una funcion de cierre para re-encender el boton (+)
					// $('#modal').openModal();
					$("#modal").openModal({
						complete : encenderFloatingButton
					});					

					selectEmpresas.change(function(event) {
						grupoUsuario.slideUp();
						grupoPorcentaje.slideUp();
						grupoTipoUsuario.find('i').removeClass('active');
						reiniciaSelectTipoUsuario();

						/* Act on the event */
						if (selectEmpresas.val() != 0) {
							grupoTipoUsuario.slideDown();
							grupoEmpresa.find('i').addClass('active');
						} else {
							grupoTipoUsuario.slideUp();
						}

					});

					selectTipoUsuario.change(function(event) {
						/* Act on the event */
						grupoTipoUsuario.find('i').addClass('active');
						var tipoUsuario = $('#tipoUsuario').val();
						cargaSelectUsuarioPorTipo(objUsuarios, tipoUsuario, 'usuario')
						grupoUsuario.slideDown();
						grupoPorcentaje.slideDown();
						tbPorcentaje.val(1);
						grupoUsuario.find('i').addClass('active');
						grupoPorcentaje.find('i').addClass('active');
						grupoPorcentaje.find('label').addClass('active');
					});

					selectUsuario.change(function(event) {
						/* Act on the event */
						tbPorcentaje.val(1);
					});

					activaSelect();
					grupoEmpresa.find('ul').addClass('selectEmpresasMaxHeight');

					/* Apaga la barra de progreso */
					apagaBarraProgreso();

				});

			}, function failure(res) {
				// body...
			});


		});

		// guarda el elemento que debe quedar abierto luego del postback;
		var cuitSeleccionado = $('span#opened').html();
		var padreEmpresa = $('li.empresa').find('span[empresacuitid="' + cuitSeleccionado.trim() + '"]').closest('li.empresa');
		padreEmpresa.addClass('active');
		padreEmpresa.find('div.collapsible-body').attr('style', 'display: block;');
		padreEmpresa.trigger('click');
	});
});