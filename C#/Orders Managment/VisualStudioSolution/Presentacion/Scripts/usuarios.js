﻿$(document).ready(function () {
	function limpiarForm() {

		var campoNombre = $('#TB_nombre');
		campoNombre.val('');
		campoNombre.parent().find('label').removeClass('active');
		campoNombre.parent().find('i').removeClass('active');

		var campoEmail = $('#TB_email');
		campoEmail.val('');
		campoEmail.parent().find('label').removeClass('active');
		campoEmail.parent().find('i').removeClass('active');

		var campoPassword = $('#TB_password');
		campoPassword.val('');
		campoPassword.parent().find('label').removeClass('active');
		campoPassword.parent().find('i').removeClass('active');

		var CampoSelectTipoUsuario = $('#usuarioTipo');
		CampoSelectTipoUsuario.val(0);

		var CampoUsuarioTipo = $('#modal .select-wrapper input');
		CampoUsuarioTipo.val('Elija un Tipo de Usuario');

		$('#modal .switch input').prop('checked', true);
		$('#TB_usuarioID').val(0);
	}

	function cargarForm(idUsuario, nombre, email, tipo, tipoDescripcion, estado) {
		var campoNombre = $('#TB_nombre');
		campoNombre.val(nombre);
		campoNombre.parent().find('label').addClass('active');
		campoNombre.parent().find('i').addClass('active');

		var campoEmail = $('#TB_email');
		campoEmail.val(email);
		if (email != '') {
			campoEmail.parent().find('label').addClass('active');
			campoEmail.parent().find('i').addClass('active');
		}
		var CampoSelectTipoUsuario = $('#usuarioTipo');
		CampoSelectTipoUsuario.val(tipo);

		var CampoUsuarioEstado = $('#modal .select-wrapper input');
		CampoUsuarioEstado.val(tipoDescripcion);

		if (estado == 1) {
			$('#modal .switch input').prop('checked', false);
		} else {
			$('#modal .switch input').prop('checked', true);
		}

		$('#TB_usuarioID').val(idUsuario);
	}

	$('select').material_select();


	$.get('consultaUsuarios.aspx').then(
		function success(res) {
			var objUsuarios = (res);
			var objCabeceras = Object.keys(objUsuarios[0]);
			var excluidos = ['Password', 'Autenticado', 'SolicitanteUsuarioID', 'PoseeLogin', 'UsuarioEstadoID', 'UsuarioTipoID'];
			for (i in excluidos) {
				remove(objCabeceras, excluidos[i])
			}

			replace(objCabeceras, 'UsuarioID', 'ID');
			replace(objCabeceras, 'UsuarioEstadoDescripcion', 'Estado');
			replace(objCabeceras, 'UsuarioTipoDescripcion', 'Tipo');
			replace(objCabeceras, 'PoseeLoginDescripcion', 'Login');

			objCabeceras.push('Acciones');

			var cabeceras = $('.usuarios-table thead tr');
			var cuerpo = $('.usuarios-table tbody');

			// carga cabeceras en la tabla
			cabeceras.html('');
			for (i in objCabeceras) {
				cabeceras.append('<th data-field="ExpedienteNro" class="">'
					+ objCabeceras[i]
					+ '</th>')
			}
			// carga los usuarios en la tabla
			for (i in objUsuarios) {
				cuerpo.append('<tr></tr>');
				cuerpo.find('tr:last-child').append('<td class="" style="padding: 0px 0px 0px 15px">'
					+ objUsuarios[i].UsuarioID
					+ '</td><td class="">'
					+ objUsuarios[i].Nombre
					+ '</td><td class="emailUsuario">'
					+ objUsuarios[i].Email
					+ '</td><td class="estadoUsuario" estadoID="'
					+ objUsuarios[i].UsuarioEstadoID
					+ '">'
					+ objUsuarios[i].UsuarioEstadoDescripcion
					+ '</td><td class="tipoUsuario" tipoUsuarioID="'
					+ objUsuarios[i].UsuarioTipoID
					+ '">'
					+ objUsuarios[i].UsuarioTipoDescripcion
					+ '</td><td class="">'
					+ objUsuarios[i].PoseeLoginDescripcion
					+ '</td><td class="left">'
					+ '<span class="secondary-content eliminaUsuario" target="'
					+ objUsuarios[i].UsuarioID
					+ '" nombre="'
					+ objUsuarios[i].Nombre
					+ '"><i class="material-icons red-text">delete</i></span>'
					+ '<span class="secondary-content editaUsuario" target="'
					+ objUsuarios[i].UsuarioID
					+ '" nombre="'
					+ objUsuarios[i].Nombre
					+ '"><i class="material-icons">mode_edit</i></span>'
					+ '</td>'
				);
			}

			$('.editaUsuario').click(function (event) {
				/* Act on the event */
				$('#BTNAgregar').hide();
				$('#BTNActualizar').show();
				limpiarForm();
				apagarFloatingButton();

				var idUsuario = $(this).attr('target');
				var NombreUsuario = $(this).attr('nombre');
				var tableRow = $(this).parent().parent();
				var EmailUsuario = tableRow.find('.emailUsuario').html();
				var EstadoUsuarioID = tableRow.find('.estadoUsuario').attr('estadoID');
				var TipoUsuarioID = tableRow.find('.tipoUsuario').attr('tipousuarioid');
				var TipoUsuarioDescripcion = tableRow.find('.tipoUsuario').html();

				$('#modal .modal-content h5').text('Editar Usuario: ' + NombreUsuario);
				$('#modal').openModal({
					complete: encenderFloatingButton
				});

				// $('#modal').find('#TB_nombre').val(NombreUsuario);
				cargarForm(idUsuario, NombreUsuario, EmailUsuario, TipoUsuarioID, TipoUsuarioDescripcion, EstadoUsuarioID);
			});

			$('.eliminaUsuario').click(function (event) {
				/* Act on the event */
				var idUsuario = $(this).attr('target');
				var NombreUsuario = $(this).attr('nombre');
				var tableRow = $(this).parent().parent();
				var EmailUsuario = tableRow.find('.emailUsuario').html();
				var EstadoUsuarioID = tableRow.find('.estadoUsuario').attr('estadoID');
				var TipoUsuarioID = tableRow.find('.tipoUsuario').attr('tipousuarioid');
				var TipoUsuarioDescripcion = tableRow.find('.tipoUsuario').html();
				apagarFloatingButton();

				$('#modal1 .modal-content h4').text('Baja de Usuario: ' + NombreUsuario);
				$('#modal1 .modal-content p').html('Ud. está a punto de dar de baja al usuario '
					+ NombreUsuario
					+ '.</br>Recuerde que sólo puede dar de baja a un Usuario que no tenga asignada ninguna empresa.</br>Si confirma no podrá volver atrás esta acción.')
				cargarForm(idUsuario, NombreUsuario, EmailUsuario, TipoUsuarioID, TipoUsuarioDescripcion, EstadoUsuarioID);
				$('#modal1').openModal({
					complete: encenderFloatingButton
				});

			});

			$('#nuevoUsuario').click(function (event) {
				/* Act on the event */
				/* Desactiva el Boton + mientras se asigna con el modal	*/
				apagarFloatingButton();

				$('#BTNAgregar').show();
				$('#BTNActualizar').hide();
				limpiarForm();

				$('#modal .modal-content h5').text('Agregar Nuevo Usuario');
				$('#modal').openModal({
					complete: encenderFloatingButton
				});
			});

		}, function failure(res) {
			// body...
		});

});


