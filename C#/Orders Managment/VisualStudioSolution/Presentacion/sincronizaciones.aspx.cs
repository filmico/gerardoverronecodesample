﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using Entidades;
using Negocio;

namespace Presentacion
{
    public partial class sincronizaciones : System.Web.UI.Page
    {

        usuario usr = new usuario();
        private bool modoDebug;

        protected void Page_Load(object sender, EventArgs e)
        {

            // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
            // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            // Apaga el history del navegador para que no guarde esta pagina.
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];

            // Si el usuario no es del tipo Administrador lo redirecciona al dashboard
            if (this.usr.UsuarioTipoID == 3)
            {
                // Apaga el Mensaje de Error
                Otros_Mensajes.IsValid = true;

                modoDebug = Convert.ToBoolean(WebConfigurationManager.AppSettings["modoDebug"]);


                try
                {
                    PHjs.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/sincronizaciones.js", "Scripts/sincronizaciones.min.js")));
                    PHjsFunciones.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/funciones.js", "Scripts/funciones.min.js")));
                }
                catch (Exception ex)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion

                    if (modoDebug == true)
                    {

                        Otros_Mensajes.ErrorMessage = "Sin conexion con el server o la base de datos!" + "<br/>" + ex.ToString();
                        Otros_Mensajes.Visible = true;
                        Otros_Mensajes.IsValid = false;
                    }
                    else
                    {
                        Otros_Mensajes.ErrorMessage = "No se pudo realizar la sincronizacion!";
                        Otros_Mensajes.Visible = true;
                        Otros_Mensajes.IsValid = false;
                    }

                    // Envia email a los desarrolladores
                    // ---------------------------------
                    try
                    {
                        string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                        string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                        string _subject = "bksys - Error de Aplicacion!";
                        // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                        _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                        _body += ex.ToString();
                        bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                    }
                    catch (Exception)
                    {

                        // Devuelve mensaje de error al usuario por problemas de conexion
                        if (modoDebug == true)
                        {
                            LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrlladores. " + ex.ToString();
                            LBLAlert.ToolTip = "pink";
                        }

                    }
                }
            }
            else
            {
                Response.Redirect("dashboard.aspx", false);
            }

        }
    }
}