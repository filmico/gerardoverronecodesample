﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sincronizaciones.aspx.cs" Inherits="Presentacion.sincronizaciones" %>


<%@ Register Src="~/cabecera.ascx" TagPrefix="uc1" TagName="cabecera" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>bksys</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

</head>
<body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <asp:PlaceHolder ID="PHjs" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
        <asp:Label ID="LBLAlert" runat="server" Text="" CssClass="hide"></asp:Label>
        <div>
            <%--CABECERA--%>
            <uc1:cabecera runat="server" ID="cabecera" />

            <div id="PanelSincronizaciones" class="row">
                <div class="col m12 s12">
                    <ul class="collection with-header z-depth-1">
                        <li class="collection-item dismissable">
                            <div>Estado 4<a href="#!" class="secondary-content"><i class="material-icons">send</i></a></div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>

        <br />
        <br />
        <br />

        <%--Mensajes al Usuario--%>
        <div class="row">

            <div class="col m12 s12">
                <p class="z-depth-3">
                    <asp:CustomValidator ID="Otros_Mensajes" runat="server" ErrorMessage="" Display="Dynamic" CssClass="error_asterisco" ForeColor="" Visible="False"></asp:CustomValidator>
                </p>
            </div>

        </div>

                        <%--Barra de Progreso--%>
        <div id="barraProgresoFooter" class="row">
            <div id="barraProgreso" class="col s12 m12 l12">
                <div class="indeterminate"></div>
            </div>
        </div>

    </form>
    <asp:PlaceHolder ID="PHjsFunciones" runat="server"></asp:PlaceHolder>
<%--    <script type="text/javascript" src="Scripts/funciones.js"></script>--%>
</body>
</html>
