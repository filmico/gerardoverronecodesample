﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="cabecera.ascx.cs" Inherits="Presentacion.cabecera" %>
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content ">
    <li><a id="cambioPassword"><i class="material-icons left">lock_open</i>cambiar Contraseña</a></li>
    <li class="divider"></li>
    <li><asp:LinkButton ID="BTNCierraSesion" runat="server" OnClick="BTNCierraSesion_Click" CssClass="red-text"><i class="material-icons left">exit_to_app</i>CERRAR SESIÓN</asp:LinkButton></li>
</ul>
<nav>
                <div class="nav-wrapper light-blue">
                    <a href="dashboard.aspx" class="brand-logo">
                        <img src="img/logobksys-01.svg" class="logo-bksys" alt="Alternate Text" />
                    </a>
                    <a href="#" data-activates="barra-lateral" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <asp:PlaceHolder ID="PHcabeceraArriba" runat="server"></asp:PlaceHolder>                    </ul>

                    
                    <ul class="side-nav" id="barra-lateral">
                        <li><img src="img/logobksys-01.svg" alt="Alternate Text" /></li>
                        
                        <asp:PlaceHolder ID="PHbarraLateral" runat="server"></asp:PlaceHolder>

                        <li>
                            <asp:LinkButton ID="BTNCierraSesionLateral"  runat="server" CssClass="red-text" OnClick="BTNCierraSesionLateral_Click"><i class="material-icons left red-text">exit_to_app</i> CERRAR SESIÓN</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </nav>
<div id="resultadoCambioPass" style="display: none;">
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
</div>
<%--MODAL--%>
<div id="modal-cambioPassword" class="modal">
    <div class="modal-content">
        <h5>Cambio de Contraseña</h5>
        <%-- Grupo Password 1 --%>
        <div id="grupoCambioPassword" class="input-field col s12 m6">
            <i class="material-icons prefix">more_horiz</i>
            <input type="password" name="usuarioPassword" id="usuarioPassword" />
            <label for="usuarioPassword">ingrese nueva contraseña</label>
        </div>

        <%-- Grupo Password 2 --%>
        <div id="grupoCambioPassword2" class="input-field col s12 m6">
            <i class="material-icons prefix">more_horiz</i>
            <input type="password" name="usuarioPassword2" id="usuarioPassword2" />
            <label for="usuarioPassword2">reingrese nueva contraseña</label>
        </div>
        <span id="alerta-password" class="bold" style="display: none;">* su contraseña debe ser idéntica en ambos campos</span>


        <br />
        <br />

    </div>
    <div class="modal-footer">
        <asp:LinkButton ID="BTNcambioPassword" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNcambioPassword_Click">Guardar</asp:LinkButton>
        <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>



    </div>
</div>
