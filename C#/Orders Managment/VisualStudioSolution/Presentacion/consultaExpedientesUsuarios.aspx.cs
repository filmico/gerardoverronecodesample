﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using Negocio;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Configuration;

namespace Presentacion
{
    public partial class consultaExpedientesUsuarios : System.Web.UI.Page
    {

        /// <summary>
        /// Instancia un usuario
        /// </summary>
        usuario usr = new usuario();

        bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);
        protected void Page_Load(object sender, EventArgs e)
        {
            // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
            // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            // Apaga el history del navegador para que no guarde esta pagina.
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];


            try
            {
                // Toma el nroExpediente que queremos consultar del query string.
                // Si no recibimos nada listamos todas las empresas asignadas segun mi tipo de usuario
                string ExpedienteNumeroID = Request.QueryString["expedientenumeroid"];

                // Instancia una lista de la antidad que queremos usar
                List<ExpedienteUsuario> ListaObjetosRetorno = new List<ExpedienteUsuario>();

                // Instancia un objeto simple de la entidad que queremos usar
                ExpedienteUsuario obj = new ExpedienteUsuario();

                obj.SolicitanteUsuarioID = this.usr.UsuarioID;

                if (ExpedienteNumeroID != null)
                {
                    obj.ExpedienteNumeroID = Convert.ToInt64(ExpedienteNumeroID);
                }

                ListaObjetosRetorno = AdmExpedienteUsuario.listarExpedienteUsuario(obj);

                // Importar con Nuget Newtonsoft -> Newtonsoft.Json  
                // http://www.newtonsoft.com/json    ->  using Newtonsoft.Json;

                var json = JsonConvert.SerializeObject(ListaObjetosRetorno);
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);

            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario
                if (modoDebug)
                {
                    var json = JsonConvert.SerializeObject(ex.ToString());
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }
                else
                {
                    var json = JsonConvert.SerializeObject("No se puede realizar la operacion solicitada!");
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                }

                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        var json = JsonConvert.SerializeObject("Hubo un Problema al intentar enviar el email a los desarrolladores!");
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(json);
                    }

                }

            }





        }
    }
}