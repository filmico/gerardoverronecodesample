﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;
using System.Collections.Specialized;
using System.Web.Security;

namespace Presentacion
{
    public partial class cabecera : System.Web.UI.UserControl
    {
        usuario usr = new usuario();
        NameValueCollection coleccionCampos = new NameValueCollection();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.usr = (usuario)Session["Usuario"];

            // Si no tenemos la variable de sesion del usuario lo deslogueamos y cerramos la sesion
            if (Context.Session != null && Context.Session.IsNewSession == true 
                && Page.Request.Headers["Cookie"] != null 
                && Page.Request.Headers["Cookie"].IndexOf("ASP.NET_SessionId") >= 0)
            {
                // session has timed out, log out the user
                if (Page.Request.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                }
                // redirect to timeout page
                Page.Response.Redirect("login.aspx");
            }



            if (usr.UsuarioTipoID > 2)
            {
                PHcabeceraArriba.Controls.Add(new Literal()
                {
                    Text = "<li><a href=\"usuarios.aspx\"><i class=\"material-icons left\">supervisor_account</i>Usuarios</a></li>"
                + "<li><a href=\"asignaciones.aspx\"><i class=\"material-icons left\">recent_actors</i> Asignaciones</a></li>"
                //+ "<li><a href=\"suscripciones.aspx\"><i class=\"material-icons left\">label</i> Suscripciones</a></li>"
                //+ "<li><a href=\"#\"><i class=\"material-icons left\">settings</i> Configuración</a></li>"
                + "<li><a href=\"sincronizaciones.aspx\"><i class=\"material-icons left\">compare_arrows</i> Sincronizaciones</a></li>"
                + "<li><a class=\"dropdown-button\" href=\"#\" data-activates=\"dropdown1\"><i class=\"material-icons left\">account_circle</i> usuario.<span class=\"bold\">"
                + usr.Nombre
                + "</span></a></li>"
                });

                PHbarraLateral.Controls.Add(new Literal()
                {
                    Text = "<li><a href=\"usuarios.aspx\"><i class=\"material-icons left\">supervisor_account</i>Usuarios</a></li>"
                + "<li><a href=\"asignaciones.aspx\"><i class=\"material-icons left\">recent_actors</i>Asignaciones</a></li>"
                //+ "<li><a href=\"suscripciones.aspx\"><i class=\"material-icons left\">label</i> Suscripciones</a></li>"
                //+ "<li><a href=\"#\"><i class=\"material-icons left\">settings</i> Configuración</a></li>"
                + "<li><a href=\"sincronizaciones.aspx\"><i class=\"material-icons left\">compare_arrows</i> Sincronizaciones</a></li>"
                + "<li><div class=\"divider\"></div></li>"
                + "<li><a class=\"subheader\">usuario.<span class=\"bold\">"
                + usr.Nombre
                + "</span></a></li>"
                + "<li><a onclick=\"$('#modal-cambioPassword').openModal()\"><i class=\"material-icons left\">lock_open</i> cambiar Contraseña</a></li>"
                }
                    );
            }
            else
            {
                PHcabeceraArriba.Controls.Add(new Literal()
                {
                    Text = "<li><a class=\"dropdown-button\" href=\"#\" data-activates=\"dropdown1\"><i class=\"material-icons left\">account_circle</i> usuario.<span class=\"bold\">"
                + usr.Nombre
                + "</span></a></li>"
                });

                PHbarraLateral.Controls.Add(new Literal()
                {
                    Text = "<li><a class=\"subheader\">usuario.<span class=\"bold\">"
                + usr.Nombre
                + "</span></a></li>"
                + "<li><a onclick=\"$('#modal-cambioPassword').openModal()\"><i class=\"material-icons left\">lock_open</i> cambiar Contraseña</a></li>"
                });
            }



        }

        protected void BTNcambioPassword_Click(object sender, EventArgs e)
        {
            coleccionCampos = Request.Form;
            usuario entidad = new usuario();
            entidad = usr;
            entidad = cargaEntidad(coleccionCampos, entidad);
            if (entidad.Password != "")
            {
                Negocio.AdmUsuario.actualizarUsuario(entidad);
                Literal1.Text = "ok";
            }

        }

        private usuario cargaEntidad(NameValueCollection coleccion, usuario entidad)
        {
            string[] nombresCampos = coleccionCampos.AllKeys;
            entidad.SolicitanteUsuarioID = usr.UsuarioID;
            string segundoPass = "";
            usuario entidad2 = new usuario();

            for (int i = 0; i < coleccionCampos.Count; i++)
            {
                if (nombresCampos[i] == "usuarioPassword")
                {
                    entidad.Password = Convert.ToString(coleccionCampos[i]);
                }

                if (nombresCampos[i] == "usuarioPassword2")
                {
                    segundoPass = Convert.ToString(coleccionCampos[i]);
                }
            }
            if (entidad.Password != segundoPass)
            {
                return entidad2;
            }
            return entidad;
        }

        protected void BTNCierraSesion_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("login.aspx");
        }

        protected void BTNCierraSesionLateral_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("login.aspx");
        }
    }
}