﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="Presentacion.dashboard2" %>

<%@ Register Src="~/cabecera.ascx" TagPrefix="uc1" TagName="cabecera" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>bksys</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <asp:PlaceHolder ID="PHjs" runat="server"></asp:PlaceHolder>
    <form id="form1" runat="server">
        <%--Mensajes de Error--%>
        <asp:Label ID="LBLAlert" runat="server" Text="" CssClass="hide"></asp:Label>
        <span id="opened" style="display: none;">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </span>
        <div>
            <%--CABECERA--%>
            <uc1:cabecera runat="server" ID="cabecera" />
            <div class="row">
                <div class="col m12 l12">
                    <ul class="dashboard-card collapsible collection with-header z-depth-1" data-collapsible="accordion">
                        <li class="collection-header">
                            <div class="row">
                                <div class="col s12 m12 l12">
                                    <h4>Expedientes Asignados por Empresa</h4>
                                    
                                </div>
                            </div>
                            <asp:Literal ID="Lt1" runat="server"></asp:Literal>
                        </li>
                    </ul>
                </div>
            </div>


  <!-- Modal Structure -->
  <div id="modalabajo" class="modal bottom-sheet">
    <div class="modal-content">
      <h4></h4>
      
    </div>
    <div class="modal-footer">
      <a href="#!" style="margin-right:20px" class=" modal-action modal-close waves-effect waves-green btn pink">Cerrar</a>
    </div>
  </div>






            <%--MODAL--%>
            <div id="modal" class="modal modal-cambioestadoliq" style="border-top: 3px solid rgb(3, 169, 244);">
                <div class="modal-content">
                    <h5>Cambiar estado de Liquidación</h5>
                    <%--Grupo Empresa--%>
                    <div id="grupoEstadoLiquidacion" class="input-field col s12 m6">
                        <i class="material-icons prefix">done</i>
                        <select name="estadoLiquidacion" id="estadoLiquidacion">
                        </select>
                    </div>
                    <input type="text" name="ExpedienteNumeroID" id="ExpedienteNumeroID" style="display: none;" />
                    <input type="text" name="EmpresaCuitID" id="EmpresaCuitID" style="display: none;" />

                    <br />
                    <br />

                </div>
                <div class="modal-footer">

                   
                     <asp:LinkButton ID="BTNCambiarEstadoLiquidacion" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNCambiarEstadoLiquidacion_Click" >Guardar</asp:LinkButton>
                    
                    <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>



                </div>
            </div>


        </div>
    </form>
    <asp:PlaceHolder ID="PHjsFunciones" runat="server"></asp:PlaceHolder>
    <div id="tu" style="display: none !important">
        <asp:Literal ID="lit2" runat="server"></asp:Literal>
    </div>
</body>
</html>
