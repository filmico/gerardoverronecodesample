﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="asignaciones.aspx.cs" Inherits="Presentacion.asignaciones" %>

<%@ Register Src="~/cabecera.ascx" TagPrefix="uc1" TagName="cabecera" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>bksys - usuarios</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="content/materialize/css/materialize.css" media="screen,projection" />
    <link href=<%="'content/estilo.css?v="+ DateTime.Now.ToString("yyyyMMddhhmmss") +"'"%> rel="stylesheet" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="Scripts/materialize/materialize.min.js"></script>
    <asp:PlaceHolder ID="PHjs" runat="server"></asp:PlaceHolder>

    <form id="form1" runat="server">
        <%--Mensajes de Error--%>
        <asp:Label ID="LBLAlert" runat="server" Text="" CssClass="hide"></asp:Label>

        <span id="opened" style="display: none;">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </span>
        <span id="opened2" style="display: none;">
             <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </span>

        <div>
            <%--CABECERA--%>
            <uc1:cabecera runat="server" ID="cabecera" />


            <div class="row">
                <div class="col s12 m12 l12">
                    <ul class="asignaciones-card collapsible collection with-header z-depth-1" data-collapsible="accordion">
                    </ul>
                </div>
            </div>

            <%-- NUEVA ASIGNACIÓN --%>
            <div class="fixed-action-btn">
                <a id="nuevaAsignacion" class="btn-floating btn-large waves-effect waves-light red boton-agregar"><i class="material-icons">add</i></a>
            </div>


            <%--MODAL--%>
            <div id="modal" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h5>Agregar Asignación</h5>
                    <%--Grupo Empresa--%>
                    <div id="grupoEmpresa" class="input-field col s12 m6">
                        <i class="material-icons prefix">work</i>
                        <select name="empresa" id="empresa">
                        </select>
                    </div>

                    <div id="grupoTipoUsuario" class="input-field col s12 m6">
                        <i class="material-icons prefix">supervisor_account</i>
                        <select name="tipoUsuario" id="tipoUsuario">
                        </select>
                    </div>


                    <div id="grupoUsuario" class="input-field col s12 m6">
                        <i class="material-icons prefix">person</i>
                        <select name="usuario" id="usuario">
                        </select>
                    </div>


                    <div id="grupoPorcentaje" class="input-field col m8 s12">
                        <i class="material-icons prefix active">pie_chart</i>
                        <input name="TB_porcentaje" type="number" id="TB_porcentaje" min="1" max="100" step="0.01">
                        <label for="icon_prefix" class="active">ingrese un porcentaje</label>
                    </div>
                    <input name="nroexpediente" type="text" id="nroexpediente" style="display: none !important" />
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="BTNAgregar" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNAgregar_Click">Guardar</asp:LinkButton>
                    <asp:LinkButton ID="BTNActualizar" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" runat="server" OnClick="BTNActualizar_Click">Actualizar</asp:LinkButton>
                    <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>

                </div>
            </div>

            <%--MODAL BAJA--%>

            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Borrar Asignación</h4>
                    <p>Texto de advertencia</p>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="BTNBorrar" runat="server" CssClass="modal-action modal-close waves-effect waves-green btn" Style="margin-left: 20px" OnClick="BTNBorrar_Click">Eliminar</asp:LinkButton>

                    <span class="modal-action modal-close waves-effect waves-green btn red">Cancelar</span>

                </div>
            </div>




            <!-- Modal Structure -->
            <div id="modalabajo" class="modal bottom-sheet">
                <div class="modal-content">
                    <h5>Editar Expediente</h5>

                </div>
                <div class="modal-footer">
                    <span id="cierraModalAbajo" style="margin-right: 20px" class=" modal-action modal-close waves-effect waves-green btn pink"><i class="material-icons left">close</i>Cerrar</span>

                    <span id="agregaAsignacion" style="margin-right: 20px" class="modal-action waves-effect waves-green btn light-green"><i class="material-icons left">add_circle_outline</i>Agregar Asignación</span>
                </div>
            </div>

        </div>


        <%--Barra de Progreso--%>
        <div id="barraProgresoFooter" class="row">
            <div id="barraProgreso" class="col s12 m12 l12">
                <div class="indeterminate"></div>
            </div>
        </div>

    </form>


    <asp:PlaceHolder ID="PHjsFunciones" runat="server"></asp:PlaceHolder>
</body>
</html>
