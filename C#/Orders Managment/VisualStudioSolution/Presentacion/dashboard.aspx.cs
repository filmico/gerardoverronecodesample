﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Entidades;
using Negocio;
using System.Web.Configuration;

namespace Presentacion
{
    public partial class dashboard2 : System.Web.UI.Page
    {
        bool modoDebug = Convert.ToBoolean(WebConfigurationManager.AppSettings["modoDebug"]);
        NameValueCollection coleccionCampos = new NameValueCollection();
        usuario usr = new usuario();
        int retorno;

        protected void Page_Load(object sender, EventArgs e)
        {
            Literal1.Text = "0";

            // Apaga el cache del navegador para que no se pueda hacer back sobre la pagina cacheada.
            // .NoCache no cahea ni en el server ni en el usuario .ServerAndNoCache no cachea en el usuario.
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            // Apaga el history del navegador para que no guarde esta pagina.
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            // Guardar la variable de sesion del usuario
            this.usr = (usuario)Session["Usuario"];

            lit2.Text = usr.UsuarioTipoID.ToString();

            bool modoDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["modoDebug"]);
            PHjs.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/dashboard.js", "Scripts/dashboard.min.js")));

            PHjsFunciones.Controls.Add(new LiteralControl(Negocio.Utilidades.serializador(modoDebug, "Scripts/funciones.js", "Scripts/funciones.min.js")));

            if (this.usr.UsuarioTipoID > 1)
            {
                Lt1.Text = "<div class=\"row\"><div class=\"col s12 m12 l3\" style=\"margin-bottom: 6px;\">"
                + "<div class=\"switch pendientes\"><span class=\"bold pendientes\">Pendientes:  </span> "
                + "<label>ocultar<input type=\"checkbox\" name=\"pendientes\" id=\"pendientes\" class=\"interruptor\"> "
                + " <span class=\"lever\"></span>mostrar</label></div></div>"

                + "<div class=\"col s12 m12 l3\" "
                + "style=\"margin-bottom: 6px;\"><div class=\"switch aliquidar\"><span class=\"bold aliquidar\">A Liquidar:  </span>"
                + "<label>ocultar<input type=\"checkbox\" name=\"aLiquidar\" id=\"aLiquidar\" class=\"interruptor\"><span class=\"lever\"></span>mostrar</label></div></div>"

                + "<div class=\"col s12 m12 l3\" style=\"margin-bottom: 6px;\"><div class=\"switch liquidados\"><span class=\"bold liquidados\">Liquidados:  </span> "
                + "<label>ocultar<input type=\"checkbox\" name=\"liquidados\" id=\"liquidados\" class=\"interruptor\"><span class=\"lever\"></span>mostrar</label></div></div>"

                + "<div class=\"col s12 m12 l3\" style=\"margin-bottom: 6px;\"><div class=\"switch finalizados\"><span class=\"bold finalizados\">Finalizados:  </span> "
                + "<label>ocultar<input type=\"checkbox\" name=\"finalizados\" id=\"finalizados\" class=\"interruptor\"><span class=\"lever\"></span>mostrar</label></div></div>"


                + "</div></div>";
            }

        }

        protected void BTNCambiarEstadoLiquidacion_Click(object sender, EventArgs e)
        {
            coleccionCampos = Request.Form;
            expedienteAdmin entidad = new expedienteAdmin();
            entidad = cargaEntidad(coleccionCampos);
            entidad.SolicitanteUsuarioID = this.usr.UsuarioID;
            Literal1.Text = Convert.ToString(entidad.Empresa.EmpresaCuitID) + "-" + Convert.ToString(entidad.EstadoLiquidacionID);

            try
            {
                retorno = Negocio.AdmExpediente.actualizarExpediente(entidad);
                if (retorno == 0)
                {
                    LBLAlert.Text = "El Expediente "
                        + entidad.ExpedienteNumeroID
                        + " se actualizó correctamente";
                    LBLAlert.ToolTip = "green";
                }
                else
                {
                    LBLAlert.Text = "El Expediente "
                        + entidad.ExpedienteNumeroID
                        + " no se pudo actualizar.";
                    LBLAlert.ToolTip = "amber";
                }
            }
            catch (Exception ex)
            {

                // Devuelve mensaje de error al usuario por problemas de conexion
                if (modoDebug == true)
                {
                    LBLAlert.Text = "Hubo un Problema al actualizar el Expediente. " + ex.ToString();
                    LBLAlert.ToolTip = "pink";
                }
                else
                {
                    LBLAlert.Text = "Hubo un Problema.Inténtelo más tarde.";
                    LBLAlert.ToolTip = "pink";
                }


                // Envia email a los desarrolladores
                // ---------------------------------
                try
                {
                    string _desarrollador1 = Utilidades.Mail.obtieneEmailDesarrollador1();
                    string _desarrollador2 = Utilidades.Mail.obtieneEmailDesarrollador2();
                    string _subject = "bksys - Error de Aplicacion!";
                    // string _body = "<br/><img src=\"http://desa.bksys.com/img/logobksys-01.jpg\" alt=\"bksys\"><BR><BR><span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    string _body = "<span style=\"color: #ff0000; font-weight: bold\">Email enviado desde el try..catch de ";
                    _body += this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx" + "</span><BR><BR>";
                    _body += ex.ToString();
                    bool envioEmail = Utilidades.Mail.EnviaEmail(_desarrollador1, _subject, _body, _desarrollador2);
                }
                catch (Exception)
                {

                    // Devuelve mensaje de error al usuario por problemas de conexion
                    if (modoDebug == true)
                    {
                        LBLAlert.Text = "Hubo un Problema al intentar enviar el email a los desarrolladores. " + ex.ToString();
                        LBLAlert.ToolTip = "pink";
                    }

                }

            }
        }

        private expedienteAdmin cargaEntidad(NameValueCollection coleccion)
        {
            expedienteAdmin entidad = new expedienteAdmin();

            string[] nombresCampos = coleccionCampos.AllKeys;
            entidad.SolicitanteUsuarioID = this.usr.UsuarioID;

            for (int i = 0; i < coleccionCampos.Count; i++)
            {
                if (nombresCampos[i] == "estadoLiquidacion")
                {
                    entidad.EstadoLiquidacionID = Convert.ToInt32(coleccionCampos[i]);
                }

                if (nombresCampos[i] == "ExpedienteNumeroID")
                {
                    entidad.ExpedienteNumeroID = Convert.ToInt64(coleccionCampos[i]);
                }

                if (nombresCampos[i] == "EmpresaCuitID")
                {
                    entidad.Empresa.EmpresaCuitID = Convert.ToInt64(coleccionCampos[i]);
                }
            }

            return entidad;
        }
    }
}