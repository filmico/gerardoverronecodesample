﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmCargaDatosLog
    {

        public static int agregarCargaDatosLog(CargaDatosLog entidad)
        {
            try
            {
                CargaDatosLogDato objCargaDatosLogDato = new CargaDatosLogDato();
                return objCargaDatosLogDato.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<CargaDatosLog> listarCargaDatosLog(CargaDatosLog entidad)
        {
            try
            {
                CargaDatosLogDato objCargaDatosLogDato = new CargaDatosLogDato();
                return objCargaDatosLogDato.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }
  

    }
}
