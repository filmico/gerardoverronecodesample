﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration; // Agregar la Referencia  a system.configuration para poder leer el web.config
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Runtime.Remoting.Contexts;
using System.Web.Security;

namespace Negocio
{
    public static class Utilidades
    {
        public static string serializador(bool modoDebug, string ruta1, string ruta2 = "")
        {
            string uTime, v;
            uTime = Convert.ToString(Math.Ceiling((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds));
            if (modoDebug == true)
            {
                v = "<script type='text/javascript' src='" + ruta1 + "?v=" + uTime + "'></script>";
            }
            else
            {
                v = "<script type='text/javascript' src='" + ruta2 + "?v=" + uTime + "'></script>";
            }
            return v;
        }


        /// <summary>
        /// Módulo Utilizado para Enviar emails desde la capa de aplicación en el caso de que ocurra un error en
        /// tiempo de ejecución.
        /// Todos los Try Catch de la app contemplan el envío de un email a los desarrolladores para alertarlos
        /// sobre inconvenientes ocurridos.
        /// Este Módulo lee del web.config que pertenece a la capa de Presentación, las etiquetas 
        /// _desarrollador1 y _desarrollador2 para obtener los emails a los que hay que enviar el Email.
        /// 
        /// <p>Links de referencia:</p>
        /// <p><a href="http://www.tutorialspoint.com/vb.net/vb.net_send_email.htm" target="_blank">Links 1</a></p>
        /// <p><a href="http://www.codeproject.com/Articles/66257/Sending-Mails-in-NET-Framework" target="_blank">Links 2</a></p>
        /// <p><a href="http://systemnetmail.com/" target="_blank">Links 3</a></p>
        /// 
        /// </summary>
        public static class Mail
        {

            public static string obtieneEmailDesarrollador1()
            {
                return System.Configuration.ConfigurationManager.AppSettings["_desarrollador1"].ToString();
            }
            public static string obtieneEmailDesarrollador2()
            {
                return System.Configuration.ConfigurationManager.AppSettings["_desarrollador2"].ToString();
            }

            // <summary>
            // Función que realiza el envío del email. Los parámetros de configuración del servidor smtp
            // se obtienen del web.config que se encuentra ubicado en la capa de Presentación.
            // </summary>
            // <param name="_to">Email de:...</param>
            // <param name="_subject">El asunto del Email.</param>
            // <param name="_body">El Cuerpo del Email. Contempla que sea un contenido html.</param>
            // <param name="_cc">Copia del Email para:....</param>
            // <returns><c>true</c> Retorna siempre True. No se contemplo un escenario de retorno de False <c>false</c> otherwise.</returns>
            public static bool EnviaEmail(string _to, string _subject, string _body, string _cc = "")
            {
                try
                {

                    // Obtiene las variables del Web.Config
                    string _smtpServer = System.Configuration.ConfigurationManager.AppSettings["smtpServer"].ToString();
                    string _useDefaultCredentials = System.Configuration.ConfigurationManager.AppSettings["useDefaultCredentials"].ToString();
                    string _senderEmail = System.Configuration.ConfigurationManager.AppSettings["senderEmail"].ToString();
                    string _senderPassword = System.Configuration.ConfigurationManager.AppSettings["senderPassword"].ToString();
                    string _useSSL = System.Configuration.ConfigurationManager.AppSettings["useSSL"].ToString();
                    string _smtpPickupDirectory = System.Configuration.ConfigurationManager.AppSettings["smtpPickupDirectory"].ToString();
                    string _smtpServerPort = System.Configuration.ConfigurationManager.AppSettings["smtpServerPort"].ToString();
                    string _deliveryMethod = System.Configuration.ConfigurationManager.AppSettings["deliveryMethod"].ToString();

                    // Configura los parametros del server
                    SmtpClient Smtp_Server = new SmtpClient();
                    MailMessage e_mail = new MailMessage();
                    Smtp_Server.Host = _smtpServer;
                    Smtp_Server.UseDefaultCredentials = Convert.ToBoolean(_useDefaultCredentials);


                    Smtp_Server.Credentials = new System.Net.NetworkCredential(_senderEmail, _senderPassword);
                    Smtp_Server.EnableSsl = Convert.ToBoolean(_useSSL);
                    Smtp_Server.PickupDirectoryLocation = _smtpPickupDirectory;
                    Smtp_Server.Port = Convert.ToInt32(_smtpServerPort);
                    // Smtp_Server.DeliveryMethod = _deliveryMethod
                    Smtp_Server.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    // Compone el email
                    e_mail = new MailMessage();
                    e_mail.From = new MailAddress(_senderEmail);
                    e_mail.To.Add(_to);
                    // Si hay CC lo agrega
                    if (_cc != null)
                    {
                        e_mail.CC.Add(_cc);
                    }

                    e_mail.Subject = _subject;
                    e_mail.IsBodyHtml = true;
                    e_mail.Body = _body;

                    // Envia el email
                    Smtp_Server.Send(e_mail);

                    return true;


                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
    }
}
