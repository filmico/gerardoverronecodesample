﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmExpedienteUsuario
    {
        public static int agregarExpedienteUsuario(ExpedienteUsuario entidad)
        {
            try
            {
                ExpedienteUsuarioDato obj = new ExpedienteUsuarioDato();
                return obj.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }
        public static int borrarExpedienteUsuario(ExpedienteUsuario entidad)
        {
            try
            {
                ExpedienteUsuarioDato obj = new ExpedienteUsuarioDato();
                return obj.eliminar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static int actualizarExpedienteUsuario(ExpedienteUsuario entidad)
        {
            try
            {
                ExpedienteUsuarioDato obj = new ExpedienteUsuarioDato();
                return obj.actualizar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<ExpedienteUsuario> listarExpedienteUsuario(ExpedienteUsuario entidad)
        {
            try
            {
                ExpedienteUsuarioDato obj = new ExpedienteUsuarioDato();
                return obj.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }


        }


    }
}
