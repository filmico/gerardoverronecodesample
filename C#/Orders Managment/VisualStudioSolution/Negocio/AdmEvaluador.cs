﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmEvaluador
    {

        public static int agregarEvaluador(evaluador entidad)
        {
            try
            {
                EvaluadorDato obj = new EvaluadorDato();
                return obj.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<evaluador> listarEvaluador(evaluador entidad)
        {
            try
            {
                EvaluadorDato obj = new EvaluadorDato();
                return obj.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }


        }

    }
}