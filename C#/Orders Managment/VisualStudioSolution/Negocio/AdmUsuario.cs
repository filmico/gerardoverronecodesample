﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmUsuario
    {
        public static int agregarUsuarioSinLogin(usuario entidad)
        {
            try
            {
                UsuarioDato objUsuarioDato = new UsuarioDato();
                int retorno;
                // Si no hay password usa modo 0 (Usuario sin login)
                if (entidad.Password == "")
                {
                    retorno = objUsuarioDato.agregar(entidad);
                }
                else
                {
                    retorno = objUsuarioDato.agregar(entidad, 1);
                }
                return retorno;
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<usuario> listarUsuarios(usuario entidad)
        {
            try
            {
                UsuarioDato objUsuarioDato = new UsuarioDato();
                return objUsuarioDato.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static int borrarUsuario(usuario entidad)
        {
            try
            {
                UsuarioDato objUsuarioDato = new UsuarioDato();
                return objUsuarioDato.eliminar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }

        }

        public static int actualizarUsuario(usuario entidad)
        {
            try
            {
                UsuarioDato objUsuarioDato = new UsuarioDato();
                return objUsuarioDato.actualizar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<usuario> autenticarUsuario(usuario entidad)
        {
            try
            {
                UsuarioDato objUsuarioDato = new UsuarioDato();
                return objUsuarioDato.autenticar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

    }
}




