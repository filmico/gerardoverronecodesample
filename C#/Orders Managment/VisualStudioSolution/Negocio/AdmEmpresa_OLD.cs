﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmEmpresa_OLD
    {

        public static int agregarEmpresa(empresa entidad)
        {
            try
            {
                EmpresaDato obj = new EmpresaDato();
                return obj.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }


        public static List<empresa> listarEmpresas(empresa entidad)
        {
            try
            {
                EmpresaDato obj = new EmpresaDato();
                return obj.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }


        }


    }
}
