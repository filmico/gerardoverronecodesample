﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using Entidades;
using Datos;

namespace Negocio
{
    public static class CargadorWeb
    {
        public static DateTime conversionHoraria()
        {
            // Obtenemos la fecha actual
            DateTime fechaHora = DateTime.Now;
            int diaDeLaSemana = Convert.ToInt32(fechaHora.DayOfWeek);

            //// ID from: 
            //// "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Time Zone"
            //// See http://msdn.microsoft.com/en-us/library/system.timezoneinfo.id.aspx
            //string nzTimeZoneKey = "New Zealand Standard Time";
            //TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
            //DateTime nzDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, nzTimeZone);


            DateTime serverTime = DateTime.Now; // gives you current Time in server timeZone
            DateTime utcTime = serverTime.ToUniversalTime(); // convert it to Utc using timezone setting of server computer
            TimeZoneInfo arg = TimeZoneInfo.FindSystemTimeZoneById("Argentina Standard Time");
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, arg); // convert from utc to local
            return localTime;

        }

        private static MatchCollection consultaDatosExternos(string tipoConsulta)
        {

            try
            {
                // Pagina para chequeo manual

                // Emulador Carga del Server Externo con una pagina web dummy dentro del proyecto
                //string url = "http://localhost:14552/emulaServerExternoEstado12.html";

                // Carga del server externo Real!
                string url = "http://bksys.com/libs/compdf/status.php?id=" + tipoConsulta;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 999999999;

                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
                //request.Accept = "text/html";

                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.Default, true);

                // StringBuilder html = new StringBuilder(sr.ReadToEnd());
                StringBuilder html = new StringBuilder(sr.ReadToEnd());

                // Limpiamos el html
                html.Replace(@"<td  >", "<td >");

                // Usamos una expresion regular para encontrar la ocurrencia de dos etiquetas y nos quedamos con el contenido
                // interno de las etiquetas. 
                MatchCollection m1 = Regex.Matches(html.ToString(), @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>", RegexOptions.Singleline);
                return m1;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        /// <summary>
        /// Trae los Expedientes en una tabla html.
        /// </summary>
        /// <param name="tipoConsulta">El tipo consulta se determina por el
        /// estado a descargar. Es un número..</param>
        /// <returns>List&lt;IConectable&gt;.</returns>
        public static int cargaWeb(int tipoConsulta, int modo = 0) // modo = 0 es automático
        {

            try
            {
                int ExpedientesObtenidos = 0;
                List<int> ExpedientesPosibles = new List<int>();
                List<int> ExpedientesActualizados = new List<int>();

                // CONSULTA FECHA Y HORA ACTUAL
                DateTime ahora = conversionHoraria();
                TimeSpan ahoraSpan = ahora.TimeOfDay;

                // CONSULTA SI ESTÁ EN EL RANGO AUTORIZADO
                // CONSULTA A DB ConfigSincronizaciones
                // - true | false
                configSincronizacion ObjConfig = new configSincronizacion();

                ObjConfig.SolicitanteUsuarioID = 1;
                ObjConfig.Estado = 1;

                List<configSincronizacion> ListaConfigRetorno = new List<configSincronizacion>();
                ListaConfigRetorno = Negocio.AdmConfigSincronizacion.listarConfigSincronizacion(ObjConfig);

                // Para cada configuración obtenida de la base de datos
                foreach (var configuracion in ListaConfigRetorno)
                {


                    // SI COINCIDE EL TIPO DE CONSULTA CON LOS QUE VINIERON DE LA BASE
                    //if ((configuracion).EstadoExpedienteID == tipoConsulta)
                    //{
                    // dentro del rango de días y horario de ConfigSincronizaciones
                    if ((modo == 1) || (modo == 0
                        && (configuracion.DiaSemanaComienzo <= Convert.ToInt16(ahora.DayOfWeek)
                        && (configuracion.DiaSemanaFinalizacion >= Convert.ToInt16(ahora.DayOfWeek)
                        && (configuracion.HoraComienzo <= ahora)
                        && (configuracion.HoraFinalizacion >= ahora)
                        && (configuracion.EstadoExpedienteID == tipoConsulta)))))
                    {
                        // Medimos Tiempo del StringBuilder
                        //DateTime ComienzoProceso = DateTime.Now;
                        DateTime ComienzoProceso = conversionHoraria();

                        // se consultan los datos externos por web
                        MatchCollection m1 = consultaDatosExternos(tipoConsulta.ToString());
                        ExpedientesObtenidos = (m1.Count / 11) - 1;

                        List<estadoSincronizacion> ListaEstadoSincroRetorno = new List<estadoSincronizacion>();
                        estadoSincronizacion ObjestadoSincro = new estadoSincronizacion();
                        ListaEstadoSincroRetorno = Negocio.AdmEstadoSincronizacion.listarEstadosSincronizacion(ObjestadoSincro);

                        // La lista de Elementos del tipo estadoSincronización
                        foreach (var estadoSincronizacion in ListaEstadoSincroRetorno)
                        {
                            // SI EL ESTADO DE SINCRONIZACIÓN ES IGUAL AL TIPO DE CONSULTA
                            if (estadoSincronizacion.ExpedienteEstado.ExpedienteEstadoID == tipoConsulta)
                            {

                                // POR CADA FILA DE LA CONSULTA AL SERVER EXTERNO
                                for (int i = 11; i < m1.Count; i+=11)
                                {
                                    DateTime fechaRegistro = DateTime.Now;
                                    if (string.IsNullOrEmpty(Convert.ToString(m1[i + 8].Groups[1].Value)) == false)
                                    {
                                        fechaRegistro = Convert.ToDateTime(m1[i + 8].Groups[1].Value);
                                    }

                                    DateTime fechaUltimaSincro = estadoSincronizacion.UltimaSincronizacionFinalizacion;

                                    // SI LA FECHA DEL EXPEDIENTE ES MAYOR A LA FECHA DE ÚLTIMA SINCRONIZACIÓN

                                    // if (fechaRegistro >= fechaUltimaSincro)

                                    // .Date separa la fecha de la hora
                                    if (fechaRegistro.Date >= fechaUltimaSincro.Date)
                                    {
                                        // INSERTA A LA BASE DE DATOS
                                        expediente ObjExpediente = new expediente();
                                        expedientesEstado ObjExpedientesEstado = new expedientesEstado();

                                        // empresa
                                        ObjExpediente.Empresa.NroEmpresa = m1[i].Groups[1].Value;
                                        ObjExpediente.Empresa.EmpresaCuitID = Convert.ToInt64(m1[i + 2].Groups[1].Value);
                                        ObjExpediente.Empresa.RazonSocial = m1[i + 3].Groups[1].Value;
                                        Negocio.AdmEmpresa.agregarEmpresa(ObjExpediente.Empresa);

                                        // Evaluador
                                        if (m1[i + 9].Groups[1].Value == ", ")
                                        {
                                            ObjExpediente.Evaluador.Nombre = ("");
                                            //ObjEvaluador.Nombre = ("");
                                        }
                                        else
                                        {
                                            ObjExpediente.Evaluador.Nombre = (m1[i + 9].Groups[1].Value);
                                        }
                                        // Evaluador Técnico
                                        if (m1[i + 10].Groups[1].Value == ", ")
                                        {
                                            ObjExpediente.EvaluadorTecnico.Nombre = ("");
                                            //ObjEvaluador.Nombre = ("");
                                        }
                                        else
                                        {
                                            ObjExpediente.EvaluadorTecnico.Nombre = (m1[i + 10].Groups[1].Value);
                                        }

                                        Negocio.AdmEvaluador.agregarEvaluador(ObjExpediente.Evaluador);
                                        Negocio.AdmEvaluador.agregarEvaluador(ObjExpediente.EvaluadorTecnico);

                                        // Expedientes
                                        // nro de expediente
                                        char[] delimitador = { '/', '-' };
                                        string[] strExpNro = (m1[i + 1].Groups[1].Value).Split(delimitador);
                                        Int64 ExpNro = Convert.ToInt64(strExpNro[0] + strExpNro[1]);
                                        ObjExpediente.ExpedienteNumeroID = ExpNro;

                                        string[] strSolicitudNro = (m1[i + 4].Groups[1].Value).Split('-');
                                        int SolicitudNro = Convert.ToInt32(strSolicitudNro[0] + strSolicitudNro[1]);
                                        ObjExpediente.NroSolicitud = SolicitudNro;

                                        // split de los Beneficios
                                        char[] delimitadoresBeneficios = { ' ', '$', '.' };
                                        string[] partBenefSol = (m1[i + 5].Groups[1].Value).Split(delimitadoresBeneficios);
                                        string[] partBenefOt = (m1[i + 6].Groups[1].Value).Split(delimitadoresBeneficios);
                                        string benefSol = "";
                                        string benefOt = "";

                                        foreach (var item in partBenefSol)
                                        {
                                            benefSol = benefSol + item;
                                        }
                                        foreach (var item in partBenefOt)
                                        {
                                            benefOt = benefOt + item;
                                        }

                                        //ObjExpediente.ExpedienteNumeroID = Convert.ToInt64(m1[i + 4].Groups[1].Value);
                                        ObjExpediente.BeneficioSolicitado = Convert.ToDecimal(benefSol);
                                        ObjExpediente.BeneficioAprobado = Convert.ToDecimal(benefOt);

                                        // ExpedienteEstado
                                        ObjExpedientesEstado.Descripcion = Convert.ToString(m1[i + 7].Groups[1].Value);

                                        ObjExpediente.ExpedienteEstado = ObjExpedientesEstado;

                                        if (m1[i + 8].Groups[1].Value != "")
                                        {
                                            ObjExpediente.FechaEstadoActual = Convert.ToDateTime(m1[i + 8].Groups[1].Value); // fecha
                                        }

                                        int retorno = AdmExpediente.agregarExpediente(ObjExpediente);

                                        ExpedientesPosibles.Add(retorno);
                                        if (retorno == 0)
                                        {
                                            ExpedientesActualizados.Add(retorno);
                                        }
                                    }
                                    //i += 10;
                                }

                                // DateTime FinProceso = DateTime.Now;
                                DateTime FinProceso = conversionHoraria();

                                // estadoSincronizacion
                                estadoSincronizacion ObjEstadoSincronizacion = new estadoSincronizacion();

                                ObjEstadoSincronizacion.ExpedienteEstado.ExpedienteEstadoID = tipoConsulta;
                                ObjEstadoSincronizacion.UltimaSincronizacionComienzo = ComienzoProceso;
                                ObjEstadoSincronizacion.UltimaSincronizacionFinalizacion = FinProceso;
                                ObjEstadoSincronizacion.ExpedientesObtenidos = ExpedientesObtenidos;
                                ObjEstadoSincronizacion.ExpedientesPosibles = ExpedientesPosibles.Count;
                                ObjEstadoSincronizacion.ExpedientesActualizados = ExpedientesActualizados.Count;
                                Negocio.AdmEstadoSincronizacion.agregarEstadoSincronizacion(ObjEstadoSincronizacion);


                            }
                        }


                    }
                    // si es manual, interrumple el ciclo de las configuraciones.
                    if (modo == 1)
                    {
                        break;
                    }
                    //}

                }
                return ExpedientesObtenidos;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}

