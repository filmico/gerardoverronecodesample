﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmExpediente
    {
        public static int agregarExpediente(expediente entidad)
        {
            try
            {
                ExpedienteDato objExpediente = new ExpedienteDato();
                return objExpediente.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }

        }

        public static int actualizarExpediente(expedienteAdmin entidad)
        {
            try
            {
                ExpedienteDato objExpediente = new ExpedienteDato();
                return objExpediente.actualizar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }

        }

        public static List<expediente> listarExpedientes(expediente entidad)
        {

            try
            {
                ExpedienteDato objExpedienteDato = new ExpedienteDato();
                return objExpedienteDato.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }

        }
    }
}
