﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmExpedientesEstado
    {
        public static int agregarExpedienteEstado(expedientesEstado entidad)
        {
            ExpedienteEstadoDato objExpedientesEstado = new ExpedienteEstadoDato();
            return objExpedientesEstado.agregar(entidad);
        }

        public static List<expedientesEstado> listarExpedientesEstado(expedientesEstado entidad)
        {
            ExpedienteEstadoDato objExpedienteEstadoDato = new ExpedienteEstadoDato();
            return objExpedienteEstadoDato.listar(entidad);
        }


    }
}
