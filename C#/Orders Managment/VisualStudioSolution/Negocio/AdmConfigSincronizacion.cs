﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmConfigSincronizacion
    {
        public static int agregarConfigSincronizacion(configSincronizacion entidad)
        {
            try
            {
                ConfigSincronizacionDato obj = new ConfigSincronizacionDato();
                return obj.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }



        }

        public static List<configSincronizacion> listarConfigSincronizacion(configSincronizacion entidad)
        {
            try
            {
                ConfigSincronizacionDato obj = new ConfigSincronizacionDato();
                return obj.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }


        }
    }
}
