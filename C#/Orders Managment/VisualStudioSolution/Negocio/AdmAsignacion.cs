﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmAsignacion
    {

        public static int agregarAsignacion(Asignacion entidad)
        {
            try
            {
                AsignacionDato obj = new AsignacionDato();
                return obj.agregar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static int borrarAsignacion(Asignacion entidad)
        {
            try
            {
                AsignacionDato obj = new AsignacionDato();
                return obj.eliminar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static int actualizarAsignacion(Asignacion entidad)
        {
            try
            {
                AsignacionDato obj = new AsignacionDato();
                return obj.actualizar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }

        public static List<Asignacion> listarAsignacion(Asignacion entidad)
        {
            try
            {
                AsignacionDato obj = new AsignacionDato();
                return obj.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }


        }

    }
}
