﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmEstadoSincronizacion
    {
        public static int agregarEstadoSincronizacion(estadoSincronizacion entidad)
        {
            EstadoSincronizacionDato objEstadoSincro = new EstadoSincronizacionDato();
            return objEstadoSincro.agregar(entidad);
        }

        public static List<estadoSincronizacion> listarEstadosSincronizacion(estadoSincronizacion entidad)
        {
            EstadoSincronizacionDato objEstadoSincro = new EstadoSincronizacionDato();
            return objEstadoSincro.listar(entidad);
        }
    }
}
