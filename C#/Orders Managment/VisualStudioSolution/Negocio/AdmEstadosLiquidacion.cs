﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public static class AdmEstadosLiquidacion
    {
        public static List<liquidacion> listarEstadosLiquidacion(liquidacion entidad)
        {
            try
            {
                liquidacionDato objLiquidacionDato = new liquidacionDato();
                return objLiquidacionDato.listar(entidad);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Presentación.
                throw ex;
            }
        }
    }
}
