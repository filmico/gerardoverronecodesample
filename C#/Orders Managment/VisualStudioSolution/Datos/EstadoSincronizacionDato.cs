﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;


namespace Datos
{
    public class EstadoSincronizacionDato : IConectable<estadoSincronizacion>
    {
        public List<estadoSincronizacion> ListaEntidades { get; set; }
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }
        public int actualizar(estadoSincronizacion entidad)
        {
            throw new NotImplementedException();
        }
        public int agregar(estadoSincronizacion entidad, int modo = 0)
        {
            nombreProcedure = "EstadoSincronizacionesInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteEstadoID", System.Data.SqlDbType.Int),
                new SqlParameter("@UltimaSincronizacionComienzo", System.Data.SqlDbType.DateTime2, 7),
                new SqlParameter("@UltimaSincronizacionFin", System.Data.SqlDbType.DateTime2, 7),
                new SqlParameter("@ExpedientesObtenidos", System.Data.SqlDbType.Int),
                new SqlParameter("@ExpedientesPosibles", System.Data.SqlDbType.Int),
                new SqlParameter("@ExpedientesActualizados", System.Data.SqlDbType.Int),
            };
            misParametros[0].Value = entidad.ExpedienteEstado.ExpedienteEstadoID;
            misParametros[1].Value = entidad.UltimaSincronizacionComienzo;
            misParametros[2].Value = entidad.UltimaSincronizacionFinalizacion;
            misParametros[3].Value = entidad.ExpedientesObtenidos;
            misParametros[4].Value = entidad.ExpedientesPosibles;
            misParametros[5].Value = entidad.ExpedientesActualizados;

            this.parametros = misParametros;
            accesoDatos();
            return this.statusDB;
        }
        public int eliminar(estadoSincronizacion entidad)
        {
            throw new NotImplementedException();
        }
        public List<estadoSincronizacion> listar(estadoSincronizacion entidad)
        {
            nombreProcedure = "EstadoSincronizacionesSelProc";

            if (entidad.ExpedienteEstado.ExpedienteEstadoID == 0)
            {
                SqlParameter[] misParametros = {
                new SqlParameter("@Modo", System.Data.SqlDbType.Int)
                };
                misParametros[0].Value = 0;
                this.parametros = misParametros;
            }
            else
            {
                SqlParameter[] misParametros = {
                new SqlParameter("@ExpedienteEstadoID", System.Data.SqlDbType.Int)
                };
                misParametros[0].Value = entidad.ExpedienteEstado.ExpedienteEstadoID;
                this.parametros = misParametros;
            }
            accesoDatos();
            return this.ListaEntidades;
        }
        public void Descomponer(SqlDataReader Rdr)
        {
            List<estadoSincronizacion> Lista = new List<estadoSincronizacion>();

            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    estadoSincronizacion Objeto = new estadoSincronizacion();
                    
                    Objeto.ExpedienteEstado.ExpedienteEstadoID = Convert.ToInt32(Rdr["ExpedienteEstadoID"]);
                    Objeto.ExpedienteEstado.Descripcion = Convert.ToString(Rdr["Descripcion"]);
                    Objeto.UltimaSincronizacionComienzo = Convert.ToDateTime(Rdr["UltimaSincronizacionComienzo"]);
                    Objeto.UltimaSincronizacionFinalizacion = Convert.ToDateTime(Rdr["UltimaSincronizacionFinalizacion"]);
                    Objeto.EstadoSincronizacion = Convert.ToInt32(Rdr["EstadoSincronizacion"]);
                    Objeto.ExpedientesObtenidos = Convert.ToInt32(Rdr["ExpedientesObtenidos"]);
                    Objeto.ExpedientesPosibles = Convert.ToInt32(Rdr["ExpedientesPosibles"]);
                    Objeto.ExpedientesActualizados = Convert.ToInt32(Rdr["ExpedientesActualizados"]);
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }
    }

}
