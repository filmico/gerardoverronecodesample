﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Datos
{
    public interface IConectable<T>
    {
        // Propiedades (Sin Implementar)
        string nombreProcedure { get; set; }
        SqlParameter[] parametros { get; set; }
        int statusDB { get; set; }
        List<T> ListaEntidades {get;set;}

        // Metodos (Sin Implementar)
        int agregar(T entidad, int modo = 0);
        int eliminar(T entidad);
        int actualizar(T entidad);
        List<T> listar(T entidad);
        void Descomponer(SqlDataReader Rdr);
        void accesoDatos();



    }
}
