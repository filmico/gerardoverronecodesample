﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    public class EmpresaDato : IConectable<empresa>
    {
        public List<empresa> ListaEntidades { get; set; }

        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }

        public int statusDB { get; set; }

        public int actualizar(empresa entidad)
        {
            throw new NotImplementedException();
        }

        public int agregar(empresa entidad, int modo = 0)
        {
            nombreProcedure = "EmpresasInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@NroEmpresa", System.Data.SqlDbType.NVarChar, 5),
                new SqlParameter("@RazonSocial", System.Data.SqlDbType.NVarChar, 200)
            };
            misParametros[0].Value = entidad.EmpresaCuitID;
            misParametros[1].Value = entidad.NroEmpresa;
            misParametros[2].Value = entidad.RazonSocial;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public int eliminar(empresa entidad)
        {
            throw new NotImplementedException();
        }

        public List<empresa> listar(empresa entidad)
        {
            nombreProcedure = "EmpresasSelProc";
            SqlParameter[] misParametros = {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@NroEmpresa", System.Data.SqlDbType.NVarChar, 5),
                new SqlParameter("@RazonSocial", System.Data.SqlDbType.NVarChar, 200)
                };

            if (entidad.EmpresaCuitID == 0
                && string.IsNullOrEmpty(entidad.NroEmpresa) == true
                && string.IsNullOrEmpty(entidad.RazonSocial) == true) // Listar todos
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
            }
            else if (entidad.EmpresaCuitID != null) // listar por cuit
            {
                misParametros[0].Value = entidad.EmpresaCuitID;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
            }
            else if (string.IsNullOrEmpty(entidad.NroEmpresa) == false) // listar por nro empresa
            {
                misParametros[0].Value = null;
                misParametros[1].Value = entidad.NroEmpresa;
                misParametros[2].Value = null;

            }
            else if (string.IsNullOrEmpty(entidad.RazonSocial) == false) // listar por razon social
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = entidad.RazonSocial;
            };

            this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;
        }

        public void Descomponer(SqlDataReader Rdr)
        {
            List<empresa> Lista = new List<empresa>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    empresa Objeto = new empresa();
                    Objeto.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                    Objeto.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                    Objeto.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}
