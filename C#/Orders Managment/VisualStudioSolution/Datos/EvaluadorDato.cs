﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    public class EvaluadorDato : IConectable<evaluador>
    {
        public List<evaluador> ListaEntidades { get; set; }
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }

        public int actualizar(evaluador entidad)
        {
            throw new NotImplementedException();
        }

        public int agregar(evaluador entidad, int modo = 0)
        {
            nombreProcedure = "EvaluadoresInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar, 100)
            };
            misParametros[0].Value = entidad.Nombre;
            parametros = misParametros;
            accesoDatos();
            return this.statusDB;
        }

        public int eliminar(evaluador entidad)
        {
            throw new NotImplementedException();
        }

        public List<evaluador> listar(evaluador entidad)
        {
            this.nombreProcedure = "EvaluadoresSelProc";

            SqlParameter[] misParametros =
                    {
                        new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar, 100),
                        new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                    };

            misParametros[0].Value = entidad.Nombre;
            misParametros[1].Value = entidad.SolicitanteUsuarioID;


            this.parametros = misParametros;

            accesoDatos();

            return this.ListaEntidades;

        }

        public void Descomponer(SqlDataReader Rdr)
        {
            List<evaluador> Lista = new List<evaluador>();

            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    evaluador Objeto = new evaluador();
                    Objeto.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorID"]);
                    Objeto.Nombre = Convert.ToString(Rdr["Nombre"]);
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}