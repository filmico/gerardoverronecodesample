﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;
using System.Data.SqlClient;

namespace Datos
{
    sealed public class AsignacionDato : IConectable<Asignacion>
    {
        public List<Asignacion> ListaEntidades { get; set; }
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }

        // Metodos
        public int actualizar(Asignacion entidad)
        {
            nombreProcedure = "AsignacionesUpdProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@Porcentaje", System.Data.SqlDbType.Decimal, 2),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)

            };
            misParametros[0].Value = entidad.EmpresaCuitID;
            misParametros[1].Value = entidad.UsuarioID;
            misParametros[2].Value = entidad.Porcentaje;
            misParametros[3].Value = entidad.SolicitanteUsuarioID;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public int agregar(Asignacion entidad, int modo = 0)
        {
            nombreProcedure = "AsignacionesInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@Porcentaje", System.Data.SqlDbType.Decimal, 2),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                
            };
            misParametros[0].Value = entidad.EmpresaCuitID;
            misParametros[1].Value = entidad.UsuarioID;
            misParametros[2].Value = entidad.Porcentaje;
            misParametros[3].Value = entidad.SolicitanteUsuarioID;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }


        public int eliminar(Asignacion entidad)
        {
            nombreProcedure = "AsignacionesDelProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)

            };
            misParametros[0].Value = entidad.EmpresaCuitID;
            misParametros[1].Value = entidad.UsuarioID;
            misParametros[2].Value = entidad.SolicitanteUsuarioID;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public List<Asignacion> listar(Asignacion entidad)
        {
            nombreProcedure = "AsignacionesSelProc";

            // Sin cuit selecciona las empresas sin repeticiones
            if (entidad.EmpresaCuitID == 0)
            {

                SqlParameter[] misParametros =
                    {
                            new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int)
                    };

                misParametros[0].Value = entidad.UsuarioID;
                this.parametros = misParametros;

            }
            // Con cuit selecciona las asignaciones de esa empresa en particular dependiendo del tipo de usuario que consulta
            else
            {
                

                SqlParameter[] misParametros =
                    {
                            new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                            new SqlParameter("@CUIT", System.Data.SqlDbType.BigInt)
                    };

                misParametros[0].Value = entidad.UsuarioID;
                misParametros[1].Value = entidad.EmpresaCuitID;
                this.parametros = misParametros;

            }
            accesoDatos();
            return this.ListaEntidades;
        }

        // ENSAMBLE Y DESENSAMBLE DE OBJETOS
        public void Descomponer(SqlDataReader Rdr)
        {
            List<Asignacion> Lista = new List<Asignacion>();

            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }

            else
            {
                while (Rdr.Read())
                {
                    Asignacion Objeto = new Asignacion();
                    Objeto.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                    Objeto.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                    Objeto.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);
                    Objeto.UsuarioID = Convert.ToInt32(Rdr["UsuarioID"]);
                    Objeto.UsuarioNombre = Convert.ToString(Rdr["Nombre"]);
                    Objeto.UsuarioTipoID = Convert.ToInt32(Rdr["UsuarioTipoID"]);
                    Objeto.Porcentaje = Convert.ToDecimal(Rdr["Porcentaje"]);
                    Lista.Add(Objeto);
                }
            }
            

            // Carga la lista de la clase
            this.ListaEntidades = Lista;

        }

        // BASE DE DATOS
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }
    }
}
