﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;
using System.Data.SqlClient;

namespace Datos
{
    sealed public class CargaDatosLogDato : IConectable<CargaDatosLog>
    {

        public List<CargaDatosLog> ListaEntidades { get; set; }
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }


        public int agregar(CargaDatosLog entidad, int modo = 0)
        {
            nombreProcedure = "CargaDatosLogInsProc";
            SqlParameter[] misParametros =
                    {
                        new SqlParameter("@fechaEjecucionCargaDatos", System.Data.SqlDbType.DateTime2)
                    };
            misParametros[0].Value = entidad.fechaEjecucionCargaDatos;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public int eliminar(CargaDatosLog entidad)
        {
            throw new NotImplementedException();
        }

        public List<CargaDatosLog> listar(CargaDatosLog entidad)
        {
            nombreProcedure = "CargaDatosLogSelProc";

            //SqlParameter[] misParametros =
            //            {
            //    new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
            //                };
            //misParametros[0].Value = entidad.SolicitanteUsuarioID;
            //this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;
        }

        public int actualizar(CargaDatosLog entidad)
        {
            throw new NotImplementedException();
        }


        // BASE DE DATOS
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();

            // Si tenemos parámetros los agregamos
            if (this.parametros != null)
            {
                objComando.Parameters.AddRange(this.parametros);
            } 

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }


        // ENSAMBLE Y DESENSAMBLE DE OBJETOS
        public void Descomponer(SqlDataReader Rdr)
        {
            List<CargaDatosLog> Lista = new List<CargaDatosLog>();

            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }

            else
            {
                while (Rdr.Read())
                {
                    CargaDatosLog Objeto = new CargaDatosLog();
                    Objeto.fechaEjecucionCargaDatos = Convert.ToDateTime(Rdr["ejecucionAutomatica"]);
                    Lista.Add(Objeto);
                }
            }


            // Carga la lista de la clase
            this.ListaEntidades = Lista;
        }


    }
}
