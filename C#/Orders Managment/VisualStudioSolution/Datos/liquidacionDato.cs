﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    sealed public class liquidacionDato : IConectable<liquidacion>
    {
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }
        public List<liquidacion> ListaEntidades { get; set; }

        public int agregar(liquidacion entidad, int modo = 0)
        {
            throw new NotImplementedException();
        }

        public int actualizar(liquidacion entidad)
        {
            throw new NotImplementedException();
        }

        public List<liquidacion> listar(liquidacion entidad)
        {
            nombreProcedure = "EstadosLiquidacionSelProc";

            SqlParameter[] misParametros =
                        {
                        new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int),
                        new SqlParameter("@EstadoLiquidacionID", System.Data.SqlDbType.Int)                        
                        };
            misParametros[0].Value = entidad.SolicitanteUsuarioID;
            misParametros[1].Value = entidad.EstadoLiquidacionID;
            this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;
        }

        public int eliminar(liquidacion entidad)
        {
            throw new NotImplementedException();
        }

        public void Descomponer(SqlDataReader Rdr)
        {
            List<liquidacion> Lista = new List<liquidacion>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    liquidacion Objeto = new liquidacion();

                    Objeto.EstadoLiquidacionID = Convert.ToInt32(Rdr["EstadoLiquidacionID"]);
                    Objeto.Descripcion = Convert.ToString(Rdr["Descripcion"]);

                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }


    }
}
