﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    public class ExpedienteEstadoDato : IConectable<expedientesEstado>
    {
        public List<expedientesEstado> ListaEntidades { get; set; }

        public string nombreProcedure { get; set; }

        public SqlParameter[] parametros { get; set; }

        public int statusDB { get; set; }

        public int actualizar(expedientesEstado entidad)
        {
            throw new NotImplementedException();
        }

        public int agregar(expedientesEstado entidad, int modo = 0)
        {
            nombreProcedure = "ExpedienteEstadosInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteEstadoID",System.Data.SqlDbType.Int),
                new SqlParameter("@Descripcion",System.Data.SqlDbType.NVarChar, 100),
            };
            misParametros[0].Value = entidad.ExpedienteEstadoID;
            misParametros[1].Value = entidad.Descripcion;
            parametros = misParametros;
            accesoDatos();
            return this.statusDB;
        }

        public int eliminar(expedientesEstado entidad)
        {
            throw new NotImplementedException();
        }

        public List<expedientesEstado> listar(expedientesEstado entidad)
        {
            nombreProcedure = "ExpedientesEstadosSelProc";
            SqlParameter[] misParametros = {
                new SqlParameter("@modo", System.Data.SqlDbType.Int),
                };
            misParametros[0].Value = 0;
            this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;
        }

        public void Descomponer(SqlDataReader Rdr)
        {
            List<expedientesEstado> Lista = new List<expedientesEstado>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    expedientesEstado Objeto = new expedientesEstado();
                    Objeto.ExpedienteEstadoID = Convert.ToInt32(Rdr["ExpedienteEstadoID"]);
                    Objeto.Descripcion = Convert.ToString(Rdr["Descripcion"]);
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}
