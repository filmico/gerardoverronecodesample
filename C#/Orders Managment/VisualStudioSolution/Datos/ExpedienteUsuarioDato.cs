﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;
using System.Data.SqlClient;

namespace Datos
{
    public class ExpedienteUsuarioDato : IConectable<ExpedienteUsuario>
    {
        public List<ExpedienteUsuario> ListaEntidades { get; set; }
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }

        public int agregar(ExpedienteUsuario entidad, int modo = 0)
        {
            nombreProcedure = "ExpedientesUsuariosInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@ExpedienteNroID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@Porcentaje", System.Data.SqlDbType.Decimal)
            };
            misParametros[0].Value = entidad.Empresa.EmpresaCuitID;
            misParametros[1].Value = entidad.ExpedienteNumeroID;
            misParametros[2].Value = entidad.Usuario.UsuarioID;
            misParametros[3].Value = entidad.SolicitanteUsuarioID;
            misParametros[4].Value = entidad.Porcentaje;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public List<ExpedienteUsuario> listar(ExpedienteUsuario entidad)
        {
            nombreProcedure = "ExpedientesUsuariosSelProc";

            // Sin cuit selecciona las empresas sin repeticiones
            if (entidad.ExpedienteNumeroID == 0)
            {
                SqlParameter[] misParametros =
                    {
                            new SqlParameter("@UsuarioSolicitanteID", System.Data.SqlDbType.Int)
                    };

                misParametros[0].Value = entidad.SolicitanteUsuarioID;
                this.parametros = misParametros;
            }
            // Con cuit selecciona las asignaciones de esa empresa en particular dependiendo del tipo de usuario que consulta
            else
            {
                SqlParameter[] misParametros =
                    {
                            new SqlParameter("@UsuarioSolicitanteID", System.Data.SqlDbType.Int),
                            new SqlParameter("@ExpedienteNumeroID", System.Data.SqlDbType.BigInt)
                    };

                misParametros[0].Value = entidad.SolicitanteUsuarioID;
                misParametros[1].Value = entidad.ExpedienteNumeroID;
                this.parametros = misParametros;
            }
            accesoDatos();
            return this.ListaEntidades;
        }
        public int actualizar(ExpedienteUsuario entidad)
        {
            nombreProcedure = "ExpedientesUsuariosUpdProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteNro", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@Porcentaje", System.Data.SqlDbType.Decimal, 2),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
            };

            misParametros[0].Value = entidad.ExpedienteNumeroID;
            misParametros[1].Value = entidad.Usuario.UsuarioID;
            misParametros[2].Value = entidad.Porcentaje;
            misParametros[3].Value = entidad.SolicitanteUsuarioID;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }
        public int eliminar(ExpedienteUsuario entidad)
        {
            nombreProcedure = "ExpedientesUsuariosDelProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteNro", System.Data.SqlDbType.BigInt),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
            };
            misParametros[0].Value = entidad.ExpedienteNumeroID;
            misParametros[1].Value = entidad.Usuario.UsuarioID;
            misParametros[2].Value = entidad.SolicitanteUsuarioID;
            parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public void Descomponer(SqlDataReader Rdr)
        {
            List<ExpedienteUsuario> Lista = new List<ExpedienteUsuario>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else if (Rdr.GetName(0) == "EmpresaCuitID") // Trae las empresas con asignaciones
            {
                while (Rdr.Read())
                {
                    ExpedienteUsuario Objeto = new ExpedienteUsuario();
                    Objeto.Empresa.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                    Objeto.Empresa.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                    Objeto.Empresa.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);
                    Objeto.AutoAsignados = Convert.ToInt32(Rdr["AutoAsignados"]);
                    Lista.Add(Objeto);

                }
                this.ListaEntidades = Lista;
            }
            else // trae los expedientes de una empresa determinada
            {
                while (Rdr.Read())
                {
                    ExpedienteUsuario Objeto = new ExpedienteUsuario();
                    Objeto.ExpedienteNumeroID = Convert.ToInt64(Rdr["ExpedienteNumeroID"]);
                    Objeto.Usuario.UsuarioTipoID = Convert.ToInt32(Rdr["UsuarioTipoID"]);
                    Objeto.Usuario.UsuarioTipoDescripcion = Convert.ToString(Rdr["Descripcion"]);
                    Objeto.Usuario.UsuarioID = Convert.ToInt32(Rdr["UsuarioID"]);
                    Objeto.Usuario.Nombre = Convert.ToString(Rdr["Nombre"]);
                    Objeto.Porcentaje = Convert.ToDecimal(Rdr["Porcentaje"]);
                    
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }


    }
}
