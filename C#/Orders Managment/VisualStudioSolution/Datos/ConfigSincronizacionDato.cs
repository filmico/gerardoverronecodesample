﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;
using System.Data.SqlClient;

namespace Datos
{
    sealed public class ConfigSincronizacionDato : IConectable<configSincronizacion>
    {
        public List<configSincronizacion> ListaEntidades { get; set; }

        public string nombreProcedure { get; set; }

        public SqlParameter[] parametros { get; set; }

        public int statusDB { get; set; }

        public int actualizar(configSincronizacion entidad)
        {
            throw new NotImplementedException();
        }

        public int agregar(configSincronizacion entidad, int modo = 0)
        {
            this.nombreProcedure = "ConfigSincronizacionesInsProc";

            SqlParameter[] misParametros =
                 {
                    new SqlParameter("@DiaSemanaComienzo", System.Data.SqlDbType.Int),
                    new SqlParameter("@DiaSemanaFinalizacion", System.Data.SqlDbType.Int),
                    new SqlParameter("@HoraComienzoDT", System.Data.SqlDbType.DateTime),
                    new SqlParameter("@HoraFinalizacionDT", System.Data.SqlDbType.DateTime),
                    new SqlParameter("@Estado", System.Data.SqlDbType.Int),
                    new SqlParameter("@EstadoExpedienteID", System.Data.SqlDbType.Int),
                    new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                 };

            misParametros[0].Value = entidad.DiaSemanaComienzo;
            misParametros[1].Value = entidad.DiaSemanaFinalizacion;
            misParametros[2].Value = entidad.HoraComienzo;
            misParametros[3].Value = entidad.HoraFinalizacion;
            misParametros[4].Value = entidad.Estado;
            misParametros[5].Value = entidad.EstadoExpedienteID;
            misParametros[6].Value = entidad.SolicitanteUsuarioID;

            this.parametros = misParametros;

            accesoDatos();

            return this.statusDB;
        }


        public int eliminar(configSincronizacion entidad)
        {
            throw new NotImplementedException();
        }

        public List<configSincronizacion> listar(configSincronizacion entidad)
        {

            this.nombreProcedure = "ConfigSincronizacionesSelProc";

            SqlParameter[] misParametros =
                    {
                        new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int),
                        new SqlParameter("@Estado", System.Data.SqlDbType.Int)
                    };

            misParametros[0].Value = entidad.SolicitanteUsuarioID;
            misParametros[1].Value = entidad.Estado;

            this.parametros = misParametros;

            accesoDatos();

            return this.ListaEntidades;

        }

        // ENSAMBLE Y DESENSAMBLE DE OBJETOS
        public void Descomponer(SqlDataReader Rdr)
        {
            List<configSincronizacion> Lista = new List<configSincronizacion>();

            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }

            else
            {
                while (Rdr.Read())
                {
                    configSincronizacion Objeto = new configSincronizacion();
                    Objeto.DiaSemanaComienzo = Convert.ToInt16(Rdr["DiaSemanaComienzo"]);
                    Objeto.DiaSemanaFinalizacion = Convert.ToInt16(Rdr["DiaSemanaFinalizacion"]);
                    Objeto.HoraComienzo = DateTime.Parse(Rdr["HoraComienzo"].ToString());
                    Objeto.HoraFinalizacion = DateTime.Parse(Rdr["HoraFinalizacion"].ToString());
                    Objeto.Estado = Convert.ToInt16(Rdr["Estado"]);
                    Objeto.EstadoExpedienteID = Convert.ToInt16(Rdr["EstadoExpedienteID"]);

                    Lista.Add(Objeto);
                }
            }


            // Carga la lista de la clase
            this.ListaEntidades = Lista;

        }

        // BASE DE DATOS
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}
