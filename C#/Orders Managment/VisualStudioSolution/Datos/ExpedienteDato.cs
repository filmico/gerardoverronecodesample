﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    public class ExpedienteDato : IConectable<expediente>
    {

        /*
            Definir una propiedad para preservar el id del usuario que utiliza cada método.
            Esto se utiliza al retorno de una lista de expedientes, para analizar si
            se construye la lista de retorno con las columnas A, B o C
        */
        public int SolicitanteUsuarioID { get; set; }

        // Para consultar y almacenar el tipo de Uuario (Gestor / Socio / Admin)
        public int UsuarioTipoID { get; set; }

        public List<expediente> ListaEntidades { get; set; }

        public string nombreProcedure { get; set; }

        public SqlParameter[] parametros { get; set; }

        public int statusDB { get; set; }

        public int actualizar(expediente entidad)
        {
            return 1;
        }

        public int actualizar(expedienteAdmin entidad)
        {

            nombreProcedure = "ExpedientesUpdProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteNumeroID",System.Data.SqlDbType.BigInt),
                new SqlParameter("@EstadoLiquidacionID",System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID",System.Data.SqlDbType.Int)

            };
            misParametros[0].Value = entidad.ExpedienteNumeroID;
            misParametros[1].Value = entidad.EstadoLiquidacionID;
            misParametros[2].Value = entidad.SolicitanteUsuarioID;

            parametros = misParametros;
            accesoDatos();
            return this.statusDB;
        }

        public int agregar(expediente entidad, int modo = 0)
        {
            nombreProcedure = "ExpedientesInsProc";
            SqlParameter[] misParametros =
            {
                new SqlParameter("@ExpedienteNumeroID",System.Data.SqlDbType.BigInt),
                new SqlParameter("@NroSolicitud",System.Data.SqlDbType.Int),
                new SqlParameter("@EmpresaCuitID",System.Data.SqlDbType.BigInt),
                new SqlParameter("@BeneficioSolicitado",System.Data.SqlDbType.Money),
                new SqlParameter("@BeneficioAprobado",System.Data.SqlDbType.Money),
                new SqlParameter("@FechaEstadoActual",System.Data.SqlDbType.Date),
                new SqlParameter("@ExpedienteEstado",System.Data.SqlDbType.NVarChar, 100),
                new SqlParameter("@Evaluador",System.Data.SqlDbType.NVarChar, 100),
                new SqlParameter("@EvaluadorTecnico",System.Data.SqlDbType.NVarChar, 100)
            };
            misParametros[0].Value = entidad.ExpedienteNumeroID;
            misParametros[1].Value = entidad.NroSolicitud;
            misParametros[2].Value = entidad.Empresa.EmpresaCuitID;
            misParametros[3].Value = entidad.BeneficioSolicitado;
            misParametros[4].Value = entidad.BeneficioAprobado;
            misParametros[5].Value = entidad.FechaEstadoActual;
            misParametros[6].Value = entidad.ExpedienteEstado.Descripcion;
            misParametros[7].Value = entidad.Evaluador.Nombre;
            misParametros[8].Value = entidad.EvaluadorTecnico.Nombre;
            parametros = misParametros;
            accesoDatos();
            return this.statusDB;
        }

        public int eliminar(expediente entidad)
        {
            throw new NotImplementedException();
        }

        public List<expediente> listar(expediente entidad)
        {
            // Guarda el ID del Solicitante
            this.SolicitanteUsuarioID = entidad.SolicitanteUsuarioID;

            // Instancia un usuario con el id del adminitsrador para consultar la lista de usuarios
            usuario usr = new usuario();
            usr.SolicitanteUsuarioID = 1;

            // Obtiene la lista de usuarios 
            UsuarioDato usrDato = new UsuarioDato();
            List<usuario> listaUsuarios = usrDato.listar(usr);

            // Busca el usuario que realiza la consulta para obtener su tipo de usuario.
            foreach (var u in listaUsuarios)
            {
                if (u.UsuarioID == this.SolicitanteUsuarioID)
                {
                    this.UsuarioTipoID = u.UsuarioTipoID;
                    break;
                }

            }

            nombreProcedure = "ExpedientesSelProc";
            SqlParameter[] misParametros = {
                new SqlParameter("@EmpresaCuitID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@NroEmpresa", System.Data.SqlDbType.NVarChar, 5),
                new SqlParameter("@RazonSocial", System.Data.SqlDbType.NVarChar, 200),
                new SqlParameter("@ExpedienteNumeroID", System.Data.SqlDbType.BigInt),
                new SqlParameter("@ExpedienteEstadoID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@sinAsignacion", System.Data.SqlDbType.Int),
                new SqlParameter("@pendiente", System.Data.SqlDbType.Int),
                new SqlParameter("@aLiquidar", System.Data.SqlDbType.Int),
                new SqlParameter("@liquidado", System.Data.SqlDbType.Int),
                new SqlParameter("@finalizado", System.Data.SqlDbType.Int)
                };

            if ((entidad.Empresa.EmpresaCuitID == null) &&
               (entidad.Empresa.NroEmpresa == null) &&
               (entidad.Empresa.RazonSocial == null) &&
               (entidad.ExpedienteNumeroID == 0) &&
               (entidad.ExpedienteEstado.ExpedienteEstadoID == 0))   // Listar todos
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
                misParametros[3].Value = null;
                misParametros[4].Value = null;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            }
            else if (entidad.Empresa.EmpresaCuitID != null) // listar por cuit
            {
                misParametros[0].Value = entidad.Empresa.EmpresaCuitID;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
                misParametros[3].Value = null;
                misParametros[4].Value = null;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            }
            else if (string.IsNullOrEmpty(entidad.Empresa.NroEmpresa) == false) // listar por nro empresa
            {
                misParametros[0].Value = null;
                misParametros[1].Value = entidad.Empresa.NroEmpresa;
                misParametros[2].Value = null;
                misParametros[3].Value = null;
                misParametros[4].Value = null;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            }
            else if (string.IsNullOrEmpty(entidad.Empresa.RazonSocial) == false) // listar por razon social
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = entidad.Empresa.RazonSocial;
                misParametros[3].Value = null;
                misParametros[4].Value = null;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            }
            else if (entidad.ExpedienteNumeroID != 0) // listar por nro de expediente
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
                misParametros[3].Value = entidad.ExpedienteNumeroID;
                misParametros[4].Value = null;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            }
            else if (entidad.ExpedienteEstado.ExpedienteEstadoID != 0) // listar por estado de expediente
            {
                misParametros[0].Value = null;
                misParametros[1].Value = null;
                misParametros[2].Value = null;
                misParametros[3].Value = null;
                misParametros[4].Value = entidad.ExpedienteEstado.ExpedienteEstadoID;
                misParametros[5].Value = entidad.SolicitanteUsuarioID;
                misParametros[6].Value = entidad.sinAsignacion;
                misParametros[7].Value = entidad.pendiente;
                misParametros[8].Value = entidad.aLiquidar;
                misParametros[9].Value = entidad.liquidado;
                misParametros[10].Value = entidad.finalizado;
            };

            this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;

        }


        public void Descomponer(SqlDataReader Rdr)
        {
            List<expediente> Lista = new List<expediente>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {

                    if (this.UsuarioTipoID == 1)   // Gestor
                    {
                        expediente expGestor = new expediente();

                        // Empresa
                        expGestor.Empresa.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                        expGestor.Empresa.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                        expGestor.Empresa.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);

                        // Expediente
                        expGestor.ExpedienteNumeroID = Convert.ToInt64(Rdr["ExpedienteNumeroID"]);
                        expGestor.NroSolicitud = Convert.ToInt32(Rdr["NroSolicitud"]);

                        // ExpedienteEstado
                        expGestor.ExpedienteEstado.ExpedienteEstadoID = Convert.ToInt32(Rdr["ExpedienteEstadoID"]);
                        expGestor.ExpedienteEstado.Descripcion = Convert.ToString(Rdr["EstadoActual"]);
                        expGestor.BeneficioSolicitado = Convert.ToDecimal(Rdr["BeneficioSolicitado"]);

                        // Expediente
                        expGestor.BeneficioAprobado = Convert.ToDecimal(Rdr["BeneficioAprobado"]);
                        if (string.IsNullOrEmpty(Convert.ToString(Rdr["FechaEstadoActual"])))
                        {
                            expGestor.FechaEstadoActual = null;
                        }
                        else
                        {
                            expGestor.FechaEstadoActual = Convert.ToDateTime(Rdr["FechaEstadoActual"]);
                        }
                        // Evaluador
                        expGestor.Evaluador.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorId"]);
                        expGestor.Evaluador.Nombre = Convert.ToString(Rdr["Evaluador"]);
                        expGestor.EvaluadorTecnico.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorTecnicoId"]);
                        expGestor.EvaluadorTecnico.Nombre = Convert.ToString(Rdr["EvaluadorTecnico"]);

                        Lista.Add(expGestor);
                    }
                    else if (this.UsuarioTipoID == 2)   // Socio
                    {
                        expedienteSocio expSocio = new expedienteSocio();

                        // Empresa
                        expSocio.Empresa.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                        expSocio.Empresa.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                        expSocio.Empresa.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);

                        // Expediente
                        expSocio.ExpedienteNumeroID = Convert.ToInt64(Rdr["ExpedienteNumeroID"]);
                        expSocio.NroSolicitud = Convert.ToInt32(Rdr["NroSolicitud"]);

                        // ExpedienteEstado
                        expSocio.ExpedienteEstado.ExpedienteEstadoID = Convert.ToInt32(Rdr["ExpedienteEstadoID"]);
                        expSocio.ExpedienteEstado.Descripcion = Convert.ToString(Rdr["EstadoActual"]);
                        expSocio.BeneficioSolicitado = Convert.ToDecimal(Rdr["BeneficioSolicitado"]);

                        // Expediente
                        expSocio.BeneficioAprobado = Convert.ToDecimal(Rdr["BeneficioAprobado"]);
                        if (string.IsNullOrEmpty(Convert.ToString(Rdr["FechaEstadoActual"])))
                        {
                            expSocio.FechaEstadoActual = null;
                        }
                        else
                        {
                            expSocio.FechaEstadoActual = Convert.ToDateTime(Rdr["FechaEstadoActual"]);
                        }
                        // Evaluador
                        expSocio.Evaluador.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorId"]);
                        expSocio.Evaluador.Nombre = Convert.ToString(Rdr["Evaluador"]);
                        expSocio.EvaluadorTecnico.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorTecnicoId"]);
                        expSocio.EvaluadorTecnico.Nombre = Convert.ToString(Rdr["EvaluadorTecnico"]);

                        // Estado Liquidacion
                        expSocio.EstadoLiquidacionID = Convert.ToInt32(Rdr["EstadoLiquidacionID"]);
                        expSocio.EstadoLiquidacion = Convert.ToString(Rdr["EstadoLiquidacion"]);

                        Lista.Add(expSocio);
                    }
                    else if (this.UsuarioTipoID == 3)   // Administrador
                    {

                        expedienteAdmin expAdmin = new expedienteAdmin();

                        // Empresa
                        expAdmin.Empresa.EmpresaCuitID = Convert.ToInt64(Rdr["EmpresaCuitID"]);
                        expAdmin.Empresa.NroEmpresa = Convert.ToString(Rdr["NroEmpresa"]);
                        expAdmin.Empresa.RazonSocial = Convert.ToString(Rdr["RazonSocial"]);

                        // Expediente
                        expAdmin.ExpedienteNumeroID = Convert.ToInt64(Rdr["ExpedienteNumeroID"]);
                        expAdmin.NroSolicitud = Convert.ToInt32(Rdr["NroSolicitud"]);

                        // ExpedienteEstado
                        expAdmin.ExpedienteEstado.ExpedienteEstadoID = Convert.ToInt32(Rdr["ExpedienteEstadoID"]);
                        expAdmin.ExpedienteEstado.Descripcion = Convert.ToString(Rdr["EstadoActual"]);
                        expAdmin.BeneficioSolicitado = Convert.ToDecimal(Rdr["BeneficioSolicitado"]);

                        // Expediente
                        expAdmin.BeneficioAprobado = Convert.ToDecimal(Rdr["BeneficioAprobado"]);
                        if (string.IsNullOrEmpty(Convert.ToString(Rdr["FechaEstadoActual"])))
                        {
                            expAdmin.FechaEstadoActual = null;
                        }
                        else
                        {
                            expAdmin.FechaEstadoActual = Convert.ToDateTime(Rdr["FechaEstadoActual"]);
                        }
                        expAdmin.AutoAsignado = Convert.ToInt32(Rdr["AutoAsignado"]);

                        // Evaluador
                        expAdmin.Evaluador.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorId"]);
                        expAdmin.Evaluador.Nombre = Convert.ToString(Rdr["Evaluador"]);
                        expAdmin.EvaluadorTecnico.EvaluadorID = Convert.ToInt32(Rdr["EvaluadorTecnicoId"]);
                        expAdmin.EvaluadorTecnico.Nombre = Convert.ToString(Rdr["EvaluadorTecnico"]);

                        // Estado Liquidacion
                        expAdmin.EstadoLiquidacionID = Convert.ToInt32(Rdr["EstadoLiquidacionID"]);
                        expAdmin.EstadoLiquidacion = Convert.ToString(Rdr["EstadoLiquidacion"]);

                        Lista.Add(expAdmin);
                    }

                }

                this.ListaEntidades = Lista;

            }
        }
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}