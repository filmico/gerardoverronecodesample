﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Entidades;

namespace Datos
{
    sealed public class UsuarioDato : IConectable<usuario>
    {
        public string nombreProcedure { get; set; }
        public SqlParameter[] parametros { get; set; }
        public int statusDB { get; set; }
        public List<usuario> ListaEntidades { get; set; }

        public int agregar(usuario entidad, int modo = 0)
        {
            nombreProcedure = "UsuariosInsProc";

            if (modo == 0) // Usuario sin Login
            {
                SqlParameter[] misParametros =
                    {
                    new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar, 50),
                    new SqlParameter("@Email", System.Data.SqlDbType.NVarChar, 50),
                    new SqlParameter("@UsuarioTipoID", System.Data.SqlDbType.Int),
                    new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                    };

                misParametros[0].Value = entidad.Nombre;
                misParametros[1].Value = entidad.Email;
                misParametros[2].Value = entidad.UsuarioTipoID;
                misParametros[3].Value = entidad.SolicitanteUsuarioID;
                this.parametros = misParametros;
            }
            if (modo == 1) // Usuario con Login
            {
                SqlParameter[] misParametros =
                    {
                        new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar, 50),
                        new SqlParameter("@Email", System.Data.SqlDbType.NVarChar, 50),
                        new SqlParameter("@Password", System.Data.SqlDbType.NVarChar, 20),
                        new SqlParameter("@UsuarioTipoID", System.Data.SqlDbType.Int),
                        new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                    };

                misParametros[0].Value = entidad.Nombre;
                misParametros[1].Value = entidad.Email;
                misParametros[2].Value = entidad.Password;
                misParametros[3].Value = entidad.UsuarioTipoID;
                misParametros[4].Value = entidad.SolicitanteUsuarioID;

                this.parametros = misParametros;
            }
            accesoDatos();
            return this.statusDB;
        }

        public int actualizar(usuario entidad)
        {
            nombreProcedure = "UsuariosUpdProc";


            SqlParameter[] misParametros =
                {
                new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar, 50),
                new SqlParameter("@Email", System.Data.SqlDbType.NVarChar, 50),
                new SqlParameter("@Password", System.Data.SqlDbType.NVarChar, 20),
                new SqlParameter("@NuevoUsuarioEstadoID", System.Data.SqlDbType.Int),
                new SqlParameter("@NuevoUsuarioTipoID", System.Data.SqlDbType.Int),
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                };

            misParametros[0].Value = entidad.Nombre;
            misParametros[1].Value = entidad.Email;
            misParametros[2].Value = entidad.Password;
            misParametros[3].Value = entidad.UsuarioEstadoID;
            misParametros[4].Value = entidad.UsuarioTipoID;
            misParametros[5].Value = entidad.UsuarioID;
            misParametros[6].Value = entidad.SolicitanteUsuarioID;
            this.parametros = misParametros;

            accesoDatos();
            return this.statusDB;
        }

        public List<usuario> listar(usuario entidad)
        {
            nombreProcedure = "UsuariosSelProc";

            SqlParameter[] misParametros =
                        {
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                            };
            misParametros[0].Value = entidad.SolicitanteUsuarioID;
            this.parametros = misParametros;
            accesoDatos();
            return this.ListaEntidades;
        }
        public int eliminar(usuario entidad)
        {
            nombreProcedure = "UsuariosDelProc";

            SqlParameter[] misParametros =
                        {
                new SqlParameter("@UsuarioID", System.Data.SqlDbType.Int),
                new SqlParameter("@SolicitanteUsuarioID", System.Data.SqlDbType.Int)
                            };
            misParametros[0].Value = entidad.UsuarioID;
            misParametros[1].Value = entidad.SolicitanteUsuarioID;
            this.parametros = misParametros;
            accesoDatos();
            return this.statusDB;

        }

        public List<usuario> autenticar(usuario entidad)
        {
            nombreProcedure = "UsuariosAutenticarProc";

            SqlParameter[] misParametros =
                        {
                new SqlParameter("@Nombre", System.Data.SqlDbType.NVarChar,50),
                new SqlParameter("@Password", System.Data.SqlDbType.NVarChar,100)
                            };
            misParametros[0].Value = entidad.Nombre;
            misParametros[1].Value = entidad.Password;
            this.parametros = misParametros;
            accesoDatos();

            return this.ListaEntidades;

        }


        // ENSAMBLE Y DESENSAMBLE DE OBJETOS
        public void Descomponer(SqlDataReader Rdr)
        {
            List<usuario> Lista = new List<usuario>();
            if (Rdr.GetName(0) == "statusDB")
            {
                while (Rdr.Read())
                {
                    this.statusDB = Convert.ToInt32(Rdr["statusDB"]);
                }
            }
            else
            {
                while (Rdr.Read())
                {
                    usuario Objeto = new usuario();
                    Objeto.UsuarioID = Convert.ToInt32(Rdr["UsuarioID"]);
                    Objeto.Nombre = Convert.ToString(Rdr["Nombre"]);
                    Objeto.Email = Convert.ToString(Rdr["Email"]);
                    Objeto.UsuarioEstadoID = Convert.ToInt32(Rdr["UsuarioEstadoID"]);
                    Objeto.UsuarioEstadoDescripcion = Convert.ToString(Rdr["UsuarioEstadoDescripcion"]);
                    Objeto.UsuarioTipoID = Convert.ToInt32(Rdr["UsuarioTipoID"]);
                    Objeto.UsuarioTipoDescripcion = Convert.ToString(Rdr["UsuarioTipoDescripcion"]);
                    Objeto.PoseeLogin = Convert.ToInt32(Rdr["PoseeLogin"]);
                    Objeto.PoseeLoginDescripcion = Convert.ToString(Rdr["PoseeLoginDescripcion"]);
                    Objeto.Autenticado = Convert.ToInt32(Rdr["Autenticado"]);
                    Lista.Add(Objeto);
                }
                this.ListaEntidades = Lista;
            }
        }

        // BASE DE DATOS
        public void accesoDatos()
        {
            string strConexion = ConfigurationManager.ConnectionStrings["StringConexion"].ConnectionString.ToString();
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand objComando = new SqlCommand();
            objComando.CommandType = System.Data.CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = this.nombreProcedure;
            objComando.Parameters.Clear();
            objComando.Parameters.AddRange(this.parametros);

            try
            {
                objConexion.Open();
                SqlDataReader rdr = objComando.ExecuteReader();
                Descomponer(rdr);
            }
            catch (Exception ex)
            {
                // Elevo el error a la capa de Negocio
                throw ex;
            }
            finally
            {
                if (objConexion.State == System.Data.ConnectionState.Open)
                {
                    objConexion.Close();
                }
            }
        }

    }
}
