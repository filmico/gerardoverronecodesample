# ======================================================================
# Script Name: A014_Hide_UnHide_Faces.py                            v1.0
# by Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
# Date: 23/03/2013
# ----------------------------------------------------------------------
# This Script will help in many circumstance to hide just the selected
# faces of one or many polymeshes as Maya does not have that function
# built in function.
#
# The closest approximation from factor is to select the faces you want
# to hide, invert the selection and use the isolate button but, this will
# not only hide the faces but, also all the other meshes in the scene.
#
# ----------------------------------------------------------------------
# Usage: Select Some Poly Faces and Run the Script
# To Retrieve the hidden faces turn off the isolated button.
# ======================================================================

import maya.cmds as mc

def hidePolyFaces():

    # Query the Active modelPanel View
    currentViewport = mc.getPanel(withFocus=True)

    # List the User selected elements
    selectedFaces = mc.ls(selection=True, long=True)

    if selectedFaces:
        # Check if at least one face was selected
        for face in selectedFaces:

            # split the name of the selected in two parts
            # from xx_name.f[0] into ---> name and f[0]
            word = face.split('.')
            if len(word) > 1:

                # check if face has the f[ word to determine it is a face
                word = word[1][0:2]

                if word == 'f[':
                    # print "Face FOUND !"

                    # ---------------------------
                    # Proceed with the hide part
                    #----------------------------

                    # Find the all the meshes in the scene
                    shapeNodes = mc.ls(type='mesh', objectsOnly=True)

                    # Clear Selection
                    mc.select(clear=True)

                    # Create a list with all the Transform nodes looking up from the ShapeNodes
                    transformNodes = []
                    for items in shapeNodes:
                        transformNodes.append(mc.listRelatives('{items}'.format(items=items), allParents=True))

                    # select all the faces of all the Transform nodes
                    for faces in transformNodes:
                        mc.select('{faces}.f[*]'.format(faces=faces[0]), add=True)

                    # Deselect The the User's selected Faces
                    for faces in selectedFaces:
                        mc.select('{faces}'.format(faces=faces), deselect=True)

                    # Add items to the isolate List
                    mc.isolateSelect(currentViewport, loadSelected=True)

                    # Activate the Isolate Function
                    mc.isolateSelect(currentViewport, state=True)

                    # Update the Current Viewport and activate the Isolate Button
                    mc.modelEditor(currentViewport, edit=True, updateMainConnection=True)

                    # Enter and Exit the component mode to refresh
                    mc.selectMode(object=False, component=True)
                    mc.selectMode(object=True, component=False)

                    # Deselect all Selection
                    mc.select(clear=True)

                    print '\n', 'Selected Face/s Hiden', '\n'


                else:
                    mc.error('\n', 'The component selected is not a PolyFace', '\n')

            else:
                mc.error('\n', 'Wrong Selection. Please, Enter component mode (f8) and select PolyFace/s', '\n')

    else:
        mc.error('\n', "Nothing Selected, Please Select a PolyFace/s", '\n')


hidePolyFaces()