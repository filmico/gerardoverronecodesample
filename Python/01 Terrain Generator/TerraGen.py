# -*- coding: utf-8 -*-
'''
@title: Terrain Generator v1.0
@author: Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
Created on 2013-04-05 02:11

@summary: This Script Creates a Random Square Terrain With Mountains, Trees and Lakes

Parameters to Change at the end of the Script:

land_MinSize, landMaxSize => Will determine the minimum and maximum terrain size.
                             Values are expressed in scene units.
                             The terrain will always be Square as this min and max
                             values are used to randomize a value to determine
                             terrain size in each directions (+-x, +-z).
                             If Min and max are equal a terrain of that fixed size will
                             be created. Using Values higher than 300 can produce terrains of
                             more than 150.000 polygons so, be aware of the PolyCounts.

landResolution            => 1=High, 2=Med, 3=Low  (This parameter change the subdivisions)
                             1 Means the Script will subdivide the terrain by the exact
                             amount of division related to the min-max size.
                             E.G Random Terrain size: 80 , subdivisions in U and V will be 80x80
                             2 This will be the terrain size /2
                             3 This will be the terrain size /3

                             If you need bigger terrains you can increment the landMaxSize
                             values and set this parameter to 1. Later Select the Terrain
                             and press 3 in your keyboard to subdivide it using the maya
                             internal subdivision level.

landUsability             => Amount of the terrain to be used for trees.

MountainsAmount           => This is a random where you can determine the range of mountains
                             to be created. If you specify (3, 3) just 3 mountains will be
                             created.

LakeAmount                => This is the same as the MountainsAmount but for lakes.

TreesMinHeight,
TreesMaxHeight            => This values determine the minimum and maximum random scale
                             for trees.

TreesRandomColor          => This is self explanatory. Remember to use R, G, B values
                             in the range 0 to 1 not in 0 to 255

'''


import maya.cmds as mc
import random

def removePreviousTerrain():
    # list scene Geometry to check if we need to delete a previous generated terrain
    sceneGeo = mc.ls(type='mesh')

    if sceneGeo:
        for item in sceneGeo:
            tmp = mc.listRelatives(item, parent=True)[0]
            if tmp == 'terrainGeo':
                mc.delete(tmp)
            if tmp == 'waterGeo':
                mc.delete(tmp)

        # remove Trees
        mc.delete('trees_grp')


def createTerrainGeo(terrainSize, landResolution):
    # Create terrain geometry
    mesh = mc.polyPlane(width=terrainSize, height=terrainSize, subdivisionsX=(terrainSize/landResolution),
                              subdivisionsY=(terrainSize/landResolution), axis=[0,1,0], createUVs=2,
                              constructionHistory=False)[0]
    geo = mc.rename(mesh, 'terrainGeo')

    return geo

def createWaterGeo(terrainSize):
    # Create water geometry
    meshWater = mc.polyPlane(width=terrainSize, height=terrainSize, subdivisionsX=1,
                              subdivisionsY=1, axis=[0,1,0], createUVs=2,
                              constructionHistory=False)[0]
    water = mc.rename(meshWater, 'waterGeo')

    # Move the water little below in Y
    mc.xform('{water}'.format(water='waterGeo'), worldSpace=True, translation=[0, -1, 0] )

    return water

def createShader(shaderType, shaderName, shadingGroupName, colorRGB):
    # Verify if shader and ShadingGroup was previously created to remove it
    materials = mc.ls(materials=True)
    for mat in materials:
        if mat == shaderName:
            mc.delete(mat)
            mc.delete(shadingGroupName)

    # Create Shader
    shader = mc.shadingNode(shaderType, asShader=True)
    shader = mc.rename(shader, shaderName)

    # Change ShaderColor
    mc.setAttr( '{shader}.color'.format(shader=shader), colorRGB[0], colorRGB[1], colorRGB[2], type='double3')

    # Create a Shading Group
    shadingGroup = mc.sets(renderable=True, noSurfaceShader=True, empty=True)
    shadingGroup = mc.rename(shadingGroup, shadingGroupName)

    # Connect Shader to Shading Group
    mc.connectAttr('{shader}.outColor'.format(shader=shader) ,'{shadingG}.surfaceShader'.format(shadingG=shadingGroup) )

    return shadingGroup


def availablePos(geoName, terrainSize, percent):

    # Divide by 2 the Land Size to have positive and negative values.
    # Subtract %20 to prevent to create the center of mountains or lakes near the borders.
    maxPosition = (terrainSize/2) - ((terrainSize / 2) * 0.2)
    maxPosition = int(maxPosition)
    # Invert symbol with multiply * -1
    minPosition = ((terrainSize/2) * -1) + ((terrainSize / 2) * 0.2)
    minPosition = int(minPosition)

    # Find the amount of faces of the terrain
    facesCount = mc.polyEvaluate(geoName, face=True)
    # % is equal to multiply total by (0, percent)
    # (0, percent) = percent /100
    face2Use = float(facesCount) * (float(percent) / 100)
    face2Use = int(face2Use)

    return maxPosition, minPosition, facesCount, face2Use


def erodeTerrain(geoName, terrainSize, mountains, lakes, percent):

    # Find Min-Max terrain Positions in the +-X and +-Z Axis
    maxPosition = availablePos(geoName, terrainSize, percent)[0] # Call function
    minPosition = availablePos(geoName, terrainSize, percent)[1] # Call function

    # Erode Terrain

    # Counter will be used to know how much mountains has been created to start creating
    # lakes. We need to iterate until the sum of mountains and lakes has been reached.

    counter = 0
    for item in range(mountains+lakes):

        counter += 1

        # Randomize To find a Place to be used as center point for the softmod
        locationX = random.randint(minPosition, maxPosition)
        locationZ = random.randint(minPosition, maxPosition)
        softModRadius = random.randint((terrainSize/6) , (terrainSize/4))

        if (counter <= mountains):
            # Create positive Level to create Mountains
            level = random.randint(3 , (terrainSize/4))
        else:
            # Create negative Level to create Lakes
            level = (random.randint(3 , (terrainSize/4)) * -1)

        # Create SoftModification
        softMod = mc.softMod('{geo}'.format(geo=geoName), falloffCenter=[locationX,0,locationZ], falloffRadius=softModRadius)
        # print 'softMod: ', softMod  #softMod:  [u'softMod1', u'softMod1Handle']

        # Move the SoftModification
        mc.setAttr('{handle}.translateY'.format(handle=softMod[1]), level)

        # Delete Geometry History to delete the handle while preserving the erosion
        mc.delete('{geo}'.format(geo=geoName), constructionHistory=True)

    print 'Mountains Created: ', mountains
    print 'Lakes Created    : ', lakes


def treeGen(position, height, randomColor):

    # Create Cube
    treePole = mc.polyCube(name='treePole', subdivisionsX=1, subdivisionsY=1, subdivisionsZ=1,
                width=0.35, depth=0.35, height=1.5, createUVs=4, constructionHistory=False)

    # Delete Top and Bottom Faces of the Cube
    mc.delete('treePole.f[1]')
    mc.delete('treePole.f[2]')

    treeCone= mc.polyCone(name='treeCone', subdivisionsX=6, subdivisionsY=1, subdivisionsZ=0,
                radius=1, height=2, createUVs=3, constructionHistory=False)

    # Move Cone Up
    mc.xform(treeCone, translation=[0, 2, 0] )
    # Move Cube Up
    mc.xform(treePole, translation=[0, 0.5, 0] )
    # Combine Both Objects in one Mesh and rename it
    tree = mc.polyUnite( 'treeCone', 'treePole', name='tree', mergeUVSets=True, constructionHistory=False )
    # Scale using the Height variable as an scale factor
    mc.xform(tree, scale=[height, height, height] )
    # Move Tree to the Final Position
    mc.xform(tree, translation=position )
    # Freeze Transform
    mc.makeIdentity(tree, apply=True, translate=True, rotate=True, scale=True)

    return tree


def distributeVegetation(geoName, terrainSize, percent, minheight, maxheight, randomColor):

    # Create an empty group node to group trees
    mc.group( em=True, name='trees_grp' )

    # Find the total amount of terrain's faces
    facesCount = availablePos(geoName, terrainSize, percent)[2]
    # Find maximum number of faces to be used considering the landUsability
    face2Use = availablePos(geoName, terrainSize, percent)[3]

    print 'Terrain Faces : ', facesCount
    print 'Trees Created :  ', face2Use

    # Create Shader for the Tree
    treeShadingGroup = createShader(shaderType='lambert',
                             shaderName='treeShader',
                             shadingGroupName='treeShadingGroup',
                             colorRGB=randomColor
                             )

    # Iterate to create trees along the maximum amount of faces stored in the face2Use variable
    for face in range(face2Use):

        # Select the Terrain
        mc.select('{obj}'.format(obj=geoName), replace=True)

        # Find a Random Face btw the face number 0 and the total amount of faces stored in the facesCount variable
        randomFace = random.randint(0, facesCount)

        # Select the random face and find the vertex by using a component conversion
        vtx = mc.polyListComponentConversion('{obj}.f[{faceId}]'.format(obj=geoName, faceId=randomFace), toVertex=True)
        # Select the vertex of the face replacing the list of selected elements
        mc.select(vtx, replace=True)
        # Create a list with vertex names expanded one by one to obtain all the positions
        vtxExpanded = mc.filterExpand(selectionMask=31)
        # Find each Vertex Position
        pntA = mc.pointPosition(vtxExpanded[0] , world=True)
        pntB = mc.pointPosition(vtxExpanded[1] , world=True)
        pntC = mc.pointPosition(vtxExpanded[2] , world=True)
        pntD = mc.pointPosition(vtxExpanded[3] , world=True)

        # find MidPoint -> midpoint = (pntA + pntB + pntC + pntD) / 4
        midpoint =  [((A+B+C+D)/4) for A,B,C,D in zip(pntA,pntB,pntC,pntD)]

        # Check if midpoint Y coordinates is not inside a lake.
        if (midpoint[1] > -1):
            # Randomize the Trees Height
            height = random.uniform(minheight, maxheight)

            tree = treeGen(position=midpoint, height=height, randomColor=randomColor) # Call Function

            # Connect Shading Group to Geometry
            mc.sets(tree, e=True, forceElement=treeShadingGroup)

            # Parent the Tree to the "Tree" group
            mc.parent( tree, 'trees_grp' )

    # Clear Selection
    mc.select(clear=True)


#---------------------------------
# Main Functions
#---------------------------------

def terrain(land_MinSize, landMaxSize, landResolution, landUsability, MountainsAmount,
            LakeAmount, TreesMinHeight, TreesMaxHeight, TreesRandomColor):


    # Check to delete a previous generated terrain
    removePreviousTerrain()
    # randomize Size for the terrain and water
    terrainSize = random.randint(land_MinSize, landMaxSize)
    # Create terrain of random size (land_MinSize, landMaxSize)
    terrainGeo = createTerrainGeo(terrainSize, landResolution)

    # Create Shader for the Terrain
    terrainShadingGroup = createShader(shaderType='lambert',
                                       shaderName='terrainShader',
                                       shadingGroupName='terrainShadingGroup',
                                       colorRGB=(0.7, 0.6, 0.4)
                                       )

    # Connect Shading Group to Geometry
    mc.sets(terrainGeo, e=True, forceElement=terrainShadingGroup)

    # Create Water with a simple polygon, same size of the terrain. offset -Y
    waterGeo = createWaterGeo(terrainSize)

    # Create Shader for the Water
    waterShadingGroup = createShader(shaderType='blinn',
                                     shaderName='waterShader',
                                     shadingGroupName='waterShadingGroup',
                                     colorRGB=(0.5, 0.85, 0.85)
                                     )

    # Connect Shading Group to Geometry
    mc.sets(waterGeo, e=True, forceElement=waterShadingGroup)

    print '\n'
    print 'Terrain Name  : ', terrainGeo
    print 'Terrain Size  : ', terrainSize
    print 'Water Name  : ', waterGeo

    # Create Mountains and Lakes
    erodeTerrain(geoName=terrainGeo, terrainSize=terrainSize,
                 mountains=MountainsAmount, lakes=LakeAmount,
                 percent=landUsability)

    # Create Trees
    distributeVegetation(geoName=terrainGeo, terrainSize=terrainSize,
                         percent=landUsability, minheight=TreesMinHeight,
                         maxheight=TreesMaxHeight, randomColor=TreesRandomColor) # Call Procedure


#---------------------------------
# Call to the Main Functions
#---------------------------------

# Clean Undo Stack to prevent System Memory to be Burnt
mc.flushUndo()

# Call the Main Function
terrain(land_MinSize = 30,     # scene units
        landMaxSize = 100,     # scene units
        landResolution = 3,    # 1=High, 2=Med, 3=Low
        landUsability = 3,    # in %
        MountainsAmount = random.randint(2, 5),
        LakeAmount = random.randint(2, 3),
        TreesMinHeight = 0.5,
        TreesMaxHeight = 1.2,
        TreesRandomColor = [random.uniform(0.1, 0.3),    # R
                            random.uniform(0.1, 0.4),    # G
                            random.uniform(0.0, 0.1)]    # B
        )


#----------------------------------------------------------------------------------------------
# End of the Script
#----------------------------------------------------------------------------------------------




#---------------------------------
# Reference Material
#---------------------------------

# Mel Code to obtain the average distance btw the vertex of a face.
# This was converted to python and used in the tree creation procedure


# for ($face in `filterExpand -sm 34 ( polyListComponentConversion("-toFace") )`)
# {
#     $verts = `filterExpand -sm 31 ( polyListComponentConversion("-toVertex", $face) )`;
#     vector $pntA = `pointPosition $verts[0]`;
#     vector $pntB = `pointPosition $verts[1]`;
#     vector $pntC = `pointPosition $verts[2]`;
#     vector $pntD = `pointPosition $verts[3]`;
#     vector $ws = ($pntA + $pntB + $pntC + $pntD) / 4;
#     scale -r -p ($ws.x) ($ws.y) ($ws.z) 2 2 2 $face;
# }

# source:
# forums.cgsociety.org/archive/index.php/t-879030.html
# http://ewertb.soundlinker.com/mel/mel.003.php
# http://ewertb.soundlinker.com/mel/mel.005.php
# http://ewertb.soundlinker.com/mel/mel.055.php