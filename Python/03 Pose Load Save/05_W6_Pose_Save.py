# -*- coding: utf-8 -*-
'''
Created on 2013-04-15 22:32

@summary:   This script will create an external .xml file storing the Pose of all the selected
            items. If no elements where selected the template.xml file will be used.
            Please, Define below the same path for the template file and the pose file.


@author: Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
'''

# -------------------------------------------------------
# Define Working Directory and Template File name:
# Edit just the path but preserve the name template.xml
# -------------------------------------------------------
xmlTemplatePath = 'C://var//tmp//W6_Assignment//template.xml'

# --------------------------------------------------------------------------------------
# Define Working Directory and Pose File name:
# Use the same path as the xmlTemplatePath but change the name to save the Pose with
# that name.
# ---------------------------------------------------------------------------------------
xmlPosePath = 'C://var//tmp//W6_Assignment//Stand_Pose.xml'

import maya.cmds as mc
from xml.etree import cElementTree as ElementTree
import os

def readTemplate():
    # Retrieve Elements names from the template file
    templateFile = open(xmlTemplatePath, 'r')
    templateTree = ElementTree.parse(templateFile).getroot()
    templateFile.close()
    items = []
    for element in templateTree:
        items.append(element.tag)
    return items

def itemsToSave():
    # Store Selected Items
    selected = mc.ls(selection=True, long=True)

    # if nothing selected retrieve names from template otherwise use selected items

    if not selected:
        # Retrieve List of elements from template
        transform = readTemplate()
        return transform

    else:
        # Process append each to return clean names
        transform = []
        for item in selected:
            item = str(item)
            tmp = item.split('|')
            transform.append(tmp[-1])
        return transform

def findMatrix(items):
    # Create a Dictionary with items as keys and matrix as value
    matrix = {}
    for item in items:
        translation = str(mc.xform(item, query=True, worldSpace=True, translation=True))
        rotation = str(mc.xform(item, query=True, worldSpace=True, rotation=True))
        scale = str(mc.xform(item, query=True, relative=True, scale=True))

        # Dictionary Design

        # {itemA : [translation,rotation,scale],
        #  itemB : [translation,rotation,scale]
        #  }

        # Build Dictionary
        matrix[item] = [translation, rotation, scale]

    return matrix


def prettyPrint(element, level=0):
    '''
    Printing in elementTree requires a little massaging
    Function taken from elementTree site:
    http://effbot.org/zone/element-lib.htm#prettyprint

    '''
    indent = '\n' + level * '  '
    if len(element):
        if not element.text or not element.text.strip():
            element.text = indent + '  '

        if not element.tail or not element.tail.strip():
            element.tail = indent

        for element in element:
            prettyPrint(element, level + 1)

        if not element.tail or not element.tail.strip():
            element.tail = indent

    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = indent

    return element

def createXmlStructure(topNode, subElements):

    # subElements it's a dictionary with
    for key, value in subElements.iteritems():
        # Append all the subElements to the root and add the matrix attribute
        subitem = ElementTree.Element(key)
        topNode.append(subitem)
        subitem.attrib['A_Translation'] = value[0]
        subitem.attrib['B_Rotation'] = value[1]
        subitem.attrib['C_Scale'] = value[2]

    # Rearrange the tree to look nicely indented
    prettyPrint(topNode)

def commitToDisk(topNode):
    # Save Xml Tree Structure to disk
    xmlFile = open(xmlPosePath, 'w')
    # Convert the Tree element to string
    xmlTree = ElementTree.tostring(topNode)
    # Write to Disk
    xmlFile.write('<?xml version="1.0" encoding="UTF-8"?>' + '\n' + xmlTree)
    # Close the file
    xmlFile.close()
    print 'Template saved as: {fileName}: '.format(fileName=xmlPosePath)


def savePose():
    # Create a list of selected items or items from template to save a Pose File
    items = itemsToSave()

    if items:
        # Obtain items Matrix and return a dictionary key=Name / value=TransRotScale
        itemsMatrix = findMatrix(items=items)
        # Find the filename with no extension to be used to name the root node
        fileName = os.path.basename(xmlPosePath) # this will remove all the path except the filename and extension
        poseName = fileName.split('.')[0]     # [0]=name .....[1]=extension
        # Create root TAG
        root = ElementTree.Element(poseName)
        # Crete XML Structure
        createXmlStructure(topNode=root , subElements=itemsMatrix)

        # Convert the tree to string
        xmlPrettyContent = ElementTree.tostring(root)

        # Save XML File
        commitToDisk(topNode=root)
        print xmlPrettyContent


savePose()