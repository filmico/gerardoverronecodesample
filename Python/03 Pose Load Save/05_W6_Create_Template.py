# -*- coding: utf-8 -*-
'''
Created on 2013-04-15 15:32

@summary:   This script will create an external template.xml file storing the selected items
            to be used as a template for Character Pose Save and Retrieve.

@author: Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
'''

# ------------------------------------------------------------
# Define Working Directory and Preserve FileName template.xml.
# ------------------------------------------------------------
xmlTemplatePath = 'C://var//tmp//W6_Assignment//template.xml'

import maya.cmds as mc
from xml.etree import cElementTree as ElementTree


def cleanSelectionList():
    # Store Selected Items
    selected = mc.ls(selection=True, long=True)
    if not selected:
        # Ask to select something to be used for the construction of the name templates file
        raise RuntimeError, 'Nothing Selected. Try Again'

    else:
        # Process append each to return clean names
        transform = []
        for item in selected:
            tmp = item.split('|')
            transform.append(tmp[-1])
        return transform

def prettyPrint(element, level=0):
    '''
    Printing in elementTree requires a little massaging
    Function taken from elementTree site:
    http://effbot.org/zone/element-lib.htm#prettyprint

    '''
    indent = '\n' + level * '  '
    if len(element):
        if not element.text or not element.text.strip():
            element.text = indent + '  '

        if not element.tail or not element.tail.strip():
            element.tail = indent

        for element in element:
            prettyPrint(element, level + 1)

        if not element.tail or not element.tail.strip():
            element.tail = indent

    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = indent

    return element

def createXmlStructure(topNode, subElements):
    # Append all the subElements to the root
    for item in subElements:
        topNode.append(ElementTree.Element(item))
    # Rearrange the tree to look nicely indented
    prettyPrint(topNode)


def commitToDisk(topNode):
    # Save Xml Tree Structure to disk
    xmlFile = open(xmlTemplatePath, 'w')
    # Convert the Tree element to string
    xmlTree = ElementTree.tostring(topNode)
    # Write to Disk
    xmlFile.write('<?xml version="1.0" encoding="UTF-8"?>' + '\n' + xmlTree)
    # Close the file
    xmlFile.close()
    print 'Template saved as: {fileName}: '.format(fileName=xmlTemplatePath)


def createTemplateFile():
    # Create a list of selected items to create template file
    items = cleanSelectionList()

    if items:
        # Create root TAG
        root = ElementTree.Element('NamesTemplate')
        # Add selected items as Children of the root node
        createXmlStructure(topNode=root , subElements=items)

        xmlPrettyContent = ElementTree.tostring(root)

        commitToDisk(topNode=root)
        print xmlPrettyContent

createTemplateFile()