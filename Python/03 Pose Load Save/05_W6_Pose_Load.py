# -*- coding: utf-8 -*-
'''
Created on 2013-04-16 01:10

@summary:   This script will load a previous stored Pose to apply Translate, Rotate and Scale
            values to specific elements of the scene. The list of elements to be changed will
            be extracted from the retrieved pose file.

@author: Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
'''

# --------------------------------------------------------------------------------------
# Define Working Directory and Pose File name to retrieve:
# ---------------------------------------------------------------------------------------
xmlPosePath = 'C://var//tmp//W6_Assignment//Stand_Pose.xml'

import maya.cmds as mc
from xml.etree import cElementTree as ElementTree


def readPose():
    xmlFile = open(xmlPosePath, 'r')
    poseTree = ElementTree.parse(xmlFile).getroot()
    xmlFile.close()
    # Retrieve Elements names and TRS_matrix from the Pose file
    for element in poseTree:
        itemName = element.tag

        # Obtain Translation
        itemTranslation = eval(element.get('A_Translation')) # eval will convert string '[a,b,c]' to list [a,b,c]

        # Obtain Rotation
        itemRotation = eval(element.get('B_Rotation'))

        # Obtain Scale
        itemScale = eval(element.get('C_Scale'))

        # print 'Translation: ', itemTranslation
        # print 'Rotation: ', itemRotation
        # print 'Scale: ', itemScale

        # Pose individual element
        mc.xform(itemName, translation=itemTranslation)
        mc.xform(itemName, rotation=itemRotation)
        mc.xform(itemName, scale=itemScale)

    print 'Pose Loaded: ', xmlPosePath

readPose()
