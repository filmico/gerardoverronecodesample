# ======================================================================
# Script Name: 15_Select_by_Distance.py                             v1.0
# by Gerardo Sebastian Verrone (animatorstudio@hotmail.com)
# Date: 30/03/2013
# ----------------------------------------------------------------------
# This Script will measure the distance btw the active model view camera
# (Top, Front, Side or Persp) and all the PolyMeshes in the scene to
# select just the ones that are inside the maxdist User variable.
# ======================================================================

# --------------
# User Section
#---------------
# Change the value of the variable maxdist to process
maxdist = float(5.00)

# --------------------
# Developer Section
#---------------------

import maya.cmds as mc
import math

def meshNamePosition(mesh):

    # Obtain the Transform Nodes name
    meshesTransform =  mc.listRelatives('{mesh}'.format(mesh=mesh), parent=True, type='transform')[0]
    # Obtain Transform Position
    meshesPosition = mc.xform('{obj}'.format(obj=meshesTransform), worldSpace=True, translation=True, query=True)
    return meshesTransform, meshesPosition

def activeCam():

    # Get the panel that currently has focus
    panel = mc.getPanel(withFocus=True);

    # Compare the panel's type to confirm it is a "modelPanel" where cameras live
    if "modelPanel" == mc.getPanel(typeOf=panel):
        # print 'ok we found a modelPanel !'

        # Obtain Active Camera
        camera = mc.modelEditor(panel, camera=True, query=True)
        # print 'Active Camera: ', camera
        return camera

def activeCamPos(cam):

    # Obtain Camera position
    camPosition = mc.xform(cam, worldSpace=True, translation=True, query=True)
    return camPosition

def distMath3D(x1, y1, z1, x2, y2, z2):
    # 2d Distance will be solved using pythagoras
    # 3d Distance will the same but adding the Z term inside the sqrt calculation
    # source: http://www.youtube.com/watch?v=VgP7L69YuT4
    d = math.sqrt( ((x2-x1)**2) + ((y2-y1)**2) + ((z2-z1)**2) )
    return d

def meshSelect(maxDistance):

    # Clear all the Selection list
    mc.select(clear=True)

    # Find active camera Name
    camera = activeCam() # Call Function to determine active view and camera name
    # print 'Camera: ', camera

    if not camera: # If the active view does not have a camera E.G. The script Editor
        print '\n', 'Please select a Top, Front, Side or Persp view to work. The ideal view to select is Persp'
        return

    else:
        # Find active camera Position
        camPos = activeCamPos(camera) # Call Function to determine the Active Camera Position
        # Spread the return position as x1,y1,z1 source variables
        x1 = camPos[0]
        y1 = camPos[1]
        z1 = camPos[2]
        # print 'camPos x,y,z: ', x1, y1, z1

        # find meshes on the scene
        meshes = mc.ls(long=True, type="mesh", dag=True)

        # Iterate for each mesh to find position
        for mesh in meshes:
            meshNamePos = meshNamePosition(mesh) # Call Function, will return [meshTransformName, meshNamePosition]
            # Spread the return position as x2,y2,z2 target variables
            x2 = meshNamePos[1][0]
            y2 = meshNamePos[1][1]
            z2 = meshNamePos[1][2]
            # print 'meshNamePos x,y,z: ', x2, y2, z2

            # Proceed with the math Function to find the Distance
            distance = distMath3D(x1, y1, z1, x2, y2, z2) # Call the Function to obtain the distance btw the camera and the object

            # Determine
            if distance < maxDistance:
                mc.select(meshNamePos[0], add=True)
                print 'Mesh Selected: ', meshNamePos[0]
            else:
                print '\n', meshNamePos[0], 'is at', distance, 'grid units from the camera'
                print 'It will not be selected because you have chosen a max distance of', maxDistance
                print 'Please reduce the Distance in the scene or use a Higher maxdist value'
                print 'The maxdist value can be changed at the top of the script!'

meshSelect(maxdist)