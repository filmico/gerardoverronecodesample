# ======================================================================
# Script Name: 14_LocatorOnMeshCV.py
# by Gerardo Sebastian Verrone (animatorstudio@hotmail.com)     03,2013
# ____________________________________________________________________
# This Script Creates Locators at cvs position of each selected mesh
# _____________________________________________________________________
# Usage: Select One or Many Meshes and run the script
# ======================================================================

import maya.cmds as mc

def locatorOnPolyMesh():

    # Get the selected mesh
    selectedPolygons = mc.filterExpand(selectionMask=12)

    if not selectedPolygons: # Filter to detect if the selected element is not polygon
        print "Please Select One or More PolyMesh"
        return None

    else:
        # Loop to process each selected PolyMesh on each iteration
        for item in selectedPolygons:
            # Create a list to store vertex positions
            vertexPositions = []

            # Get the number of vertices
            numberOfVertices = mc.polyEvaluate('{item}'.format(item=item), vertex=True)

            # Loop until the max number of vertices is reached
            for vertex in range(numberOfVertices):
                # Find and append CVs Positions
                vertexPos = mc.xform('{item}.vtx[{vertex}]'.format(item=item, vertex=vertex), query=True, worldSpace=True, translation=True)
                vertexPositions.append(vertexPos)

                # Create locators at Cv Absolute World Space Positions
                mc.spaceLocator(name='{item}_Locator_000'.format(item=item), position=vertexPositions[vertex])

locatorOnPolyMesh()