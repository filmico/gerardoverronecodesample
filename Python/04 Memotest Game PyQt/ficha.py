'''
Created on 17/07/2014

@author: Gerardo Verrone
'''

from PyQt4 import QtGui, QtCore

class Ficha(QtGui.QLabel):
    
    def __init__(self, imagen_tapado, imagen_destapado , parent = None):
        
        QtGui.QLabel.__init__(self, "", parent)
        self.imagen = imagen_destapado
        self.pixmap_imagen_destapado = QtGui.QPixmap.fromImage(QtGui.QImage(imagen_destapado))
        self.pixmap_imagen_tapado = QtGui.QPixmap.fromImage(QtGui.QImage(imagen_tapado))
        self.set_tapar()
        self.set_disable_click()        
        
    def set_enable_click(self):
        self.enabled_click = True
    
    def set_disable_click(self):
        self.enabled_click = False
                
    def set_tapar(self):
        self.setPixmap(self.pixmap_imagen_tapado)
        self.estoy_tapado = True
    
    def set_destapar(self):
        self.setPixmap(self.pixmap_imagen_destapado)
        self.estoy_tapado = False
        
    def set_img(self, new_imagen_destapado):
        self.imagen = new_imagen_destapado
        self.pixmap_imagen_destapado = QtGui.QPixmap.fromImage(QtGui.QImage(new_imagen_destapado))        
        
    def mousePressEvent(self, event):
        if self.enabled_click:
            self.emit(QtCore.SIGNAL('clicked(QObject)'),self)
            
                    