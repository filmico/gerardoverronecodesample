#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 17/07/2014

@author: Gerardo Verrone
'''
from PyQt4 import QtGui
from PyQt4 import QtCore
from threads import Threads
from functools import partial 
from db_Manager import DB_Manager


class Controler(object):
    '''
    classdocs
    '''
    def __init__(self, view):
        '''
        Constructor
        '''
        self.view = view

        self.maxTime = 80
        self.previewTime = 3        
        self.fichasElegidas = []
        self.aciertos = 0
        self.errores = 0
        self.puntaje = 0
        self.db_Manager = DB_Manager()
        
        # Actualizar labels de tiempo de juego y preview de fichas 
        self.view.dict_widgets['L1_tiempoJuego_lbl'].setText('Tiempo: ' + str(self.maxTime))
        self.view.dict_widgets['L1_tiempoPreview_lbl'].setText('Preview: ' + str(self.previewTime))
        self.view.dict_widgets['L2_tiempoJuego_lbl'].setText('Tiempo: ' + str(self.maxTime/2))
        self.view.dict_widgets['L2_tiempoPreview_lbl'].setText('Preview: ' + str(self.previewTime-1))
        self.view.dict_widgets['L3_tiempoJuego_lbl'].setText('Tiempo: ' + str(self.maxTime/3))
        self.view.dict_widgets['L3_tiempoPreview_lbl'].setText('Preview: ' + str(self.previewTime-2))        
                        
        
        # Signals & Slots Main Menu
        # -------------------------        
        #                        objQt,     signal,      method,     *args, *kwrds        
        self.view.set_listener('L1_pb', 'clicked()', self.gameStart, self.maxTime, self.previewTime )
        self.view.set_listener('L2_pb', 'clicked()', self.gameStart, self.maxTime/2, self.previewTime - 1 )
        self.view.set_listener('L3_pb', 'clicked()', self.gameStart, self.maxTime/3, self.previewTime - 2 )                
        
   
        # Signals & Slots Fichas
        # -------------------------
        
        for i in range(0, 16):
            #                           objQt,             signal,          method,          *args, *kwrds
            self.view.set_listener('ficha_'+ str(i), 'clicked(QObject)', self.verificarAcierto, i )
            
            
        # Signals & Slots Ingresar Nombre Jugador
        # -------------------------            
        #                        objQt,     signal,         method,       *args, *kwrds        
        self.view.set_listener('tableroNickBtn', 'clicked()', self.storePlayer, self.puntaje )
        
        # Signals & Slots Replay Game
        # ----------------------------            
        #                        objQt,     signal,         method,       *args, *kwrds        
        self.view.set_listener('replay_pb', 'clicked()', self.replay, () )
        self.view.set_listener('replay_pb_perdiste', 'clicked()', self.replay, () )              
                    
            
        # Mostrar Ventana del juego
        # -------------------------        
        self.view.run()        
            
    def gameStart(self, *args, **kwrds):   
        
        # Apagar Ingreso de Nick
        # -----------------------       
        self.view.tableroNickLabel.hide()   
        self.view.tableroNickInput.hide()
        self.view.tableroNickBtn.hide()
        self.view.replay_pb_perdiste.hide()   
                     
        maxTime = args[0]
        previewTime = (args[1] * 1000) # por 1000 para convertir milisegundos en segundos
        
        # Setear el index del Stacked Layout para pasar al tablero de juego
        self.view.changeScreen(1)
        # self.view.changeScreen(2) # Pasa directo al ranking
        
        # Destapar las fichas para hacer el preview
        for i in range(0, 16):
            self.view.dict_widgets[('ficha_'+ str(i))].set_destapar()
            
        # Lanzar un contador hasta tiempo de preview y cumplido el tiempo tapar las fichas y
        # lanzar el cronometro general
        QtCore.QTimer.singleShot(previewTime, partial(self.lanzaSegundero, maxTime) )      


    def lanzaSegundero(self, *args, **kwrds):        
        maxTime = args[0]
        # Tapar y Habilitar el click de las fichas
        for i in range(0, 16):
            self.view.dict_widgets[('ficha_'+ str(i))].set_tapar()
            self.view.dict_widgets[('ficha_'+ str(i))].set_enable_click()            
                
        # Lanzar thread para incrementar el reloj principal        
        objQt = self.view.tiempoCount
        self.counter = Threads(maxTime, objQt)
        self.counter.start() 
        self.counter.cron_start()
        
        # Conecta la señal de tiempo agotado del thread para que de por terminado el juego
        self.counter.connect(self.counter, QtCore.SIGNAL("termino(float)"), self.tiempoAgotado ) 
        
    def tiempoAgotado(self):
        self.view.set_text('leyenda', '-= Tiempo Agotado, Perdiste =-')
        self.view.replay_pb_perdiste.show()  
           
    
    def verificarAcierto(self, *args, **kwrds):
        
        # Agregar la ficha a la lista temporal de fichas elegidas
        self.fichasElegidas.append(args[0])        
        
        if (len(self.fichasElegidas)) < 3:
            
            # Destapar Ficha
            self.view.dict_widgets[('ficha_'+ str(args[0]))].set_destapar()
            
            # Apagarle el click a la ficha destapada
            self.view.dict_widgets[('ficha_'+ str(args[0]))].set_disable_click()
            
            # Si es la segunda ficha elegida comparar si son iguales
            if (len(self.fichasElegidas)) == 2:
                
                img1 = self.view.dict_widgets['ficha_'+ str(self.fichasElegidas[0])].imagen
                img2 = self.view.dict_widgets['ficha_'+ str(self.fichasElegidas[1])].imagen
                
                # Si son iguales acumula aciertos y actualiza el contador de aciertos
                if img1 == img2:
                    self.aciertos += 1                    

                    # Actualizar cantidad de aciertos en la ventana
                    self.view.set_text('aciertosCount', str(self.aciertos))
                    
                    # Blanquear la lista temporal de fichas elegidas
                    self.fichasElegidas = []    
                    
                    # Si llegamos a los 8 aciertos terminar la partida.
                    if self.aciertos == 8:

                        self.view.set_text('leyenda', '-= GANASTE =-')
                        
                        # Apagar contador general                        
                        self.counter.stop() 
                        
                        # Encender Caja de Texto entrada nick name y boton   
                        self.view.tableroNickLabel.show()   
                        self.view.tableroNickInput.show()
                        self.view.tableroNickBtn.show()
    

                else:
                    # Si son distintas espera dos segundos, las tapa 
                    # y habilita los clicks para volver a probar
                    QtCore.QTimer.singleShot(1000, partial(self.resetFichas, self.fichasElegidas[0], self.fichasElegidas[1]) )

                    # Aumenta el contador de errores
                    self.errores += 1                    

                    # Actualizar cantidad de errores en la ventana
                    self.view.set_text('erroresCount', str(self.errores))                    

        '''
        Calculo de Puntaje
        ------------------
        Aciertos multiplica * 10
        Errores Resta 1
        ''' 
        if (((self.aciertos * 10) - (self.errores)) > -1): 
            
            self.puntaje = (self.aciertos * 10) - self.errores            
            self.view.set_text('puntajeCount', str(self.puntaje))    

    def storePlayer(self, *args, **kwrds):
        
        self.nombrePlayer = self.view.dict_widgets['tableroNickInput'].text()

        # Agregar jugador a la base de datos
        self.db_Manager.addPlayer(self.nombrePlayer, self.puntaje)
        
        # Consulta la DB en modo descendente
        self.scores = self.db_Manager.scores()  
        
        self.tablaPuntaje = []
        
        # Crea una lista de nombres y puntajes desde la lista de obj de la DB
        for i in self.scores:
            for j in i:
                self.tablaPuntaje.append(j)
   
        # Setear la cantidad de filas de la tabla de la pantalla de ranking
        cantRegistros = self.db_Manager.countRecords()        
        self.view.tablaRanking.setRowCount(cantRegistros)
        
        # Actualiza la tabla del layout del ranking                        
        self.view.updateRanking(self.tablaPuntaje)                
        
        # Pasar a la pantalla del ranking
        self.view.changeScreen(2)             
        
            
    def resetFichas(self, *args, **kwrds):
        
        for i in range(len(args)):  
            # Tapar Fichas
            self.view.dict_widgets['ficha_'+ str(args[i])].set_tapar()        
            # Habilitar cliks
            self.view.dict_widgets['ficha_'+ str(args[i])].set_enable_click()
        
        # Resetear lista temporal de fichas elegidas
        self.fichasElegidas = []
        

    def replay(self, *args, **kwrds):        
        
        # Resetear Variables
        self.maxTime = 80
        self.previewTime = 3        
        self.fichasElegidas = []
        self.aciertos = 0
        self.errores = 0
        self.puntaje = 0
        
        # Actualizar cantidad de aciertos en la ventana
        self.view.set_text('aciertosCount', str(self.aciertos))
        self.view.set_text('leyenda', '')   
        # Actualizar cantidad de errores en la ventana
        self.view.set_text('erroresCount', str(self.errores))
        # Actualizar puntaje en la ventana
        self.view.set_text('puntajeCount', str(self.puntaje))     
        # Actualizar caja de entrada del texto
        self.view.set_text('tableroNickInput', '') 
        
        # Resetear Contador
        self.counter.cron_reset()
        self.view.set_text('tiempoCount', '00:00')
        
        # Revolver Fichas
        self.view.suffleFichas()
        
                
        # Setear el index del Stacked Layout a la pantalla 1
        self.view.changeScreen(0)      
        
        
              
        
        
        
       
        
        
        
        
        
        
        
        
               
        