#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 17/07/2014

@author: Gerardo Verrone

# Procedimiento para crear y distribuir los elementos en el view

1) Crear botones y Agregarlos al diccionario
2) Crear un widget para esa pantalla y setearle un Gridlayout
3) Crear Sublayouts horizontales para cada franja de botones e ir agregando los botones
4) Agregar los Sublayouts al GridLayout del punto 2 indicando su distribucion vertical

5) Definir un stackedWidget
6) Agregar el widget del punto 1 al stackedWidget

7) Crear un Layout global Vertical y agregar el stackedWidget que contiene todas las pantallas
8) Setearle el layout general a la ventana


'''

import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from random import shuffle
from ficha import Ficha
from functools import partial
from db_Manager import DB_Manager


class View(QtGui.QDialog):

    def __init__(self, parent=None):
        
        self.app = QtGui.QApplication(sys.argv)        
        self.app.setStyle("cleanlooks")
             
        super(View, self).__init__()
        
        # Crea dict para guardar los widgets
        self.dict_widgets = {}
        
        self.db_Manager = DB_Manager()
        
        # ------------------------------                        
        # Comienza Definicion Pantallas
        # ------------------------------         

        # Main Menu
        # ---------

        # Widgets
        self.mainTitle = QtGui.QLabel('')          
        self.mainTitleImg = QtGui.QPixmap('ima/Intro_BG.jpg')          
        self.mainTitle.setPixmap(self.mainTitleImg)  
                
        self.L1_pb = QtGui.QPushButton('Nivel Facil')        
        self.L2_pb = QtGui.QPushButton('Nivel Medio')
        self.L3_pb = QtGui.QPushButton('Nivel Dificil')
        
        # Indicadores de Tiempo de Juego y tiempo de preview
        self.L1_tiempoJuego_lbl = QtGui.QLabel('')        
        self.L1_tiempoPreview_lbl = QtGui.QLabel('')        
        self.L2_tiempoJuego_lbl = QtGui.QLabel('')        
        self.L2_tiempoPreview_lbl = QtGui.QLabel('')        
        self.L3_tiempoJuego_lbl = QtGui.QLabel('')        
        self.L3_tiempoPreview_lbl = QtGui.QLabel('') 
               
        # Alinear todos los indicadores de tiempo y preview a la izquierda
        self.L1_tiempoJuego_lbl.setAlignment(QtCore.Qt.AlignLeft)
        self.L2_tiempoJuego_lbl.setAlignment(QtCore.Qt.AlignLeft)
        self.L3_tiempoJuego_lbl.setAlignment(QtCore.Qt.AlignLeft)    
        self.L1_tiempoPreview_lbl.setAlignment(QtCore.Qt.AlignLeft)
        self.L2_tiempoPreview_lbl.setAlignment(QtCore.Qt.AlignLeft)
        self.L3_tiempoPreview_lbl.setAlignment(QtCore.Qt.AlignLeft)      
        
        self.dict_widgets['mainTitle'] = self.mainTitle
        
        self.dict_widgets['L1_pb'] = self.L1_pb
        self.dict_widgets['L2_pb'] = self.L2_pb
        self.dict_widgets['L3_pb'] = self.L3_pb  
        
        self.dict_widgets['L1_tiempoJuego_lbl'] = self.L1_tiempoJuego_lbl
        self.dict_widgets['L1_tiempoPreview_lbl'] = self.L1_tiempoPreview_lbl
        
        self.dict_widgets['L2_tiempoJuego_lbl'] = self.L2_tiempoJuego_lbl
        self.dict_widgets['L2_tiempoPreview_lbl'] = self.L2_tiempoPreview_lbl 
               
        self.dict_widgets['L3_tiempoJuego_lbl'] = self.L3_tiempoJuego_lbl
        self.dict_widgets['L3_tiempoPreview_lbl'] = self.L3_tiempoPreview_lbl        
              
        
        # Definir QWidget y Layout del Main Menu
        self.mainMenu = QtGui.QWidget()
        self.mainMenuLayout = QtGui.QGridLayout()
        self.mainMenu.setLayout(self.mainMenuLayout) 
        
        # Sub-Layouts        
        
        # Crear un Hlayout para el titulo de arriba colocando dos spacerItem en cada lado
        self.mainTitleLayout = QtGui.QHBoxLayout()
        self.mainTitleLayout.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        self.mainTitleLayout.addWidget(self.mainTitle)
        self.mainTitleLayout.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        
        # Crear un layout y agregar los botones de niveles
        self.btnsLayout = QtGui.QHBoxLayout()
        self.btnsLayout.addWidget(self.L1_pb)
        self.btnsLayout.addWidget(self.L2_pb)
        self.btnsLayout.addWidget(self.L3_pb)
        
        # Crear un layout para la indicacion de tiempo de juego
        self.tiempoJuegoLayout = QtGui.QHBoxLayout()
        self.tiempoJuegoLayout.addWidget(self.L1_tiempoJuego_lbl)
        self.tiempoJuegoLayout.addWidget(self.L2_tiempoJuego_lbl)
        self.tiempoJuegoLayout.addWidget(self.L3_tiempoJuego_lbl)    
        
        # Crear un layout para la indicacion del tiempo de preview
        self.tiempoPreviewLayout = QtGui.QHBoxLayout()
        self.tiempoPreviewLayout.addWidget(self.L1_tiempoPreview_lbl)
        self.tiempoPreviewLayout.addWidget(self.L2_tiempoPreview_lbl)
        self.tiempoPreviewLayout.addWidget(self.L3_tiempoPreview_lbl)                    
                 
        # Crear un layout Vertical y agregar un QSpacer
        self.blankBtmLayout = QtGui.QVBoxLayout()
        self.blankBtmLayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )        
 
        # Separadores Verticales 
        self.verticalSpacer_01 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Expanding)
        self.verticalSpacer_02 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Expanding)
        
        # Agregar los Sub-Layouts al mainMenuLayout                
        self.mainMenuLayout.addItem(self.verticalSpacer_01, 0, 0, 1, 1)        
        self.mainMenuLayout.addLayout(self.mainTitleLayout, 1, 0, 1, 1) # el del titulo
        self.mainMenuLayout.addItem(self.verticalSpacer_02, 2, 0, 1, 1)                
        self.mainMenuLayout.addLayout(self.btnsLayout, 3, 0, 1, 1) # el de los botones
        self.mainMenuLayout.addLayout(self.tiempoJuegoLayout, 4, 0, 1, 1) # el de los tiempos de juego
        self.mainMenuLayout.addLayout(self.tiempoPreviewLayout, 5, 0, 1, 1) # el del tiempo de preview
        self.mainMenuLayout.addLayout(self.blankBtmLayout, 6, 0, 1, 1) # el blanco de abajo
        
        
        # Tablero Top
        # -----------
                
        # Definir QWidget y Layout del Tablero
        self.tablero = QtGui.QWidget()
        self.tableroLayout = QtGui.QGridLayout()
        self.tablero.setLayout(self.tableroLayout)
         
        # Aciertos
        self.aciertosLabel = QtGui.QLabel('Aciertos:  ')
        self.aciertosLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.dict_widgets['aciertosLabel'] = self.aciertosLabel
        self.aciertosCount = QtGui.QLabel('0')
        self.aciertosCount.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)            
        self.dict_widgets['aciertosCount'] = self.aciertosCount    
         
        # Tiempo Transcurrido        
        self.tiempoLabel = QtGui.QLabel('Tiempo:  ')
        self.tiempoLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.dict_widgets['tiempoLabel'] = self.tiempoLabel         
        self.tiempoCount = QtGui.QLabel('00:00')
        self.dict_widgets['tiempoCount'] = self.tiempoCount
        self.tiempoCount.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        
        # Errores
        self.erroresLabel = QtGui.QLabel('Errores:  ')
        self.erroresLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)        
        self.dict_widgets['erroresLabel'] = self.erroresLabel
        self.erroresCount = QtGui.QLabel('0')
        self.erroresCount.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)            
        self.dict_widgets['erroresCount'] = self.erroresCount            
        
        # Puntaje  
        self.puntajeLabel = QtGui.QLabel('Puntaje:  ')
        self.puntajeLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.dict_widgets['puntajeLabel'] = self.puntajeLabel         
        self.puntajeCount = QtGui.QLabel('0')
        self.dict_widgets['puntajeCount'] = self.puntajeCount
        self.puntajeCount.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)        
        
         
        # Crear un Hlayout para los aciertos y el tiempo
        self.tableroInfoLayout = QtGui.QHBoxLayout()
        self.tableroInfoLayout.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        self.tableroInfoLayout.addWidget(self.aciertosLabel)
        self.tableroInfoLayout.addWidget(self.aciertosCount)
        self.tableroInfoLayout.addItem(QtGui.QSpacerItem(150, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        self.tableroInfoLayout.addWidget(self.tiempoLabel)
        self.tableroInfoLayout.addWidget(self.tiempoCount)        
        self.tableroInfoLayout.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        
        # Crear un Hlayout para los errores y el puntaje
        self.tableroInfoLayout2 = QtGui.QHBoxLayout()
        self.tableroInfoLayout2.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        self.tableroInfoLayout2.addWidget(self.erroresLabel)
        self.tableroInfoLayout2.addWidget(self.erroresCount)
        self.tableroInfoLayout2.addItem(QtGui.QSpacerItem(150, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )
        self.tableroInfoLayout2.addWidget(self.puntajeLabel)
        self.tableroInfoLayout2.addWidget(self.puntajeCount)        
        self.tableroInfoLayout2.addItem(QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum) )        
        

        # Tablero Mid (Fichas)
        # -------------------- 
        
        lista = 2 * [("ima/img_%i.png")%i for i in range(1,9)]
        shuffle(lista)
        
        self.lista_layoutFichas = []
         
        for ficha in range(len(lista)):            
            self.aux_ficha = Ficha("ima/question_icon.png", lista[ficha], self)
            self.dict_widgets[('ficha_'+ str(ficha))] = self.aux_ficha
 
         
        # Necesitamos poner 4 fichas por cada Hbox Layouts hasta completar 4 HBoxlayouts => 16 fichas        
        indexLista = 0        
        for fila in range(1,5):            
            # crear QHBoxLayout            
            aux_layout = QtGui.QHBoxLayout()
            self.lista_layoutFichas.append(aux_layout)            
             
            for columna in range(1,5):
                # agregar la ficha al layout creado
                self.lista_layoutFichas[-1].addWidget(self.dict_widgets[('ficha_'+ str(indexLista))])
                # Incrementar el index para avanzar hasta la ficha 16             
                indexLista += 1 


        # Tablero Btm (Ganaste / Perdiste / Ingreso Nick)
        # -----------------------------------------------
        
        # Leyenda Ganaste / Perdiste
        self.tableroLeyendaLayout = QtGui.QVBoxLayout()
        self.tableroLeyenda = QtGui.QLabel('')
        
        self.dict_widgets['leyenda'] = self.tableroLeyenda
        self.tableroLeyenda.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)        
        
        self.tableroLeyendaLayout.addWidget(self.tableroLeyenda)     

        self.tableroNickLayout = QtGui.QHBoxLayout()
        self.tableroNickLabel = QtGui.QLabel('Ingrese su Apodo')
        self.dict_widgets['tableroNickLabel'] = self.tableroNickLabel
        self.tableroNickInput = QtGui.QLineEdit()
        self.dict_widgets['tableroNickInput'] = self.tableroNickInput
        self.tableroNickBtn = QtGui.QPushButton('Aceptar')
        self.dict_widgets['tableroNickBtn'] = self.tableroNickBtn

        self.tableroNickLayout.addWidget(self.tableroNickLabel)
        self.tableroNickLayout.addWidget(self.tableroNickInput)
        self.tableroNickLayout.addWidget(self.tableroNickBtn)
        
        # Definir el boton de volver a Jugar para la pantalla del tablero por si pierde
        
        self.tableroReplay_Layout = QtGui.QVBoxLayout()        
        self.replay_pb_perdiste = QtGui.QPushButton('Volver a Jugar')
        self.dict_widgets['replay_pb_perdiste'] = self.replay_pb_perdiste           
        self.tableroReplay_Layout.addWidget(self.replay_pb_perdiste)

        # Agregar los Sub-Layouts al tableroLayout
        self.tableroLayout.addLayout(self.tableroInfoLayout, 0, 0, 1, 1) # el de los aciertos y tiempo transcurrido
        self.tableroLayout.addLayout(self.tableroInfoLayout2, 1, 0, 1, 1) # el de los errores y puntaje
        
        fila = 2
        for i in range(len(self.lista_layoutFichas)):
            self.tableroLayout.addLayout(self.lista_layoutFichas[i], fila, 0, 1, 1) # el de las fichas
            fila += 1            

        self.tableroLayout.addLayout(self.tableroLeyendaLayout, 6, 0, 1, 1) # el de la leyenda        
        self.tableroLayout.addLayout(self.tableroNickLayout, 7, 0, 1, 1) # el del Nick
        self.tableroLayout.addLayout(self.tableroReplay_Layout, 8, 0, 1, 1) # el del boton de replay del tablero si perdio
        
        
        # Ranking
        # --------

        # Leyenda RANKING
        self.rankingLeyendaLayout = QtGui.QVBoxLayout()
        self.rankingLeyenda = QtGui.QLabel('-= R A N K I N G =-')        
        self.dict_widgets['leyendaRanking'] = self.rankingLeyenda
        self.rankingLeyenda.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)               

        # Define una Tabla para mostrar el Ranking de Jugadores
        self.tablaRanking = QtGui.QTableWidget(50, 2)

        # Encabezados de las columnas
        encabezados = ['Jugador', 'Puntaje']
        self.tablaRanking.setHorizontalHeaderLabels(encabezados)

        # Ancho de las columnas
        self.tablaRanking.setColumnWidth(0, 129)
        self.tablaRanking.setColumnWidth(1, 129)
  

        # Boton Volver a Jugar
        # --------------------                
        self.replay_pb = QtGui.QPushButton('Volver a Jugar')
        self.dict_widgets['replay_pb'] = self.replay_pb
         
        # Definir QWidget y Layout de toda la pantalla de Ranking
        self.ranking = QtGui.QWidget()        
        self.rankingLayout = QtGui.QVBoxLayout()
        self.ranking.setLayout(self.rankingLayout)
        self.ranking.setLayout(self.rankingLayout) 

        self.rankingLayout.addWidget(self.rankingLeyenda)
        self.rankingLayout.addWidget(self.tablaRanking)
        self.rankingLayout.addWidget(self.replay_pb)        
        
        # -----------------------------                        
        # Termina Definicion Pantallas
        # -----------------------------        
        
        # Definir un QStackedWidget para contener todas las pantallas    
        self.stackedWidget = QtGui.QStackedWidget()
        
        # Agregar las pantallas al QStackedWidget     
        self.stackedWidget.addWidget(self.mainMenu)
        self.stackedWidget.addWidget(self.tablero)
        self.stackedWidget.addWidget(self.ranking)          

        # Definir el Layout contenedor de todo y setearlo para la ventana
        # ----------------------------------------------------------------
        self.windowLayout = QtGui.QVBoxLayout()
        self.windowLayout.addWidget(self.stackedWidget)        
        self.setLayout(self.windowLayout)

        # Setea posicion, tamaño y lock de la ventana        
        self.setGeometry(100, 100, 340, 480)        
        self.setMinimumSize(340, 480)
        self.setMaximumSize(340, 480)
        self.center()       

        self.setWindowTitle('Memotest')  
        
    def center(self):        
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2, 
            (screen.height()-size.height())/2)         


    def set_text(self, objeto, texto):
        try:
            widget = self.dict_widgets[objeto]
            widget.setText(texto)
            return True
         
        except Exception, msg:
            print msg
            return False
  
    def get_text(self, objeto):
        try:
            widget = self.dict_widgets[objeto]
            return widget.text()
              
         
        except Exception, msg:
            print msg
            return False        
        
    def set_listener(self, objQt, signal, method, *args, **kwrds):

        # Recibimos todo por separado para conectar una señal a un
        # metodo y de existir enviamos argumentos.
        # functools y partial lo usamos para poder seguir la metodologia
        # proc(args)
        
        try:                        
            func2Call = partial(method, *args, **kwrds)
            widget = self.dict_widgets[objQt]                
            widget.connect(widget, QtCore.SIGNAL(signal), func2Call )            
            return True
        
        except Exception, msg:
            print 'No se pudo realizar la conexion de la señal'
            print 'objQt: ', objQt
            print 'signal: ', signal
            print 'method: ', method
            print 'Argumentos: '
            for i in args:
                print i
            print msg
            return False

    def updateRanking(self, *args, **kwrds):
        
        listaJugadores = args[0]     
        
        # Carga la tabla
        for i in range(0, len(listaJugadores), 2):
            jugador = QtGui.QTableWidgetItem(listaJugadores[i])
            puntaje = QtGui.QTableWidgetItem(str(listaJugadores[i+1]))
            
            self.tablaRanking.setItem(0, i, jugador)
            self.tablaRanking.setItem(0, i+1, puntaje)
            
    def suffleFichas(self, *args, **kwrds):
        
        # Vuelve a mezclar la lista de imagenes
        lista = 2 * [("ima/img_%i.png")%i for i in range(1,9)]
        shuffle(lista)
        
        # Llama al metodo set_img de ficha para cambiar las imagenes
        for ficha in range(len(lista)):           
            self.dict_widgets[('ficha_'+ str(ficha))].set_img(lista[ficha])            
                        
    def changeScreen(self, *args, **kwrds):
        self.stackedWidget.setCurrentIndex(args[0])                
        
    def run(self):
        self.show()
        self.app.exec_()


    def closeApp(self, arg):
        
        # Recibimos arg ya que la cruz de cierre de la ventana
        # no envia el nombre del objeto que se esta cerrando
        # y necesitamos capturarlo aunque no lo usemos
        
        # Cerrar la base de datos
        self.db_Manager.closeDB() 

        print 'Quit'
        
        try:
            QtCore.QCoreApplication.instance().quit()
            
        except Exception, msg:
            print msg
            return False  
        