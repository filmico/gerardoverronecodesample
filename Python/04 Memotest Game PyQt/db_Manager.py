#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 22/09/2014

@author: Gerardo Verrone
'''
import sqlite3

class DB_Manager(object):    
    
    def __init__(self):
        self.DB = sqlite3.connect('scoreDB.sqlite')
        self.cursor = self.DB.cursor()

        # Crear Table si no existe        
        try:
            self.cursor.execute("""create table score (nombre text, puntaje float)""")
        except:
            pass    

    def addPlayer(self, nombre, puntaje):
        
        self.cursor.execute("""insert into score (nombre, puntaje) values (?, ?)""",(unicode(nombre) , puntaje))
        
        # forzar a la DB a actualizar los datos usando commit
        self.DB.commit() 

    def scores(self):        
        # Buscar Jugadores ordenados por Puntaje en modo descendente
        self.ranking = self.cursor.execute('select * from score order by puntaje desc')
            
        return self.ranking
    
    def countRecords(self):
        # Devuelve la cantidad de registros de la DB
        self.cursor.execute("select Count() from score")
        cantidadRegistros = self.cursor.fetchone()[0]
        return cantidadRegistros            
            
    def closeDB(self):
        # Cerrar la base de datos
        self.DB.close()