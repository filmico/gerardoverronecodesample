#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 17/07/2014

@author: Gerardo Verrone

'''

from PyQt4 import QtCore
from PyQt4 import QtGui


class Threads(QtCore.QThread):
    '''
    classdocs
    '''

    def __init__(self, maxTime, objQt):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
#         super(Threading, self).__init__(self)
        self.__time = 0
        self.__is_paused = True  
        self.__maxTime = maxTime
        self.__objQt = objQt
        
        
    def run(self):
        
        while(not self.isFinished()):            
            
            self.msleep(100)
            if self.__is_paused == False:
                self.__time = self.__time + 0.1
                if self.__time > self.__maxTime:
                    self.emit(QtCore.SIGNAL("termino(float)"), self.__time)
                    self.stop()
                else:
                    # Update del contador global
                    self.__objQt.setText(str(self.get_status()))
                                       
    def get_status(self):
        return self.__time
    
    def cron_start(self):
        self.__is_paused = False
    
    def cron_pause(self):
        self.__is_paused = True
    
    def cron_reset(self):    
        self.__time = 0
        self.__is_paused = True
    
    def stop(self):
        self.terminate()